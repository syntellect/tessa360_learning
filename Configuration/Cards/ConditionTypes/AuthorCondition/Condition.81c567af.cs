﻿var card = await context.GetCardAsync();

if(card is null)
{
	return false;
}

var authorID = card.Sections.GetOrAdd("DocumentCommonInfo").Fields.TryGet<Guid?>("AuthorID");

return context.Settings["KrUsersCondition"].Rows.Any(row => 
{
	return row.TryGet<Guid>("UserID") == authorID;
});