﻿var card = await context.GetCardAsync();

if(card is null)
{
	return false;
}

var partnerID = card.Sections.GetOrAdd("DocumentCommonInfo").Fields.TryGet<Guid?>("PartnerID");

return context.Settings["KrPartnerCondition"].Rows.Any(row => 
{
	return partnerID == row.TryGet<Guid>("PartnerID");
});