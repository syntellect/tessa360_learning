﻿DECLARE @tablename nvarchar(255) = 'FmMessages'
DECLARE @dbname nvarchar(255);
DECLARE @colname nvarchar(255) = 'PlainText'
select @dbname = DB_NAME()

DECLARE @scr nvarchar(max) = 'select top 1 @scr = name from ' + @dbname + '.sys.indexes where object_id = OBJECT_ID(''' + @tablename + ''') and is_unique = 1'
DECLARE @EXEC nvarchar(max) = @dbname + N'..sp_executesql'
EXEC @EXEC @scr, N'@scr nvarchar(255) out', @scr OUT
EXEC ('use ' + @dbname + ';SELECT name into #null FROM sys.fulltext_catalogs WHERE name = ''' + @tablename + '''')
IF (@@ROWCOUNT = 0) EXEC ('use ' + @dbname + ';CREATE FULLTEXT CATALOG ' + @tablename + ' WITH accent_sensitivity = OFF AUTHORIZATION [dbo]')
EXEC ('use ' + @dbname + ';SELECT object_id into #null FROM sys.fulltext_indexes WHERE object_id = object_id(''' + @dbname + '.dbo.' + @tablename + ''')')
IF (@@ROWCOUNT = 0) EXEC ('use ' + @dbname + ';CREATE FULLTEXT INDEX ON dbo.' + @tablename + '(' + @colname + ') KEY INDEX ' + @scr + ' ON ' + @tablename + ' WITH STOPLIST = SYSTEM')
EXEC ('use ' + @dbname + ';SELECT object_id into #null FROM sys.fulltext_index_columns WHERE COL_NAME(object_id,column_id) = ''' + @colname + '''')
IF (@@ROWCOUNT = 0) EXEC ('use ' + @dbname + ';ALTER FULLTEXT INDEX ON dbo.' + @tablename + ' ADD (' + @colname + ')')
GO
