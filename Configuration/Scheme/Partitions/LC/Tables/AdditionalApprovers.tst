﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="0d95e949-d37e-4318-9868-b6c63c4675c2" ID="6b8ce61e-6509-4e4b-ba38-96ffbc9ca5f9" Name="AdditionalApprovers" Group="LC" InstanceType="Cards" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="6b8ce61e-6509-004b-2000-06ffbc9ca5f9" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="6b8ce61e-6509-014b-4000-06ffbc9ca5f9" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="6b8ce61e-6509-004b-3100-06ffbc9ca5f9" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="20b00a88-49d0-4674-a3bd-fa4888ee0f0b" Name="User" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="20b00a88-49d0-0074-4000-0a4888ee0f0b" Name="UserID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="32687b5a-b5fe-46d9-891a-037e0675ce7a" Name="UserName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="6b8ce61e-6509-004b-5000-06ffbc9ca5f9" Name="pk_AdditionalApprovers">
		<SchemeIndexedColumn Column="6b8ce61e-6509-004b-3100-06ffbc9ca5f9" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="6b8ce61e-6509-004b-7000-06ffbc9ca5f9" Name="idx_AdditionalApprovers_ID" IsClustered="true">
		<SchemeIndexedColumn Column="6b8ce61e-6509-014b-4000-06ffbc9ca5f9" />
	</SchemeIndex>
</SchemeTable>