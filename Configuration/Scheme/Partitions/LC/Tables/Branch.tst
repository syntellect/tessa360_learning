﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="2d319363-7d20-4943-a93b-505ed8643aec" ID="eb0b3153-4f80-4172-9f9d-af3047dd66a6" Name="Branch" Group="LC" InstanceType="Cards" ContentType="Entries">
	<Description>Таблица для типа карточки Филиал</Description>
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="eb0b3153-4f80-0072-2000-0f3047dd66a6" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="eb0b3153-4f80-0172-4000-0f3047dd66a6" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="780f169d-bbad-4f70-b04b-54c19889030d" Name="Name" Type="String(128) Not Null" />
	<SchemePhysicalColumn ID="5bb5a589-3997-4ba8-a06f-2a1dafc93179" Name="Code" Type="String(128) Null" />
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="eb0b3153-4f80-0072-5000-0f3047dd66a6" Name="pk_Branch" IsClustered="true">
		<SchemeIndexedColumn Column="eb0b3153-4f80-0172-4000-0f3047dd66a6" />
	</SchemePrimaryKey>
</SchemeTable>