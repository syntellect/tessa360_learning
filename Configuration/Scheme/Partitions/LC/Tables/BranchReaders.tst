﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="2d319363-7d20-4943-a93b-505ed8643aec" ID="9ae82439-7887-4da8-a7db-3832efd5a677" Name="BranchReaders" Group="LC" InstanceType="Cards" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="9ae82439-7887-00a8-2000-0832efd5a677" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="9ae82439-7887-01a8-4000-0832efd5a677" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="9ae82439-7887-00a8-3100-0832efd5a677" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="87bddd32-8704-4f46-b631-2480e32b617b" Name="User" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="87bddd32-8704-0046-4000-0480e32b617b" Name="UserID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="8473c9e4-bdbb-472d-98a7-6860b8fec595" Name="UserName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="9ae82439-7887-00a8-5000-0832efd5a677" Name="pk_BranchReaders">
		<SchemeIndexedColumn Column="9ae82439-7887-00a8-3100-0832efd5a677" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="9ae82439-7887-00a8-7000-0832efd5a677" Name="idx_BranchReaders_ID" IsClustered="true">
		<SchemeIndexedColumn Column="9ae82439-7887-01a8-4000-0832efd5a677" />
	</SchemeIndex>
</SchemeTable>