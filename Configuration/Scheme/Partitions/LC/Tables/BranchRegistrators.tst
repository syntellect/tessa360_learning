﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="2d319363-7d20-4943-a93b-505ed8643aec" ID="88c778f4-3629-4d61-841b-51d7fbb91b30" Name="BranchRegistrators" Group="LC" InstanceType="Cards" ContentType="Collections">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="88c778f4-3629-0061-2000-01d7fbb91b30" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="1074eadd-21d7-4925-98c8-40d1e5f0ca0e">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="88c778f4-3629-0161-4000-01d7fbb91b30" Name="ID" Type="Guid Not Null" ReferencedColumn="9a58123b-b2e9-4137-9c6c-5dab0ec02747" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="88c778f4-3629-0061-3100-01d7fbb91b30" Name="RowID" Type="Guid Not Null" />
	<SchemeComplexColumn ID="041a5974-13c4-43e2-b195-93e250d2eb11" Name="User" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="041a5974-13c4-00e2-4000-03e250d2eb11" Name="UserID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="c0150828-abf3-44f0-8c4b-6d3ecf150c86" Name="UserName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="88c778f4-3629-0061-5000-01d7fbb91b30" Name="pk_BranchRegistrators">
		<SchemeIndexedColumn Column="88c778f4-3629-0061-3100-01d7fbb91b30" />
	</SchemePrimaryKey>
	<SchemeIndex IsSystem="true" IsPermanent="true" IsSealed="true" ID="88c778f4-3629-0061-7000-01d7fbb91b30" Name="idx_BranchRegistrators_ID" IsClustered="true">
		<SchemeIndexedColumn Column="88c778f4-3629-0161-4000-01d7fbb91b30" />
	</SchemeIndex>
</SchemeTable>