﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable ID="a161e289-2f99-4699-9e95-6e3336be8527" Partition="d1b372f3-7565-4309-9037-5e5a0969d94e">
	<SchemeComplexColumn Partition="2d319363-7d20-4943-a93b-505ed8643aec" ID="3a0667d7-cef7-4e4f-9157-b4126ab7ce93" Name="Branch" Type="Reference(Typified) Null" ReferencedTable="eb0b3153-4f80-4172-9f9d-af3047dd66a6">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="3a0667d7-cef7-004f-4000-04126ab7ce93" Name="BranchID" Type="Guid Null" ReferencedColumn="eb0b3153-4f80-0172-4000-0f3047dd66a6" />
		<SchemeReferencingColumn ID="2b6b8bed-ec12-48d4-8329-18d3e507b001" Name="BranchName" Type="String(128) Null" ReferencedColumn="780f169d-bbad-4f70-b04b-54c19889030d" />
	</SchemeComplexColumn>
	<Predicate Dbms="SqlServer">[RefDocID] IS NOT NULL</Predicate>
	<Predicate Dbms="PostgreSql">"RefDocID" IS NOT NULL</Predicate>
	<Predicate Dbms="SqlServer">[ReceiverRowID] IS NOT NULL</Predicate>
	<Predicate Dbms="PostgreSql">"ReceiverRowID" IS NOT NULL</Predicate>
	<Predicate Dbms="SqlServer">[CategoryID] IS NOT NULL</Predicate>
	<Predicate Dbms="PostgreSql">"CategoryID" IS NOT NULL</Predicate>
</SchemeTable>