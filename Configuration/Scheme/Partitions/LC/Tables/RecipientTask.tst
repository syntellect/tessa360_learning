﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeTable Partition="2d319363-7d20-4943-a93b-505ed8643aec" ID="c59e0bac-824b-4d27-a27b-0f90c95872e1" Name="RecipientTask" Group="LC" InstanceType="Tasks" ContentType="Entries">
	<SchemeComplexColumn IsSystem="true" IsPermanent="true" IsSealed="true" ID="c59e0bac-824b-0027-2000-0f90c95872e1" Name="ID" Type="Reference(Typified) Not Null" ReferencedTable="5bfa9936-bb5a-4e8f-89a9-180bfd8f75f8">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="c59e0bac-824b-0127-4000-0f90c95872e1" Name="ID" Type="Guid Not Null" ReferencedColumn="5bfa9936-bb5a-008f-3100-080bfd8f75f8" />
	</SchemeComplexColumn>
	<SchemeComplexColumn ID="fae928fa-c052-43bf-83a5-28d1193f65b9" Name="Controller" Type="Reference(Typified) Null" ReferencedTable="6c977939-bbfc-456f-a133-f1c2244e3cc3">
		<SchemeReferencingColumn IsSystem="true" IsPermanent="true" ID="fae928fa-c052-00bf-4000-08d1193f65b9" Name="ControllerID" Type="Guid Null" ReferencedColumn="6c977939-bbfc-016f-4000-01c2244e3cc3" />
		<SchemeReferencingColumn ID="a39ccdbf-dc8b-4e6a-960b-ee991027af3c" Name="ControllerName" Type="String(128) Null" ReferencedColumn="1782f76a-4743-4aa4-920c-7edaee860964" />
	</SchemeComplexColumn>
	<SchemePhysicalColumn ID="2bcf97c5-2dbb-4fe0-a24e-6864e56aa55c" Name="Comment" Type="String(1000) Null" />
	<SchemePrimaryKey IsSystem="true" IsPermanent="true" IsSealed="true" ID="c59e0bac-824b-0027-5000-0f90c95872e1" Name="pk_RecipientTask" IsClustered="true">
		<SchemeIndexedColumn Column="c59e0bac-824b-0127-4000-0f90c95872e1" />
	</SchemePrimaryKey>
</SchemeTable>