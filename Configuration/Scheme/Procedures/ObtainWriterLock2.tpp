﻿<?xml version="1.0" encoding="utf-8"?>
<SchemeProcedure ID="212681fa-5be2-4601-8aa8-3885ee87b326" Name="ObtainWriterLock2" Group="System">
	<Description>Перед выполнением хранимой процедуры следует указать максимальное время ожидания блокировки (lock_timeout для PostgreSQL).</Description>
	<Definition Dbms="PostgreSql" IsExternal="true" />
</SchemeProcedure>