﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Chronos.Contracts;
using NLog;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared;
using Tessa.Forums;
using Tessa.Notices;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Operations;
using Tessa.Platform.Runtime;
using Tessa.Platform.Validation;
using Tessa.Roles;
using Unity;

namespace Tessa.Extensions.Default.Chronos.Notices
{
    /// <summary>
    /// Плагин, выполняющий добавление уведомлений о текущих заданиях пользователя.
    /// </summary>
    [Plugin(
        Name = "Forum new messages notifications plugin",
        Description = "Plugin starts at configured time and send emails to users with information about new unread messages in forums.",
        Version = 1,
        ConfigFile = ConfigFilePath)]
    public class ForumNewMessagesNotificationPlugin :
        Plugin

    {
        #region Constants

        /// <summary>
        /// Относительный путь к конфигурационному файлу плагина.
        /// </summary>
        public const string ConfigFilePath = "configuration/ForumNewMessagesNotification.xml";

        public const string FmMessagesPluginTable = nameof(FmMessagesPluginTable);

        private const string TimeoutTemplate =
            "Forums messages lock: failed to acquire, timeout in {0} seconds. Please, check whether forums messages lock is stuck" +
            " in active operations table \"Operations\". Remove the row if that`s the case.";

        #endregion

        #region Fields

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private IDbScope dbScope;
        private ISession session;
        private INotificationManager notificationManager;
        private IOperationRepository operationRepository;
        private ITransactionStrategy transactionStrategy;

        #endregion

        #region Private Methods

        private static async Task<string> GetNormalizedWebAddressAsync(
            DbManager db,
            IQueryBuilderFactory builderFactory,
            CancellationToken cancellationToken = default)
        {
            string webAddress = await db
                .SetCommand(
                    builderFactory
                        .Select().Top(1).C("WebAddress")
                        .From("ServerInstances").NoLock()
                        .Limit(1)
                        .Build())
                .LogCommand()
                .ExecuteAsync<string>(cancellationToken);

            return LinkHelper.NormalizeWebAddress(webAddress);
        }

        private static async Task<int> GetDefaultUtcOffsetMinutesAsync(
            DbManager db,
            IQueryBuilderFactory builderFactory,
            CancellationToken cancellationToken = default) =>
            await db
                .SetCommand(
                    builderFactory
                        .Select().Top(1).C("UtcOffsetMinutes")
                        .From("DefaultTimeZone").NoLock()
                        .Limit(1)
                        .Build())
                .LogCommand()
                .ExecuteAsync<int>(cancellationToken);

        /// <summary>
        /// Получает дату последнего запуска плагина.
        /// До этой даты "включительно" рассылались сообщения при прошлом запуске.
        /// </summary>
        /// <param name="db">DbManager</param>
        /// <param name="builderFactory">IQueryBuilderFactory</param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns>Дату последнего запуска плагина</returns>
        private static async Task<DateTime> GetLastPluginRunDateAsync(
            DbManager db,
            IQueryBuilderFactory builderFactory,
            CancellationToken cancellationToken = default)
        {
            var lastPluginRunDate = await db
                .SetCommand(
                    builderFactory
                        .Select().Top(1).C("LastPluginRunDate")
                        .From(FmMessagesPluginTable).NoLock()
                        .Limit(1)
                        .Build())
                .LogCommand()
                .ExecuteAsync<DateTime>(cancellationToken);

            return DateTime.SpecifyKind(lastPluginRunDate, DateTimeKind.Utc);
        }

        /// <summary>
        /// Обновляет дату последнего запуска плагина.
        /// До этой даты "включительно" рассылались сообщения при текущем запуске.
        /// </summary>
        /// <param name="db">DbManager</param>
        /// <param name="builderFactory">IQueryBuilderFactory</param>
        /// <param name="newLastPluginRunDate">Новая дата последнего запуска плагина.</param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns>Асинхронная задача.</returns>
        private static async Task UpdateLastPluginRunDateAsync(
            DbManager db,
            IQueryBuilderFactory builderFactory,
            DateTime newLastPluginRunDate,
            CancellationToken cancellationToken = default)
        {
            int updated = await db
                .SetCommand(
                    builderFactory
                        .Update(FmMessagesPluginTable)
                        .C("LastPluginRunDate").Equals().P("NewLastPluginRunDate")
                        .Build(),
                    db.Parameter("NewLastPluginRunDate", newLastPluginRunDate))
                .LogCommand()
                .ExecuteNonQueryAsync(cancellationToken);

            if (updated == 0)
            {
                await db
                    .SetCommand(
                        builderFactory
                            .InsertInto(FmMessagesPluginTable, "LastPluginRunDate")
                            .Values(v => v.P("NewLastPluginRunDate"))
                            .Build(),
                        db.Parameter("NewLastPluginRunDate", newLastPluginRunDate))
                    .LogCommand()
                    .ExecuteNonQueryAsync(cancellationToken);
            }
        }

        private async Task<IList<TopicNotificationInfo>> GetNotificationsInfoAsync(
            DbManager db,
            IQueryBuilderFactory builderFactory,
            string normalizedWebAddress,
            int defaultUtcOffsetMinutes,
            DateTime currentPluginRunDateTime,
            DateTime lastPluginRunDateTime,
            CancellationToken cancellationToken = default)
        {
            var result = new List<TopicNotificationInfo>();

            await using (var reader = await db
                .SetCommand(
                    builderFactory
                        .Select()
                        .C("usr", "UserID")
                        .C("s", "MainCardID")
                        .C("msg", "TopicRowID")
                        .C("tp", "Title")
                        .C("r", "TimeZoneUtcOffsetMinutes")
                        .C("msg", "Created", "AuthorName", "Body")
                        .C("tp", "Description")
                        .From("FmMessages", "msg").NoLock()
                        .InnerJoin("FmTopics", "tp").NoLock()
                        .On().C("msg", "TopicRowID").Equals().C("tp", "RowID")
                        .InnerJoin("Satellites", "s").NoLock()
                        .On().C("tp", "ID").Equals().C("s", "ID")
                        .And().C("s", "TypeID").Equals().V(ForumHelper.ForumSatelliteTypeID)
                        .InnerJoinLateral(q => q
                                .SelectDistinct()
                                .C("t", "TopicRowID", "UserID")
                                .From(t => t
                                        .Select()
                                        .C("tp", "TopicRowID", "UserID")
                                        .From("FmTopicParticipants", "tp").NoLock()
                                        .Where()
                                        .C("tp", "TopicRowID").Equals().C("msg", "TopicRowID")
                                        .UnionAll()
                                        .Select()
                                        .C("tpr", "TopicRowID")
                                        .C("ru", "UserID")
                                        .From("FmTopicParticipantRoles", "tpr").NoLock()
                                        .InnerJoin(RoleStrings.RoleUsers, "ru").NoLock()
                                        .On().C("tpr", "RoleID").Equals().C("ru", "ID")
                                        .Where()
                                        .C("tpr", "TopicRowID").Equals().C("msg", "TopicRowID"),
                                    "t")
                            , "usr")
                        .InnerJoin(RoleStrings.Roles, "r").NoLock()
                        .On()
                        .C("r", "ID").Equals().C("usr", "UserID")
                        // пока пользователь не открыл обсуждение в карточке, в таблице FmUserStat для него отсутствует строка,
                        // но почтовые уведомления мы хотим отправлять сразу после подписки - поэтому LEFT JOIN
                        .LeftJoin("FmUserStat", "fst").NoLock()
                        .On()
                        .C("tp", "RowID").Equals().C("fst", "TopicRowID")
                        .And()
                        .C("usr", "UserID").Equals().C("fst", "UserID")
                        .Where()
                        .C("msg", "Created").LessOrEquals().P("CurrentPluginRunDateTime")
                        .And()
                        .C("msg", "Created").Greater().P("LastPluginRunDateTime")
                        .And(q => q
                            .C("msg", "Created").Greater().C("fst", "LastReadMessageTime")
                            .Or()
                            .C("msg", "Created").IsNull())
                        .OrderBy("usr", "UserID").By("usr", "TopicRowID").By("msg", "Created")
                        .Build(),
                    db.Parameter("CurrentPluginRunDateTime", currentPluginRunDateTime),
                    db.Parameter("LastPluginRunDateTime", lastPluginRunDateTime))
                .LogCommand()
                .WithoutTimeout()
                .ExecuteReaderAsync(CommandBehavior.SequentialAccess, cancellationToken))
            {
                while (await reader.ReadAsync(cancellationToken))
                {
                    Guid userID = reader.GetGuid(0);
                    Guid cardID = reader.GetGuid(1);
                    Guid topicID = reader.GetGuid(2);
                    string topicTitle = reader.GetValue<string>(3);
                    int? utcOffsetMinutes = reader.GetValue<int?>(4);
                    DateTime? messageDate = reader.GetNullableDateTimeUtc(5);
                    string authorName = reader.GetValue<string>(6);
                    string htmlText = await reader.GetSequentialNullableStringAsync(7, cancellationToken);
                    string topicDescription = await reader.GetSequentialNullableStringAsync(8, cancellationToken);
                    
                    htmlText = ForumSerializationHelper.DeserializeMessageBody(htmlText).Text;
                    htmlText = Regex.Replace(htmlText, "color:#[0-9a-fA-F]{8}", p => p.Value[..^2]);

                    var info = new TopicNotificationInfo
                    {
                        UserID = userID,
                        CardID = cardID,
                        TopicID = topicID,
                        TopicTitle = HttpUtility.HtmlEncode(topicTitle),
                        MessageDate = messageDate.HasValue ? messageDate.Value + TimeSpan.FromMinutes(utcOffsetMinutes ?? defaultUtcOffsetMinutes) : null,
                        AuthorName = authorName,
                        HtmlText = htmlText,
                        TopicDescription = topicDescription,
                    };

                    info.Link = CardHelper.GetLink(this.session, info.CardID);
                    info.WebLink = CardHelper.GetWebLink(normalizedWebAddress, info.CardID, normalize: false);

                    result.Add(info);
                }
            }

            return result.OrderBy(p => p.UserID).ToList();
        }

        private static void ProceedNotificationRow(
            TopicNotificationInfo info,
            ForumMessagesNotification currentNotification)
        {
            if (info != null)
            {
                info.HtmlText = Regex.Replace(info.HtmlText, "<img ", "<img alt=\"[image]\" ");
                currentNotification.TopicsNotifications.Add(info);
            }
        }

        private async Task ProcessOperationAsync(CancellationToken cancellationToken = default)
        {
            if (this.StopRequested)
            {
                return;
            }

            logger.Trace("Forums new messages notifications processing: started");

            await using (this.dbScope.Create())
            {
                await this.operationRepository.ExecuteInLockAsync(
                    "$Forums_Operation_NewMessagesNotificationsSending",
                    lockOperationTypeID: OperationTypes.ForumNewMessagesProcessingNotificationsOperationID,
                    timeoutSeconds: 300,
                    timeoutMessage: TimeoutTemplate,
                    lockName: "Forums messages lock",
                    logger: logger,
                    cancellationToken: cancellationToken,
                    actionFunc: async (_, ct) =>
                    {
                        var db = this.dbScope.Db;
                        var builderFactory = this.dbScope.BuilderFactory;

                        try
                        {
                            if (this.StopRequested)
                            {
                                return;
                            }

                            // Записываем текущую дату/время запуска в переменную
                            // и считываем из базы дату/время предыдущего запуска
                            DateTime currentPluginRunDateTime = DateTime.UtcNow;
                            DateTime lastPluginRunDateTime = await GetLastPluginRunDateAsync(db, builderFactory, cancellationToken);

                            // Получаем кусочек с web адресом для ссылки на ЛК
                            string normalizedWebAddress = await GetNormalizedWebAddressAsync(db, builderFactory, cancellationToken);
                            int defaultUtcOffsetMinutes = await GetDefaultUtcOffsetMinutesAsync(db, builderFactory, cancellationToken);

                            logger.Trace("Getting notifications.");

                            // Инфо упорядочена по пользователям
                            IList<TopicNotificationInfo> notificationList =
                                    await this.GetNotificationsInfoAsync(
                                        db,
                                        builderFactory,
                                        normalizedWebAddress,
                                        defaultUtcOffsetMinutes,
                                        currentPluginRunDateTime,
                                        lastPluginRunDateTime,
                                        cancellationToken)
                                ;

                            if (this.StopRequested || notificationList.Count == 0)
                            {
                                return;
                            }

                            logger.Trace("Processing notifications.");

                            // Открываем транзакцию, чтобы, если что,
                            // всегда можно было откатиться на момент до отправки первого уведомления
                            var validationResult = new ValidationResultBuilder();
                            bool success = await this.transactionStrategy.ExecuteInTransactionAsync(
                                validationResult,
                                async p =>
                                {
                                    ForumMessagesNotification currentNotification =
                                        new ForumMessagesNotification(
                                            notificationList[0].UserID,
                                            normalizedWebAddress);

                                    // Парсим уведомления
                                    foreach (TopicNotificationInfo notification in notificationList)
                                    {
                                        if (currentNotification.UserID != notification.UserID)
                                        {
                                            var sendResult = await notificationManager.SendAsync(
                                                DefaultNotifications.ForumNewMessagesNotification,
                                                new[] { currentNotification.UserID },
                                                new NotificationSendContext
                                                {
                                                    ExcludeDeputies = true,
                                                    MainCardID = currentNotification.UserID,
                                                    Info = currentNotification.GetInfo(),
                                                },
                                                cancellationToken);

                                            logger.LogResult(sendResult);

                                            // Начинаем обработку следующего пользователя
                                            currentNotification =
                                                new ForumMessagesNotification(
                                                    notification.UserID,
                                                    normalizedWebAddress);

                                            ProceedNotificationRow(notification, currentNotification);
                                        }
                                        else
                                        {
                                            ProceedNotificationRow(notification, currentNotification);
                                        }
                                    }

                                    var lastSendResult = await notificationManager.SendAsync(
                                        DefaultNotifications.ForumNewMessagesNotification,
                                        new[] { currentNotification.UserID },
                                        new NotificationSendContext
                                        {
                                            ExcludeDeputies = true,
                                            MainCardID = currentNotification.UserID,
                                            Info = currentNotification.GetInfo(),
                                        },
                                        cancellationToken);

                                    logger.LogResult(lastSendResult);

                                    // Обновляем дату последнего запуска плагина (LastPluginRunDate) из даты СurrentPluginRunDateTime
                                    logger.Trace("Updating LastPluginRunDate.");
                                    await UpdateLastPluginRunDateAsync(db, builderFactory, currentPluginRunDateTime, cancellationToken);
                                },
                                cancellationToken);

                            if (!success)
                            {
                                throw new ValidationException(validationResult.Build());
                            }

                            logger.Trace("Notifications were successfully processed.");
                        }
                        catch (Exception ex)
                        {
                            // исключения игнорируются
                            if (!(ex is OperationCanceledException))
                            {
                                logger.LogException(ex);
                            }

                            if (db.DataConnection.Transaction != null)
                            {
                                try
                                {
                                    await db.RollbackTransactionAsync(CancellationToken.None);
                                }
                                catch (Exception ex2)
                                {
                                    // исключения игнорируются
                                    logger.LogException(ex2);
                                }
                            }
                        }
                    });
            }

            logger.Trace("Forums new messages notifications processing: completed");
        }

        #endregion

        #region Base Overrides

        public override async Task EntryPointAsync(CancellationToken cancellationToken = default)
        {
            logger.Trace("Starting forum new messages notifications plugin.");

            await TessaPlatform.InitializeFromConfigurationAsync(cancellationToken: cancellationToken);
            RuntimeHelper.ServerRequestID = Guid.NewGuid();

            IUnityContainer container = await new UnityContainer().RegisterServerForPluginAsync();

            this.dbScope = container.Resolve<IDbScope>();
            this.session = container.Resolve<ISession>();
            this.operationRepository = container.Resolve<IOperationRepository>();
            this.transactionStrategy = container.Resolve<ITransactionStrategy>();
            this.notificationManager = container.Resolve<INotificationManager>();

            await this.ProcessOperationAsync(cancellationToken);
        }

        #endregion
    }
}