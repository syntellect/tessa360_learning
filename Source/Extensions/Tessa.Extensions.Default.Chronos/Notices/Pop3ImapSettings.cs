﻿namespace Tessa.Extensions.Default.Chronos.Notices
{
    public class Pop3ImapSettings
    {
        public string Host { get; set; }

        public int? Port { get; set; }

        public string User { get; set; }

        public string Password { get; set; }

        public bool? UseSsl { get; set; }
    }
}