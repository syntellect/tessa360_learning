﻿using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Cards;
using Tessa.Platform;
using Tessa.UI.Cards;
using Unity;
using Unity.Lifetime;

namespace Tessa.Extensions.Default.Client.Extensions
{
    [Registrator]
    public sealed class Registrator : RegistratorBase
    {
        public override void RegisterUnity()
        {
            this.UnityContainer
                .RegisterType<InitializeFilesViewExtensionType>(new ContainerControlledLifetimeManager())
                ;
        }

        public override void FinalizeRegistration()
        {
            this.UnityContainer
                .TryResolve<ITypeExtensionTypeResolver>()
                ?.Register<InitializeFilesViewExtensionType>(DefaultCardTypeExtensionTypes.InitializeFilesView);

            DefaultCardTypeExtensionTypes.Register(CardTypeExtensionTypeRegistry.Instance.Register);
        }
    }
}
