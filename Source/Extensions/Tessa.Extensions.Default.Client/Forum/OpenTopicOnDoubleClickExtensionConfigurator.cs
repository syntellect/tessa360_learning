﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tessa.UI.Views.Extensions;

namespace Tessa.Extensions.Default.Client.Forum
{
    /// <summary>
    /// Конфигуратор расширения <see cref="OpenTopicOnDoubleClickExtension"/>.
    /// </summary>
    public sealed class OpenTopicOnDoubleClickExtensionConfigurator
        : ExtensionSettingsConfiguratorBase
    {
        private const string DescriptionLocalization = "$OpenTopicOnDoubleClickExtension_Description";
        private const string NameLocalization = null;

        public OpenTopicOnDoubleClickExtensionConfigurator()
            : base(ViewExtensionConfiguratorType.None, NameLocalization, DescriptionLocalization)
        {
        }
    }
}
