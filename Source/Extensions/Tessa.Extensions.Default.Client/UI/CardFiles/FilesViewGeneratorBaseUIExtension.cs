﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Cards;
using Tessa.Extensions.Default.Shared.Views;
using Tessa.Files;
using Tessa.Localization;
using Tessa.Platform.Collections;
using Tessa.Platform.Runtime;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Scheme;
using Tessa.UI;
using Tessa.UI.Cards;
using Tessa.UI.Cards.Controls;
using Tessa.UI.Controls;
using Tessa.UI.Files;
using Tessa.UI.Files.Controls;
using Tessa.UI.Menu;
using Tessa.UI.Views.Content;
using Tessa.Views;
using Tessa.Views.Metadata;
using Tessa.Views.Metadata.Criteria;

namespace Tessa.Extensions.Default.Client.UI.CardFiles
{
    public abstract class FilesViewGeneratorBaseUIExtension : CardUIExtension
    {
        #region private fields & constructor

        /// <inheritdoc />
        protected FilesViewGeneratorBaseUIExtension(
            ISession session,
            IExtensionContainer extensionContainer,
            IViewService viewService)
        {
            this.Session = session ?? throw new ArgumentNullException(nameof(session));
            this.ExtensionContainer = extensionContainer ?? throw new ArgumentNullException(nameof(extensionContainer));
            this.ViewService = viewService ?? throw new ArgumentNullException(nameof(viewService));
        }

        #endregion

        #region Static helpers

        public static ViewFileControl TryGetFileControl(ISerializableObject info, string viewName)
        {
            return info.TryGetValue(viewName, out var o)
                ? o as ViewFileControl
                : null;
        }

        #endregion

        #region Protected Properties

        protected IExtensionContainer ExtensionContainer { get; }

        protected ISession Session { get; }

        protected IViewService ViewService { get; }

        #endregion

        #region protected API

        /// <summary>
        /// Создает скрытый FileControl, через который представление
        /// будет взаимодействовать с файловым API. Для каждого алиаса представления
        /// должен быть создан свой файлконтрол. Создание происходит в CardUIExtension.Initializing.
        /// </summary>
        /// <param name="cardModel">модель карточки.</param>
        /// <param name="viewControlName">Алиас контрола представления, которое будет адаптировано под отображение файлов.</param>
        /// <param name="creationParams">Параметры файлового контрола.</param>
        protected void AddCardModelInitializers(
            ICardModel cardModel,
            string viewControlName,
            FileControlCreationParams creationParams)
        {
            cardModel.ControlInitializers.Add(async (control, cm, r, ct) =>
            {
                if (control is CardViewControlViewModel viewControl)
                {
                    if (viewControl.Name != viewControlName)
                    {
                        return;
                    }

                    if (FormCreationContext.Current.FileControls.Any(x => x.Name == viewControl.Name))
                    {
                        TessaDialog.ShowError($"Multiple FileViewControlViewModel with Name='{viewControl.Name}' was found on the form.");
                        return;
                    }

                    var categoriesView = await this.ViewService.GetByNameAsync(creationParams.CategoriesViewAlias, ct);
                    if (categoriesView is null)
                    {
                        TessaDialog.ShowError(
                            $"Categories View:'{creationParams.CategoriesViewAlias}' isn't found'");
                        return;
                    }

                    var cardMetadata = cm.GeneralMetadata;
                    var fileContainer = cm.FileContainer;
                    var fileTypes =
                        CardHelper.GetCardFileTypes(
                            await CardHelper.GetFileCardTypesAsync(
                                cardMetadata,
                                this.Session.User.IsAdministrator(),
                                ct));


                    var fileControl = new ViewFileControl(
                        viewControl,
                        fileContainer,
                        this.ExtensionContainer,
                        cm.MenuContext,
                        fileTypes,
                        creationParams.IsCategoriesEnabled,
                        creationParams.IsManualCategoriesCreationDisabled,
                        creationParams.IsNullCategoryCreationDisabled,
                        false,
                        creationParams.IsIgnoreExistingCategories,
                        this.Session,
                        previewControlName: creationParams.PreviewControlName,
                        name: viewControl.Name)
                    {
                        CategoryFilterAsync = async (categories, ct2) =>
                        {
                            ITessaViewResult result = null;
                            IViewMetadata categoriesViewMetadata = await categoriesView.GetMetadataAsync(ct2);

                            var request = new TessaViewRequest(categoriesViewMetadata);

                            var parameters =
                                await ViewMappingHelper.AddRequestParametersAsync(
                                    creationParams.CategoriesViewMapping,
                                    cardModel,
                                    this.Session,
                                    categoriesView,
                                    cancellationToken: ct);
                            if (parameters != null)
                            {
                                request.Values.AddRange(parameters);
                            }

                            await cm.ExecuteInContextAsync(
                                async (c, ct3) => { result = await categoriesView.GetDataAsync(request, ct3).ConfigureAwait(false); },
                                ct2).ConfigureAwait(false);

                            var rows = (result != null ? result.Rows : null)
                                ?? Array.Empty<object>();

                            // категории из представления в порядке, в котором их вернуло представление (кроме строчек null)
                            var viewCategories = rows
                                .Cast<IList<object>>()
                                .Where(x => x.Count > 0)
                                .Select(x => (IFileCategory) new FileCategory((Guid) x[0], (string) x[1]))
                                .ToArray();

                            // категории из представления плюс вручную добавленные или другие присутствующие в карточке категории, кроме null
                            var mainCategories = viewCategories
                                .Union(categories)
                                .Where(x => x != null)
                                .ToArray();

                            // добавляем наверх "Без категории" и возвращаем результирующий список
                            return new List<IFileCategory> { null }
                                .Union(mainCategories);
                        }
                    };

                    await fileControl.InitializeAsync(fileContainer.Files, cancellationToken: ct);

                    cm.Info[viewControl.Name] = fileControl;
                    FormCreationContext.Current.Register(fileControl);

                    control.Unloaded += async (s, e) =>
                    {
                        using (e.Defer())
                        {
                            await fileControl.UnloadAsync(e.ValidationResult);
                        }
                    };
                }
            });
        }


        /// <summary>
        /// Свзяывает представление с файловым API через FileControl, созданный в InitializeFileControlAsync.
        /// </summary>
        /// <param name="cardModel"> Модель карточки </param>
        /// <param name="settings"> Настройки расширения.</param>
        /// <param name="createRowFunc"> Функция создающая строку представления</param>
        /// <param name="initializationStrategy"> Стратегия инициализации представления </param>
        /// <param name="viewModifierAction"> Функция для небольших модификации представления, например, заданию дефолного столбца для сортировки</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Файловый контрол <see cref="ViewFileControl"/>.</returns>
        protected async ValueTask AttachViewToFileControlAsync(
            ICardModel cardModel,
            ISerializableObject settings,
            Func<TableRowCreationOptions, ViewControlRowViewModel> createRowFunc,
            IViewCardControlInitializationStrategy initializationStrategy = null,
            Action<CardViewControlViewModel> viewModifierAction = null,
            CancellationToken cancellationToken = default)
        {
            var viewControlName = settings.TryGet<string>(DefaultCardTypeExtensionSettings.FilesViewAlias);

            if (!cardModel.Controls.TryGet(viewControlName, out var controlViewModel))
            {
                return;
            }

            if (controlViewModel is not CardViewControlViewModel viewControlViewModel)
            {
                return;
            }

            viewControlViewModel.CreateRowFunc = createRowFunc;
            var fileControl = TryGetFileControl(cardModel.Info, viewControlViewModel.Name);
            this.InitializeContextMenu(viewControlViewModel, fileControl);
            if (initializationStrategy != null)
            {
                await viewControlViewModel.InitializeStrategyAsync(initializationStrategy, true, cancellationToken);
                viewModifierAction?.Invoke(viewControlViewModel);
                await viewControlViewModel.InitializeOnTabAsync();
            }

            AttachToFileControl(viewControlViewModel, fileControl);
            InitializeGrouping(viewControlViewModel, fileControl);
            InitializeFiltering(viewControlViewModel, fileControl);
            InitializeDragDrop(viewControlViewModel, cardModel);
            InitializeClickCommands(viewControlViewModel, fileControl);
            InitializeMenuButton(viewControlViewModel, fileControl);
            InitializeKeyDownHandlers(viewControlViewModel, fileControl);
            viewControlViewModel.Unloaded += fileControl.StopTimer;

            var defaultGroup = settings.TryGet<string>(DefaultCardTypeExtensionSettings.DefaultGroup);
            if (!string.IsNullOrEmpty(defaultGroup))
            {
                await fileControl.SelectGroupingAsync(fileControl.Groupings.TryGet(defaultGroup), cancellationToken);
            }
        }

        #endregion

        #region Initialization

        /// <summary>
        /// Привязывает измнение коллекции файлов к представлению
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> ViewModel Скрытого контрола с файлами</param>
        private static void AttachToFileControl(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            fileControl.Items.CollectionChanged += (sender, e) =>
            {
                viewModel.DelayedViewRefresh.RunAfterDelay(150);
            };
        }

        /// <summary>
        /// Добавляет Drag&amp;Drop
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="cardModel"> Модель карточки </param>
        private static void InitializeDragDrop(CardViewControlViewModel viewModel, ICardModel cardModel)
        {
            viewModel.AllowDrop = true;
            viewModel.DragDrop = new FilesDragDrop(cardModel, viewModel.Name);
        }

        /// <summary>
        /// Синхронизация группировки в файловом контроле и предсталвении
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> Контрол файлов </param>
        private void InitializeGrouping(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            foreach(var column in viewModel.Table.Columns.Cast<TableColumnViewModel>())
            {
                column.ContextMenuGenerators.Clear();
            }
            var groupCaptionColumn = viewModel.Table.Columns.Cast<TableColumnViewModel>().FirstOrDefault(x => x.ColumnName == ColumnsConst.GroupCaption);
            if (groupCaptionColumn != null)
            {
                groupCaptionColumn.Visibility = false;
            }

            fileControl.PropertyChanged += async (o, e) =>
            {
                if (e.PropertyName == nameof(FileControl.SelectedGrouping))
                {
                    if (fileControl.SelectedGrouping == null)
                    {
                        viewModel.Table.GroupingColumn = null;
                        var categoryColumn = viewModel.Table.Columns.Cast<TableColumnViewModel>().First(x => x.ColumnName == ColumnsConst.CategoryCaption);
                        categoryColumn.Visibility = true;
                    }
                    else
                    {
                        if (fileControl.SelectedGrouping.Name == "Category")
                        {
                            var categoryColumn = viewModel.Table.Columns.Cast<TableColumnViewModel>().First(x => x.ColumnName == ColumnsConst.CategoryCaption);
                            categoryColumn.Visibility = false;
                        }
                        viewModel.Table.GroupingColumn = null;
                        viewModel.Table.GroupingColumn = viewModel.Table.Columns.Cast<TableColumnViewModel>().FirstOrDefault(x => x.ColumnName == ColumnsConst.GroupCaption).Metadata;
                    }

                    var groupCaptionColumnValue = viewModel.Table.Columns.Cast<TableColumnViewModel>().FirstOrDefault(x => x.ColumnName == ColumnsConst.GroupCaption);
                    if (groupCaptionColumnValue != null)
                    {
                        groupCaptionColumnValue.Visibility = false;
                    }
                }
            };
        }

        /// <summary>
        /// Синхронизация фильтрации в файловом контроле и предсталвении
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> Контрол файлов </param>
        private void InitializeFiltering(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            fileControl.PropertyChanged += async (o, e) =>
            {
                // во время обновления представления фильтрация может быть сброшена в дата провайдере. Это событие обрабатывать не нужно.
                if (viewModel.IsDataLoading)
                {
                    return;
                }
                if (e.PropertyName == nameof(IFileControl.SelectedFiltering))
                {
                    var previousFilteringParameter = viewModel.Parameters.FirstOrDefault(x => x.Metadata.Alias == ColumnsConst.FilterParameter);
                    if (previousFilteringParameter != null)
                    {
                        viewModel.Parameters.Remove(previousFilteringParameter);
                    }
                    if (fileControl.SelectedFiltering != null && fileControl.SelectedFiltering is FileGroupingFiltering filter)
                    {
                        var parameterMetadata = new ViewParameterMetadata();
                        parameterMetadata.Caption = filter.Grouping.Caption;
                        parameterMetadata.Alias = ColumnsConst.FilterParameter;
                        parameterMetadata.SchemeType = SchemeType.String;
                        var newFilteringParameter = new RequestParameterBuilder()
                                    .WithMetadata(parameterMetadata)
                                    .AddCriteria(new EqualsCriteriaOperator(), filter.Caption, filter.Caption)
                                    .AsRequestParameter();
                        viewModel.Parameters.Add(newFilteringParameter);
                    }

                    await viewModel.RefreshAsync();
                }
            };
        }

        /// <summary>
        /// Инициализирует обработчики нажатий на клавиатуру
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> ViewModel Скрытого контрола с файлами</param>
        private static void InitializeKeyDownHandlers(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            viewModel.KeyDownHandlers.Add(async (item, data, e) =>
            {
                if (data is TableFileRowViewModel row)
                {
                    if (row.GridViewModel.SelectedItems.Count == 1 &&
                        row.GridViewModel.SelectedItem == row)
                    {
                        if (e.Key == Key.Enter && Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
                        {
                            var file = row.FileViewModel.Model;
                            if (file == null)
                            {
                                TessaDialog.ShowMessage(string.Format(LocalizationManager.Localize("$UI_Common_FileNotFound"), ""));

                                return;
                            }

                            await FileControlHelper.OpenAsync(fileControl, new[] { file }, FileOpeningMode.ForEdit);
                        }
                    }
                }
            });
        }

        /// <summary>
        /// биндинг событий, возникающих при нажатии кнопок мыши
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> ViewModel Скрытого контрола с файлами</param>
        private static void InitializeClickCommands(CardViewControlViewModel viewModel, ViewFileControl fileControl)
        {
            viewModel.Table.RowUnselected += async (o, e) =>
            {
                if (e.Row is null)
                {
                    return;
                }

                var gridViewModel = e.Row.GridViewModel;
                var row = (TableFileRowViewModel) e.Row;
                var fileViewModel = row.FileViewModel;
                var file = fileViewModel.Model.Versions.Last.File;

                //<- Обработка случая, когда строка выбрана, а файл еще прогружается.
                if (e.Row.IsSelected == fileViewModel.IsSelected)
                {
                    return;
                }

                if (fileControl.Manager.IsPreviewInProgress())
                {
                    fileViewModel.IsSelected = true;
                    e.Row.IsSelected = true;
                    return;
                }
                //->

                fileViewModel.IsSelected = false;

                // следующий код асинхронный, использовать аргументы "e" нельзя

                if (file.IsLocal && fileControl.Manager.IsInPreview(file.Content.GetLocalFilePath()))
                {
                    await fileControl.Manager.ResetPreviewAsync();
                }
                else if (gridViewModel.SelectedItems.Count == 1)
                {
                    await fileControl.Manager.ResetPreviewAsync();
                }

                if (Keyboard.Modifiers.Has(ModifierKeys.Control) || Keyboard.Modifiers.Has(ModifierKeys.Shift))
                {
                    var presenter = new FileControlPresenter(fileControl);
                    await presenter.ShowSelectedFilesMessageAsync(fileControl.SelectedItems.ToArray());
                }
            };

            viewModel.Table.RowSelected += async (o, e) =>
            {
                if (e.Row is null)
                {
                    return;
                }

                var gridViewModel = e.Row.GridViewModel;
                var row = (TableFileRowViewModel) e.Row;
                var fileViewModel = row.FileViewModel;

                //<- Обработка случая, когда строка выбрана, а файл еще прогружается.
                if (e.Row.IsSelected == fileViewModel.IsSelected)
                {
                    return;
                }

                if (fileControl.Manager.IsPreviewInProgress())
                {
                    fileViewModel.IsSelected = false;
                    e.Row.IsSelected = false;
                    return;
                }

                //->
                fileViewModel.IsSelected = true;

                // следующий код асинхронный, использовать аргументы "e" нельзя

                // если мы перевели фокус на это представление
                if (gridViewModel.SelectedItems.Count == 1)
                {
                    await fileControl.Manager.ClearSelectionAsync(fileControl);
                }

                if (Keyboard.Modifiers.HasNot(ModifierKeys.Control) && Keyboard.Modifiers.HasNot(ModifierKeys.Shift))
                {
                    fileControl.BeginShowPreview(fileViewModel);
                }

                if (Keyboard.Modifiers.Has(ModifierKeys.Control) || Keyboard.Modifiers.Has(ModifierKeys.Shift))
                {
                    var presenter = new FileControlPresenter(fileControl);
                    await presenter.ShowSelectedFilesMessageAsync(fileControl.SelectedItems.ToArray());
                }
            };

            viewModel.LeftButtonClickCommand = new DelegateCommand(async (o) =>
            {
                var clickInfo = (IViewClickInfo) o;
                var row = (TableFileRowViewModel) clickInfo.Row;
                var fileViewModel = row.FileViewModel;
                if (fileControl.Manager.IsPreviewInProgress())
                {
                    clickInfo.EventArgs.Handled = true;
                    return;
                }

                if (Keyboard.Modifiers.HasNot(ModifierKeys.Control) && Keyboard.Modifiers.HasNot(ModifierKeys.Shift) && row.IsSelected)
                {
                    fileControl.BeginShowPreview(fileViewModel);
                }
            });

            viewModel.DoubleClickCommand =
                new DelegateCommand(async o =>
                {
                    var clickInfo = (IViewClickInfo) o;
                    var row = (TableFileRowViewModel) clickInfo.Row;
                    var selectedFileID = row.FileID;
                    var file = fileControl.Files.FirstOrDefault(f => f.ID == selectedFileID);
                    if (file == null)
                    {
                        await TessaDialog.ShowMessageAsync(string.Format(LocalizationManager.Localize("$UI_Common_FileNotFound"), ""));
                        return;
                    }

                    if (Keyboard.Modifiers.HasFlag(ModifierKeys.Alt))
                    {
                        await FileControlHelper.OpenAsync(fileControl, new[] { file }, FileOpeningMode.ForEdit);
                    }
                    else
                    {
                        await FileControlHelper.OpenAsync(fileControl, new[] { file }, FileOpeningMode.ForRead);
                    }
                });

            viewModel.RightButtonClickCommand = new DelegateCommand(async o =>
            {
                var clickInfo = (IViewClickInfo) o;
                clickInfo.EventArgs.Handled = true;
                var presenter = new FileControlPresenter(fileControl);
                var row = (TableFileRowViewModel) clickInfo.Row;
                var fileID = row.FileID;
                var file = fileControl.Items.FirstOrDefault(f => f.Model.ID == fileID);
                await presenter.ShowFileMenuAsync(file, clickInfo.frameworkElement).ConfigureAwait(false);
            });
        }

        /// <summary>
        /// Создаем контекстное меню контрола
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> ViewModel Скрытого контрола с файлами</param>
        private void InitializeContextMenu(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            viewModel.ContextMenuGenerators.Add(async context =>
            {
                (IMenuActionCollection actions, _, _) = await fileControl.GenerateControlMenuAsync();
                var toRemoveActions = new[] {FileMenuActionNames.Sortings};
                context.MenuActions.AddRange(actions.Where(x => !toRemoveActions.Contains(x.Name)));
            });
        }

        /// <summary>
        /// Добавление кнопки Меню в контрол представления
        /// </summary>
        /// <param name="viewModel"> ViewModel контрола предсталвения </param>
        /// <param name="fileControl"> ViewModel Скрытого контрола с файлами</param>
        private static void InitializeMenuButton(CardViewControlViewModel viewModel, IFileControl fileControl)
        {
            var refreshButtonIndex = viewModel.TopItems.Items.IndexOf(i => i is RefreshButton);
            if (refreshButtonIndex != -1)
            {
                viewModel.TopItems.Items.RemoveAt(refreshButtonIndex);
            }

            var addMenuButton = new ShowContextMenuButtonViewModel { FileControl = fileControl, ViewModel = viewModel };
            viewModel.TopItems.Items.Insert(0, addMenuButton);
        }

        #endregion

        #region Drag&Drop

        private sealed class FilesDragDrop : DefaultDragDrop
        {
            private readonly ICardModel cardModel;
            private readonly string fileControlName;

            public FilesDragDrop(
                ICardModel cardModel,
                string fileControlName)
            {
                this.cardModel = cardModel ?? throw new ArgumentNullException(nameof(cardModel));
                this.fileControlName = fileControlName;
            }

            public override async void OnDrop(object sender, DragEventArgs e)
            {
                var fileControl = TryGetFileControl(this.cardModel.Info, this.fileControlName);
                fileControl?.DropAction(sender, e);
            }

            public override async void DragOver(object sender, DragEventArgs e)
            {
                var fileControl = TryGetFileControl(this.cardModel.Info, this.fileControlName);
                fileControl?.DragOverAction(sender, e);
            }
        }

        #endregion
    }
}