﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.UI;
using Tessa.UI.Cards.Controls;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;

namespace Tessa.Extensions.Default.Client.UI.KrProcess.StageHandlers
{
    public class DialogUIHandler :
        StageTypeUIHandlerBase
    {
        #region Fields

        private CardRow settings;

        private BlockContentIndicator indicator;

        private readonly ICardMetadata metadata;

        #endregion

        #region Constructors
        
        public DialogUIHandler(
            ICardMetadata metadata)
        {
            this.metadata = metadata;
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc />
        public override Task Validate(IKrStageTypeUIHandlerContext context)
        {
            if (context.Row.TryGet<int?>(KrDialogStageTypeSettingsVirtual.CardStoreModeID) is null)
            {
                context.ValidationResult.AddError(this, "$KrStages_Dialog_CardStoreModeNotSpecified");
            }

            if (context.Row.TryGet<int?>(KrDialogStageTypeSettingsVirtual.OpenModeID) is null)
            {
                context.ValidationResult.AddError(this, "$KrStages_Dialog_CardOpenModeNotSpecified");
            }

            if (!context.Row.TryGet<Guid?>(KrDialogStageTypeSettingsVirtual.DialogTypeID).HasValue
                && !context.Row.TryGet<Guid?>(KrDialogStageTypeSettingsVirtual.TemplateID).HasValue)
            {
                context.ValidationResult.AddError(this, "$KrStages_Dialog_TemplateAndTypeNotSpecified");
            }

            if (context.Row.TryGet<Guid?>(KrDialogStageTypeSettingsVirtual.DialogTypeID).HasValue
                && context.Row.TryGet<Guid?>(KrDialogStageTypeSettingsVirtual.TemplateID).HasValue)
            {
                context.ValidationResult.AddError(this, "$KrStages_Dialog_TemplateAndTypeSelected");
            }

            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public override async Task Initialize(IKrStageTypeUIHandlerContext context)
        {
            var rowModel = context.RowModel;
            if (rowModel.Controls.TryGet("ButtonSettings") is GridViewModel grid)
            {
                grid.RowEditorClosing += this.ButtonSettings_RowClosing;
            }

            this.settings = context.Row;
            this.settings.FieldChanged += this.OnSettingsFieldChanged;

            if (context.RowModel.Blocks.TryGet(Ui.KrDialogScriptsBlock, out var control))
            {
                var sectionMeta = (await this.metadata.GetSectionsAsync(context.CancellationToken))[KrStages.Virtual];
                var fieldIDs = sectionMeta.Columns.ToDictionary(k => k.ID, v => v.Name);
                this.indicator = new BlockContentIndicator(control, context.Row, fieldIDs);
            }
        }

        /// <inheritdoc />
        public override Task Finalize(IKrStageTypeUIHandlerContext context)
        {
            var rowModel = context.RowModel;
            if (rowModel.Controls.TryGet("ButtonSettings") is GridViewModel grid)
            {
                grid.RowEditorClosing -= this.ButtonSettings_RowClosing;
            }

            this.settings.FieldChanged -= this.OnSettingsFieldChanged;
            this.settings = default;

            if (this.indicator is not null)
            {
                this.indicator.Dispose();
                this.indicator = null;
            }

            return Task.CompletedTask;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Проверяет корректность настройки кнопок.
        /// </summary>
        /// <param name="sender">Источник события.</param>
        /// <param name="e">Информация о событии.</param>
        private void ButtonSettings_RowClosing(object sender, GridRowEventArgs e)
        {
            var row = e.Row;
            var validationResult = new ValidationResultBuilder();

            if (!row.TryGet<int?>(KrDialogButtonSettingsVirtual.TypeID).HasValue)
            {   
                validationResult.AddError(this, "$KrStages_Dialog_ButtonTypeIDNotSpecified");
                e.Cancel = true;
            }

            if (string.IsNullOrEmpty(row.TryGet<string>(KrDialogButtonSettingsVirtual.Caption)))
            {
                validationResult.AddError(this, "$KrStages_Dialog_ButtonCaptionNotSpecified");
                e.Cancel = true;
            }

            if (string.IsNullOrEmpty(row.TryGet<string>(KrDialogButtonSettingsVirtual.Name)))
            {
                validationResult.AddError(this, "$KrStages_Dialog_ButtonAliasNotSpecified");
                e.Cancel = true;
            }

            TessaDialog.ShowNotEmpty(validationResult);
        }

        private void OnSettingsFieldChanged(object sender, CardFieldChangedEventArgs e)
        {
            if (e.FieldName == KrDialogStageTypeSettingsVirtual.DialogTypeID)
            {
                if (e.FieldValue != null)
                {
                    this.settings.Fields[KrDialogStageTypeSettingsVirtual.TemplateID] = default;
                    this.settings.Fields[KrDialogStageTypeSettingsVirtual.TemplateCaption] = default;
                }
            }
            else if (e.FieldName == KrDialogStageTypeSettingsVirtual.TemplateID)
            {
                if (e.FieldValue != null)
                {
                    this.settings.Fields[KrDialogStageTypeSettingsVirtual.DialogTypeID] = default;
                    this.settings.Fields[KrDialogStageTypeSettingsVirtual.DialogTypeName] = default;
                    this.settings.Fields[KrDialogStageTypeSettingsVirtual.DialogTypeCaption] = default;
                }
            }
        }

        #endregion
    }
}