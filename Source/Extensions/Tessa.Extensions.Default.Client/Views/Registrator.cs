﻿using Tessa.Extensions.Default.Client.Workplaces;
using Tessa.Platform;
using Tessa.UI.Views.Charting;
using Tessa.UI.Views.Extensions;
using Tessa.Views;
using Unity;
using Unity.Lifetime;
using Tessa.Extensions.Default.Client.Workplaces.WebChart;
using Tessa.Extensions.Default.Client.Workplaces.Manager;

namespace Tessa.Extensions.Default.Client.Views
{
    [Registrator]
    public sealed class Registrator : RegistratorBase
    {
        public override void RegisterUnity()
        {
            // Регистрация клиентских представлений в контейнере приложения должна осуществлятся с уникальным именем
            // желательно совпадающим с алиасом представления в метаданных. В случае не уникальности имен
            // в контенере и IViewService будет зарегестировано представление последним осуществившее
            // регистрацию в контейнере. Регистрация в IViewService будет осуществлена по алиасу из метаданных представления

            this.UnityContainer
                .RegisterType<ITessaView, ClientProgramView>(nameof(ClientProgramView), new ContainerControlledLifetimeManager())
                ;
        }


        public override void FinalizeRegistration()
        {
            // типы могут быть не зарегистрированы в тестах или плагинах Chronos

            this.UnityContainer
                .RegisterType<ImageCache>(new ContainerControlledLifetimeManager())
                .TryResolve<IWorkplaceExtensionRegistry>()
                ?
                .Register(typeof(CreateCardExtension))
                .RegisterConfiguratorType(
                    typeof(CreateCardExtension),
                    type => this.UnityContainer.Resolve<CreateCardExtensionConfigurator>())

                .Register(typeof(CustomButtonWorkplaceComponentExtension))
                .RegisterConfiguratorType(
                    typeof(CustomButtonWorkplaceComponentExtension),
                    type => this.UnityContainer.Resolve<CustomButtonWorkplaceComponentExtensionConfigurator>())

                .Register(typeof(RecordViewExtension))
                .RegisterConfiguratorType(
                    typeof(RecordViewExtension),
                    type => this.UnityContainer.Resolve<RecordViewExtensionConfigurator>())

                .Register(typeof(GetDataWithDelayExtension))
                .RegisterConfiguratorType(
                    typeof(GetDataWithDelayExtension),
                    type => this.UnityContainer.Resolve<GetDataWithDelayExtensionConfigurator>())

                .Register(typeof(TreeViewItemTestExtension))
                .RegisterConfiguratorType(
                    typeof(TreeViewItemTestExtension),
                    type => this.UnityContainer.Resolve<TreeViewItemTestExtensionConfigurator>())

                .Register(typeof(CustomFolderViewExtension))
                .RegisterConfiguratorType(
                    typeof(CustomFolderViewExtension),
                    type => this.UnityContainer.Resolve<CustomFolderViewExtensionConfigurator>())

                .Register(typeof(CustomNavigationViewExtension))
                .RegisterConfiguratorType(
                    typeof(CustomNavigationViewExtension),
                    type => this.UnityContainer.Resolve<CustomNavigationViewExtensionConfigurator>())

                .Register(typeof(ViewsContextMenuExtension))
                .RegisterConfiguratorType(
                    typeof(ViewsContextMenuExtension),
                    type => this.UnityContainer.Resolve<ViewsContextMenuExtensionConfigurator>())

                .Register(typeof(ChartViewExtension))
                .RegisterConfiguratorType(
                    typeof(ChartViewExtension),
                    type => this.UnityContainer.Resolve<ChartViewExtensionConfigurator>())

                .Register(typeof(AutomaticNodeRefreshExtension))
                .RegisterConfiguratorType(
                    typeof(AutomaticNodeRefreshExtension),
                    type => this.UnityContainer.Resolve<AutomaticNodeRefreshExtensionConfigurator>())

                .Register(typeof(ManagerWorkplaceExtension))
                .RegisterConfiguratorType(
                    typeof(ManagerWorkplaceExtension),
                    type => this.UnityContainer.Resolve<ManagerWorkplaceExtensionConfigurator>())

                .Register(typeof(WebChartWorkplaceExtension))
                .RegisterConfiguratorType(
                    typeof(WebChartWorkplaceExtension),
                    type => this.UnityContainer.Resolve<WebChartWorkplaceExtensionConfigurator>())

                .Register(typeof(PreviewExtension))
                .RegisterConfiguratorType(
                    typeof(PreviewExtension),
                    type => this.UnityContainer.Resolve<PreviewExtensionConfigurator>())

                .Register(typeof(RefSectionExtension))
                .RegisterConfiguratorType(
                    typeof(RefSectionExtension),
                    type => this.UnityContainer.Resolve<RefSectionExtensionConfigurator>())

                ;
        }
    }
}
