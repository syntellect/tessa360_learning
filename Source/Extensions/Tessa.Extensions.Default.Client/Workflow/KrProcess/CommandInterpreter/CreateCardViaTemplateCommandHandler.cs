﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess.ClientCommandInterpreter;
using Tessa.Platform;
using Tessa.Platform.Storage;
using Tessa.UI;

namespace Tessa.Extensions.Default.Client.Workflow.KrProcess.CommandInterpreter
{
    /// <summary>
    /// Обработчик клиентской команды <see cref="DefaultCommandTypes.CreateCardViaTemplate"/>.
    /// </summary>
    public sealed class CreateCardViaTemplateCommandHandler : ClientCommandHandlerBase
    {
        #region Fields

        private readonly IUIHost uiHost;

        #endregion

        #region Constructor

        public CreateCardViaTemplateCommandHandler(
            IUIHost uiHost)
        {
            this.uiHost = uiHost;
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc/>
        public override async Task Handle(
            IClientCommandHandlerContext context)
        {
            var command = context.Command;
            if (command.Parameters.TryGetValue(KrConstants.Keys.TemplateID, out var templateIDObj)
                && templateIDObj is Guid templateID)
            {
                await DispatcherHelper.InvokeInUIAsync(async () =>
                {
                    using ISplash splash = TessaSplash.Create(TessaSplashMessage.CreatingCard);
                    await this.uiHost.CreateFromTemplateAsync(
                        templateID,
                        templateInfo: new Dictionary<string, object>
                        {
                            [CardHelper.NewCardBilletKey] = command.Parameters.TryGet<byte[]>(KrConstants.Keys.NewCard),
                            [CardHelper.NewCardBilletSignatureKey]  = command.Parameters.TryGet<byte[]>(KrConstants.Keys.NewCardSignature),
                        },
                        options: new OpenCardOptions
                        {
                            Splash = splash,
                        },
                        cancellationToken: context.CancellationToken);
                });
            }
        }

        #endregion
    }
}