using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Forums;

namespace Tessa.Extensions.Default.Server.Forum
{
    /// <summary>
    /// При создании шаблона по карточке, в <c>Card.Info</c> кладем признак, чтобы не подгружать данные топиков для этой карточки при обновлении вкладки.
    /// </summary>
    public sealed class ForumCardTemplateExtension : CardGetExtension
    {
        #region Base Overrides

        public override async Task AfterRequest(ICardGetExtensionContext context)
        {
            Card card;
            if (!context.RequestIsSuccessful
                || context.CardType == null
                || context.CardType?.Flags.HasAny(CardTypeFlags.Hidden | CardTypeFlags.Administrative) == true
                || context.CardType.InstanceType != CardInstanceType.Card
                || context.Request.ServiceType == CardServiceType.Default
                || (card = context.Response.TryGetCard()) == null)
            {
                return;
            }

            if (context.Method == CardGetMethod.Export)
            {
                card.Info.Add(ForumHelper.ExportCardTempateKey, true);
            }
        }

        #endregion
    }
}