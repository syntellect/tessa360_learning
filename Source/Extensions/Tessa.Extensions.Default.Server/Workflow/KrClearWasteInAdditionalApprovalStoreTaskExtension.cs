﻿using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Platform.Storage;

using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;

namespace Tessa.Extensions.Default.Server.Workflow
{
    /// <summary>
    /// Расширение очищает поля с параметрами вариантов завершения у задачи доп. согласования,
    /// когда задача завершается без удаления.
    /// </summary>
    public sealed class KrClearWasteInAdditionalApprovalStoreTaskExtension : CardStoreTaskExtension
    {
        #region Private Methods

        private static void MarkRowsAsDeleted(CardSection section)
        {
            ListStorage<CardRow> rows = section.TryGetRows();

            if (rows != null && rows.Count > 0)
            {
                foreach (CardRow row in rows)
                {
                    row.State = CardRowState.Deleted;
                }
            }
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc/>
        public override Task StoreTaskBeforeRequest(ICardStoreTaskExtensionContext context)
        {
            Card card;
            StringDictionaryStorage<CardSection> sections;

            if (!context.IsCompletion
                || context.State != CardRowState.Modified
                || (card = context.Task.TryGetCard()) == null
                || (sections = card.TryGetSections()) == null)
            {
                return Task.CompletedTask;
            }

            if (sections.TryGetValue(KrCommentators.Name, out var krCommentators))
            {
                MarkRowsAsDeleted(krCommentators);
            }

            if (sections.TryGetValue(KrAdditionalApprovalTaskInfo.Name, out var krAdditionalApprovalTaskInfo))
            {
                // TODO: страшный костыль. В ближайшее время подружить расширение с KrProcess
                card.Info["Comment"] = krAdditionalApprovalTaskInfo.RawFields.TryGet(KrAdditionalApprovalTaskInfo.Comment, string.Empty);
                krAdditionalApprovalTaskInfo.Fields[KrAdditionalApprovalTaskInfo.Comment] = null;
            }

            return Task.CompletedTask;
        }

        #endregion
    }
}
