﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Cards.Metadata;
using Tessa.Platform;
using Tessa.Platform.Storage;

using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;

namespace Tessa.Extensions.Default.Server.Workflow
{
    /// <summary>
    /// Расширение очищает поля с параметрами вариантов завершения у задачи согласования,
    /// когда задача завершается без удаления.
    /// </summary>
    public sealed class KrClearWasteInApprovalStoreTaskExtension : CardStoreTaskExtension
    {
        #region Private Methods

        private static void MarkRowsAsDeleted(CardSection section)
        {
            ListStorage<CardRow> rows = section.TryGetRows();

            if (rows != null && rows.Count > 0)
            {
                foreach (CardRow row in rows)
                {
                    row.State = CardRowState.Deleted;
                }
            }
        }

        #endregion

        #region Base Overrides

        public override async Task StoreTaskBeforeRequest(ICardStoreTaskExtensionContext context)
        {
            Card card;
            StringDictionaryStorage<CardSection> sections;

            if (!context.IsCompletion
                || context.State != CardRowState.Modified
                || (card = context.Task.TryGetCard()) == null
                || (sections = card.TryGetSections()) == null)
            {
                return;
            }

            if (sections.TryGetValue(KrTask.Name, out var krTask))
            {
                // TODO: страшный костыль. В ближайшее время подружить расширение с KrProcess
                card.Info[Comment] = krTask.RawFields.TryGet(KrTask.Comment, string.Empty);
                krTask.Fields[KrTask.Comment] = null;
            }

            if (sections.TryGetValue(KrCommentators.Name, out var krCommentators))
            {
                MarkRowsAsDeleted(krCommentators);
            }

            if (sections.TryGetValue(KrAdditionalApprovalUsers.Name, out var krAdditionalApprovalUsers))
            {
                MarkRowsAsDeleted(krAdditionalApprovalUsers);
            }

            if (sections.TryGetValue(KrAdditionalApproval.Name, out var krAdditionalApproval))
            {
                CardMetadataColumnCollection columns = (await context.CardMetadata.GetSectionsAsync(context.CancellationToken))
                    [KrAdditionalApproval.Name].Columns;

                IDictionary<string, object> fields = krAdditionalApproval.Fields;

                // TODO: страшный костыль. В ближайшее время подружить расширение с KrProcess
                var comment = fields[KrAdditionalApproval.Comment];
                if (comment != null)
                {
                    card.Info[Comment] = fields[KrAdditionalApproval.Comment];
                }

                card.Info[KrAdditionalApproval.TimeLimitation] = fields[KrAdditionalApproval.TimeLimitation];
                card.Info[KrAdditionalApproval.FirstIsResponsible] = fields.TryGet(KrAdditionalApproval.FirstIsResponsible, BooleanBoxes.False);

                fields[KrAdditionalApproval.Comment] = columns[KrAdditionalApproval.Comment].DefaultValidValue;
                fields[KrAdditionalApproval.TimeLimitation] = columns[KrAdditionalApproval.TimeLimitation].DefaultValidValue;
                fields[KrAdditionalApproval.FirstIsResponsible] = columns[KrAdditionalApproval.FirstIsResponsible].DefaultValidValue;
            }
        }

        #endregion
    }
}
