﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Tessa.Extensions.Default.Server.Workflow.KrCompilers
{
    public interface IKrCompilationResultStorage
    {
        Task UpsertAsync(
            Guid cardID,
            IKrCompilationResult compilationResult,
            bool withCompilationResult = false,
            CancellationToken cancellationToken = default);

        Task<IKrCompilationResult> GetCompilationResultAsync(
            Guid cardID,
            CancellationToken cancellationToken = default);

        Task<KrCompilationOutput> GetCompilationOutputAsync(
            Guid cardID,
            CancellationToken cancellationToken = default);

        Task DeleteCompilationResultAsync(
            Guid cardID,
            CancellationToken cancellationToken = default);

        Task DeleteAsync(
            Guid cardID,
            CancellationToken cancellationToken = default);
    }
}