﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards.Caching;
using Tessa.Platform;
using Tessa.Platform.Collections;
using Unity;

namespace Tessa.Extensions.Default.Server.Workflow.KrCompilers
{
    /// <summary>
    /// Кэш содержащий результаты компиляции.
    /// </summary>
    public sealed class KrCompilationCache :
        IKrCompilationCache,
        IDisposable
    {
        #region constants

        private const string CacheKey = "KrCompilationCache";

        #endregion

        #region fields

        private readonly ICardCache cardCache;
        private readonly IKrProcessCache processCache;
        private readonly IKrCompiler krCompiler;
        private readonly IKrCompilationResultStorage сompilationResultStorage;

        private readonly AsyncLock asyncLock = new AsyncLock();

        #endregion

        #region constructor

        public KrCompilationCache(
            ICardCache cardCache,
            IKrProcessCache processCache,
            IKrCompiler krCompiler,
            IKrCompilationResultStorage сompilationResultStorage,
            [OptionalDependency] IUnityDisposableContainer container = default)
        {
            this.cardCache = cardCache;
            this.processCache = processCache;
            this.krCompiler = krCompiler;
            this.сompilationResultStorage = сompilationResultStorage;

            container?.Register(this);
        }

        #endregion

        #region private

        private static bool TessaVersionIsEqual(
            IKrCompilationResult res) =>
            res.Result.BuildDate == BuildInfo.Date && res.Result.BuildVersion == BuildInfo.Version;

        private async Task<IKrCompilationResult> CompileAsync(string key, CancellationToken cancellationToken = default)
        {
            using (await this.asyncLock.EnterAsync(cancellationToken))
            {
                var res = await this.cardCache.Settings.TryGetAlreadyCachedAsync<IKrCompilationResult>(CacheKey, cancellationToken);
                if (res != null)
                {
                    return res;
                }

                res = await this.сompilationResultStorage.GetCompilationResultAsync(Guid.Empty, cancellationToken);
                if (res != null)
                {
                    if (TessaVersionIsEqual(res))
                    {
                        return res;
                    }
                    await this.сompilationResultStorage.DeleteCompilationResultAsync(Guid.Empty, cancellationToken);
                }

                var krCompileContext = new KrCompilationContext();
                krCompileContext.Stages.AddRange((await this.processCache.GetAllRuntimeStagesAsync(cancellationToken)).Values);
                krCompileContext.StageTemplates.AddRange((await this.processCache.GetAllStageTemplatesAsync(cancellationToken)).Values);
                krCompileContext.CommonMethods.AddRange(await this.processCache.GetAllCommonMethodsAsync(cancellationToken));
                krCompileContext.StageGroups.AddRange((await this.processCache.GetAllStageGroupsAsync(cancellationToken)).Values);
                krCompileContext.SecondaryProcesses.AddRange((await this.processCache.GetAllPureProcessesAsync(cancellationToken)).Values);
                krCompileContext.SecondaryProcesses.AddRange((await this.processCache.GetAllButtonsAsync(cancellationToken)).Values);
                krCompileContext.SecondaryProcesses.AddRange((await this.processCache.GetAllActionsAsync(cancellationToken)).Values);

                var result = this.krCompiler.Compile(krCompileContext);
                await this.сompilationResultStorage.UpsertAsync(Guid.Empty, result, true, cancellationToken);
                return result;
            }
        }

        #endregion

        #region implementation

        /// <inheritdoc/>
        public async Task<IKrCompilationResult> BuildAsync(CancellationToken cancellationToken = default)
        {
            if(await this.cardCache.Settings.ContainsAsync(CacheKey, cancellationToken))
            {
                return null;
            }
            return await this.cardCache.Settings.GetAsync(CacheKey, this.CompileAsync, cancellationToken);
        }

        /// <inheritdoc/>
        public async Task<IKrCompilationResult> RebuildAsync(CancellationToken cancellationToken = default)
        {
            await this.InvalidateAsync(cancellationToken);
            return await this.cardCache.Settings.GetAsync(CacheKey, this.CompileAsync, cancellationToken);
        }

        /// <inheritdoc/>
        public ValueTask<IKrCompilationResult> GetAsync(CancellationToken cancellationToken = default)
        {
            return this.cardCache.Settings.GetAsync(CacheKey, this.CompileAsync, cancellationToken);
        }

        /// <inheritdoc/>
        public async Task InvalidateAsync(CancellationToken cancellationToken = default)
        {
            using (await this.asyncLock.EnterAsync(cancellationToken))
            {
                await this.сompilationResultStorage.DeleteCompilationResultAsync(Guid.Empty, cancellationToken);
                await this.cardCache.Settings.InvalidateAsync(CacheKey, cancellationToken);
            }
        }

        #endregion

        #region IDisposable Members

        /// <inheritdoc/>
        public void Dispose() => this.asyncLock.Dispose();

        #endregion
    }
}
