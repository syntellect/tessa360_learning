﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards.Caching;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Serialization;
using Tessa.Platform;
using Tessa.Platform.Collections;
using Tessa.Platform.Data;
using Unity;

namespace Tessa.Extensions.Default.Server.Workflow.KrCompilers
{
    /// <summary>
    /// Кэш содержащий данные из карточек шаблонов этапов.
    /// </summary>
    public sealed class KrProcessCache :
        IKrProcessCache,
        IDisposable
    {
        #region CachedObject Private Class

        private sealed class CachedObject
        {
            #region Constructors

            public CachedObject(
                IEnumerable<IKrStageTemplate> stages,
                IEnumerable<IKrRuntimeStage> runtimeStages,
                IReadOnlyCollection<IKrStageGroup> stageGroups,
                IEnumerable<IKrPureProcess> pureProcesses,
                IEnumerable<IKrAction> actions,
                IEnumerable<IKrProcessButton> buttons,
                IEnumerable<IKrCommonMethod> methods)
            {
                this.Stages = new ReadOnlyDictionary<Guid, IKrStageTemplate>(stages.ToDictionary(k => k.ID, v => v));

                this.StageTemplatesByGroups = new ReadOnlyDictionary<Guid, ReadOnlyCollection<IKrStageTemplate>>(this.Stages
                    .GroupBy(p => p.Value.StageGroupID)
                    .ToDictionary(k => k.Key, v => v.Select(p => p.Value).ToList().AsReadOnly()));

                this.RuntimeStages = new ReadOnlyDictionary<Guid, IKrRuntimeStage>(runtimeStages.ToDictionary(k => k.StageID, v => v));

                this.RuntimeStagesByTemplates = new ReadOnlyDictionary<Guid, ReadOnlyCollection<IKrRuntimeStage>>(this.RuntimeStages
                    .GroupBy(p => p.Value.TemplateID)
                    .ToDictionary(k => k.Key, v => v.Select(p => p.Value).ToList().AsReadOnly()));

                this.StageGroups = new ReadOnlyDictionary<Guid, IKrStageGroup>(stageGroups.ToDictionary(k => k.ID, v => v));

                this.OrderedStageGroups = stageGroups.OrderBy(p => p.Order).ThenBy(p => p.ID).ToList().AsReadOnly();

                this.StageGroupsByProcesses = new ReadOnlyDictionary<Guid, ReadOnlyCollection<IKrStageGroup>>(this.StageGroups
                    .GroupBy(p => p.Value.SecondaryProcessID ?? Guid.Empty)
                    .ToDictionary(k => k.Key, v => v.Select(p => p.Value).ToList().AsReadOnly()));

                this.PureProcesses = new ReadOnlyDictionary<Guid, IKrPureProcess>(pureProcesses.ToDictionary(k => k.ID, v => v));

                this.Actions = new ReadOnlyDictionary<Guid, IKrAction>(actions.ToDictionary(k => k.ID, v => v));

                this.ActionsByTypes = new ReadOnlyDictionary<string, ReadOnlyCollection<IKrAction>>(this.Actions
                    .Where(p => p.Value.EventType != null)
                    .GroupBy(p => p.Value.EventType)
                    .ToDictionary(k => k.Key, v => v.Select(p => p.Value).ToList().AsReadOnly()));

                this.Buttons = new ReadOnlyDictionary<Guid, IKrProcessButton>(buttons.ToDictionary(k => k.ID, v => v));

                this.Methods = methods.ToList().AsReadOnly();
            }

            #endregion

            #region Properties

            public ReadOnlyDictionary<Guid, IKrStageTemplate> Stages { get; }

            public ReadOnlyDictionary<Guid, ReadOnlyCollection<IKrStageTemplate>> StageTemplatesByGroups { get; }

            public ReadOnlyDictionary<Guid, IKrRuntimeStage> RuntimeStages { get; }

            public ReadOnlyDictionary<Guid, ReadOnlyCollection<IKrRuntimeStage>> RuntimeStagesByTemplates { get; }

            public ReadOnlyDictionary<Guid, IKrStageGroup> StageGroups { get; }

            public ReadOnlyCollection<IKrStageGroup> OrderedStageGroups { get; }

            public ReadOnlyDictionary<Guid, ReadOnlyCollection<IKrStageGroup>> StageGroupsByProcesses { get; }

            public ReadOnlyDictionary<Guid, IKrPureProcess> PureProcesses { get; }

            public ReadOnlyDictionary<Guid, IKrAction> Actions { get; }

            public ReadOnlyDictionary<string, ReadOnlyCollection<IKrAction>> ActionsByTypes { get; }

            public ReadOnlyDictionary<Guid, IKrProcessButton> Buttons { get; }

            public ReadOnlyCollection<IKrCommonMethod> Methods { get; }

            #endregion
        }

        #endregion

        #region constants

        private const string CacheKey = "KrProcessCache";

        #endregion

        #region fields

        private readonly ICardCache cardCache;
        private readonly IDbScope dbScope;
        private readonly IExtraSourceSerializer extraSourceSerializer;
        private readonly IKrStageSerializer stageSerializer;

        private readonly AsyncLock asyncLock = new AsyncLock();

        #endregion

        #region constructor

        public KrProcessCache(
            ICardCache cardCache,
            IDbScope dbScope,
            IExtraSourceSerializer extraSourceSerializer,
            IKrStageSerializer stageSerializer,
            [OptionalDependency] IUnityDisposableContainer container = default)
        {
            Check.ArgumentNotNull(cardCache, nameof(cardCache));
            Check.ArgumentNotNull(dbScope, nameof(dbScope));

            this.cardCache = cardCache;
            this.dbScope = dbScope;
            this.extraSourceSerializer = extraSourceSerializer;
            this.stageSerializer = stageSerializer;

            container?.Register(this);
        }

        #endregion

        #region private

        private async Task<CachedObject> UpdateCachedObjectAsync(string key, CancellationToken cancellationToken = default)
        {
            using (await this.asyncLock.EnterAsync(cancellationToken))
            {
                // соединение с БД для заполнения кэша не должно зависеть от текущего соединения и его транзакции
                await using (this.dbScope.CreateNew())
                {
                    List<IKrStageTemplate> stages = await KrCompilersSqlHelper.SelectStageTemplatesAsync(this.dbScope, cancellationToken: cancellationToken);
                    stages.AddRange(await KrCompilersSqlHelper.SelectVirtualStageTemplatesAsync(this.dbScope, cancellationToken: cancellationToken));
                    List<IKrRuntimeStage> runtimeStages = await KrCompilersSqlHelper.SelectRuntimeStagesAsync(this.dbScope, this.stageSerializer, this.extraSourceSerializer, cancellationToken: cancellationToken);
                    runtimeStages.AddRange(await KrCompilersSqlHelper.SelectSecondaryProcessRuntimeStagesAsync(this.dbScope, this.stageSerializer, this.extraSourceSerializer, cancellationToken: cancellationToken));
                    List<IKrStageGroup> stageGroups = await KrCompilersSqlHelper.SelectStageGroupsAsync(this.dbScope, cancellationToken: cancellationToken);
                    stageGroups.AddRange(await KrCompilersSqlHelper.SelectVirtualStageGroupsAsync(this.dbScope, cancellationToken: cancellationToken));
                    List<IKrCommonMethod> methods = await KrCompilersSqlHelper.SelectCommonMethodsAsync(this.dbScope, cancellationToken: cancellationToken);
                    (List<IKrPureProcess> pureProcesses, List<IKrAction> actions, List<IKrProcessButton> buttons) = await KrCompilersSqlHelper.SelectKrSecondaryProcessesAsync(
                        this.dbScope,
                        null,
                        cancellationToken);
                    return new CachedObject(stages, runtimeStages, stageGroups, pureProcesses, actions, buttons, methods);
                }
            }
        }

        #endregion

        #region implementation

        /// <inheritdoc />
        public async ValueTask<IReadOnlyDictionary<Guid, IKrStageGroup>> GetAllStageGroupsAsync(CancellationToken cancellationToken = default)
        {
            return (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).StageGroups;
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyList<IKrStageGroup>> GetOrderedStageGroupsAsync(CancellationToken cancellationToken = default)
        {
            return (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).OrderedStageGroups;
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyList<IKrStageGroup>> GetStageGroupsForSecondaryProcessAsync(
            Guid? process,
            CancellationToken cancellationToken = default)
        {
            var groupsByProcesses = (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).StageGroupsByProcesses;

            return groupsByProcesses.TryGetValue(process ?? Guid.Empty, out var groupList)
                ? groupList
                : EmptyHolder<IKrStageGroup>.Collection;
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyDictionary<Guid, IKrStageTemplate>> GetAllStageTemplatesAsync(CancellationToken cancellationToken = default)
        {
            return (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).Stages;
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyList<IKrStageTemplate>> GetStageTemplatesForGroupAsync(
            Guid groupID,
            CancellationToken cancellationToken = default)
        {
            var stagesByGroups = (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).StageTemplatesByGroups;

            return stagesByGroups.TryGetValue(groupID, out var templateList)
                ? templateList
                : EmptyHolder<IKrStageTemplate>.Collection;
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyDictionary<Guid, IKrRuntimeStage>> GetAllRuntimeStagesAsync(CancellationToken cancellationToken = default)
        {
            return (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).RuntimeStages;
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyList<IKrRuntimeStage>> GetRuntimeStagesForTemplateAsync(
            Guid templateID,
            CancellationToken cancellationToken = default)
        {
            var runtimeStagesByTemplates = (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).RuntimeStagesByTemplates;

            return runtimeStagesByTemplates.TryGetValue(templateID, out var stagesList)
                ? stagesList
                : EmptyHolder<IKrRuntimeStage>.Collection;
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyList<IKrCommonMethod>> GetAllCommonMethodsAsync(CancellationToken cancellationToken = default)
        {
            return (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).Methods;
        }

        /// <inheritdoc />
        public async ValueTask<IKrSecondaryProcess> GetSecondaryProcessAsync(
            Guid pid,
            CancellationToken cancellationToken = default)
        {
            var cachedObject = await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken);
            if (cachedObject.PureProcesses.TryGetValue(pid, out var pure))
            {
                return pure;
            }
            if (cachedObject.Buttons.TryGetValue(pid, out var button))
            {
                return button;
            }
            if (cachedObject.Actions.TryGetValue(pid, out var action))
            {
                return action;
            }
            throw new InvalidOperationException($"Process with ID = {pid.ToString()} doesn't exist.");
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyDictionary<Guid, IKrPureProcess>> GetAllPureProcessesAsync(CancellationToken cancellationToken = default)
        {
            return (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).PureProcesses;
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyCollection<IKrAction>> GetActionsByTypeAsync(
            string actionType,
            CancellationToken cancellationToken = default)
        {
            var actionsByTypes = (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).ActionsByTypes;

            return actionsByTypes.TryGetValue(actionType, out var actionsList)
                ? actionsList
                : EmptyHolder<IKrAction>.Collection;
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyDictionary<Guid, IKrAction>> GetAllActionsAsync(CancellationToken cancellationToken = default)
        {
            return (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).Actions;
        }

        /// <inheritdoc />
        public async ValueTask<IReadOnlyDictionary<Guid, IKrProcessButton>> GetAllButtonsAsync(CancellationToken cancellationToken = default)
        {
            return (await this.cardCache.Settings.GetAsync(CacheKey, this.UpdateCachedObjectAsync, cancellationToken)).Buttons;
        }

        /// <inheritdoc />
        public async Task InvalidateAsync(CancellationToken cancellationToken = default)
        {
            using (await this.asyncLock.EnterAsync(cancellationToken))
            {
                await this.cardCache.Settings.InvalidateAsync(CacheKey, cancellationToken);
            }
        }

        #endregion

        #region IDisposable Members

        /// <inheritdoc/>
        public void Dispose() => this.asyncLock.Dispose();

        #endregion
    }
}
