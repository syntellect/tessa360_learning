﻿using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Localization;
using Tessa.Platform.Data;
using Tessa.Platform.Validation;

namespace Tessa.Extensions.Default.Server.Workflow.KrCompilers.Requests
{
    public abstract class KrCompileSourceStoreExtension : CardStoreExtension
    {
        #region fields

        protected readonly IDbScope DbScope;
        protected readonly IKrProcessCache StageCache;
        protected readonly IKrCompilationCache CompileCache;
        protected readonly IKrCompilationResultStorage CompilationResultStorage;

        #endregion

        #region constructor

        protected KrCompileSourceStoreExtension(
            IDbScope dbScope,
            IKrProcessCache stageCache,
            IKrCompilationCache compileCache,
            IKrCompilationResultStorage compilationResultStorage)
        {
            this.DbScope = dbScope;
            this.StageCache = stageCache;
            this.CompileCache = compileCache;
            this.CompilationResultStorage = compilationResultStorage;
        }

        #endregion

        #region private

        private Task SetLastBuildOutputAsync(ICardStoreExtensionContext context, IKrCompilationResult result, CancellationToken cancellationToken = default)
        {
            return this.CompilationResultStorage.UpsertAsync(
                 context.Request.Card.ID,
                 result,
                 cancellationToken: cancellationToken);
        }

        private void FillValidationResult(ICardStoreExtensionContext context, IKrCompilationResult result)
        {
            context.ValidationResult.AddInfo(
                this,
                result.Result.Assembly != null ?
                    LocalizationManager.GetString("KrMessages_KrStageSourceSuccessfulBuild"):
                    LocalizationManager.GetString("KrMessages_KrStageSourceFailedBuild"));
            context.ValidationResult.Add(result.ValidationResult);
        }

        private async Task<IKrCompilationResult> RebuildAllAsync(ICardStoreExtensionContext context)
        {
            await this.CompileCache.InvalidateAsync(context.CancellationToken);
            await this.StageCache.InvalidateAsync(context.CancellationToken);
            return await this.CompileCache.RebuildAsync(context.CancellationToken);
        }

        protected virtual bool CanBuild(
            ICardStoreExtensionContext context) => true;

        protected abstract Task<IKrCompilationResult> BuildAsync(ICardStoreExtensionContext context);

        protected abstract bool SourceChanged(Card card);

        protected abstract bool CardChanged(Card card);

        #endregion

        #region base overrides

        public override async Task AfterRequest(ICardStoreExtensionContext context)
        {
            if (!context.ValidationResult.IsSuccessful())
            {
                return;
            }

            if (context.Request.Info.ContainsKey(KrConstants.Keys.Compile)
                && this.CanBuild(context))
            {
                var result = await this.BuildAsync(context);
                await this.SetLastBuildOutputAsync(context, result, context.CancellationToken);
            }
            else if (context.Request.Info.ContainsKey(KrConstants.Keys.CompileWithValidationResult)
                && this.CanBuild(context))
            {
                var result = await this.BuildAsync(context);
                await this.SetLastBuildOutputAsync(context, result, context.CancellationToken);
                this.FillValidationResult(context, result);
            }
            else if (context.Request.Info.ContainsKey(KrConstants.Keys.CompileAll)
                && this.CanBuild(context))
            {
                await this.RebuildAllAsync(context);
            }
            else if (context.Request.Info.ContainsKey(KrConstants.Keys.CompileAllWithValidationResult)
                && this.CanBuild(context))
            {
                var result = await this.RebuildAllAsync(context);
                this.FillValidationResult(context, result);
            }
            else if (context.Request.Card.StoreMode == CardStoreMode.Insert
                    || this.SourceChanged(context.Request.Card))
            {
                await this.StageCache.InvalidateAsync(context.CancellationToken);
                await this.CompileCache.InvalidateAsync(context.CancellationToken);
            }
            else if (context.Request.Card.StoreMode == CardStoreMode.Insert
                || this.CardChanged(context.Request.Card))
            {
                await this.StageCache.InvalidateAsync(context.CancellationToken);
            }
        }

        #endregion
    }
}
