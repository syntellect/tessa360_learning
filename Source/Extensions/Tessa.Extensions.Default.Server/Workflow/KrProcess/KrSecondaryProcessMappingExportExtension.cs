﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess
{
    /// <summary>
    /// Действия для экспорта карточки, дополняющие стандартное API.
    /// Данное расширение задает параметры сериализации для выгрузки контента карточек во внешние файлы,
    /// для типа KrSecondaryProcess.
    /// </summary>
    public class KrSecondaryProcessMappingExportExtension : CardGetExtension
    {
        #region Base Overrides

        public override Task AfterRequest(ICardGetExtensionContext context)
        {
            Card card;
            if (!context.RequestIsSuccessful
                || (card = context.Response.TryGetCard()) is null
                || context.Request.ExportFormat != CardFileFormat.Json)
            {
                return Task.CompletedTask;
            }

            var pathMappings = GetPathMappings(card);

            context.Response.SetStorageFilePaths(pathMappings);

            return Task.CompletedTask;
        }

        #endregion

        #region Private Methods

        private static IList<IStorageContentMapping> GetPathMappings(Card card)
        {
            var result = new List<IStorageContentMapping>
            {
                new StorageContentMapping(
                    "Sections.KrSecondaryProcesses.Fields.ExecutionSourceCondition",
                    "ExecutionSourceCondition.cs"),
                new StorageContentMapping(
                    "Sections.KrSecondaryProcesses.Fields.ExecutionSqlCondition",
                    "ExecutionSqlCondition.sql"),
                new StorageContentMapping(
                    "Sections.KrSecondaryProcesses.Fields.VisibilitySourceCondition",
                    "VisibilitySourceCondition.cs"),
                new StorageContentMapping(
                    "Sections.KrSecondaryProcesses.Fields.VisibilitySqlCondition",
                    "VisibilitySqlCondition.sql"),
                new StorageContentMapping(
                    $"Sections.KrStages.Rows[].RuntimeSourceAfter", // Таличные секции с wildcard'ами типа []
                    "RuntimeSourceAfter.cs",
                    CardHelper.DefaultRowIDKey), // К имени файла в табличных секциях нужно добавлять hash, учитывая RowID.
                new StorageContentMapping(
                    $"Sections.KrStages.Rows[].RuntimeSourceBefore",
                    "RuntimeSourceBefore.cs",
                    CardHelper.DefaultRowIDKey),
                new StorageContentMapping(
                    $"Sections.KrStages.Rows[].RuntimeSourceCondition",
                    "RuntimeSourceCondition.cs",
                    CardHelper.DefaultRowIDKey),
                new StorageContentMapping(
                    $"Sections.KrStages.Rows[].RuntimeSqlCondition",
                    "RuntimeSqlCondition.sql",
                    CardHelper.DefaultRowIDKey)
            };
            
            return result;
        }

        #endregion
    }
}