﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Extensions;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess
{
    /// <summary>
    /// Действия для экспорта карточки, дополняющие стандартное API.
    /// Данное расширение задает параметры сериализации для выгрузки контента карточек во внешние файлы.
    /// </summary>
    public class KrStageGroupMappingExportExtension : CardGetExtension
    {
        #region Base Overrides

        public override Task AfterRequest(ICardGetExtensionContext context)
        {
            Card card;
            if (!context.RequestIsSuccessful
                || (card = context.Response.TryGetCard()) is null
                || context.Request.ExportFormat != CardFileFormat.Json)
            {
                return Task.CompletedTask;
            }

            var pathMappings = GetPathMappings(card);

            context.Response.SetStorageFilePaths(pathMappings);

            return Task.CompletedTask;
        }

        #endregion

        #region Private Methods

        private static IList<IStorageContentMapping> GetPathMappings(Card card)
        {
            var result = new List<IStorageContentMapping>
            {
                new StorageContentMapping(
                    "Sections.KrStageGroups.Fields.RuntimeSourceAfter",
                    "RuntimeSourceAfter.cs"),
                new StorageContentMapping(
                    "Sections.KrStageGroups.Fields.RuntimeSourceBefore",
                    "RuntimeSourceBefore.cs"),
                new StorageContentMapping(
                    "Sections.KrStageGroups.Fields.RuntimeSourceCondition",
                    "RuntimeSourceCondition.cs"),
                new StorageContentMapping(
                    "Sections.KrStageGroups.Fields.RuntimeSqlCondition",
                    "RuntimeSqlCondition.sql"),
                new StorageContentMapping(
                    "Sections.KrStageGroups.Fields.SourceAfter",
                    "SourceAfter.cs"),
                new StorageContentMapping(
                    "Sections.KrStageGroups.Fields.SourceBefore",
                    "SourceBefore.cs"),
                new StorageContentMapping(
                    "Sections.KrStageGroups.Fields.SourceCondition",
                    "SourceCondition.cs"),
                new StorageContentMapping(
                    "Sections.KrStageGroups.Fields.SqlCondition",
                    "SqlCondition.sql")
            };

            return result;
        }

        #endregion
    }
}