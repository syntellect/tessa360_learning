﻿using System;
using System.Threading.Tasks;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers;
using Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform.Storage;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Serialization
{
    /// <summary>
    /// Расширения на сериализацию параметров типовых этапов. Выполняет получение и сохранение информации о дополнительных методах этапов.
    /// </summary>
    public sealed class ExtraSourcesStageRowExtension : ExtraSourcesStageRowExtensionBase
    {
        #region Constructors

        public ExtraSourcesStageRowExtension(
            IExtraSourceSerializer extraSourceSerializer)
            : base(extraSourceSerializer)
        {
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc />
        public override Task BeforeSerialization(
            IKrStageRowExtensionContext context)
        {
            var rows = context.InnerCard.GetStagesSection().Rows;

            foreach (var row in rows)
            {
                var stageTypeID = row.TryGet<Guid?>(KrConstants.KrStages.StageTypeID);

                if (stageTypeID == StageTypeDescriptors.ForkDescriptor.ID)
                {
                    var extraSources = this.GetExtraSources(row);

                    MoveSourceFromStageSettingsToExtraSources(
                        extraSources,
                        context,
                        row,
                        KrConstants.KrForkSettingsVirtual.AfterEachNestedProcess,
                        "$KrStages_Fork_AfterNested",
                        ForkStageTypeHandler.AfterNestedMethodName,
                        ForkStageTypeHandler.ScriptContextParameterType,
                        ForkStageTypeHandler.MethodParameterName);

                    this.SetExtraSources(row, extraSources);
                }
                else if (stageTypeID == StageTypeDescriptors.TypedTaskDescriptor.ID)
                {
                    var extraSources = this.GetExtraSources(row);

                    MoveSourceFromStageSettingsToExtraSources(
                        extraSources,
                        context,
                        row,
                        KrConstants.KrTypedTaskSettingsVirtual.AfterTaskCompletion,
                        "$KrStages_TypedTask_AfterTask",
                        TypedTaskStageTypeHandler.AfterTaskMethodName,
                        TypedTaskStageTypeHandler.ScriptContextParameterType,
                        TypedTaskStageTypeHandler.MethodParameterName);

                    this.SetExtraSources(row, extraSources);
                }
                else if (stageTypeID == StageTypeDescriptors.NotificationDescriptor.ID)
                {
                    var extraSources = this.GetExtraSources(row);

                    MoveSourceFromStageSettingsToExtraSources(
                        extraSources,
                        context,
                        row,
                        KrConstants.KrNotificationSettingVirtual.EmailModificationScript,
                        "$CardTypes_Controls_EmailModifyScenario",
                        NotificationStageTypeHandler.MethodName,
                        NotificationStageTypeHandler.ScriptContextParameterType,
                        NotificationStageTypeHandler.MethodParameterName);

                    this.SetExtraSources(row, extraSources);
                }
                else if (stageTypeID == StageTypeDescriptors.DialogDescriptor.ID)
                {
                    var extraSources = this.GetExtraSources(row);

                    MoveSourceFromStageSettingsToExtraSources(
                        extraSources,
                        context,
                        row,
                        KrConstants.KrDialogStageTypeSettingsVirtual.DialogActionScript,
                        "$UI_KrDialog_Script",
                        DialogStageTypeHandler.MethodName,
                        DialogStageTypeHandler.ScriptContextParameterType,
                        DialogStageTypeHandler.MethodParameterName);

                    MoveSourceFromStageSettingsToExtraSources(
                        extraSources,
                        context,
                        row,
                        KrConstants.KrDialogStageTypeSettingsVirtual.DialogCardSavingScript,
                        "$UI_KrDialog_SavingScript",
                        DialogStageTypeHandler.SavingMethodName,
                        DialogStageTypeHandler.SavingScriptContextParameterType,
                        DialogStageTypeHandler.SavingMethodParameterName);

                    this.SetExtraSources(row, extraSources);
                }
            }

            return Task.CompletedTask;
        }

        /// <inheritdoc />
        public override Task DeserializationBeforeRepair(
            IKrStageRowExtensionContext context)
        {
            var rows = context.CardToRepair.Sections[KrConstants.KrStages.Virtual].Rows;

            foreach (var row in rows)
            {
                var stageTypeID = row.TryGet<Guid?>(KrConstants.KrStages.StageTypeID);

                if (stageTypeID == StageTypeDescriptors.ForkDescriptor.ID)
                {
                    var extraSources = this.GetExtraSources(row);
                    MoveSourceFromExtraSourcesToStageSettings(
                        extraSources,
                        row,
                        KrConstants.KrForkSettingsVirtual.AfterEachNestedProcess);
                }
                else if (stageTypeID == StageTypeDescriptors.TypedTaskDescriptor.ID)
                {
                    var extraSources = this.GetExtraSources(row);
                    MoveSourceFromExtraSourcesToStageSettings(
                        extraSources,
                        row,
                        KrConstants.KrTypedTaskSettingsVirtual.AfterTaskCompletion);
                }
                else if (stageTypeID == StageTypeDescriptors.NotificationDescriptor.ID)
                {
                    var extraSources = this.GetExtraSources(row);
                    MoveSourceFromExtraSourcesToStageSettings(
                        extraSources,
                        row,
                        KrConstants.KrNotificationSettingVirtual.EmailModificationScript);
                }
                else if (stageTypeID == StageTypeDescriptors.DialogDescriptor.ID)
                {
                    var extraSources = this.GetExtraSources(row);
                    MoveSourceFromExtraSourcesToStageSettings(
                        extraSources,
                        row,
                        KrConstants.KrDialogStageTypeSettingsVirtual.DialogActionScript,
                        DialogStageTypeHandler.MethodName);
                    MoveSourceFromExtraSourcesToStageSettings(
                        extraSources,
                        row,
                        KrConstants.KrDialogStageTypeSettingsVirtual.DialogCardSavingScript,
                        DialogStageTypeHandler.SavingMethodName);
                }
            }

            return Task.CompletedTask;
        }

        #endregion
    }
}