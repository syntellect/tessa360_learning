﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers;
using Tessa.Extensions.Default.Server.Workflow.KrCompilers.SourceBuilders;
using Tessa.Notices;
using Tessa.Platform.Runtime;
using Tessa.Platform.Storage;
using Unity;
using static Tessa.Extensions.Default.Shared.Workflow.KrProcess.KrConstants;

namespace Tessa.Extensions.Default.Server.Workflow.KrProcess.Workflow.Handlers
{
    /// <summary>
    /// Обработчик этапа <see cref="Shared.Workflow.KrProcess.StageTypeDescriptors.NotificationDescriptor"/>.
    /// </summary>
    public class NotificationStageTypeHandler : StageTypeHandlerBase
    {
        #region Fields

        /// <summary>
        /// Тип параметра <see cref="MethodParameterName"/>.
        /// </summary>
        public static readonly string ScriptContextParameterType =
            $"global::{typeof(NotificationEmail).FullName}";

        /// <summary>
        /// Имя метода "Сценария изменения уведомления".
        /// </summary>
        public const string MethodName = "ModifyEmailAction";

        /// <summary>
        /// Имя параметра метода <see cref="MethodName"/>.
        /// </summary>
        public const string MethodParameterName = "email";

        #endregion

        #region Constructors

        /// <summary>
        /// Инициалиализирует новый экземпляр класса <see cref="NotificationStageTypeHandler"/>.
        /// </summary>
        /// <param name="notificationManager">Объект для отправки уведомлений, построенных по карточке уведомления.</param>
        /// <param name="session">Сессия пользователя.</param>
        /// <param name="compilationCache">Кэш содержащий результаты компиляции.</param>
        /// <param name="unityContainer">Unity-контейнер.</param>
        public NotificationStageTypeHandler(
            [Dependency(NotificationManagerNames.WithoutTransaction)] INotificationManager notificationManager,
            ISession session,
            IKrCompilationCache compilationCache,
            IUnityContainer unityContainer)
        {
            this.NotificationManager = notificationManager;
            this.Session = session;
            this.CompilationCache = compilationCache;
            this.UnityContainer = unityContainer;
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Возвращает объект для отправки уведомлений, построенных по карточке уведомления.
        /// </summary>
        protected INotificationManager NotificationManager { get; }

        /// <summary>
        /// Возвращает сессию пользователя.
        /// </summary>
        protected ISession Session { get; }

        /// <summary>
        /// Возвращает кэш содержащий результаты компиляции.
        /// </summary>
        protected IKrCompilationCache CompilationCache { get; }

        /// <summary>
        /// Возвращает unity-контейнер.
        /// </summary>
        protected IUnityContainer UnityContainer { get; }

        #endregion

        #region Base Overrides

        /// <inheritdoc/>
        public override async Task<StageHandlerResult> HandleStageStartAsync(IStageTypeHandlerContext context)
        {
            var roles = context.Stage.Performers.Select(x => x.PerformerID).ToList();
            if (roles.Count == 0)
            {
                // Некому отправлять уведомления, считаем, что этап завершен
                return StageHandlerResult.CompleteResult;
            }
            var mainCardID = context.Stage.InfoStorage.TryGet<Guid?>("MainCardID") ?? context.MainCardID ?? this.Session.User.ID;
            var notificationID = context.Stage.SettingsStorage.TryGet<Guid>(KrNotificationSettingVirtual.NotificationID);
            var excludeDeputies = context.Stage.SettingsStorage.TryGet<bool?>(KrNotificationSettingVirtual.ExcludeDeputies) ?? false;
            var excludeSubscribers = context.Stage.SettingsStorage.TryGet<bool?>(KrNotificationSettingVirtual.ExcludeSubscribers) ?? false;

            var inst = await KrProcessHelper.CreateScriptInstanceAsync(
                this.CompilationCache,
                context.Stage.ID,
                SourceIdentifiers.StageAlias,
                context.ValidationResult,
                context.CancellationToken);
            await HandlerHelper.InitScriptContextAsync(this.UnityContainer, inst, context);

            context.ValidationResult.Add(
                await this.NotificationManager.SendAsync(
                    notificationID,
                    roles,
                    new NotificationSendContext
                    {
                        MainCardID = mainCardID,
                        ExcludeDeputies = excludeDeputies,
                        DisableSubscribers = excludeSubscribers,
                        ModifyEmailActionAsync = (e, ct) => inst.InvokeExtraAsync(MethodName, e, throwOnError: false)
                    },
                    context.CancellationToken));

            return StageHandlerResult.CompleteResult;
        }

        #endregion
    }
}