﻿using System;
using System.Collections.Generic;
using Tessa.Cards;
using Tessa.Platform.Validation;

namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess
{
    /// <summary>
    /// Описывает результат запуска процесса.
    /// </summary>
    public interface IKrProcessLaunchResult
    {
        /// <summary>
        /// Возвращает статус процесса после запуска.
        /// </summary>
        KrProcessLaunchStatus LaunchStatus { get; }

        /// <summary>
        /// Возвращает идентификатор запущеного асинхронного процесса или значение <see langword="null"/>, если при запуске процесса произошла ошибка или запускался синхронный процесс.
        /// </summary>
        Guid? ProcessID { get; }

        /// <summary>
        /// Возвращает результат валидации запуска процесса.
        /// </summary>
        ValidationStorageResultBuilder ValidationResult { get; }

        /// <summary>
        /// Возвращает дополнительную информацию процесса после его завершения.
        /// Может быть <see langword="null"/>.
        /// </summary>
        IDictionary<string, object> ProcessInfo { get; }

        /// <summary>
        /// Возвращает ответ на запрос на сохранение, при котором был запущен процесс.
        /// Может быть <see langword="null"/>.
        /// </summary>
        CardStoreResponse StoreResponse { get; }

        /// <summary>
        /// Возвращает ответ на универсальный запрос, при котором был запущен процесс.
        /// Может быть <see langword="null"/>.
        /// </summary>
        CardResponse CardResponse { get; }
    }
}