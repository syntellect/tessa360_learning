﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using Tessa.Cards;
using Tessa.Platform;
using Tessa.Platform.Storage;
using Tessa.Properties.Resharper;

namespace Tessa.Extensions.Default.Shared.Workflow.KrProcess
{
    /// <summary>
    /// Предоставляет информацию о позиции этапа.
    /// </summary>
    [Serializable]
    [DebuggerDisplay("{" + nameof(KrStagePositionInfo.ToDebugString) + "(), nq}")]
    public sealed class KrStagePositionInfo: StorageObject
    {
        [NonSerialized]
        private CardRow cardRow;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="KrStagePositionInfo"/>.
        /// </summary>
        /// <param name="stageRow">Строка для которой формируется информация о позиции.</param>
        /// <param name="absoluteOrder">Абсолютный порядок этапа в маршруте.</param>
        /// <param name="shiftedOrder">Сдвинутый порядок с учетом скрытых этапов.</param>
        /// <param name="saveRow">Значение <see langword="true"/>, если необходимо сохранить информацию о строке этапа для которой формируется информация о позиции, иначе - <see langword="false"/>.</param>
        /// <param name="hidden">Значение <see langword="true"/>, если этап является скрытым, иначе - <see langword="false"/>.</param>
        public KrStagePositionInfo(
            CardRow stageRow,
            int absoluteOrder,
            int? shiftedOrder,
            bool saveRow,
            bool hidden)
            : base(new Dictionary<string, object>())
        {
            this.Set(nameof(this.RowID), stageRow.RowID);
            this.Set(nameof(this.GroupOrder), stageRow[KrConstants.StageGroupOrder]);
            this.Set(nameof(this.AbsoluteOrder), absoluteOrder);
            this.Set(nameof(this.ShiftedOrder), shiftedOrder);
            this.Set(nameof(this.Hidden), BooleanBoxes.Box(hidden));
            this.Set(nameof(this.Name), stageRow[KrConstants.Name]);
            this.Set(nameof(this.StageGroupID), stageRow[KrConstants.StageGroupID]);
            this.Set(nameof(this.GroupPosition), stageRow[KrConstants.KrStages.BasedOnStageTemplateGroupPositionID]);
            this.Set(nameof(this.CardRow), saveRow ? stageRow.GetStorage() : null);
            this.Set(nameof(this.Skip), stageRow.Get<object>(KrConstants.KrStages.Skip));
        }

        /// <inheritdoc />
        public KrStagePositionInfo(
            Dictionary<string, object> storage)
            : base(storage)
        {
        }

        /// <inheritdoc />
        public KrStagePositionInfo(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Идентификатор скрытого этапа (оригинала из шаблона).
        /// </summary>
        public Guid RowID => this.Get<Guid>(nameof(this.RowID));

        /// <summary>
        /// Порядок сортировки группы этапов.
        /// </summary>
        public int GroupOrder => this.Get<int>(nameof(this.GroupOrder));

        /// <summary>
        /// Абсолютный порядок этапа в маршруте.
        /// </summary>
        public int AbsoluteOrder => this.Get<int>(nameof(this.AbsoluteOrder));

        /// <summary>
        /// Сдвинутый порядок с учетом скрытых этапов.
        /// </summary>
        public int? ShiftedOrder => this.Get<int?>(nameof(this.ShiftedOrder));

        /// <summary>
        /// Признак того, что этап является скрытым.
        /// </summary>
        public bool Hidden => this.Get<bool>(nameof(this.Hidden));

        /// <summary>
        /// Название этапа.
        /// </summary>
        public string Name => this.Get<string>(nameof(this.Name));

        /// <summary>
        /// Групповая позиция этапа в рамках одной группы. <see cref="GroupPosition"/>
        /// </summary>
        public int? GroupPosition => this.Get<int?>(nameof(this.GroupPosition));

        /// <summary>
        /// Идентификатор группы этапа.
        /// </summary>
        public Guid StageGroupID => this.Get<Guid>(nameof(this.StageGroupID));

        /// <summary>
        /// Строка этапа.
        /// </summary>
        public CardRow CardRow => 
            this.cardRow ??= this.CreateCardRow();

        /// <summary>
        /// Признак пропуска этапа.
        /// </summary>
        public bool Skip => this.Get<bool>(nameof(this.Skip));

        private CardRow CreateCardRow()
        {
            var storage = this.Get<Dictionary<string, object>>(nameof(this.CardRow));
            return storage != null
                ? new CardRow(storage)
                : null;
        }

        public static KrStagePositionInfo CreateVisible(
            CardRow stageRow,
            int absoluteOrder,
            int? shiftedOrder)
        {
            return new KrStagePositionInfo(
                stageRow,
                absoluteOrder,
                shiftedOrder,
                false,
                false);
        }

        public static KrStagePositionInfo CreateHidden(
            CardRow stageRow,
            int absoluteOrder,
            bool saveRow)
        {
            return new KrStagePositionInfo(
                stageRow,
                absoluteOrder,
                null,
                saveRow,
                true);
        }

        /// <summary>
        /// Возвращает информацию содержащую отладочную информацию.
        /// </summary>
        /// <returns>
        /// Строковое представление объекта отображаемое в режиме отладки.
        /// </returns>
        [UsedImplicitly]
        private string ToDebugString()
        {
            return $"{DebugHelper.GetTypeName(this)}: "
                + nameof(this.RowID) + $" = {this.RowID.ToString()}, "
                + nameof(this.Name) + $" = {this.Name}, "
                + nameof(this.AbsoluteOrder) + $" = {this.AbsoluteOrder.ToString()}, "
                + nameof(this.GroupOrder) + $" = {this.GroupOrder.ToString()}, "
                + nameof(this.GroupPosition) + $" = {DebugHelper.FormatNullable(this.GroupPosition)}, "
                + nameof(this.Skip) + $" = {this.Skip.ToString()}, "
                + nameof(this.Hidden) + $" = {this.Hidden.ToString()}";
        }
    }
}