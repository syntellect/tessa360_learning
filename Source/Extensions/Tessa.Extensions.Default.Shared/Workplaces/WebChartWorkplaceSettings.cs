﻿using System;
using System.Collections.Generic;
using Tessa.Platform.Storage;
using Tessa.Views.Json;

namespace Tessa.Extensions.Default.Shared.Workplaces
{
    /// <summary>
    ///     Настройки для рабочего места руководителя
    /// </summary>
    [Serializable]
    public class WebChartWorkplaceSettings :
        IStorageSerializable
    {
        #region Properties

        /// <summary>
        ///     Gets or sets Имя столбца содержащего изображение для выбранной плитки
        /// </summary>
        public WebChartDiagramType DiagramType { get; set; }

        public WebChartDiagramDirection DiagramDirection { get; set; }

        public WebChartLegendPosition LegendPosition { get; set; }

        public string CaptionColumn { get; set; }

        public string Caption { get; set; }

        /// <summary>
        ///     Gets or sets Имя столбца содержащего количество
        /// </summary>
        public string XColumn { get; set; }

        /// <summary>
        ///     Gets or sets Имя столбца содержащего изображение для плитки над которой находится курсор
        /// </summary>
        public string YColumn { get; set; }

        public int? LegendItemMinWidth { get; set; }

        public bool LegendNotWrap { get; set; }

        public bool DoesntShowZeroValues { get; set; }

        public string SelectedColor { get; set; }

        public string PaletteTypeId { get; set; }

        /// <summary>
        /// Кол-во столбцов диаграмм в строке
        /// </summary>
        public int? ColumnCount { get; set; }

        #endregion

        #region IStorageSerializable Members

        public void Serialize(Dictionary<string, object> storage)
        {
            storage[nameof(this.DiagramType)] = this.DiagramType.ToString();
            storage[nameof(this.DiagramDirection)] = this.DiagramDirection.ToString();
            storage[nameof(this.LegendPosition)] = this.LegendPosition.ToString();
            storage[nameof(this.XColumn)] = this.XColumn;
            storage[nameof(this.YColumn)] = this.YColumn;
            storage[nameof(this.LegendItemMinWidth)] = this.LegendItemMinWidth;
            storage[nameof(this.ColumnCount)] = this.ColumnCount;
            storage[nameof(this.LegendNotWrap)] = this.LegendNotWrap;
            storage[nameof(this.DoesntShowZeroValues)] = this.DoesntShowZeroValues;
            storage[nameof(this.SelectedColor)] = this.SelectedColor;
            storage[nameof(this.CaptionColumn)] = this.CaptionColumn;
            storage[nameof(this.Caption)] = this.Caption;
            storage[nameof(this.PaletteTypeId)] = this.PaletteTypeId;
        }

        public void Deserialize(Dictionary<string, object> storage)
        {
            this.DiagramType = storage.GetSerializedEnum(nameof(this.DiagramType), WebChartDiagramType.Bar);
            this.DiagramDirection = storage.GetSerializedEnum(nameof(this.DiagramDirection), WebChartDiagramDirection.Horizontal);
            this.LegendPosition = storage.GetSerializedEnum(nameof(this.LegendPosition), WebChartLegendPosition.Bottom);
            this.XColumn = storage.TryGet<string>(nameof(this.XColumn));
            this.YColumn = storage.TryGet<string>(nameof(this.YColumn));
            this.LegendItemMinWidth = storage.TryGet<int?>(nameof(this.LegendItemMinWidth));
            this.ColumnCount = storage.TryGet<int?>(nameof(this.ColumnCount));
            this.LegendNotWrap = storage.TryGet<bool>(nameof(this.LegendNotWrap));
            this.DoesntShowZeroValues = storage.TryGet<bool>(nameof(this.DoesntShowZeroValues));
            this.SelectedColor = storage.TryGet<string>(nameof(this.SelectedColor));
            this.CaptionColumn = storage.TryGet<string>(nameof(this.CaptionColumn));
            this.Caption = storage.TryGet<string>(nameof(this.Caption));
            this.PaletteTypeId = storage.TryGet<string>(nameof(this.PaletteTypeId));
        }

        #endregion
    }
}