﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using NUnit.Framework;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Extensions;
using Tessa.Platform;
using Tessa.Platform.ConsoleApps;
using Tessa.Platform.Data;
using Tessa.Platform.IO;
using Tessa.Platform.Runtime;
using Tessa.Platform.Storage;
using Tessa.Roles;
using Tessa.Test.Default.Shared;
using Tessa.Test.Default.Shared.Kr;
using Tessa.Test.Default.Shared.Roles;
using Tessa.Views;
using Tessa.Views.Json;
using Tessa.Views.Json.Converters;
using Tessa.Views.Parser;
using Tessa.Views.Parser.SyntaxTree.ExchangeFormat;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace Tessa.Test.Default.Client
{
    /// <summary>
    /// Базовый абстрактный класс для клиентских тестов без поддержки пользовательского интерфейса.
    /// </summary>
    public abstract class ClientTestBase :
        TestBase
    {
        #region Fields

        private IRoleRepository roleRepository;

        #endregion

        #region Protected Properties

        /// <summary>
        /// Тестируемый объект <see cref="ICardStreamClientRepository"/> с расширениями.
        /// </summary>
        protected ICardStreamClientRepository CardStreamRepository { get; private set; }

        /// <summary>
        /// Тестируемый объект <see cref="ICardTypeClientRepository"/>.
        /// </summary>
        protected ICardTypeClientRepository CardTypeRepository { get; private set; }

        /// <summary>
        /// Возвращает или задаёт значение, показывающее, должна ли выполняться расширенная инициализация при открытии сессии
        /// (с выполняемым стримом инициализации).
        /// </summary>
        protected virtual bool ExtendedInitialization { get; }

        /// <summary>
        /// Вовзвращает базовый адрес для подключения к серверу тессы.
        /// Если свойство возвращает <see langword="null"/>, то используется значение из конфигурационного файла.
        /// </summary>
        protected virtual string BaseAddressOverride => null;

        /// <summary>
        /// Вовзвращает имя пользователя для подключения к серверу тессы.
        /// Если свойство возвращает <see langword="null"/>, то используется значение из конфигурационного файла.
        /// </summary>
        protected virtual string UserNameOverride => null;

        /// <summary>
        /// Вовзвращает пароль пользователя для подключения к серверу тессы.
        /// Если свойство возвращает <see langword="null"/>, то используется значение из конфигурационного файла.
        /// </summary>
        protected virtual string PasswordOverride => null;

        #endregion

        #region AssertThatAreEqual Protected Method

        /// <summary>
        /// Выполняет сравнение объекта карточки <see cref="Card"/> с объектами ролевой модели.
        /// </summary>
        /// <param name="card">Объект карточки.</param>
        /// <param name="sessionUserID">Идентификатор пользователя, выполнявшего сохранение карточки.</param>
        /// <param name="sessionUserName">Имя пользователя, выполнявшего сохранение карточки.</param>
        /// <param name="role">Объект, описывающий персональную роль.</param>
        /// <param name="deputies">Объекты, описывающие заместителей.</param>
        /// <param name="users">Объекты, описывающие состав роли.</param>
        protected static void AssertThatAreEqual(
            Card card,
            Guid sessionUserID,
            string sessionUserName,
            PersonalRole role,
            ICollection<RoleDeputyRecord> deputies,
            ICollection<RoleUserRecord> users)
        {
            PersonalRoleTestHelper.AssertPersonalRole(card, sessionUserID, sessionUserName, role);

            Assert.That(deputies.Count, Is.EqualTo(card.Sections["RoleDeputies"].Rows.Count));
            foreach (var deputy in deputies)
            {
                var deputyRow = card.Sections["RoleDeputies"].Rows.Single(x => x.RowID == deputy.RowID);
                AssertThatAreEqual(deputyRow, deputy);
            }

            Assert.That(users.Count, Is.EqualTo(card.Sections["RoleUsers"].Rows.Count));
            foreach (var user in users)
            {
                var userRow = card.Sections["RoleUsers"].Rows.Single(x => x.RowID == user.RowID);
                AssertThatAreEqual(userRow, user);
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Возвращает объект <see cref="PersonalRole"/>, полученный для текущей сессии.
        /// </summary>
        /// <returns>Объект <see cref="PersonalRole"/>, полученный для текущей сессии.</returns>
        protected PersonalRole CreateSessionPersonalRole() =>
            PersonalRoleTestHelper.Create(this.Session.User.ID, this.Session.User.Name);

        /// <summary>
        /// Копировать файл из ресурсов во временную папку.
        /// </summary>
        /// <param name="resourceDirName">Имя папки с ресурсами.</param>
        /// <param name="fileName">Имя файла.</param>
        /// <param name="tempDir">Временная папка.</param>
        /// <returns></returns>
        protected async Task CopyFileFromResourcesToTempDirAsync(string resourceDirName, string fileName, ITempFolder tempDir)
        {
            var content = AssemblyHelper.GetResourceTextFile(this.GetType().Assembly, Path.Combine(resourceDirName, fileName));
            var tempFile = tempDir.AcquireFile(fileName);
            await File.WriteAllTextAsync(tempFile.Path, content);
        }

        #endregion

        #region Base override

        /// <inheritdoc/>
        protected override IRoleRepository RoleRepository => this.roleRepository ??= new RoleRepository(this.DbScope ?? Platform.Data.DbScope.CreateDefault());

        /// <inheritdoc/>
        protected override ValueTask<IUnityContainer> CreateContainerAsync()
        {
            var container = this.CreateClientContainer(
                this.DbFactory is null ? null : this.DbFactory.Create,
                this.DbScope,
                this.GetBaseAddress(),
                this.BeforeRegisterExtensionsOnClient,
                this.BeforeFinalizeClientRegistration)
                .RegisterType<ICardLifecycleCompanionRequestExtender, CardLifecycleCompanionClientRequestExtender>(new ContainerControlledLifetimeManager())
                .RegisterType<ICardLifecycleCompanionDependencies, CardLifecycleCompanionDependencies>(
                    new TransientLifetimeManager(),
                    new InjectionConstructor(
                        typeof(ICardRepository),
                        typeof(ICardMetadata),
                        typeof(Func<ICardFileManager>),
                        typeof(Func<ICardStreamClientRepository>),
                        typeof(ICardCache),
                        typeof(IDbScope),
                        typeof(ICardLifecycleCompanionRequestExtender)))
                .RegisterType<TestConfigurationBuilder>(
                    new ContainerControlledLifetimeManager(),
                    new InjectionConstructor(
                        new InjectionParameter<Assembly>(this.ResourceAssembly),
                        typeof(IDbScope),
                        typeof(ICardManager),
                        typeof(ICardRepository),
                        typeof(ICardFileSourceSettings),
                        typeof(ICardLifecycleCompanionDependencies),
                        typeof(ICardTypeClientRepository),
                        typeof(ICardCachedMetadata),
                        typeof(IExchangeFormatInterpreter),
                        typeof(IIndentationStrategy),
                        typeof(ITessaViewService),
                        typeof(IJsonViewModelImporter),
                        typeof(IJsonViewModelAdapter)))
                .RegisterType<IMessageProvider, TestMessageProvider>(new ContainerControlledLifetimeManager());

            return new ValueTask<IUnityContainer>(container);
        }

        /// <inheritdoc/>
        protected override async Task InitializeCoreAsync()
        {
            await base.InitializeCoreAsync();

            this.DeleteCacheFolder();

            await this.UnityContainer
                .Resolve<ISessionClient>()
                .OpenTestSessionAsync(this.UserNameOverride, this.PasswordOverride);

            if (this.ExtendedInitialization)
            {
                if (!await this.UnityContainer.Resolve<IApplicationInitializer>().InitializeAsync())
                {
                    return;
                }
            }

            this.CardStreamRepository = this.UnityContainer.Resolve<ICardStreamClientRepository>();
            this.CardTypeRepository = this.UnityContainer.Resolve<ICardTypeClientRepository>();
        }

        /// <inheritdoc/>
        protected override async Task OneTimeTearDownCoreAsync()
        {
            await this.UnityContainer
                .Resolve<ISessionClient>()
                .CloseSessionSafeAsync();

            this.DeleteCacheFolder();

            await base.OneTimeTearDownCoreAsync();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Выполняет действия перед поиском и выполнением клиентских регистраторов расширений в папке приложения.
        /// </summary>
        /// <param name="unityContainer">Unity-контейнер.</param>
        protected virtual void BeforeRegisterExtensionsOnClient(IUnityContainer unityContainer)
        {
        }

        /// <summary>
        /// Выполняет действия перед завершением регистрации клиента приложений.
        /// </summary>
        /// <param name="unityContainer">Unity-контейнер.</param>
        protected virtual void BeforeFinalizeClientRegistration(IUnityContainer unityContainer)
        {
        }

        /// <summary>
        /// Создаёт и конфигурирует контейнер Unity для выполнения тестов на стороне клиента с расширениями.
        /// Дополнительно инициализирует зависимости платформы.
        /// </summary>
        /// <param name="createDbManagerFunc">Функция, создающая и возвращающая <see cref="DbManager"/>. Если задано значение по умолчанию для типа, то перерегистрация не выполняется.</param>
        /// <param name="dbScope">Экземпляр объекта осуществляющий взаимодействие с базой данных. Если задано значение по умолчанию для типа, то используется <see cref="DbScope.CreateDefault"/>.</param>
        /// <param name="baseAddress">
        /// Базовый адрес сервера, который переопределяет соответствующую настройку, заданную в конфигурационном файле,
        /// или <c>null</c>, если используется адрес из конфигурационного файла.
        /// </param>
        /// <param name="beforeRegisterExtensionsOnClientAction">Метод выполняющий действия перед поиском и выполнением клиентских регистраторов расширений в папке приложения.</param>
        /// <param name="beforeFinalizeClientRegistrationAction">Метод выполняющий действия перед завершением регистрации клиента приложений.</param>
        /// <returns>Созданный контейнер.</returns>
        protected virtual IUnityContainer CreateClientContainer(
            Func<DbManager> createDbManagerFunc = default,
            IDbScope dbScope = default,
            string baseAddress = null,
            Action<IUnityContainer> beforeRegisterExtensionsOnClientAction = default,
            Action<IUnityContainer> beforeFinalizeClientRegistrationAction = default)
        {
            var unityContainer = this.CreateClientContainerBase(
                createDbManagerFunc,
                dbScope,
                baseAddress,
                beforeRegisterExtensionsOnClientAction,
                beforeFinalizeClientRegistrationAction);

            return unityContainer;
        }

        /// <summary>
        /// Создаёт и конфигурирует контейнер Unity для выполнения тестов на стороне клиента с расширениями.
        /// </summary>
        /// <param name="createDbManagerFunc">Функция, создающая и возвращающая <see cref="DbManager"/>. Если задано значение по умолчанию для типа, то перерегистрация не выполняется.</param>
        /// <param name="dbScope">Экземпляр объекта осуществляющий взаимодействие с базой данных. Если задано значение по умолчанию для типа, то используется <see cref="DbScope.CreateDefault"/>.</param>
        /// <param name="baseAddress">
        /// Базовый адрес сервера, который переопределяет соответствующую настройку, заданную в конфигурационном файле,
        /// или <c>null</c>, если используется адрес из конфигурационного файла.
        /// </param>
        /// <param name="beforeRegisterExtensionsOnClientAction">Метод выполняющий действия перед поиском и выполнением клиентских регистраторов расширений в папке приложения.</param>
        /// <param name="beforeFinalizeClientRegistrationAction">Метод выполняющий действия перед завершением регистрации клиента приложений.</param>
        /// <returns>Созданный контейнер.</returns>
        protected virtual IUnityContainer CreateClientContainerBase(
            Func<DbManager> createDbManagerFunc = default,
            IDbScope dbScope = default,
            string baseAddress = null,
            Action<IUnityContainer> beforeRegisterExtensionsOnClientAction = default,
            Action<IUnityContainer> beforeFinalizeClientRegistrationAction = default)
        {
            var unityContainer = new UnityContainer()
                .RegisterClientForConsole(baseAddress: baseAddress)
                ;
            return this.RegisterClientContainerBase(
                unityContainer,
                createDbManagerFunc,
                dbScope,
                baseAddress,
                beforeRegisterExtensionsOnClientAction,
                beforeFinalizeClientRegistrationAction);
        }

        /// <summary>
        /// Конфигурирует контейнер Unity для выполнения тестов на стороне клиента с расширениями.
        /// </summary>
        /// <param name="unityContainer">Unity-контейнер.</param>
        /// <param name="createDbManagerFunc">Функция, создающая и возвращающая <see cref="DbManager"/>. Если задано значение по умолчанию для типа, то перерегистрация не выполняется.</param>
        /// <param name="dbScope">Экземпляр объекта осуществляющий взаимодействие с базой данных. Если задано значение по умолчанию для типа, то используется <see cref="DbScope.CreateDefault"/>.</param>
        /// <param name="baseAddress">
        /// Базовый адрес сервера, который переопределяет соответствующую настройку, заданную в конфигурационном файле,
        /// или <c>null</c>, если используется адрес из конфигурационного файла.
        /// </param>
        /// <param name="beforeRegisterExtensionsOnClientAction">Метод выполняющий действия перед поиском и выполнением клиентских регистраторов расширений в папке приложения.</param>
        /// <param name="beforeFinalizeClientRegistrationAction">Метод выполняющий действия перед завершением регистрации клиента приложений.</param>
        /// <returns>Созданный контейнер.</returns>
        protected virtual IUnityContainer RegisterClientContainerBase(
            IUnityContainer unityContainer,
            Func<DbManager> createDbManagerFunc = default,
            IDbScope dbScope = default,
            string baseAddress = null,
            Action<IUnityContainer> beforeRegisterExtensionsOnClientAction = default,
            Action<IUnityContainer> beforeFinalizeClientRegistrationAction = default)
        {
            if (createDbManagerFunc is not null)
            {
                unityContainer
                    .RegisterDbManager(createDbManagerFunc);
            }

            unityContainer
                .RegisterInstance(dbScope ?? Tessa.Platform.Data.DbScope.CreateDefault(), new ContainerControlledLifetimeManager())
                .SetCachingSourceForFileSettings();

            beforeRegisterExtensionsOnClientAction?.Invoke(unityContainer);

            unityContainer
                .FindAndRegisterExtensionsOnClient(out var actualFoldersList);

            beforeFinalizeClientRegistrationAction?.Invoke(unityContainer);

            unityContainer
                .FinalizeClientRegistration(actualFoldersList);

            return unityContainer;
        }

        /// <summary>
        /// Возвращает базовый адрес подключения к серверу TESSA.
        /// </summary>
        /// <returns>Адрес сервера.</returns>
        protected virtual string GetBaseAddress() =>
            this.BaseAddressOverride ?? ConfigurationManager.Settings.TryGet<string>("BaseAddress");

        #endregion

        #region Private Methods

        /// <summary>
        /// Удаляет папку содержащую локальный кэш, используемый в тестах.
        /// </summary>
        private void DeleteCacheFolder()
        {
            var connectionSettings = this.UnityContainer.Resolve<IConnectionSettings>();

            var folderPath = Path.Combine(
                ApplicationFolders.LocalData,
                ApplicationFolders.Cache,
                ApplicationFolders.GetBaseAddressFolderName(connectionSettings.BaseAddress));

            // Не используется метод FileHelper.ReleaseFolderPath из-за поглощения исключений, что может привести к маскировке исходной проблемы связанной с использованием локального кэша в тестах.
            if (Directory.Exists(folderPath))
            {
                Directory.Delete(folderPath, true);
            }
        }

        private static void AssertThatAreEqual(CardRow deputyRow, RoleDeputyRecord deputy)
        {
            Assert.That((int) deputy.RoleType, Is.EqualTo(deputyRow["TypeID"]));
            Assert.That(deputy.DeputyID, Is.EqualTo(deputyRow["DeputyID"]));
            Assert.That(deputy.DeputyName, Is.EqualTo(deputyRow["DeputyName"]));
            Assert.That(deputy.MinDate.Date, Is.EqualTo(deputyRow.Get<DateTime>("MinDate").Date));
            Assert.That(deputy.MaxDate.Date, Is.EqualTo(deputyRow.Get<DateTime>("MaxDate").Date));
            Assert.That(deputy.IsActive, Is.EqualTo(deputyRow["IsActive"]));
            Assert.That(deputy.IsEnabled, Is.EqualTo(deputyRow["IsEnabled"]));
        }

        private static void AssertThatAreEqual(CardRow userRow, RoleUserRecord user)
        {
            Assert.That((int) user.RoleType, Is.EqualTo(userRow["TypeID"]));
            Assert.That(user.UserID, Is.EqualTo(userRow["UserID"]));
            Assert.That(user.UserName, Is.EqualTo(userRow["UserName"]));
            Assert.That(user.IsDeputy, Is.EqualTo(userRow["IsDeputy"]));
        }

        #endregion
    }
}
