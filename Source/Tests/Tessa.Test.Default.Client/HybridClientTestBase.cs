﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Tessa.Platform;
using Tessa.Platform.Runtime;
using Tessa.Server;
using Tessa.Test.Default.Client.Web;
using Tessa.Test.Default.Shared;
using Tessa.Test.Default.Shared.Web;
using Tessa.Web;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace Tessa.Test.Default.Client
{
    /// <summary>
    /// Абстрактный базовый класс, предоставляющий методы для выполнения клиентских тестов 
    /// без поддержки пользовательского интерфейса на специально подготовленном сервере приложений.
    /// </summary>
    public abstract class HybridClientTestBase :
        ClientTestBase
    {
        #region Constants

        /// <summary>
        /// Базовый адрес сервера приложений по умолчанию.
        /// </summary>
        public const string DefaultBaseAddress = "http://localhost/";

        #endregion

        #region Properties

        /// <summary>
        /// Возвращает фабрику, предназначенную для создания объектов, с помощью которых можно реализовать функциональные тесты для web-приложений.
        /// </summary>
        public IWebApplicationFactory WebApplicationFactory { get; private set; }

        #endregion

        #region Base Overrides

        /// <inheritdoc/>
        protected override string BaseAddressOverride => DefaultBaseAddress;

        /// <inheritdoc/>
        protected override string UserNameOverride => "admin";

        /// <inheritdoc/>
        protected override string PasswordOverride => "admin";

        /// <inheritdoc/>
        protected override async ValueTask<IUnityContainer> CreateContainerAsync()
        {
            await WebHelper.InitializeWebServerAsync();

            var factory = new TessaWebApplicationFactory(this.CreateContainerServer);
            factory.InitializeAndStart();
            this.WebApplicationFactory = factory;

            return await base.CreateContainerAsync();
        }

        /// <inheritdoc/>
        protected override void BeforeRegisterExtensionsOnClient(IUnityContainer unityContainer)
        {
            base.BeforeRegisterExtensionsOnClient(unityContainer);

            unityContainer
                .RegisterType<IHttpClientFactory, TestServerHttpClientFactory>(
                    new ContainerControlledLifetimeManager(),
                    new InjectionConstructor(
                        new InjectionParameter<IWebApplicationFactory>(this.WebApplicationFactory)));
        }

        /// <inheritdoc/>
        protected override async Task InitializeCoreAsync()
        {
            await base.InitializeCoreAsync();
            TestHelper.RemoveFileStoragePath(this.GetType());
        }

        /// <inheritdoc/>
        protected override async Task OneTimeTearDownCoreAsync()
        {
            TestHelper.RemoveFileStoragePath(this.GetType());
            await base.OneTimeTearDownCoreAsync();
        }

        /// <inheritdoc/>
        protected override string GetBaseAddress()
        {
            var address = base.GetBaseAddress();

            if (this.GetType().GetCustomAttribute<SetupTempDbAttribute>() is null)
            {
                return address;
            }

            if (string.IsNullOrEmpty(address))
            {
                address = DefaultBaseAddress;
            }

            var builder = new UriBuilder(address);
            builder.Host += "_" + TestHelper.GetFixtureTypeCode();
            return builder.ToString();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Создаёт Unity-контейнер используемый на сервере.
        /// </summary>
        /// <param name="instanceName">Имя экземпляра сервера. Может быть равно пустой строке или значению <see langword="null"/>, если используется имя по умолчанию.</param>
        /// <param name="multipleInstances">Признак того, что активен режим работы с несколькими экземплярами сервера. При этом в запросах к серверу обязательно передаётся <c>InstanceName</c>, в т.ч. для ссылок на web-клиент.</param>
        /// <param name="serverRootPath">Путь к папке с конфигурационным файлами, или <see langword="null"/>, если путь определяется по умолчанию как значение свойства <see cref="RuntimeHelper.ConfigRootPath"/>.</param>
        /// <returns>Созданный Unity-контейнер.</returns>
        protected virtual IUnityContainer CreateContainerServer(
            string instanceName,
            bool multipleInstances,
            string serverRootPath)
        {
            if (!(TessaPlatform.ServerDependencies is TessaServerDependencies))
            {
                TessaPlatform.ServerDependencies = new TessaServerDependencies();
            }

            return TestHelper.CreateServerContainerBase(
                createDbManagerFunc: this.DbFactory is null ? null : this.DbFactory.Create,
                dbScope: this.DbScope,
                tryGetTokenFunc: () => WebHelper.TryGetSessionToken(WebHelper.HttpContext),
                beforeRegisterExtensionsOnServerAction: this.BeforeRegisterExtensionsOnServer,
                beforeFinalizeServerRegistrationAction: this.BeforeFinalizeServerRegistration);
        }

        /// <summary>
        /// Выполняет действия перед поиском и выполнением серверных регистраторов расширений в папке приложения.
        /// </summary>
        /// <param name="unityContainer">Unity-контейнер.</param>
        protected virtual void BeforeRegisterExtensionsOnServer(IUnityContainer unityContainer)
        {

        }

        /// <summary>
        /// Выполняет действия перед завершением регистрации сервера приложений.
        /// </summary>
        /// <param name="unityContainer">Unity-контейнер.</param>
        protected virtual void BeforeFinalizeServerRegistration(IUnityContainer unityContainer)
        {

        }

        #endregion
    }
}
