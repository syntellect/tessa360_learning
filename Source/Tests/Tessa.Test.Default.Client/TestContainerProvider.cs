﻿using System;
using Tessa.Platform;
using Tessa.Web.Services;
using Unity;

namespace Tessa.Test.Default.Client
{
    /// <summary>
    /// Предоставляет методы для создания Unity-контейнера используемого на серевере в тестах с настраиваемым сервером приложений.
    /// </summary>
    public sealed class TestContainerProvider :
        ContainerProvider
    {
        #region Fields

        private readonly Func<string, bool, string, IUnityContainer> createContainerFunc;

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TestContainerProvider"/>.
        /// </summary>
        /// <param name="configurationManager">Объект, управляющий конфигурацией приложений Tessa.</param>
        /// <param name="serviceProvider">Предоставляет методы для получения зависимостей.</param>
        /// <param name="createContainerFunc">Метод создающий серверный контейнер. Параметры соответвуют методу <see cref="IContainerProvider.GetContainer(string, bool, string)"/>.</param>
        public TestContainerProvider(
            IConfigurationManager configurationManager,
            IServiceProvider serviceProvider,
            Func<string, bool, string, IUnityContainer> createContainerFunc)
            : base(configurationManager, serviceProvider)
        {
            Check.ArgumentNotNull(createContainerFunc, nameof(createContainerFunc));

            this.createContainerFunc = createContainerFunc;
        }

        #endregion

        #region Base Overrides

        /// <inheritdoc/>
        protected override IUnityContainer CreateContainer(string instanceName, bool multipleInstances, string serverRootPath) => this.createContainerFunc(instanceName, multipleInstances, serverRootPath);

        #endregion
    }
}
