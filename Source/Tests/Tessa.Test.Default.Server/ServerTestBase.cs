﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Cards.Metadata;
using Tessa.Platform.Data;
using Tessa.Platform.Runtime;
using Tessa.Roles;
using Tessa.Test.Default.Shared;
using Tessa.Test.Default.Shared.Cards;
using Tessa.Test.Default.Shared.Kr;
using Tessa.Views;
using Tessa.Views.Json;
using Tessa.Views.Json.Converters;
using Tessa.Views.Parser;
using Tessa.Views.Parser.SyntaxTree.ExchangeFormat;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace Tessa.Test.Default.Server
{
    /// <summary>
    /// Базовый абстрактный класс для серверных тестов.
    /// </summary>
    public abstract class ServerTestBase :
        TestBase,
        ICardTypeRepositoryContainer
    {
        #region Fields

        private readonly bool useDatabaseAsDefault;

        private IRoleRepository roleRepository;

        #endregion

        #region Properties

        /// <inheritdoc/>
        protected override IRoleRepository RoleRepository => this.roleRepository;

        #region ICardTypeRepositoryContainer Members

        /// <inheritdoc/>
        public ICardTypeServerRepository CardTypeRepository { get; set; }

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ServerTestBase"/>.
        /// </summary>
        /// <param name="useDatabaseAsDefault">Значение <see langword="true"/>, если в качестве источника файлов по умолчанию используется база данных, иначе - <see langword="false"/>.</param>
        protected ServerTestBase(bool useDatabaseAsDefault = default)
        {
            this.useDatabaseAsDefault = useDatabaseAsDefault;
        }

        #endregion

        #region Base override

        /// <inheritdoc/>
        protected override ValueTask<IUnityContainer> CreateContainerAsync()
        {
            var fileSourceSettings = TestHelper.CreateDefaultFileSourceSettings(randomizeFileBasePath: true, useDatabaseAsDefault: this.useDatabaseAsDefault);
            var token = new SessionToken(
                 Platform.Runtime.Session.SystemID,
                 Platform.Runtime.Session.SystemName,
                 seal: true);
            var container = TestServerHelper.CreateServerContainer(
                this.DbFactory is null ? null : this.DbFactory.Create,
                this.DbScope,
                () => token,
                fileSourceSettings,
                this.BeforeRegisterExtensionsOnServer,
                this.BeforeFinalizeServerRegistration);

            container.RegisterType<ICardLifecycleCompanionRequestExtender, CardLifecycleCompanionClientRequestExtender>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICardLifecycleCompanionDependencies, CardLifecycleCompanionDependencies>(
                new TransientLifetimeManager(),
                new InjectionConstructor(
                    typeof(ICardRepository),
                    typeof(ICardMetadata),
                    typeof(Func<ICardFileManager>),
                    typeof(Func<ICardStreamServerRepository>),
                    typeof(ICardCache),
                    typeof(IDbScope),
                    typeof(ICardLifecycleCompanionRequestExtender)));

            container.RegisterType<TestConfigurationBuilder>(
                new ContainerControlledLifetimeManager(),
                new InjectionConstructor(
                    new InjectionParameter<Assembly>(this.ResourceAssembly),
                    typeof(IDbScope),
                    typeof(ICardManager),
                    typeof(ICardRepository),
                    typeof(ICardFileSourceSettings),
                    typeof(ICardLifecycleCompanionDependencies),
                    typeof(ICardTypeServerRepository),
                    typeof(CardMetadataCache),
                    typeof(IExchangeFormatInterpreter),
                    typeof(IIndentationStrategy),
                    typeof(ITessaViewService),
                    typeof(IJsonViewModelImporter),
                    typeof(IJsonViewModelAdapter)));

            return new ValueTask<IUnityContainer>(container);
        }

        /// <inheritdoc/>
        protected override async Task InitializeCoreAsync()
        {
            await base.InitializeCoreAsync();

            this.RemoveFileStoragePath();
            this.roleRepository = this.UnityContainer.Resolve<IRoleRepository>();
        }

        /// <inheritdoc/>
        protected override async Task OneTimeTearDownCoreAsync()
        {
            this.RemoveFileStoragePath();
            await base.OneTimeTearDownCoreAsync();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Выполняет действия перед поиском и выполнением серверных регистраторов расширений в папке приложения.
        /// </summary>
        /// <param name="unityContainer">Unity-контейнер.</param>
        protected virtual void BeforeRegisterExtensionsOnServer(IUnityContainer unityContainer)
        {
        }

        /// <summary>
        /// Выполняет действия перед завершением регистрации сервера приложений.
        /// </summary>
        /// <param name="unityContainer">Unity-контейнер.</param>
        protected virtual void BeforeFinalizeServerRegistration(IUnityContainer unityContainer)
        {
        }

        #endregion

        #region Private Methods

        private void RemoveFileStoragePath()
        {
            if (!this.useDatabaseAsDefault)
            {
                TestHelper.RemoveFileStoragePath(this.GetType());
            }
        }

        #endregion
    }
}
