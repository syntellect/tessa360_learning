﻿using System;
using Tessa.Cards;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Runtime;
using Tessa.Server;
using Tessa.Test.Default.Shared;
using Unity;

namespace Tessa.Test.Default.Server
{
    /// <summary>
    /// Вспомогательные методы для серверных тестов.
    /// </summary>
    public static class TestServerHelper
    {
        #region Methods

        /// <summary>
        /// Создаёт и конфигурирует контейнер Unity для выполнения тестов карточек на стороне сервера с расширениями.
        /// Дополнительно инициализирует серверные зависимости.
        /// </summary>
        /// <param name="createDbManagerFunc">Функция, создающая и возвращающая <see cref="DbManager"/>. Если задано значение по умолчанию для типа, то перерегистрация не выполняется.</param>
        /// <param name="dbScope">Экземпляр объекта осуществляющий взаимодействие с базой данных. Если задано значение по умолчанию для типа, то перерегистрация не выполняется.</param>
        /// <param name="tryGetTokenFunc">
        /// Функция, возвращающая токен, по которому определяются поля сессии,
        /// или <c>null</c>, если сессия определяется только внутри области, созданной в <see cref="SessionContext"/>,
        /// т.е. токен сессии недоступен в текущий момент.
        /// </param>
        /// <param name="fileSourceSettings">Настройки файлов, используемые по умолчанию.</param>
        /// <param name="beforeRegisterExtensionsOnServerAction">Метод выполняющий действия перед поиском и выполнением серверных регистраторов расширений в папке приложения.</param>
        /// <param name="beforeFinalizeServerRegistrationAction">Метод выполняющий действия перед завершением регистрации сервера приложений.</param>
        /// <returns>Созданный контейнер.</returns>
        public static IUnityContainer CreateServerContainer(
            Func<DbManager> createDbManagerFunc = default,
            IDbScope dbScope = default,
            Func<ISessionToken> tryGetTokenFunc = default,
            ICardFileSourceSettings fileSourceSettings = default,
            Action<IUnityContainer> beforeRegisterExtensionsOnServerAction = default,
            Action<IUnityContainer> beforeFinalizeServerRegistrationAction = default)
        {
            if (!(TessaPlatform.ServerDependencies is TessaServerDependencies))
            {
                TessaPlatform.ServerDependencies = new TessaServerDependencies();
            }

            return TestHelper.CreateServerContainerBase(
                createDbManagerFunc,
                dbScope,
                tryGetTokenFunc,
                fileSourceSettings,
                beforeRegisterExtensionsOnServerAction,
                beforeFinalizeServerRegistrationAction);
        }

        #endregion
    }
}