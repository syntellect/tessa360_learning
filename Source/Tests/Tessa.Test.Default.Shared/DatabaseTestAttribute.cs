﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using NUnit.Framework.Internal.Builders;
using NUnit.Framework.Internal.Commands;
using Tessa.Platform;
using Tessa.Platform.Data;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Устанавливает информацию о подключении к базе данные, которое должно использоваться в тесте.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class DatabaseTestAttribute :
        TestAttribute,
        ITestBuilder,
        IApplyToTest,
        IWrapSetUpTearDown
    {
        #region Nested Types

        private sealed class ConnectionFactory
        {
            #region Fields

            internal IPropertyBag Properties;

            #endregion

            #region Methods

            public DbConnection CreateConnection()
            {
                var connectionString =
                    (string) this.Properties.Get(DatabasePropertyNames.ConnectionString) ??
                    throw new InvalidOperationException("Connection string is not specified");
                var providerName =
                    (string) this.Properties.Get(DatabasePropertyNames.ProviderName) ??
                    throw new InvalidOperationException("Provider name is not specified");

                var factory = ConfigurationManager
                    .GetConfigurationDataProviderFromType(providerName)
                    .GetDbProviderFactory();
                var connection = factory.CreateConnection();

                // Для всех тестов, использующих данный аттрибут, пулинг выключается.
                // Все тесты явно создают соединения без использования пула
                var builder = factory.CreateConnectionStringBuilder();
                builder.ConnectionString = connectionString;
                builder["Pooling"] = "False";
                connectionString = builder.ConnectionString;

                connection.ConnectionString = connectionString;

                return connection;
            }

            #endregion
        }

        #endregion

        #region Fields

        private static readonly NUnitTestCaseBuilder sTestBuilder = new NUnitTestCaseBuilder();

        private object expectedResult;

        #endregion

        #region ITestBuilder Overrides

        /// <inheritdoc/>
        IEnumerable<TestMethod> ITestBuilder.BuildFrom(IMethodInfo method, NUnit.Framework.Internal.Test testSuite)
        {
            Check.ArgumentNotNull(method, nameof(method));

            var connectionStringName = this.ConnectionString;
            var usingPrefix = false;

            if (connectionStringName == null)
            {
                connectionStringName = string.Empty;
                usingPrefix = true;
            }
            else
            {
                var length = this.ConnectionString.Length - 1;
                if (length > 0 && this.ConnectionString[length] == '*')
                {
                    connectionStringName = this.ConnectionString.Substring(0, length);
                    usingPrefix = true;
                }
            }

            var hasNoMatches = true;
            foreach (var connection in ConfigurationManager.Connections)
            {
                if (ConnectionStringMatches(connection.Key, connectionStringName, usingPrefix))
                {
                    hasNoMatches = false;

                    foreach (var testCaseData in this.GetTestCases(method))
                    {
                        var testCase = (TestCaseParameters) testCaseData;
                        var testMethod = sTestBuilder.BuildTestMethod(method, testSuite, testCase);
                        var nameBuilder = StringBuilderHelper.Acquire();

                        testMethod.Name = nameBuilder
                            .Append(testMethod.Name)
                            .Append(" using ")
                            .Append(connection.Key)
                            .ToStringAndClear();

                        if (testSuite == null)
                        {
                            testMethod.FullName = testMethod.Name;
                        }
                        else
                        {
                            testMethod.FullName = nameBuilder
                                .Append(testSuite.FullName)
                                .Append('.')
                                .Append(testMethod.Name)
                                .ToString();
                        }

                        nameBuilder.Release();

                        var properties = testMethod.Properties;
                        var connectionProperties = connection.Value;

                        properties.Add(DatabasePropertyNames.ConnectionStringName, connection.Key);
                        properties.Add(DatabasePropertyNames.ProviderName, connectionProperties.DataProvider ?? string.Empty);

                        foreach (var argument in testCase.Arguments)
                        {
                            if (argument is Func<DbConnection> factoryDelegate &&
                                factoryDelegate.Target is ConnectionFactory factory)
                            {
                                factory.Properties = properties;
                            }
                        }

                        try
                        {
                            var factory = ConfigurationManager
                                .GetConfigurationDataProviderFromType(connectionProperties.DataProvider)
                                .GetDbProviderFactory();
                            var conStr = this.BuildConnectionString(factory, connectionProperties.ConnectionString, testMethod);

                            properties.Add(DatabasePropertyNames.ConnectionString, conStr);

                            TestHelper.SetTestCategory(properties, factory.GetDbms());
                        }
                        catch (Exception ex)
                        {
                            testMethod.RunState = RunState.NotRunnable;
                            properties.Set(PropertyNames.SkipReason, ex.Message);
                        }

                        foreach (var attribute in method.GetCustomAttributes<DatabaseScriptsAttribute>(true))
                        {
                            attribute.ApplyToTest(testMethod);
                        }

                        yield return testMethod;
                    }
                }
            }

            if (hasNoMatches)
            {
                foreach (var testCaseData in this.GetTestCases(method))
                {
                    var testCase = (TestCaseParameters) testCaseData;
                    var testMethod = sTestBuilder.BuildTestMethod(method, testSuite, testCase);
                    var properties = testMethod.Properties;

                    testMethod.RunState = RunState.NotRunnable;
                    properties.Set(PropertyNames.SkipReason, "No connection strings were provided");

                    yield return testMethod;
                }
            }
        }

        #endregion

        #region IApplyToTest Overrides

        /// <inheritdoc/>
        void IApplyToTest.ApplyToTest(NUnit.Framework.Internal.Test test)
        {
            Check.ArgumentNotNull(test, nameof(test));

            var properties = test.Properties;

            SetProperty(properties, "Description", this.Description);
            SetProperty(properties, "Author", this.Author);
            SetProperty(properties, "TestOf", this.TestOf);
        }

        #endregion

        #region IWrapSetUpTearDown Overrides

        /// <inheritdoc/>
        TestCommand ICommandWrapper.Wrap(TestCommand command) =>
            new DatabaseTestCommand(command ?? throw new ArgumentNullException(nameof(command)));

        #endregion

        #region Properties

        /// <summary>
        /// Возвращает или задаёт описание этого теста.
        /// </summary>
        public new string Description { get; set; }

        /// <summary>
        /// Возвращает или задаёт автора этого теста.
        /// </summary>
        public new string Author { get; set; }

        /// <summary>
        /// Возвращает или задаёт тип тестирующий этот тест.
        /// </summary>
        public new Type TestOf { get; set; }

        /// <summary>
        /// Возвращает или задаёт ожидаемое значение.
        /// </summary>
        /// <remarks>Значение проверяется, если выставлен флаг <see cref="HasExpectedResult"/>, иначе игнорируется.</remarks>
        public new object ExpectedResult
        {
            get => this.expectedResult;
            set
            {
                this.expectedResult = value;
                this.HasExpectedResult = true;
            }
        }

        /// <summary>
        /// Возвращает или задаёт значение, показывающее, что ожидается значение <see cref="ExpectedResult"/>.
        /// </summary>
        public bool HasExpectedResult { get; private set; }

        /// <summary>
        /// Возвращает или задаёт имя строки подключения.
        /// </summary>
        /// <remarks>
        /// Может быть задан формат имени строки подключения, которые должны использоваться в тесте.<para/>
        /// Формат имени строки подключения: &lt;Имя строки подключения&gt;[*]. Если задана звёздочка, то выполняется поиск всех строк подключения начинающихся с заданной строки.
        /// </remarks>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Признак того, что имя базы данных надо рандомизировать в зависимости от полного имени теста.
        /// Это позволяет параллельно выполнять тесты на разных базах данных.
        /// </summary>
        public bool RandomizeDbName { get; set; } = true;

        #endregion

        #region Methods

        protected virtual IEnumerable<ITestCaseData> GetTestCases(IMethodInfo method)
        {
            TestCaseParameters testCase;

            try
            {
                var parameters = method.GetParameters();
                var arguments = new object[parameters.Length];

                for (var i = 0; i < parameters.Length; i++)
                {
                    var parameter = parameters[i];
                    if (parameter.IsOptional)
                    {
                        arguments[i] = Type.Missing;
                    }
                    else
                    {
                        var parameterType = parameter.ParameterType;
                        if (parameterType == typeof(Func<DbConnection>))
                        {
                            arguments[i] = this.CreateConnectionFactory();
                        }
                        else
                        {
                            throw new InvalidOperationException("Unsupported parameter type");
                        }
                    }
                }

                testCase = new TestCaseParameters(arguments);

                if (this.HasExpectedResult)
                {
                    testCase.ExpectedResult = this.ExpectedResult;
                }
            }
            catch (Exception ex)
            {
                testCase = new TestCaseParameters(ex);
            }

            return new[] { testCase };
        }

        protected Func<DbConnection> CreateConnectionFactory() =>
            new ConnectionFactory().CreateConnection;

        #endregion

        #region Private methods

        private static void SetProperty(IPropertyBag properties, string key, object value)
        {
            if (properties.ContainsKey(key) || value == null)
            {
                return;
            }

            properties.Set(key, value);
        }

        private static bool ConnectionStringMatches(string name, string pattern, bool usingPrefix) =>
            usingPrefix
                ? name.StartsWith(pattern, StringComparison.Ordinal)
                : name.Equals(pattern, StringComparison.Ordinal);

        private string BuildConnectionString(DbProviderFactory factory, string connectionString, TestMethod testMethod)
        {
            var builder = factory.CreateConnectionStringBuilder();

            builder.ConnectionString = connectionString;

            var dbms = factory.GetDbms();

            var databaseKey = dbms switch
            {
                Dbms.SqlServer => "Initial Catalog",
                Dbms.PostgreSql => "Database",
                _ => throw new NotSupportedException()
            };

            var databaseName = (string) builder[databaseKey];
            string databasePrefix;
            if (string.IsNullOrEmpty(databaseName))
            {
                databasePrefix = testMethod.MethodName;
            }
            else
            {
                if (this.RandomizeDbName)
                {
                    databasePrefix = databaseName;
                }
                else
                {
                    return connectionString;
                }
            }

            builder[databaseKey] = string.Concat(databasePrefix, "_", testMethod.FullName.GetConstantHashCode().ToString("X4"));
            return builder.ToString();
        }

        #endregion
    }
}
