﻿using System.Collections.Generic;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Описывает действия выполняемые в тестах.
    /// </summary>
    public interface ITestActions
    {
        /// <summary>
        /// Список действий которые должны быть выполнены перед выполнением любых других выполняемых однократно.
        /// </summary>
        IList<ITestAction> BeforeInitializeActions { get; }

        /// <summary>
        /// Список действий которые должны быть выполнены после выполнением всех действий выполняемых однократно.
        /// </summary>
        IList<ITestAction> AfterInitializeActions { get; }

        /// <summary>
        /// Список действий которые должны быть выполнены перед выполнением любых других действий выполняемых перед выполнением каждого теста.
        /// </summary>
        IList<ITestAction> BeforeSetUpActions { get; }

        /// <summary>
        /// Список действий которые должны быть выполнены после выполнением всех действий выполняемых перед выполнением каждого теста.
        /// </summary>
        IList<ITestAction> AfterSetUpActions { get; }

        /// <summary>
        /// Список действий которые должны быть выполнены перед выполнением любых других действий выполняемых после выполнения каждого теста.
        /// </summary>
        IList<ITestAction> BeforeTearDownActions { get; }

        /// <summary>
        /// Список действий которые должны быть выполнены после выполнения всех действий выполняемых после выполнения каждого теста.
        /// </summary>
        IList<ITestAction> AfterTearDownActions { get; }

        /// <summary>
        /// Список действий которые должны быть выполнены перед выполнением любых других действий выполняемых после выполнения всех тестов.
        /// </summary>
        IList<ITestAction> BeforeOneTimeTearDownActions { get; }

        /// <summary>
        /// Список действий которые должны быть выполнены после выполнения всех действий выполняемых после выполнения всех тестов.
        /// </summary>
        IList<ITestAction> AfterOneTimeTearDownActions { get; }
    }
}
