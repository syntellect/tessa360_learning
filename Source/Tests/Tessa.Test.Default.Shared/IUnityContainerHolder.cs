﻿using Unity;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Контейнер для объекта <see cref="IUnityContainer"/>, содержащего регистрации
    /// для различных API, например, для клиентской сессии, обеспечивающей доступ к веб-сервисам.
    /// 
    /// Интерфейс должен быть реализован на TestFixture для успешного применения таких атрибутов,
    /// как <c>[SetupClient]</c>, который автоматически инициализирует сессию.
    /// </summary>
    public interface IUnityContainerHolder
    {
        /// <summary>
        /// Контейнер Unity. Не должен быть равен <c>null</c>.
        /// </summary>
        IUnityContainer UnityContainer { get; }
    }
}
