﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.Workflow.KrCompilers;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Extensions.Default.Shared.Workflow.Wf;
using Tessa.Platform;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;
using Tessa.Test.Default.Shared.Cards;
using Tessa.Test.Default.Shared.Kr.Routes;

namespace Tessa.Test.Default.Shared.Kr
{
    /// <summary>
    /// Предоставляет статические методы расширения.
    /// </summary>
    public static class CardLifecycleCompanionExtensions
    {
        #region CardLifecycleCompanion Extensions

        /// <summary>
        /// Запускает процесс, подсистемы маршрутов, имеющий указанный идентификатор и инициализирует объект типа <see cref="KrRouteProcessInstanceLifecycleCompanion"/>.
        /// </summary>
        /// <param name="clc">Объект, управляющий жизненным циклом карточки в кототорой должен быть запущен процесс.</param>
        /// <param name="processID">Идентификатор запускаемого процесса.</param>
        /// <param name="info">Дополнительная информация передаваемая в процесс при запуске.</param>
        /// <returns>Объект <see cref="KrRouteProcessInstanceLifecycleCompanion"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Обратите внимание: запуск производится при сохранении карточки. После сохранения выполняется автоматическая загрузка карточки.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на сохранение карточки.<para/>
        /// По умолчанию всегда генерируется ошибка, если запуск процесса запрещён ограничениями. Если это не требуется, то необходимо задать дополнительному параметру <see cref="KrConstants.RaiseErrorWhenExecutionIsForbidden"/>, расположенному в дополнительной информации отложенного действия, значение <see cref="BooleanBoxes.False"/>.
        /// </remarks>
        /// <returns>Объект типа <see cref="KrRouteProcessInstanceLifecycleCompanion"/> для создания цепочки.</returns>
        public static KrRouteProcessInstanceLifecycleCompanion CreateKrProcess(
            this CardLifecycleCompanion clc,
            Guid processID,
            Dictionary<string, object> info = default)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            var krProcess = KrProcessBuilder
                .CreateProcess()
                .SetProcess(processID)
                .SetCard(clc.CardID)
                .SetProcessInfo(info)
                .Build();

            return clc.CreateKrProcess(krProcess);
        }

        /// <summary>
        /// Запускает процесс подсистемы маршрутов и инициализирует объект типа <see cref="KrRouteProcessInstanceLifecycleCompanion"/>.
        /// </summary>
        /// <param name="clc">Объект, управляющий жизненным циклом карточки в кототорой должен быть запущен процесс.</param>
        /// <param name="krProcess">Информация об экземпляре процесса.</param>
        /// <returns>Объект <see cref="KrRouteProcessInstanceLifecycleCompanion"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Обратите внимание: запуск производится при сохранении картчоки. После сохранения выполняется автоматическая загрузка карточки.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на сохранение карточки.<para/>
        /// По умолчанию всегда генерируется ошибка, если запуск процесса запрещён ограничениями. Если это не требуется, то необходимо задать дополнительному параметру <see cref="KrConstants.RaiseErrorWhenExecutionIsForbidden"/>, расположенному в дополнительной информации отложенного действия, значение <see cref="BooleanBoxes.False"/>.
        /// </remarks>
        /// <returns>Объект типа <see cref="KrRouteProcessInstanceLifecycleCompanion"/> для создания цепочки.</returns>
        public static KrRouteProcessInstanceLifecycleCompanion CreateKrProcess(
            this CardLifecycleCompanion clc,
            KrProcessInstance krProcess)
        {
            Check.ArgumentNotNull(clc, nameof(clc));
            Check.ArgumentNotNull(krProcess, nameof(krProcess));

            var routeProcessInstanceLifecycleCompanion = new KrRouteProcessInstanceLifecycleCompanion(clc, default);

            var saveAction = clc.Save().GetLastPendingAction();
            saveAction.Info.SetKrProcessInstance(krProcess);
            saveAction.Info.Add(KrConstants.RaiseErrorWhenExecutionIsForbidden, BooleanBoxes.True);

            saveAction.AddAfterAction(
                new PendingAction(
                    $"{nameof(CardLifecycleCompanionExtensions)}.{nameof(CreateKrProcess)}: ProcessID = {krProcess.ProcessID:B}",
                    (pendingAction, ct) =>
                    {
                        var storeResponse = clc.LastData.StoreResponse;
                        var result = storeResponse.GetKrProcessLaunchResult();

                        routeProcessInstanceLifecycleCompanion.SetProcessInstanceID(result?.ProcessID);

                        // Результат выполнения был добавлен при выполнении ICardLifecycleCompanion<T>.Save().
                        return new ValueTask<ValidationResult>(ValidationResult.Empty);
                    }));

            return routeProcessInstanceLifecycleCompanion;
        }

        /// <summary>
        /// Инициализирует объект типа <see cref="KrRouteProcessInstanceLifecycleCompanion"/>.
        /// </summary>
        /// <param name="clc">Объект, управляющий жизненным циклом карточки в кототорой должен быть запущен процесс.</param>
        /// <returns>Объект <see cref="KrRouteProcessInstanceLifecycleCompanion"/> для создания цепочки.</returns>
        /// <returns>Объект типа <see cref="KrRouteProcessInstanceLifecycleCompanion"/> для создания цепочки.</returns>
        public static KrRouteProcessInstanceLifecycleCompanion CreateKrProcess(
            this CardLifecycleCompanion clc)
        {
            return new KrRouteProcessInstanceLifecycleCompanion(clc, default);
        }

        #endregion

        #region ICardLifecycleCompanion<T> Extensions

        /// <summary>
        /// Устанавливает тип документа имеющий указанный идентификатор и имя.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="docTypeID">Идентификатор типа документа.</param>
        /// <param name="docTypeName">Имя типа документа.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public static T WithDocType<T>(
            this T clc,
            Guid docTypeID,
            string docTypeName)
            where T : ICardLifecycleCompanion<T>
        {
            return clc
                .WithInfoPair(KrConstants.Keys.DocTypeID, docTypeID)
                .WithInfoPair(KrConstants.Keys.DocTypeTitle, docTypeName);
        }

        /// <summary>
        /// Завершает указаннное задание.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="task">Завершаемое задание.</param>
        /// <param name="optionID">Идентификатор варианта завершения.</param>
        /// <param name="beforeCompleteTaskActionAsync">Действие выполняемое перед завершением задания.</param>
        /// <param name="deleteTask">Значение <see langword="true"/>, если задание должно быть удалено при завершении, иначе - <see langword="false"/>.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <exception cref="InvalidOperationException">Задание с указанным идентификатором не найдено.</exception>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// После завершения задания, выполняется сохранение карточки.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на сохранение карточки.
        /// </remarks>
        public static T CompleteTask<T>(
            this T clc,
            CardTask task,
            Guid optionID,
            Func<CardTask, CancellationToken, ValueTask> beforeCompleteTaskActionAsync = default,
            bool deleteTask = true)
            where T : ICardLifecycleCompanion<T>
        {
            Check.ArgumentNotNull(task, nameof(task));

            clc
                .Save()
                .GetLastPendingAction()
                .AddPreparationAction(
                    new PendingAction(
                        nameof(CardLifecycleCompanionExtensions) + "." + nameof(CompleteTask),
                        async (action, ct) =>
                        {
                            _ = CompleteTaskInternal(task, optionID, deleteTask);

                            if (beforeCompleteTaskActionAsync != null)
                            {
                                await beforeCompleteTaskActionAsync(task, ct);
                            }

                            return ValidationResult.Empty;
                        }));

            return clc;
        }

        /// <summary>
        /// Устанавливает заданное действие выполняемое с заданием.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="task">Задание в котором устанавливается действие.</param>
        /// <param name="taskAction">Устанавлиемое действие.</param>
        /// <param name="beforeCompleteActionAsync">Действие выполняемое перед заданием действия и состояния строки задания.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Состояние строки с заданием устанавливается равным <see cref="CardRowState.Modified"/>.<para/>
        /// После выполнения действия задания, выполняется сохранение карточки.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на сохранение карточки.
        /// </remarks>
        public static T ActionTask<T>(
            this T clc,
            CardTask task,
            CardTaskAction taskAction,
            Func<Card, CardTask, ValueTask> beforeCompleteActionAsync = default)
            where T : ICardLifecycleCompanion<T>
        {
            Check.ArgumentNotNull(task, nameof(task));

            clc
                .Save()
                .GetLastPendingAction()
                .AddPreparationAction(
                    new PendingAction(
                        nameof(CardLifecycleCompanionExtensions) + "." + nameof(ActionTask),
                        async (action, ct) =>
                        {
                            var card = clc.GetCardOrThrow();

                            if (beforeCompleteActionAsync != null)
                            {
                                await beforeCompleteActionAsync(card, task);
                            }
                            task.Action = taskAction;
                            task.State = CardRowState.Modified;

                            return ValidationResult.Empty;
                        }));

            return clc;
        }

        /// <summary>
        /// Выполняет пересчёт маршрута в указанной карточке.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="iac">Режим вывода информации об изменениях в маршруте после пересчёта.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// После пересчёта маршрута выполняется сохранение карточки.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на сохранение карточки.
        /// </remarks>
        public static T Recalc<T>(
            this T clc,
            InfoAboutChanges iac = InfoAboutChanges.ToInfo)
            where T : ICardLifecycleCompanion<T>
        {
            clc.Save();

            var lastActionInfo = clc.GetLastPendingAction().Info;
            lastActionInfo.SetRecalcFlag();
            lastActionInfo.SetInfoAboutChanges(iac);

            return clc;
        }

        /// <summary>
        /// Загружает карточку с скрытыми этапами.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на загрузку карточки.
        /// </remarks>
        public static T LoadWithtHiddenStages<T>(
            this T clc)
            where T : ICardLifecycleCompanion<T>
        {
            clc
                .Load()
                .GetLastPendingAction().Info.DontHideStages();
            return clc;
        }

        #endregion

        #region ICardLifecycleCompanion Extensions

        /// <summary>
        /// Изменяет значение указанного поля строковой секции карточки добавляя к значению ".".
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="section">Имя секции.</param>
        /// <param name="field">Имя поля.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public static T ModifyDocument<T>(
            this T clc,
            string section = KrConstants.DocumentCommonInfo.Name,
            string field = KrConstants.DocumentCommonInfo.Subject)
            where T : ICardLifecycleCompanion, IPendingActionsProvider<T>
        {
            clc.AddPendingAction(
                new PendingAction(
                    nameof(CardLifecycleCompanionExtensions) + "." + nameof(ModifyDocument),
                    (action, ct) =>
                    {
                        var fields = clc.GetCardOrThrow().Sections[section].Fields;
                        fields[field] = fields.Get<object>(field) + ".";
                        return new ValueTask<ValidationResult>(ValidationResult.Empty);
                    }));

            return clc;
        }

        /// <summary>
        /// Устанавливает значение указанного поля строковой секции карточки.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="section">Имя секции.</param>
        /// <param name="field">Имя поля.</param>
        /// <param name="newValue">Задаваемое значение.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public static T SetValue<T>(
            this T clc,
            string section,
            string field,
            object newValue)
            where T : ICardLifecycleCompanion, IPendingActionsProvider<T>
        {
            clc.AddPendingAction(
                new PendingAction(
                    $"{nameof(CardLifecycleCompanionExtensions)}.{nameof(SetValue)}: Section name: \"{section}\", Field name: \"{field}\".",
                    (action, ct) =>
                    {
                        clc.GetCardOrThrow().Sections[section].Fields[field] = newValue;
                        return new ValueTask<ValidationResult>(ValidationResult.Empty);
                    }));

            return clc;
        }

        /// <summary>
        /// Устанаваливает значение в указанном поле заданной строки коллекционной или древовидной секции.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="section">Имя секции.</param>
        /// <param name="rowNumber">Порядковый номер строки.</param>
        /// <param name="field">Имя поля.</param>
        /// <param name="newValue">Задаваемое значение.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public static T SetValueRow<T>(
            this T clc,
            string section,
            int rowNumber,
            string field,
            object newValue)
            where T : ICardLifecycleCompanion, IPendingActionsProvider<T>
        {
            clc.AddPendingAction(
                new PendingAction(
                    $"{nameof(CardLifecycleCompanionExtensions)}.{nameof(SetValue)}: Section name: \"{section}\", Row number: {rowNumber}, Field name: \"{field}\".",
                    (action, ct) =>
                    {
                        clc.GetCardOrThrow().Sections[section].Rows[rowNumber].Fields[field] = newValue;
                        return new ValueTask<ValidationResult>(ValidationResult.Empty);
                    }));
            return clc;
        }

        /// <summary>
        /// Возвращает значение поля <paramref name="fieldName"/> содержащегося в строковой секции <paramref name="sectionName"/>.
        /// </summary>
        /// <typeparam name="T">Тип значения.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="sectionName">Имя секции.</param>
        /// <param name="fieldName">Имя поля.</param>
        /// <returns>Значение поля или значение по умолчанию для типа <typeparamref name="T"/>, если секция не содержит указанного поля.</returns>
        public static T GetValue<T>(
            this ICardLifecycleCompanion clc,
            string sectionName,
            string fieldName)
        {
            return clc
                .GetCardOrThrow()
                .Sections[sectionName]
                .RawFields
                .Get<T>(fieldName);
        }

        /// <summary>
        /// Возвращает значение поля <paramref name="fieldName"/> содержащегося в строковой секции <paramref name="sectionName"/>.
        /// </summary>
        /// <typeparam name="T">Тип значения.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="sectionName">Имя секции.</param>
        /// <param name="fieldName">Имя поля.</param>
        /// <returns>Значение поля или значение по умолчанию для типа <typeparamref name="T"/>, если секция не содержит указанного поля.</returns>
        public static T TryGetValue<T>(
            this ICardLifecycleCompanion clc,
            string sectionName,
            string fieldName)
        {
            return clc
                .GetCardOrThrow()
                .Sections[sectionName]
                .RawFields
                .TryGet<T>(fieldName);
        }

        /// <summary>
        /// Возвращает значение поля <paramref name="fieldName"/> содержащегося в строке <paramref name="rowIndex"/> коллекционной иил древовидной секции <paramref name="sectionName"/>.
        /// </summary>
        /// <typeparam name="T">Тип значения.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="sectionName">Имя секции.</param>
        /// <param name="rowIndex">Порядковый индекс строки.</param>
        /// <param name="fieldName">Имя поля.</param>
        /// <returns>Значение поля или значение по умолчанию для типа <typeparamref name="T"/>, если строка не содержит указанного поля.</returns>
        public static T GetValue<T>(
            this ICardLifecycleCompanion clc,
            string sectionName,
            int rowIndex,
            string fieldName)
        {
            return clc
                .GetCardOrThrow()
                .Sections[sectionName]
                .Rows[rowIndex]
                .TryGet<T>(fieldName);
        }

        /// <summary>
        /// Возвращает значение поля <paramref name="fieldName"/> содержащегося в строке удовлетворяющей условию <paramref name="rowPredicate"/> коллекционной иил древовидной секции <paramref name="sectionName"/>.
        /// </summary>
        /// <typeparam name="T">Тип значения.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="sectionName">Имя секции.</param>
        /// <param name="rowPredicate">Поисковое условие.</param>
        /// <param name="fieldName">Имя поля.</param>
        /// <returns>Значение поля или значение по умолчанию для типа <typeparamref name="T"/>, если не найдена строка удовлетворяющая поисковому условию или строка не содержит указанного поля.</returns>
        public static T GetValue<T>(
            this ICardLifecycleCompanion clc,
            string sectionName,
            Func<CardRow, bool> rowPredicate,
            string fieldName)
        {
            var row = clc
                .GetCardOrThrow()
                .Sections[sectionName]
                .TryGetRows()
                ?.FirstOrDefault(rowPredicate);

            return row != null ? row.TryGet<T>(fieldName) : default;
        }

        /// <summary>
        /// Добавляет новую строку в коллекционную или древовидную секцию.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку, в секцию которой должна быть добавлена новая строка.</param>
        /// <param name="section">Имя секции в которую должна быть добавлена новая строка.</param>
        /// <param name="orderField">Имя поля содержащего порядок сортироваки (order).</param>
        /// <param name="rowValues">Перечисление пар &lt;ключ; значение&gt; которыми выполняется инициализация полей новой строки. Может быть не задано.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// RowID строки задаётся автоматически, если не был указан явно в <paramref name="rowValues"/>.
        /// </remarks>
        public static T AddRow<T>(
            this T clc,
            string section,
            string orderField = default,
            IEnumerable<KeyValuePair<string, object>> rowValues = default)
            where T : ICardLifecycleCompanion, IPendingActionsProvider<T>
        {
            clc.AddPendingAction(
                new PendingAction(
                    nameof(CardLifecycleCompanionExtensions) + "." + nameof(AddRow),
                    (action, ct) =>
                    {
                        var rows = clc.GetCardOrThrow().Sections[section].Rows;
                        TestCardHelper.AddRow(rows, clc.CardID, orderField, rowValues);
                        return new ValueTask<ValidationResult>(ValidationResult.Empty);
                    }));

            return clc;
        }

        /// <summary>
        /// Возвращает строку содержащую информацию о первом этапе имеющем указанное имя.
        /// </summary>
        /// <param name="clc">Объект, содержащий карточку с маршрутом из которого требуется получить этап с указанным именем.</param>
        /// <param name="name">Имя этапа.</param>
        /// <returns>Строка содержащая информацию о первом этапе имеющем указанное имя или значение по умолчанию для типа, если такую строку не удалось найти.</returns>
        public static CardRow GetStage(
            this ICardLifecycleCompanion clc,
            string name)
        {
            // Проверка значения clc.Card будет выполнена в GetStagesSection().

            return clc
                .Card
                .GetStagesSection()
                .TryGetRows()
                ?.FirstOrDefault(p => string.Equals(p.Fields.Get<string>(KrConstants.Name), name, StringComparison.Ordinal));
        }

        /// <summary>
        /// Проверяет содержит ли объект, управляющий жизненным циклом карточки, карточку указанного типа.
        /// </summary>
        /// <param name="clc">Объект, управляющий жизнанным циклом карточки.</param>
        /// <param name="expectedTypeID">Идентификатор ожидаемого типа.</param>
        /// <param name="expectedTypeName">Имя ожидаемого типа.</param>
        /// <returns>Карточка содержащаяся в <paramref name="clc"/>.</returns>
        /// <exception cref="ArgumentException">The <see cref="ICardLifecycleCompanion"/> object must contain a card of type <paramref name="expectedTypeName"/> (ID = "<paramref name="expectedTypeID"/>"). Actual type <see cref="Card.TypeCaption"/> (ID = "<see cref="Card.TypeID"/>").</exception>
        public static Card GetAndCheckCardTypeThrow(
            this ICardLifecycleCompanion clc,
            Guid expectedTypeID,
            string expectedTypeName)
        {
            var card = clc.GetCardOrThrow();

            if (card.TypeID != expectedTypeID)
            {
                throw new ArgumentException($"The {nameof(ICardLifecycleCompanion)} object must contain a card of type {expectedTypeName} (ID = \"{expectedTypeID}\"). Actual type {card.TypeName} (ID = \"{card.TypeID}\").", nameof(clc));
            }

            return card;
        }

        /// <summary>
        /// Возвращает первое задание удовлетворяющее указаному предикату. Если задание не удалось найти, то генерируется исключение <see cref="AssertionException"/>.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="predicate">Поисковое условие.</param>
        /// <param name="notFoundMessage">Сообщение добавляемое к стандартному тексту собщению об отсутствии удовлетворяющего условия задания.</param>
        /// <param name="task">Возвращаемое значение. Первое задание удовлетворяющее условие или значение по умолчанию для типа, если таковое не удалось найти.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <exception cref="AssertionException">Task not found: <paramref name="notFoundMessage"/>.</exception>
        public static T GetTaskOrThrow<T>(
            this T clc,
            Func<CardTask, bool> predicate,
            string notFoundMessage,
            out CardTask task) where T : ICardLifecycleCompanion
        {
            task = clc.GetTask(predicate);

            if (task is null)
            {
                Assert.Fail($"Task not found: {notFoundMessage}.");
            }

            return clc;
        }

        /// <summary>
        /// Возвращает первое задание имеющее указанный тип. Если задание не удалось найти, то генерируется исключение <see cref="AssertionException"/>.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="taskTypeID">Идентификатор типа задания.</param>
        /// <param name="task">Возвращаемое значение. Первое задание имеющее указанный тип или значение по умолчанию для типа, если таковое не удалось найти.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <exception cref="AssertionException">Task not found: Task type ID = <paramref name="taskTypeID"/>.</exception>
        public static T GetTaskOrThrow<T>(
            this T clc,
            Guid taskTypeID,
            out CardTask task) where T : ICardLifecycleCompanion
        {
            return clc.GetTaskOrThrow(task => task.TypeID == taskTypeID, $"Task type ID = {taskTypeID:B}", out task);
        }

        /// <summary>
        /// Возвращает первое задание произвольного типа, кроме виртуального задания типа <see cref="DefaultTaskTypes.KrInfoForInitiatorTypeID"/>. Если задание не удалось найти, то генерируется исключение <see cref="AssertionException"/>.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="task">Возвращаемое значение. Первое задание, тип которого не равен <see cref="DefaultTaskTypes.KrInfoForInitiatorTypeID"/>, или значение по умолчанию для типа, если таковое не удалось найти.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <exception cref="AssertionException">Task not found: Except task type ID = <see cref="DefaultTaskTypes.KrInfoForInitiatorTypeID"/>.</exception>
        public static T GetAnyTaskOrThrow<T>(
            this T clc,
            out CardTask task) where T : ICardLifecycleCompanion
        {
            return clc.GetTaskOrThrow(
                task => task.TypeID != DefaultTaskTypes.KrInfoForInitiatorTypeID,
                $"Except task type ID = {DefaultTaskTypes.KrInfoForInitiatorTypeID:B}.",
                out task);
        }

        /// <summary>
        /// Возвращает любое первое задание типового процесса согласования, кроме виртуального задания типа <see cref="DefaultTaskTypes.KrInfoForInitiatorTypeID"/>. Если задание не удалось найти, то генерируется исключение <see cref="AssertionException"/>.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="task">Возвращаемое значение. Первое задание типового процесса согласования, кроме виртуального задания типа <see cref="DefaultTaskTypes.KrInfoForInitiatorTypeID"/> или значение по умолчанию для типа, если таковое не удалось найти.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <exception cref="AssertionException">Task not found: Card does not contain Kr tasks.</exception>
        /// <seealso cref="KrConstants.KrTaskTypeIDList"/>
        public static T GetAnyKrTaskOrThrow<T>(
            this T clc,
            out CardTask task) where T : ICardLifecycleCompanion
        {
            return clc.GetTaskOrThrow(
                task => KrConstants.KrTaskTypeIDList.Contains(task.TypeID),
                "Card does not contain Kr tasks.",
                out task);
        }

        /// <summary>
        /// Возвращает любое первое задание процесса резолюции. Если задание не удалось найти, то генерируется исключение <see cref="AssertionException"/>.
        /// </summary>
        /// <typeparam name="T">Тип объекта управляющего жизненным циклом карточки.</typeparam>
        /// <param name="clc">Объект, содержащий карточку.</param>
        /// <param name="task">Возвращаемое значение. Первое задание процесса резолюции или значение по умолчанию для типа, если таковое не удалось найти.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <exception cref="AssertionException">Task not found: Card does not contain WfResolution tasks.</exception>
        /// <seealso cref="WfHelper.ResolutionTaskTypeIDList"/>
        public static T GetAnyWfTaskOrThrow<T>(
            this T clc,
            out CardTask task) where T : ICardLifecycleCompanion
        {
            return clc.GetTaskOrThrow(
                task => WfHelper.TaskTypeIsResolution(task.TypeID),
                "Card does not contain WfResolution tasks.",
                out task);
        }

        /// <summary>
        /// Возвращает первое задание удовлетворяющее заданному условию или значение по умолчанию для типа, если такое задание не было найдено.
        /// </summary>
        /// <param name="clc">Объект, содержащий карточку, в которой выполняется поиск задания.</param>
        /// <param name="predicate">Предикат, определяющий искомое задание.</param>
        /// <returns>Задание удовлетворяющее заданному условию или значение по умолчанию для типа, если такое задание не было найдено.</returns>
        public static CardTask GetTask(
            this ICardLifecycleCompanion clc,
            Func<CardTask, bool> predicate)
            => clc.GetCardOrThrow().TryGetTasks()?.FirstOrDefault(predicate);

        #endregion

        #region Private methods

        private static CardTask CompleteTaskInternal(
            CardTask task,
            Guid optionID,
            bool deleteTask)
        {
            task.State = deleteTask
                ? CardRowState.Deleted
                : CardRowState.Modified;
            task.OptionID = optionID;
            task.Result = "Test";
            task.Action = CardTaskAction.Complete;

            return task;
        }

        #endregion
    }
}