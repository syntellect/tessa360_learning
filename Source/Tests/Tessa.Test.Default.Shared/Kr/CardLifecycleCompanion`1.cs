﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Cards.Numbers;
using Tessa.Cards.Workflow;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Files;
using Tessa.Platform;
using Tessa.Platform.Validation;

namespace Tessa.Test.Default.Shared.Kr
{
    /// <summary>
    /// Предоставляет базовую реализацию <see cref="ICardLifecycleCompanion{T}"/>.
    /// </summary>
    /// <typeparam name="T">Тип объекта запланированные действия которого выполненяются методом <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.</typeparam>
    [DebuggerDisplay("{" + nameof(GetDebuggerDisplay) + "()}")]
    public class CardLifecycleCompanion<T> :
        PendingActionsProvider<T>,
        ICardLifecycleCompanion<T>
        where T : CardLifecycleCompanion<T>
    {
        #region Fields

        private ICardFileContainer cardFileContainer;

        private readonly ParentStageRowIDVisitor visitor;

        private readonly T thisObj;

        private readonly CardLifecycleCompanionData lastData = new CardLifecycleCompanionData();

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CardLifecycleCompanion{T}"/>.
        /// </summary>
        /// <param name="deps">Зависимости используемые при взаимодействии с карточкой.</param>
        private CardLifecycleCompanion(
            ICardLifecycleCompanionDependencies deps)
        {
            Check.ArgumentNotNull(deps, nameof(deps));

            this.Dependencies = deps;
            this.visitor = new ParentStageRowIDVisitor(deps.CardMetadata);
            this.thisObj = (T)this;
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CardLifecycleCompanion{T}"/>.
        /// </summary>
        /// <param name="cardID">Идентификатор карточки.</param>
        /// <param name="cardTypeID">Идентификатор типа карточки.</param>
        /// <param name="cardTypeName">Имя типа карточки.</param>
        /// <param name="deps">Зависимости используемые при взаимодействии с карточкой.</param>
        public CardLifecycleCompanion(
            Guid cardID,
            Guid cardTypeID,
            string cardTypeName,
            ICardLifecycleCompanionDependencies deps)
            : this(deps)
        {
            this.CardID = cardID;
            this.CardTypeID = cardTypeID;
            this.CardTypeName = cardTypeName;
            this.Card = default;
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CardLifecycleCompanion{T}"/>.
        /// </summary>
        /// <param name="cardTypeID">Идентификатор типа карточки.</param>
        /// <param name="cardTypeName">Имя типа карточки.</param>
        /// <param name="deps">Зависимости используемые при взаимодействии с карточкой.</param>
        public CardLifecycleCompanion(
            Guid cardTypeID,
            string cardTypeName,
            ICardLifecycleCompanionDependencies deps)
            : this(Guid.NewGuid(), cardTypeID, cardTypeName, deps)
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CardLifecycleCompanion{T}"/>.
        /// </summary>
        /// <param name="card">Карточка, жизненным циклом которой необходимо управлять.</param>
        /// <param name="deps">Зависимости используемые при взаимодействии с карточкой.</param>
        public CardLifecycleCompanion(
            Card card,
            ICardLifecycleCompanionDependencies deps)
            : this(deps)
        {
            Check.ArgumentNotNull(card, nameof(card));

            this.CardID = card.ID;
            this.CardTypeID = card.TypeID;
            this.Card = card;
        }

        #endregion

        #region Properties

        /// <inheritdoc/>
        public Guid CardTypeID { get; protected set; }

        /// <inheritdoc/>
        public string CardTypeName { get; protected set; }

        /// <inheritdoc/>
        public Guid CardID { get; protected set; }

        /// <inheritdoc/>
        public Card Card { get; protected set; }

        /// <summary>
        /// Возвращает зависимости используемые объектом управляющими жизненным циклом карточек.
        /// </summary>
        protected ICardLifecycleCompanionDependencies Dependencies { get; }

        /// <inheritdoc/>
        public ICardLifecycleCompanionData LastData => this.lastData;

        #endregion

        #region Public methods

        /// <inheritdoc/>
        public virtual async ValueTask<ICardFileContainer> GetCardFileContainerAsync(
            IFileRequest request = default,
            IList<IFileTag> additionalTags = default,
            CancellationToken cancellationToken = default)
        {
            return this.cardFileContainer ??= await this.Dependencies.CardFileManager.CreateContainerAsync(
                this.GetCardOrThrow(),
                request: request,
                additionalTags: additionalTags,
                cancellationToken: cancellationToken);
        }

        /// <inheritdoc/>
        public virtual T Create()
        {
            this.AddPendingAction(
                new PendingAction(
                    nameof(CardLifecycleCompanion<T>) + "." + nameof(this.Create),
                    this.CreateActionAsync));
            return this.thisObj;
        }

        /// <inheritdoc/>
        public virtual T Save()
        {
            this.AddPendingAction(
                new PendingAction(
                    nameof(CardLifecycleCompanion<T>) + "." + nameof(this.Save),
                    this.SaveActionAsync));
            return this.thisObj;
        }

        /// <inheritdoc/>
        public virtual T Load()
        {
            this.AddPendingAction(
                new PendingAction(
                    nameof(CardLifecycleCompanion<T>) + "." + nameof(this.Load),
                    this.LoadInternalAsync));
            return this.thisObj;
        }

        /// <inheritdoc/>
        public virtual T Delete()
        {
            this.AddPendingAction(
                new PendingAction(
                    nameof(CardLifecycleCompanion<T>) + "." + nameof(this.Delete),
                    this.DeleteActionAsync));
            return this.thisObj;
        }

        /// <inheritdoc/>
        public T WithInfoPair(
            string key,
            object val)
        {
            var pendingAction = this.GetLastPendingAction();
            pendingAction.Info[key] = val;
            return this.thisObj;
        }

        /// <inheritdoc/>
        public T WithInfo(
            Dictionary<string, object> info)
        {
            this.GetLastPendingAction().SetInfo(info);
            return this.thisObj;
        }

        /// <inheritdoc/>
        public Card GetCardOrThrow()
        {
            var card = this.Card;
            if (card is null)
            {
                throw new InvalidOperationException("Card isn't specified.");
            }

            return card;
        }

        /// <inheritdoc/>
        public virtual T CreateOrLoadSingleton()
        {
            this.AddPendingAction(
                new PendingAction(
                    nameof(CardLifecycleCompanion) + "." + nameof(CreateOrLoadSingleton),
                    this.CreateOrLoadSingletonAsync));

            return this.thisObj;
        }

        #endregion

        #region Protected Methods

        /// <inheritdoc/>
        protected override async ValueTask<T> GoCoreAsync(
            Action<ValidationResult> validationFunc = default,
            CancellationToken cancellationToken = default)
        {
            await using (this.Dependencies.DbScope.Create())
            {
                return await base.GoCoreAsync(validationFunc: validationFunc, cancellationToken: cancellationToken);
            }
        }

        /// <summary>
        /// Проверяет, что <see cref="Card"/> имеет значение <see langword="null"/>, если это не так, то создаёт исключение <see cref="InvalidOperationException"/>.
        /// </summary>
        /// <exception cref="InvalidOperationException">Card already specified.</exception>
        protected void CheckCardNotExists()
        {
            if (this.Card != null)
            {
                throw new InvalidOperationException("Card already specified.");
            }
        }

        #endregion

        #region Private methods

        private async ValueTask<ValidationResult> CreateActionAsync(
            Dictionary<string, object> info,
            CancellationToken cancellationToken = default)
        {
            this.CheckCardNotExists();

            var newRequest = new CardNewRequest
            {
                CardTypeID = this.CardTypeID,
                Info = info,
            };

            this.Dependencies.RequestExtender?.ExtendNewRequest(newRequest);

            this.lastData.NewRequest = newRequest;
            this.lastData.NewResponse = default;

            var newResponse = await this.Dependencies.CardRepository.NewAsync(newRequest, cancellationToken);
            this.lastData.NewResponse = newResponse;

            if (newResponse.ValidationResult.IsSuccessful())
            {
                this.Card = newResponse.Card;

                if (newResponse.Card.ID == Guid.Empty)
                {
                    if (this.CardID != Guid.Empty)
                    {
                        newResponse.Card.ID = this.CardID;
                    }
                }
                else
                {
                    this.CardID = this.Card.ID;
                }

                this.CardTypeID = this.Card.TypeID;
                this.CardTypeName = this.Card.TypeName;
            }
            else
            {
                this.Card = default;
            }

            return newResponse.ValidationResult.Build();
        }

        private ValueTask<ValidationResult> CreateActionAsync(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            return this.CreateActionAsync(action.Info, cancellationToken: cancellationToken);
        }

        private async ValueTask<ValidationResult> SaveActionAsync(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            var card = this.GetCardOrThrow();

            if (this.Dependencies.ServerSide)
            {
                if (card.Sections.ContainsKey(KrConstants.KrStages.Virtual))
                {
                    await this.visitor.VisitAsync(
                        card.Sections,
                        DefaultCardTypes.KrCardTypeID,
                        KrConstants.KrStages.Virtual,
                        cancellationToken: cancellationToken);
                }
                card.RemoveAllButChanged(card.StoreMode);
            }

            var storeRequest = new CardStoreRequest
            {
                Card = card,
                Info = action.Info,
            };

            this.Dependencies.RequestExtender?.ExtendStoreRequest(storeRequest);

            this.lastData.StoreRequest = storeRequest;
            this.lastData.StoreResponse = default;

            CardStoreResponse storeResponse;
            if (this.cardFileContainer is null)
            {
                storeResponse = await this.Dependencies.CardRepository.StoreAsync(storeRequest, cancellationToken);
            }
            else
            {
                if (this.Dependencies.ServerSide)
                {
                    storeResponse = await CardHelper.StoreAsync(
                        storeRequest,
                        this.cardFileContainer?.FileContainer,
                        this.Dependencies.CardRepository,
                        this.Dependencies.CardStreamServerRepository,
                        cancellationToken: cancellationToken);
                }
                else
                {
                    storeResponse = await CardHelper.StoreAsync(
                        storeRequest,
                        this.cardFileContainer?.FileContainer,
                        this.Dependencies.CardRepository,
                        this.Dependencies.CardStreamClientRepository,
                        cancellationToken: cancellationToken);
                }

                if (this.cardFileContainer != null)
                {
                    await this.cardFileContainer.DisposeAsync();
                    this.cardFileContainer = default;
                }
            }

            this.lastData.StoreResponse = storeResponse;

            card.RemoveWorkflowQueue();
            card.RemoveNumberQueue();

            return storeResponse.ValidationResult.IsSuccessful()
                ? new ValidationResultBuilder { storeResponse.ValidationResult, await this.LoadInternalAsync(default(Dictionary<string, object>), cancellationToken) }.Build()
                : storeResponse.ValidationResult.Build();
        }

        private async ValueTask<ValidationResult> DeleteActionAsync(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            var deleteRequest = new CardDeleteRequest
            {
                CardID = this.CardID,
                CardTypeID = this.CardTypeID,
                Info = action.Info,
                DeletionMode = CardDeletionMode.WithoutBackup,
            };

            this.Dependencies.RequestExtender?.ExtendDeleteRequest(deleteRequest);
            
            this.lastData.DeleteRequest = deleteRequest;
            this.lastData.DeleteResponse = default;

            var deleteResponse = await this.Dependencies.CardRepository.DeleteAsync(deleteRequest, cancellationToken);
            this.lastData.DeleteResponse = deleteResponse;

            var validationResult = deleteResponse.ValidationResult.Build();
            if (validationResult.IsSuccessful)
            {
                this.Card = default;
                this.cardFileContainer = default;
            }

            return validationResult;
        }

        private async Task<IValidationResultBuilder> LoadInternalAsync(
            Dictionary<string, object> info,
            CancellationToken cancellationToken = default)
        {
            var getRequest = new CardGetRequest
            {
                CardID = this.CardID,
                CardTypeID = this.CardTypeID,
                CompressionMode = CardCompressionMode.None,
                GetTaskMode = CardGetTaskMode.All,
            };

            if (info != null)
            {
                getRequest.Info = info;
            }

            this.Dependencies.RequestExtender?.ExtendGetRequest(getRequest);

            this.lastData.GetRequest = getRequest;
            this.lastData.GetResponse = default;

            var getResponse = await this.Dependencies.CardRepository.GetAsync(getRequest, cancellationToken);
            this.lastData.GetResponse = getResponse;

            this.Card = getResponse.ValidationResult.IsSuccessful()
                ? getResponse.Card
                : default;
            this.cardFileContainer = default;

            return getResponse.ValidationResult;
        }

        private async ValueTask<ValidationResult> LoadInternalAsync(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            return (await this.LoadInternalAsync(action.Info, cancellationToken)).Build();
        }

        private async ValueTask<ValidationResult> CreateOrLoadSingletonAsync(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            this.CheckCardNotExists();

            CardCacheValue<Card> value = await this.Dependencies.CardCache.Cards
                .GetAsync(this.CardTypeName, cancellationToken: cancellationToken);

            if (value.Result == CardCacheResult.SingletonNotFound)
            {
                if (value.ValidationResult.Items.Any(i =>
                    i.Type == ValidationResultType.Error
                    && i.Key != CardValidationKeys.UnknownSingleton))
                {
                    this.Card = default;
                    value.GetValue(); // throw
                }

                return await this.CreateActionAsync(default(Dictionary<string, object>), cancellationToken: cancellationToken);
            }

            this.Card = value.GetValue().Clone();
            this.CardID = this.Card.ID;

            return ValidationResult.Empty;
        }

        /// <summary>
        /// Возвращает строковое представление объекта отображаемое в окне отладчика.
        /// </summary>
        /// <returns>Строковое представление объекта отображаемое в окне отладчика.</returns>
        private string GetDebuggerDisplay()
        {
            return $"{nameof(this.CardID)} = {this.CardID:B}, {nameof(this.CardTypeID)} = {this.CardTypeID:B}, {nameof(this.CardTypeName)} = {this.CardTypeName}, CardIsSet = {this.Card != null}";
        }

        #endregion

    }
}