﻿using System;
using System.Collections.Generic;
using System.Threading;
using Tessa.Cards;
using Tessa.Platform.Validation;

namespace Tessa.Test.Default.Shared.Kr
{
    /// <summary>
    /// Описывает объект управляющий жизненным циклом карточки.
    /// </summary>
    /// <typeparam name="T">Тип объекта реализующего данный интерфейс.</typeparam>
    public interface ICardLifecycleCompanion<T> :
        IPendingActionsProvider<T>,
        ICardLifecycleCompanion where T : ICardLifecycleCompanion<T>
    {
        #region Properties

        /// <summary>
        /// Возвращает информацию по последним запросам и ответам на них выполненным этим объектом.
        /// </summary>
        ICardLifecycleCompanionData LastData { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Создаёт карточку типа <see cref="ICardLifecycleCompanion.CardTypeID"/>.
        /// </summary>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на создание карточки.
        /// </remarks>
        T Create();

        /// <summary>
        /// Cохраняет карточку <see cref="Card"/>.
        /// </summary>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на сохранение карточки.
        /// </remarks>
        T Save();

        /// <summary>
        /// Загружает карточку.
        /// </summary>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на получение карточки.
        /// </remarks>
        T Load();

        /// <summary>
        /// Удаляет карточку.
        /// </summary>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на удаление карточки.
        /// </remarks>
        T Delete();

        /// <summary>
        /// Добавляет указанную пару ключ-значение в дополнительную информацию (Info) передаваемую при выполнении последнего добавленного действия.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <param name="val">Значение.</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        T WithInfoPair(string key, object val);

        /// <summary>
        /// Устанавливает указанный словарь в качестве дополнительной информации (Info) для последнего добавленного действия.
        /// </summary>
        /// <param name="info">Дополнительная информация. Если для действия уже была задана дополнительная информация, то она будет объединена с указанной (см. <see cref="Tessa.Platform.Storage.StorageHelper.Merge(IDictionary{string, object}, IDictionary{string, object})"/>).</param>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        T WithInfo(Dictionary<string, object> info);

        /// <summary>
        /// Создаёт новую или загружает существующую карточку синглтон.
        /// </summary>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        T CreateOrLoadSingleton();

        #endregion
    }
}