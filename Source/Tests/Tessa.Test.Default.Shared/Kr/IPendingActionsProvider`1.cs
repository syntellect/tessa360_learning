﻿using System;
using Tessa.Platform;

namespace Tessa.Test.Default.Shared.Kr
{
    /// <summary>
    /// Описывает объект предоставляющий действия с отложенным выполнением.
    /// </summary>
    /// <typeparam name="T">Тип объекта реализующего интерфейс.</typeparam>
    public interface IPendingActionsProvider<T> :
        IPendingActionsExecutor<T>,
        ISealable
        where T : IPendingActionsProvider<T>
    {
        #region Properties

        /// <summary>
        /// Возвращает значение, показывающее, наличие запланированные отложенных действий.
        /// </summary>
        bool HasPendingActions { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Добавляет указанное отложенное действие в список запланированных действий.
        /// </summary>
        /// <param name="pendingAction">Отложенное действие.</param>
        /// <exception cref="ObjectSealedException">Произведена попытка изменения объекта, защищённого от изменений.</exception>
        void AddPendingAction(PendingAction pendingAction);

        /// <summary>
        /// Возвращает последнее добавленное отложенное действие.
        /// </summary>
        /// <returns>Последнее добавленное отложенное действие.</returns>
        /// <exception cref="InvalidOperationException">Запланированные отложенные действия отсутствуют.</exception>
        PendingAction GetLastPendingAction();

        #endregion
    }
}