﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Storage;

namespace Tessa.Test.Default.Shared.Kr
{
    /// <summary>
    /// Предоставляет статические методы для выражения утверждений.
    /// </summary>
    /// <seealso href="https://github.com/nunit/docs/wiki/Assertions">NUnit assertions</seealso>
    public static class KrAssert
    {
        #region Static Fields

        /// <inheritdoc cref="KrConstants.KrTaskTypeIDList"/>
        /// <remarks>Оптимизация, для использования при проверке условий.</remarks>
        private static readonly object[] KrTaskTypeIDObjList = KrConstants.KrTaskTypeIDList.Cast<object>().ToArray();

        #endregion

        #region Public Methods

        /// <summary>
        /// Проверяет, что в карточке, которой управляет указанный объект, есть хотя бы один этап находящийся в состоянии <see cref="KrStageState.Active"/>.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        public static void IsAtLeastOneStageActive(
            ICardLifecycleCompanion clc)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            IsAtLeastOneStageActive(clc.GetCardOrThrow());
        }

        /// <summary>
        /// Проверяет, что в карточке есть хотя бы один этап находящийся в состоянии <see cref="KrStageState.Active"/>.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        public static void IsAtLeastOneStageActive(Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            var states = card
                .GetStagesSection()
                .Rows
                .Select(p => p.Get<int>(KrConstants.KrStages.StageStateID))
                .ToArray();

            Assert.That(states, Has.Some.EqualTo(KrStageState.Active.ID));
        }

        /// <summary>
        /// Проверяет, что в карточке, которой управляет указанный объект, ни один из этапов не находится в состоянии <see cref="KrStageState.Active"/>.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        public static void IsNoneStageActive(
            ICardLifecycleCompanion clc)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            IsNoneStageActive(clc.GetCardOrThrow());
        }

        /// <summary>
        /// Проверяет, что в карточке ни один из этапов не находится в состоянии <see cref="KrStageState.Active"/>.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        public static void IsNoneStageActive(Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            var states = card
                .GetStagesSection()
                .Rows
                .Select(p => p.Get<int>(KrConstants.KrStages.StageStateID))
                .ToArray();

            Assert.That(states, Has.None.EqualTo(KrStageState.Active.ID));
        }

        /// <summary>
        /// Проверяет, что этап, информация о котором содержится в указанной строке, имеет заданное состояние.
        /// </summary>
        /// <param name="row">Строка, содержащая информацию об этапе. Строка должна соотвествовать строке секции <see cref="KrConstants.KrStages.Name"/>.</param>
        /// <param name="expectedState">Ожидаемое состоние этапа.</param>
        public static void StateIs(
            CardRow row,
            KrStageState expectedState)
        {
            Check.ArgumentNotNull(row, nameof(row));

            var state = (KrStageState?) row.TryGet<int?>(KrConstants.StateID) ?? KrStageState.Inactive;

            Assert.That(
                state,
                Is.EqualTo(expectedState),
                () => $"Expected: {expectedState.TryGetDefaultName()}{Environment.NewLine}"
                + $"But got: {state.TryGetDefaultName()}");
        }

        /// <summary>
        /// Проверяет, что карточка имеет заданное состояние.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        /// <param name="expectedState">Ожидаемое состояние проверяемой карточки.</param>
        public static void StateIs(
            Card card,
            KrState expectedState)
        {
            Check.ArgumentNotNull(card, nameof(card));

            var aci = card.GetApprovalInfoSection();
            var state = (KrState?) aci.RawFields.TryGet<int?>(KrConstants.StateID) ?? KrState.Draft;

            const string defaultNameStr = "<The card state does not have a default name. See ID.>";

            Assert.That(
                state,
                Is.EqualTo(expectedState),
                () => $"Expected: {expectedState.TryGetDefaultName() ?? defaultNameStr}{Environment.NewLine}"
                + $"But got: {state.TryGetDefaultName() ?? defaultNameStr}");
        }

        /// <summary>
        /// Проверяет, что карточка, которой управляет указанный объект, имеет заданное состояние.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="expectedState">Ожидаемое состояние проверяемой карточки.</param>
        public static void StateIs(
            ICardLifecycleCompanion clc,
            KrState expectedState)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            StateIs(clc.GetCardOrThrow(), expectedState);
        }

        /// <summary>
        /// Проверяет, что этап, информация о котором содержится в указанной строке, имеет заданный тип.
        /// </summary>
        /// <param name="row">Строка, содержащая информацию об этапе. Строка должна соотвествовать строке секции <see cref="KrConstants.KrStages.Name"/>.</param>
        /// <param name="descriptor">Дескриптор типа этапа.</param>
        public static void StageTypeIs(
            CardRow row,
            StageTypeDescriptor descriptor)
        {
            Check.ArgumentNotNull(row, nameof(row));

            Assert.That(
                row[KrConstants.KrStages.StageTypeID],
                Is.EqualTo(descriptor.ID),
                "Expected: {0} ({1}){2}But got: {3} ({4})",
                descriptor.ID,
                descriptor.Caption,
                Environment.NewLine,
                row[KrConstants.KrStages.StageTypeID],
                row[KrConstants.KrStages.StageTypeCaption]);
        }

        /// <summary>
        /// Проверяет, что указанная карточка содержит хотя бы одно задание отправленное типовым процессом согласования.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        public static void HasKrProcessTasks(
            Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            Assert.That(
                card.TryGetTasks()?.Select(p => p.TypeID),
                Is.Not.Null.And.Not.Empty.And.Some.AnyOf(KrTaskTypeIDObjList),
                nameof(Card) + "." + nameof(Card.Tasks) + " does not contain any task with type, assotiated with kr process. " +
                "See " + nameof(KrConstants) + "." + nameof(KrConstants.KrTaskTypeIDList) + ".");
        }

        /// <summary>
        /// Проверяет, что указанная карточка содержит хотя бы одно задание отправленное типовым процессом согласования.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        public static void HasKrProcessTasks(
            ICardLifecycleCompanion clc)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            HasKrProcessTasks(clc.GetCardOrThrow());
        }

        /// <summary>
        /// Проверяет, что указанная карточка не содержит заданий отправленных типовым процессом согласования.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        public static void HasNoKrProcessTasks(
            Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            Assert.That(
                card.TryGetTasks()?.Select(p => p.TypeID),
                Is.Null.Or.Empty.Or.Not.Some.AnyOf(KrTaskTypeIDObjList),
                () => nameof(Card) + "." + nameof(Card.Tasks) + " contain task with type, assotiated with kr process. "
                + "See " + nameof(KrConstants) + "." + nameof(KrConstants.KrTaskTypeIDList) + "." + Environment.NewLine
                + "Identifiers of task types that violate the condition: "
                + string.Join(", ", card.Tasks.Select(p => p.TypeID).Intersect(KrConstants.KrTaskTypeIDList)));
        }

        /// <summary>
        /// Проверяет, что карточка, которой управляет указанный объект, не содержит заданий отправленных типовым процессом согласования.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        public static void HasNoKrProcessTasks(
            ICardLifecycleCompanion clc)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            HasNoKrProcessTasks(clc.GetCardOrThrow());
        }

        /// <summary>
        /// Проверяет, что маршрут, в указанной карточке, содержит хотя бы один активный этап заданного типа.
        /// </summary>
        /// <param name="card">Карточка, содержащая проверяемый маршрут.</param>
        /// <param name="descriptor">Дескриптор типа этапа.</param>
        public static void IsStageTypeActive(
            Card card,
            StageTypeDescriptor descriptor)
        {
            // Проверка корректности значения card выполняется в GetStagesSection.

            var stages = card.GetStagesSection();
            var row = stages
                .Rows
                .FirstOrDefault(p => p[KrConstants.KrStages.StageStateID] as int? == KrStageState.Active.ID);

            Assert.That(row, Is.Not.Null, "Route has no active stage.");

            var actualStageTypeID = row[KrConstants.KrStages.StageTypeID] as Guid?;

            Assert.That(
                actualStageTypeID,
                Is.EqualTo(descriptor.ID),
                () => $"Expected stage type id is \"{descriptor.Caption}\" but got \"{row[KrConstants.KrStages.StageTypeCaption]}\".");
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке, которой управляет указанный объект, содержит хотя бы один активный этап заданного типа.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="descriptor">Дескриптор типа этапа.</param>
        public static void IsStageTypeActive(
            ICardLifecycleCompanion clc,
            StageTypeDescriptor descriptor)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            IsStageTypeActive(clc.GetCardOrThrow(), descriptor);
        }

        /// <summary>
        /// Проверяет, что маршрут, в указанной карточке, содержит хотя бы один активный этап с заданным именем.
        /// </summary>
        /// <param name="card">Карточка, содержащая проверяемый маршрут.</param>
        /// <param name="stageName">Имя этапа.</param>
        public static void IsStageActive(
            Card card,
            string stageName)
        {
            // Проверка корректности значения card выполняется в GetStagesSection.

            var stages = card.GetStagesSection();
            var rows = stages
                .Rows
                .Where(p => p[KrConstants.KrStages.StageStateID] as int? == KrStageState.Active.ID);

            var row = rows.FirstOrDefault(p => string.Equals(p.Get<string>(KrConstants.Name), stageName, StringComparison.Ordinal));

            Assert.That(
                row,
                Is.Not.Null,
                () => $"Route has no active stage with name \"{stageName}\".{Environment.NewLine}"
                + $"Active stages:{Environment.NewLine}"
                + $"{string.Join(Environment.NewLine, rows.Select(p => $"\"{p.Get<string>(KrConstants.Name)}\""))}");
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке, которой управляет указанный объект, содержит хотя бы один активный этап с заданным именем.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="stageName">Имя этапа.</param>
        public static void IsStageActive(
            ICardLifecycleCompanion clc,
            string stageName)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            IsStageActive(clc.GetCardOrThrow(), stageName);
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке, которой управляет указанный объект, содержит хотя бы один этап имеющий заданное имя и состояние.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="stageName">Имя этапа.</param>
        /// <param name="state">Состоние этапа.</param>
        public static void StageHasState(
            ICardLifecycleCompanion clc,
            string stageName,
            KrStageState state)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            StageHasState(clc.GetCardOrThrow(), stageName, state);
        }

        /// <summary>
        /// Проверяет, что маршрут в указанной карточке содержит хотя бы один этап имеющий заданное имя и состояние.
        /// </summary>
        /// <param name="card">Карточка, содержащая маршрут с искомым этапом.</param>
        /// <param name="stageName">Имя этапа.</param>
        /// <param name="state">Состоние этапа.</param>
        public static void StageHasState(
            Card card,
            string stageName,
            KrStageState state)
        {
            // Проверка корректности значения card выполняется в GetStagesSection.

            var stages = card.GetStagesSection();
            var row = stages
                .Rows
                .FirstOrDefault(p => string.Equals(p.Get<string>(KrConstants.Name), stageName, StringComparison.Ordinal));

            Assert.That(row, Is.Not.Null, "Route has no stage with name \"{0}\".", stageName);

            Assert.That(
                row[KrConstants.KrStages.StageStateID],
                Is.EqualTo(state.ID),
                () => $"Expected state ID is \"{state.ID}\" but got \"{row[KrConstants.KrStages.StageStateID]}\".");
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке содержит один этап с указанным именем.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        /// <param name="name">Имя этапа.</param>
        public static void HasStage(
            Card card,
            string name)
        {
            Check.ArgumentNotNull(card, nameof(card));

            Assert.That(
                card.GetStagesSection().Rows.Select(r => r.Get<string>(KrConstants.Name)).ToArray(),
                Has.One.EqualTo(name));
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке, которой управляет указанный объект, содержит один этап с указанным именем.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="name">Имя этапа.</param>
        public static void HasStage(
            ICardLifecycleCompanion clc,
            string name)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            HasStage(clc.GetCardOrThrow(), name);
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке не содержит этапа с указанным именем.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        /// <param name="name">Имя этапа.</param>
        public static void HasNoStage(
            Card card,
            string name)
        {
            Check.ArgumentNotNull(card, nameof(card));

            Assert.That(
                card.GetStagesSection().Rows.Select(r => r.Get<string>(KrConstants.Name)).ToArray(),
                Has.None.EqualTo(name));
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке, которой управляет указанный объект, не содержит этапа с указанным именем.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="name">Имя этапа.</param>
        public static void HasNoStage(
            ICardLifecycleCompanion clc,
            string name)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            HasNoStage(clc.GetCardOrThrow(), name);
        }

        /// <summary>
        /// Проверяет наличие в карточке задания указанного типа.
        /// </summary>
        /// <param name="clc">Объект управляющий жизненным циклом проверяемой карточки.</param>
        /// <param name="typeID">Тип задания.</param>
        /// <param name="exactly">Число ожидаемых экземпляров заданий. Если не задано, то проверка считаеся успешной, если карточка содержит хотя бы одно задание указанного типа.</param>
        /// <param name="message">Сообщение отображаемое при не выполнении проверки.</param>
        public static void HasTask(
            ICardLifecycleCompanion clc,
            Guid typeID,
            int? exactly = null,
            string message = default)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            HasTask(clc.GetCardOrThrow(), typeID, exactly, message);
        }

        /// <summary>
        /// Проверяет наличие в карточке задания указанного типа.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        /// <param name="typeID">Тип задания.</param>
        /// <param name="exactly">Число ожидаемых заданий. Если не задано, то проверка считаеся успешной, если карточка содержит хотя бы одно задание указанного типа.</param>
        /// <param name="message">Сообщение отображаемое при не выполнении проверки.</param>
        public static void HasTask(
            Card card,
            Guid typeID,
            int? exactly = default,
            string message = default)
        {
            Check.ArgumentNotNull(card, nameof(card));

            if (exactly.HasValue)
            {
                Assert.That(
                    card.Tasks.Select(p => p.TypeID),
                    Has.Exactly(exactly.Value).EqualTo(typeID),
                    message);
            }
            else
            {
                Assert.That(
                    card.Tasks.Select(p => p.TypeID),
                    Has.Some.EqualTo(typeID),
                    message);
            }
        }

        /// <summary>
        /// Проверяет, что в карточке не содержится заданий указанного типа.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        /// <param name="typeID">Тип задания, которого не должно быть в каточке.</param>
        public static void HasNoTask(
            Card card,
            Guid typeID)
        {
            Check.ArgumentNotNull(card, nameof(card));

            Assert.That(
                card.Tasks.Select(p => p.TypeID),
                Has.None.EqualTo(typeID));
        }

        /// <summary>
        /// Проверяет, что в карточке, которой управляет указанный объект, не содержится заданий указанного типа.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="typeID">Тип задания, которого не должно быть в каточке.</param>
        public static void HasNoTask(
            ICardLifecycleCompanion clc,
            Guid typeID)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            HasNoTask(clc.GetCardOrThrow(), typeID);
        }

        /// <summary>
        /// Проверяет, что в карточке не содержится заданий.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        public static void HasNoTask(
            Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            Assert.That(
                card.TryGetTasks(),
                Is.Null.Or.Empty,
                nameof(Card) + "." + nameof(Card.Tasks) + " contains tasks.");
        }

        /// <summary>
        /// Проверяет, что в карточке, которой управляет указанный объект, не содержится заданий.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        public static void HasNoTask(
            ICardLifecycleCompanion clc)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            HasNoTask(clc.GetCardOrThrow());
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке содержит этап с указанным названием и порядковым номером.
        /// </summary>
        /// <param name="card">Поверяемая карточка.</param>
        /// <param name="name">Имя этапа.</param>
        /// <param name="order">Порядковый номер этапа.</param>
        public static void StageHasOrder(
            Card card,
            string name,
            int order)
        {
            Check.ArgumentNotNull(card, nameof(card));

            var rows = card
                .GetStagesSection()
                .Rows;

            var stage = rows.FirstOrDefault(p => string.Equals(p.Fields.Get<string>(KrConstants.Name), name, StringComparison.Ordinal));

            Assert.That(stage, Is.Not.Null, () => $"Route doesn't contain stage \"{name}\".{Environment.NewLine}{GetStageRowsStr(rows)}");

            var actual = stage.Fields.Get<int>(KrConstants.Order);

            Assert.That(
                actual,
                Is.EqualTo(order),
                () => $"Stage \"name\" has {actual} order actual but expected {order}.{Environment.NewLine}{GetStageRowsStr(rows)}");
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке, которой управляет указанный объект, содержит этап с указанным названием и порядковым номером.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="name">Имя этапа.</param>
        /// <param name="order">Порядковый номер этапа.</param>
        public static void StageHasOrder(
            ICardLifecycleCompanion clc,
            string name,
            int order)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            StageHasOrder(clc.GetCardOrThrow(), name, order);
        }

        /// <summary>
        /// Проверяет, что в карточке маршрут содержит приведенные этапы в указанном порядке.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        /// <param name="stageNames">Имена этапов, перечисленные в порядке в котором они должны быть в маршруте.</param>
        public static void SequenceOfStagesIs(
            Card card,
            params string[] stageNames)
        {
            Check.ArgumentNotNull(card, nameof(card));
            Check.ArgumentNotNull(stageNames, nameof(stageNames));

            for (var i = 0; i < stageNames.Length; i++)
            {
                StageHasOrder(card, stageNames[i], i);
            }
        }

        /// <summary>
        /// Проверяет, что в карточке, которой управляет указанный объект, маршрут содержит приведенные этапы в указанном порядке.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="stageNames">Имена этапов, перечисленные в порядке в котором они должны быть в маршруте.</param>
        public static void SequenceOfStagesIs(
            ICardLifecycleCompanion clc,
            params string[] stageNames)
        {
            Check.ArgumentNotNull(clc, nameof(clc));
            Check.ArgumentNotNull(stageNames, nameof(stageNames));

            SequenceOfStagesIs(clc.GetCardOrThrow(), stageNames);
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке содержит указанное число видимых этапов.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        /// <param name="expectedCnt">Ожидаемое число этапов.</param>
        public static void VisibleStagesCount(
            Card card,
            int expectedCnt)
        {
            Check.ArgumentNotNull(card, nameof(card));

            var rows = card
                .GetStagesSection()
                .Rows;

            var cnt = rows.Count;

            Assert.That(
                cnt,
                Is.EqualTo(expectedCnt),
                () => $"Visible stages: {Environment.NewLine}{GetStageRowsStr(rows)}");
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке, которой управляет указанный объект, содержит указанное число видимых этапов.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="expectedCnt">Ожидаемое число этапов.</param>
        public static void VisibleStagesCount(
            ICardLifecycleCompanion clc,
            int expectedCnt)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            VisibleStagesCount(clc.GetCardOrThrow(), expectedCnt);
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке содержит указанное число этапов.
        /// </summary>
        /// <param name="card">Проверяемая карточка.</param>
        /// <param name="expectedCnt">Ожидаемое число этапов.</param>
        public static void StagesCount(
            Card card,
            int expectedCnt)
        {
            Check.ArgumentNotNull(card, nameof(card));

            Assert.That(card.GetStagePositions(), Has.Count.EqualTo(expectedCnt));
        }

        /// <summary>
        /// Проверяет, что маршрут в карточке, которой управляет указанный объект, содержит указанное число этапов.
        /// </summary>
        /// <param name="clc">Объект, содержащий проверяемую карточку.</param>
        /// <param name="expectedCnt">Ожидаемое число этапов.</param>
        public static void StagesCount(
            ICardLifecycleCompanion clc,
            int expectedCnt)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            StagesCount(clc.GetCardOrThrow(), expectedCnt);
        }

        /// <summary>
        /// Проверяет, что по карточке, с указанным идентификатором, есть хотя бы один запущенный типовой процесс согласования указанного типа.
        /// </summary>
        /// <param name="dbScope">Объект для взаимодействия с базой данных.</param>
        /// <param name="mainCardID">Идентификатор проверяемой карточки.</param>
        /// <param name="processType">Тип проверяемоего процесса. Если не указано значение по умолчанию для типа, то проверяется наличие любого процесса по карточке с указанным идентификатором.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>\
        /// <returns>Асинхронная задача.</returns>
        public static async Task HasWorkflowProcessAsync(
            IDbScope dbScope,
            Guid mainCardID,
            string processType = default,
            CancellationToken cancellationToken = default)
        {
            // Параметр dbScope будет проверен в GetWorkflowProcessAsync.

            var workflowProcesses = await KrTestHelper.GetWorkflowProcessAsync(dbScope, mainCardID, cancellationToken);
            var expression = processType is null ? (IResolveConstraint) Is.Not.Empty : Is.Not.Not.Contains(processType);

            Assert.That(workflowProcesses, expression, "Card with ID = {0:B} has no workflow API process.", mainCardID);
        }

        /// <summary>
        /// Проверяет, что по карточке, с указанным идентификатором, нет запущенных процессов.
        /// </summary>
        /// <param name="dbScope">Объект для взаимодействия с базой данных.</param>
        /// <param name="mainCardID">Идентификатор проверяемой карточки.</param>
        /// <param name="processType">Тип проверяемоего процесса или значение <see langword="null"/>, если по карточке не должно быть запущено никаких процессов.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Асинхронная задача.</returns>
        public static async Task HasNoWorkflowProcessAsync(
            IDbScope dbScope,
            Guid mainCardID,
            string processType = default,
            CancellationToken cancellationToken = default)
        {
            // Параметр dbScope будет проверен в GetWorkflowProcessAsync.

            var workflowProcesses = await KrTestHelper.GetWorkflowProcessAsync(dbScope, mainCardID, cancellationToken);
            var expression = processType is null ? (IResolveConstraint) Is.Empty : Is.Not.Contains(processType);

            Assert.That(workflowProcesses, expression, "Card with ID = {0:B} has workflow API process.", mainCardID);
        }

        #endregion

        #region Private Methods

        private static string GetStageRowsStr(ICollection<CardRow> rows) =>
            rows.Count > 0 ? string.Join(Environment.NewLine, rows.Select(GetStageRowStr)) : "<EMPTY>";

        private static string GetStageRowStr(CardRow row)
        {
            return
                $"RowState = {row.State}, " +
                $"RowID = {DebugHelper.FormatNullable(row.TryGetRowID()?.ToString("B"))}, " +
                $"State = {row.TryGet<string>(KrConstants.KrStages.StageStateName)}, " +
                $"Name = \"{row.TryGet<string>(KrConstants.Name)}\", " +
                $"TemplateName = \"{DebugHelper.FormatNullable(row.TryGet<string>(KrConstants.KrStages.BasedOnStageTemplateName))}\"";
        }

        #endregion
    }
}
