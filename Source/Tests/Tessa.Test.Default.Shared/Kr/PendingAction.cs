﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Platform;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;

namespace Tessa.Test.Default.Shared.Kr
{
    /// <summary>
    /// Предоставляет информацию об отложенном действии.
    /// </summary>
    [DebuggerDisplay("Name = {" + nameof(Name) + "}, Preparation actions count = {" + nameof(PreparationActions) + ".Count}")]
    public sealed class PendingAction :
        ISealable
    {
        #region Fields

        /// <summary>
        /// Метод реализующий действие.
        /// </summary>
        private readonly Func<PendingAction, CancellationToken, ValueTask<ValidationResult>> actionAsync;

        private readonly Collection<PendingAction> preparationActions = new Collection<PendingAction>();

        private readonly Collection<PendingAction> afterActions = new Collection<PendingAction>();

        private Dictionary<string, object> info;

        #endregion

        #region Properties

        /// <summary>
        /// Возвращает название отложенного действия.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Возвращает список действий выполняющихся перед выполнением отложенного действия.
        /// </summary>
        public IReadOnlyCollection<PendingAction> PreparationActions { get; }

        /// <summary>
        /// Возвращает список действий выполняющихся после выполнения отложенного действия.
        /// </summary>
        public IReadOnlyCollection<PendingAction> AfterActions { get; }

        /// <summary>
        /// Возвращает дополнительную информацию. Возвращаемое значение всегда не равно <see langword="null"/>.
        /// </summary>
        public Dictionary<string, object> Info
        {
            get => this.info ??= new Dictionary<string, object>(StringComparer.Ordinal);
            private set => this.info = value;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="PendingAction"/>.
        /// </summary>
        /// <param name="name">Название действия.</param>
        /// <param name="actionAsync">Метод реализующий действие.</param>
        public PendingAction(
            string name,
            Func<PendingAction, CancellationToken, ValueTask<ValidationResult>> actionAsync)
        {
            Check.ArgumentNotNullOrEmpty(name, nameof(name));
            Check.ArgumentNotNull(actionAsync, nameof(actionAsync));

            this.Name = name;
            this.actionAsync = actionAsync;

            this.PreparationActions = new ReadOnlyCollection<PendingAction>(this.preparationActions);
            this.AfterActions = new ReadOnlyCollection<PendingAction>(this.afterActions);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Выполняет отложенное действие и подготовительные действия передавая текущий экземпляр в качестве первого параметра метода реализующего действие.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Результат выполнения.</returns>
        /// <remarks>
        /// При возникновении ошибки, при обработке подготовительных действий, дальнейшее выполнение прекращается, основное действие не выполняется.<para/>
        /// Результаты выполнения предварительных действий и основного действия, если они есть, предваряются информационными сообщениями. Для их исключения, на итоговом результате выполнения, необходимо вызвать метод <see cref="TestValidationKeys.ExceptPendingActionValidationResult(IReadOnlyCollection{IValidationResultItem})"/>.
        /// </remarks>
        public async ValueTask<ValidationResult> ExecuteAsync(
            CancellationToken cancellationToken = default)
        {
            this.Seal();

            var validationResults = new ValidationResultBuilder();

            await ExecuteAdditionalActions(
                this.PreparationActions,
                validationResults,
                TestValidationKeys.PendingActionPreparationActionMessages,
                TestValidationKeys.PendingActionPreparationActionException,
                cancellationToken);

            if (validationResults.IsSuccessful())
            {
                try
                {
                    var validationResult = await this.actionAsync(this, cancellationToken);

                    if (validationResult?.Items.Count > 0)
                    {
                        ValidationSequence
                            .Begin(validationResults)
                            .SetObjectName(this)
                            .InfoText(
                                TestValidationKeys.PendingActionMessages,
                                string.Format(
                                    TestValidationKeys.PendingActionMessages.Message,
                                    this.Name))
                            .End();

                        validationResults.Add(validationResult);
                    }
                }
                catch (OperationCanceledException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    ValidationSequence
                        .Begin(validationResults)
                        .SetObjectName(this)
                        .ErrorDetails(
                            TestValidationKeys.PendingActionException,
                            string.Format(
                                TestValidationKeys.PendingActionException.Message,
                                this.Name),
                            e)
                        .End();
                }
            }

            if (validationResults.IsSuccessful())
            {
                await ExecuteAdditionalActions(
                    this.AfterActions,
                    validationResults,
                    TestValidationKeys.PendingActionAfterActionMessages,
                    TestValidationKeys.PendingActionAfterActionException,
                    cancellationToken);
            }

            this.IsSealed = false;

            return validationResults.Build();
        }

        private async Task ExecuteAdditionalActions(
            IEnumerable<PendingAction> pendingActions,
            ValidationResultBuilder validationResults,
            ValidationKey pendingActionMessages,
            ValidationKey pendingActionException,
            CancellationToken cancellationToken = default)
        {
            foreach (var pendingAction in pendingActions)
            {
                try
                {
                    var validationResult = await pendingAction.ExecuteAsync(cancellationToken: cancellationToken);

                    if (validationResult?.Items.Count > 0)
                    {
                        ValidationSequence
                            .Begin(validationResults)
                            .SetObjectName(this)
                            .InfoText(
                                pendingActionMessages,
                                string.Format(
                                    pendingActionMessages.Message,
                                    pendingAction.Name,
                                    this.Name))
                            .End();

                        validationResults.Add(validationResult);

                        if (!validationResult.IsSuccessful)
                        {
                            break;
                        }
                    }
                }
                catch (OperationCanceledException)
                {
                    throw;
                }
                catch (Exception e)
                {
                    ValidationSequence
                        .Begin(validationResults)
                        .SetObjectName(this)
                        .ErrorDetails(
                            pendingActionException,
                            string.Format(
                                pendingActionException.Message,
                                pendingAction.Name,
                                this.Name),
                            e)
                        .End();
                }
            }
        }

        /// <summary>
        /// Устанавливает значение свойства <see cref="Info"/>.
        /// </summary>
        /// <param name="dict">Словарь, содержащий дополнительную информацию.</param>
        /// <param name="isReplaceInfo">Значение <see langword="true"/>, если текущее значение <see cref="Info"/> должно быть заменено на значение <paramref name="dict"/>, иначе текущее значение <see cref="Info"/> будет объединено с переданным словарём по правилам <see cref="StorageHelper.Merge(IDictionary{string, object}, IDictionary{string, object})"/>.</param>
        public void SetInfo(
            Dictionary<string, object> dict,
            bool isReplaceInfo = default)
        {
            Check.ArgumentNotNull(dict, nameof(dict));

            if (this.info is null || isReplaceInfo)
            {
                this.Info = dict;
            }
            else
            {
                StorageHelper.Merge(dict, this.Info);
            }
        }

        /// <summary>
        /// Добавляет указанное действие в список действий выполняющихся перед выполнением отложенного действия.
        /// </summary>
        /// <param name="pendingAction">Отложенное действие добавлемое в список действий выполняющихся перед отложенным действием.</param>
        /// <exception cref="ArgumentNullException">Параметр <paramref name="pendingAction"/> имеет значение <see langword="null"/>.</exception>
        /// <exception cref="ObjectSealedException">Нельзя изменять список действий выполняющихся перед выполнением отложенного действия при выполнении действия.</exception>
        /// <exception cref="ArgumentException">Can't add the action into its own actions list, which is used to execute before the pending action.</exception>
        public void AddPreparationAction(
            PendingAction pendingAction)
        {
            Check.ArgumentNotNull(pendingAction, nameof(pendingAction));

            if (ReferenceEquals(pendingAction, this))
            {
                throw new ArgumentException("Can't add the action into its own actions list, which is used to execute before the pending action.", nameof(pendingAction));
            }

            Check.ObjectNotSealed(this);

            this.preparationActions.Add(pendingAction);
        }

        /// <summary>
        /// Добавляет указанное действие в список действий, выполняющихся после выполнения отложенного действия.
        /// </summary>
        /// <param name="pendingAction">Отложенное действие добавлемое в список действий выполняющихся после отложенного действия.</param>
        /// <exception cref="ArgumentNullException">Параметр <paramref name="pendingAction"/> имеет значение <see langword="null"/>.</exception>
        /// <exception cref="ObjectSealedException">Нельзя изменять список действий выполняющихся после выполнения отложенного действия при выполнении действия.</exception>
        /// <exception cref="ArgumentException">Can't add the action into its own actions list, which is used to execute after the pending action.</exception>
        public void AddAfterAction(
            PendingAction pendingAction)
        {
            Check.ArgumentNotNull(pendingAction, nameof(pendingAction));

            if (ReferenceEquals(pendingAction, this))
            {
                throw new ArgumentException("Can't add the action into its own actions list, which is used to execute after the pending action.", nameof(pendingAction));
            }

            Check.ObjectNotSealed(this);

            this.afterActions.Add(pendingAction);
        }

        #endregion

        #region ISealable Members

        /// <inheritdoc/>
        public bool IsSealed { get; private set; }

        /// <inheritdoc/>
        public void Seal()
        {
            this.IsSealed = true;
        }

        #endregion
    }
}
