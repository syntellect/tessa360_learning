﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Platform;
using Tessa.Platform.Validation;

namespace Tessa.Test.Default.Shared.Kr
{
    /// <summary>
    /// Предоставляет методы для работы с отложенными действиями.
    /// </summary>
    /// <typeparam name="T">Тип объекта запланированные действия которого выполненяются методом <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.</typeparam>
    [DebuggerDisplay("{" + nameof(GetDebuggerDisplay) + "()}")]
    public class PendingActionsProvider<T> :
        IPendingActionsProvider<T>
        where T : PendingActionsProvider<T>
    {
        #region Fields
        
        private readonly List<PendingAction> pendingActions;

        private readonly T thisObj;

        #endregion

        #region Properties
        
        /// <inheritdoc/>
        public bool HasPendingActions => pendingActions.Any();

        /// <inheritdoc/>
        public bool IsSealed { get; private set; }

        #endregion

        #region Contructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="PendingActionsProvider{T}"/> с пустым списком запланированных действий.
        /// </summary>
        public PendingActionsProvider()
        {
            this.pendingActions = new List<PendingAction>();
            this.thisObj = (T) this;
        }

        #endregion

        #region Public methods

        /// <inheritdoc/>
        public void AddPendingAction(PendingAction pendingAction)
        {
            Check.ArgumentNotNull(pendingAction, nameof(pendingAction));
            Check.ObjectNotSealed(this);

            this.pendingActions.Add(pendingAction);
        }

        /// <inheritdoc/>
        public PendingAction GetLastPendingAction()
        {
            if (!this.HasPendingActions)
            {
                throw new InvalidOperationException("Planned pending actions are absent.");
            }

            return this.pendingActions.Last();
        }

        /// <inheritdoc/>
        public void Seal()
        {
            this.IsSealed = true;
        }

        /// <summary>
        /// Подготавливает запланированные действия к выполнению.
        /// </summary>
        /// <param name="pendingActions">Список запланированных действий.</param>
        /// <remarks>В реализации по умолчанию не выполняет никаких действий.</remarks>
        protected virtual void PreparePendingActions(
            List<PendingAction> pendingActions)
        {
        }

        /// <inheritdoc/>
        /// <remarks>
        /// Логика обработки отложенных действий определяется в <see cref="GoCoreAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public virtual async ValueTask<T> GoAsync(
            Action<ValidationResult> validationFunc = default,
            CancellationToken cancellationToken = default)
        {
            if (!this.HasPendingActions)
            {
                return this.thisObj;
            }

            this.PreparePendingActions(this.pendingActions);
            this.Seal();

            await this.GoCoreAsync(validationFunc: validationFunc, cancellationToken: cancellationToken);

            this.pendingActions.Clear();
            this.IsSealed = false;

            return this.thisObj;
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Выполняет все запланированные действия.
        /// </summary>
        /// <param name="validationFunc">Метод выполняющий дополнительную валидацию.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Объект типа <typeparamref name="T"/> запланированные действия которого были выполнены.</returns>
        /// <remarks>
        /// Выполнение прерывается при обнаружении ошибки при выполнении запланированных действий.<para/>
        /// Результаты выполнения дополняются информационными сообщениями. Для их исключения, на итоговом результате выполнения, необходимо вызвать метод <see cref="TestValidationKeys.ExceptPendingActionValidationResult(IReadOnlyCollection{IValidationResultItem})"/>.
        /// </remarks>
        protected virtual async ValueTask<T> GoCoreAsync(
            Action<ValidationResult> validationFunc = default,
            CancellationToken cancellationToken = default)
        {
            PendingActionTrace pendingActionTrace = new PendingActionTrace(this.pendingActions.Count);

            foreach (var pendingAction in this.pendingActions)
            {
                pendingActionTrace.Add(pendingAction);

                var result = await pendingAction.ExecuteAsync(cancellationToken: cancellationToken);

                if (result.Items.Count != 0)
                {
                    var validationResults = new ValidationResultBuilder().Add(result);

                    ValidationSequence
                        .Begin(validationResults)
                        .SetObjectName(this)
                        .InfoText(
                            TestValidationKeys.PendingActionTrace,
                            string.Format(
                                TestValidationKeys.PendingActionTrace.Message,
                                pendingActionTrace.ToString()))
                        .End();

                    result = validationResults.Build();
                }

                if (validationFunc is null)
                {
                    ValidationAssert.IsSuccessful(result);
                }
                else
                {
                    validationFunc(result);
                }
            }

            return this.thisObj;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Возвращает строковое представление объекта отображаемое в окне отладчика.
        /// </summary>
        /// <returns>Строковое представление объекта отображаемое в окне отладчика.</returns>
        private string GetDebuggerDisplay()
        {
            return $"Count = {this.pendingActions.Count}";
        }

        #endregion
    }
}
