﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared;
using Tessa.Extensions.Default.Shared.Workflow.KrPermissions;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform;
using Tessa.Platform.Storage;
using Tessa.Platform.Validation;

namespace Tessa.Test.Default.Shared.Kr
{
    /// <summary>
    /// Предоставляет методы выполняющие настройку правил доступа.
    /// </summary>
    public sealed class PermissionsConfigurator :
        CardCollectionConfigurator<Guid>,
        IConfiguratorScopeManager<TestConfigurationBuilder>,
        IPendingActionsExecutor<PermissionsConfigurator>
    {
        #region Constants

        private const string PendingActionParameter = nameof(PendingActionParameter);
        private const string PendingActionIsContextParameter = nameof(PendingActionIsContextParameter);
        private const string PendingActionClcParameter = nameof(PendingActionClcParameter);

        #endregion

        #region Fields

        private readonly ICardLifecycleCompanionDependencies deps;

        private readonly ConfiguratorScopeManager<TestConfigurationBuilder> configuratorScopeManager;

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="PermissionsConfigurator"/>.
        /// </summary>
        /// <param name="deps">Зависимости используемые при взаимодействии с карточкой.</param>
        public PermissionsConfigurator(
            ICardLifecycleCompanionDependencies deps)
            : this(deps, default)
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="PermissionsConfigurator"/>.
        /// </summary>
        /// <param name="deps">Зависимости используемые при взаимодействии с карточкой.</param>
        /// <param name="scope">Конфигуратор верхнего уровня.</param>
        public PermissionsConfigurator(
            ICardLifecycleCompanionDependencies deps,
            TestConfigurationBuilder scope)
            : base()
        {
            Check.ArgumentNotNull(deps, nameof(deps));

            this.deps = deps;
            this.configuratorScopeManager = new ConfiguratorScopeManager<TestConfigurationBuilder>(scope);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Возвращает объект выполняющий конфигурирование карточки правил доступа имеющую указанный идентификатор.
        /// </summary>
        /// <param name="cardID">Идентификатор карточки правила доступа.</param>
        /// <param name="isLoad">Значение <see langword="true"/>, если карточка правила доступа c идентификатором <paramref name="cardID"/> должна быть загружена, если она отсутствует в кэше конфигуратора, иначе создана - <see langword="false"/>.</param>
        /// <returns>Объект <see cref="PermissionsConfigurator"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public PermissionsConfigurator GetPermissionsCard(
            Guid cardID,
            bool isLoad = default)
        {
            this.SetCurrent(
                cardID,
                () =>
                {
                    var permCardCompanion =
                        new CardLifecycleCompanion(
                            cardID,
                            DefaultCardTypes.KrPermissionsTypeID,
                            DefaultCardTypes.KrPermissionsTypeName,
                            this.deps);
                    if (isLoad)
                    {
                        permCardCompanion
                        .Load();
                    }
                    else
                    {
                        permCardCompanion
                            .Create()
                            .ApplyAction((clc, action) =>
                            {
                                var krPermissionsFields = clc.Card.Sections["KrPermissions"].Fields;
                                krPermissionsFields["Caption"] = cardID.ToString("B");
                                krPermissionsFields["Types"] = string.Empty;
                                krPermissionsFields["Roles"] = string.Empty;
                                krPermissionsFields["States"] = string.Empty;
                                krPermissionsFields["Permissions"] = string.Empty;
                            })
                            ;
                    }

                    return permCardCompanion;
                });

            return this;
        }

        /// <summary>
        /// Выставляет указанные права доступа.
        /// </summary>
        /// <param name="flags">Массив дескрипторов прав доступа.</param>
        /// <returns>Объект <see cref="PermissionsConfigurator"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public PermissionsConfigurator AddFlags(
            params KrPermissionFlagDescriptor[] flags)
        {
            Check.ArgumentNotNull(flags, nameof(flags));
            this.CheckCurrent();

            var pendingAction = new PendingAction(
                nameof(PermissionsConfigurator) + "." + nameof(AddFlags),
                AddFlagsInternal);

            pendingAction.Info[PendingActionParameter] = flags;
            pendingAction.Info[PendingActionClcParameter] = this.Current;

            this.Current.AddPendingAction(pendingAction);

            return this;
        }

        /// <summary>
        /// Удаляет указанные права доступа.
        /// </summary>
        /// <param name="flags">Массив дескрипторов прав доступа.</param>
        /// <returns>Объект <see cref="PermissionsConfigurator"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public PermissionsConfigurator RemoveFlags(
            params KrPermissionFlagDescriptor[] flags)
        {
            Check.ArgumentNotNull(flags, nameof(flags));
            this.CheckCurrent();

            var pendingAction = new PendingAction(
                nameof(PermissionsConfigurator) + "." + nameof(RemoveFlags),
                RemoveFlagsInternal);

            pendingAction.Info[PendingActionParameter] = flags;
            pendingAction.Info[PendingActionClcParameter] = this.Current;

            this.Current.AddPendingAction(pendingAction);

            return this;
        }

        /// <summary>
        /// Заменяет текущие права доступа указанными.
        /// </summary>
        /// <param name="flags">Массив дескрипторов прав доступа.</param>
        /// <returns>Объект <see cref="PermissionsConfigurator"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public PermissionsConfigurator ReplaceFlags(
            params KrPermissionFlagDescriptor[] flags)
        {
            Check.ArgumentNotNull(flags, nameof(flags));
            this.CheckCurrent();

            var pendingAction = new PendingAction(
                nameof(PermissionsConfigurator) + "." + nameof(ReplaceFlags),
                ReplaceFlagsInternal);

            pendingAction.Info[PendingActionParameter] = flags;
            pendingAction.Info[PendingActionClcParameter] = this.Current;

            this.Current.AddPendingAction(pendingAction);

            return this;
        }

        /// <summary>
        /// Изменяет список типов к которым применяется текущее правило доступа.
        /// </summary>
        /// <param name="modifyAction">Функция выполняющая модификацию списка типов. Принимаемое значение: текущий список идентификаторов типов карточек. Возвращаемое значение: результирующий список идентификаторов типов.</param>
        /// <returns>Объект <see cref="PermissionsConfigurator"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public PermissionsConfigurator ModifyTypes(
            Func<IList<Guid>, IEnumerable<Guid>> modifyAction)
        {
            Check.ArgumentNotNull(modifyAction, nameof(modifyAction));
            this.CheckCurrent();

            var pendingAction = new PendingAction(
                nameof(PermissionsConfigurator) + "." + nameof(ModifyTypes),
                ModifyTypesInternal);

            pendingAction.Info[PendingActionParameter] = modifyAction;
            pendingAction.Info[PendingActionClcParameter] = this.Current;

            this.Current.AddPendingAction(pendingAction);

            return this;
        }

        /// <summary>
        /// Изменяет список состояний карточек, в которых будут работать определенные разрешения в текущем правиле доступа.
        /// </summary>
        /// <param name="modifyAction">Функция выполняющая модификацию списка состояний. Принимаемое значение: текущий список состояний. Возвращаемое значение: результирующий список состояний.</param>
        /// <returns>Объект <see cref="PermissionsConfigurator"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public PermissionsConfigurator ModifyStates(
            Func<IList<KrState>, IEnumerable<KrState>> modifyAction)
        {
            Check.ArgumentNotNull(modifyAction, nameof(modifyAction));
            this.CheckCurrent();

            var pendingAction = new PendingAction(
                nameof(PermissionsConfigurator) + "." + nameof(ModifyStates),
                ModifyStatesInternal);

            pendingAction.Info[PendingActionParameter] = modifyAction;
            pendingAction.Info[PendingActionClcParameter] = this.Current;

            this.Current.AddPendingAction(pendingAction);

            return this;
        }

        /// <summary>
        /// Изменяет список ролей, для которых выдаются указанные права для указанных типов карточек в указанных состояниях. В списке ролей можно указать, в том числе, и контекстные роли.
        /// </summary>
        /// <param name="modifyAction">Функция выполняющая модификацию списка ролей. Принимаемое значение: текущий список идентификаторов ролей. Возвращаемое значение: результирующий список идентификаторов ролей.</param>
        /// <returns>Объект <see cref="PermissionsConfigurator"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public PermissionsConfigurator ModifyRoles(
            Func<IList<Guid>, IEnumerable<Guid>> modifyAction,
            bool contextRole = false)
        {
            Check.ArgumentNotNull(modifyAction, nameof(modifyAction));
            this.CheckCurrent();

            var pendingAction = new PendingAction(
                nameof(PermissionsConfigurator) + "." + nameof(ModifyRoles),
                ModifyRolesInternal);

            pendingAction.Info[PendingActionParameter] = modifyAction;
            pendingAction.Info[PendingActionIsContextParameter] = BooleanBoxes.Box(contextRole);
            pendingAction.Info[PendingActionClcParameter] = this.Current;

            this.Current.AddPendingAction(pendingAction);

            return this;
        }

        /// <summary>
        /// Выполняет указанное действие над объектом управляющим жизненным циклом текущей карточки правила доступа.
        /// </summary>
        /// <param name="modifyAction">Действие, выполняемое над объектом управляющим жизненным циклом текущей карточки правила доступа.</param>
        /// <returns>Объект <see cref="PermissionsConfigurator"/> для создания цепочки.</returns>
        public PermissionsConfigurator ModifyCard(
            Action<PermissionsConfigurator, CardLifecycleCompanion> modifyAction)
        {
            Check.ArgumentNotNull(modifyAction, nameof(modifyAction));
            this.CheckCurrent();

            modifyAction(this, this.Current);

            return this;
        }

        /// <inheritdoc/>
        public async ValueTask<PermissionsConfigurator> GoAsync(
            Action<ValidationResult> validationFunc = default,
            CancellationToken cancellationToken = default)
        {
            foreach (var clc in this.Where(p => p.Value.HasPendingActions || p.Value.Card?.HasChanges() == true))
            {
                await clc.Value.Save().GoAsync(validationFunc: validationFunc, cancellationToken: cancellationToken);
            }

            return this;
        }

        /// <inheritdoc/>
        public TestConfigurationBuilder Complete()
        {
            return this.configuratorScopeManager.Complete();
        }

        #endregion

        #region Private Methods

        private static void ModifyRowsInternal<T>(
            Card card,
            Func<IList<T>, IEnumerable<T>> modifyAction,
            string section,
            string field,
            Action<CardRow> addRowAction)
        {
            var rows = card.Sections[section].Rows;
            var sourceIDs = rows
                .Where(p => p.State != CardRowState.Deleted)
                .Select(p => p.Fields.Get<T>(field))
                .ToList();
            var resultIDs = modifyAction(sourceIDs).ToList();

            var toDelete = sourceIDs.Except(resultIDs);
            foreach (var deleteID in toDelete)
            {
                var idx = rows.IndexOf(p => object.Equals(p.Get<object>(field), deleteID));
                var row = rows[idx];

                if (row.State == CardRowState.Inserted)
                {
                    rows.RemoveAt(idx);
                }
                else
                {
                    row.State = CardRowState.Deleted;
                }
            }

            var toAdd = resultIDs.Except(sourceIDs);
            foreach (var addID in toAdd)
            {
                var idx = rows.IndexOf(p => object.Equals(p.Get<object>(field), addID));
                if (idx != -1)
                {
                    rows[idx].State = CardRowState.None;
                }
                else
                {
                    var row = rows.Add();
                    row.RowID = Guid.NewGuid();
                    row.State = CardRowState.Inserted;
                    row.Fields[field] = addID;
                    addRowAction(row);
                }
            }
        }

        private static void DropFlags(
            Card card)
        {
            UpdateCanFlags(card, false, KrPermissionFlagDescriptors.Full.IncludedPermissions);
        }

        private static void UpdateCanFlags(
            Card card,
            bool isAllow,
            IEnumerable<KrPermissionFlagDescriptor> flags)
        {
            var sec = card.Sections["KrPermissions"];

            foreach (var flag in flags)
            {
                if (flag.IncludedPermissions != null
                    && flag.IncludedPermissions.Count > 0)
                {
                    UpdateCanFlags(card, isAllow, flag.IncludedPermissions);
                }
                if (flag.IsVirtual)
                {
                    continue;
                }

                sec.Fields[flag.SqlName] = BooleanBoxes.Box(isAllow);
            }
        }

        private static ValueTask<ValidationResult> AddFlagsInternal(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            var info = action.Info;
            UpdateCanFlags(
                info.Get<CardLifecycleCompanion>(PendingActionClcParameter).GetCardOrThrow(),
                true,
                info.Get<KrPermissionFlagDescriptor[]>(PendingActionParameter));

            return new ValueTask<ValidationResult>(ValidationResult.Empty);
        }
        
        private static ValueTask<ValidationResult> RemoveFlagsInternal(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            var info = action.Info;
            UpdateCanFlags(
                info.Get<CardLifecycleCompanion>(PendingActionClcParameter).GetCardOrThrow(),
                false,
                info.Get<KrPermissionFlagDescriptor[]>(PendingActionParameter));

            return new ValueTask<ValidationResult>(ValidationResult.Empty);
        }
        
        private static ValueTask<ValidationResult> ReplaceFlagsInternal(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            var info = action.Info;
            var card = info.Get<CardLifecycleCompanion>(PendingActionClcParameter).GetCardOrThrow();
            DropFlags(card);
            UpdateCanFlags(card, true, info.Get<KrPermissionFlagDescriptor[]>(PendingActionParameter));

            return new ValueTask<ValidationResult>(ValidationResult.Empty);
        }
        
        private static ValueTask<ValidationResult> ModifyTypesInternal(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            var info = action.Info;
            ModifyRowsInternal(
                info.Get<CardLifecycleCompanion>(PendingActionClcParameter).GetCardOrThrow(),
                info.Get<Func<IList<Guid>, IEnumerable<Guid>>>(PendingActionParameter),
                "KrPermissionTypes",
                "TypeID",
                r => r.Fields["TypeCaption"] = "TypeCaption");

            return new ValueTask<ValidationResult>(ValidationResult.Empty);
        }
        
        private static ValueTask<ValidationResult> ModifyStatesInternal(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            var info = action.Info;

            ModifyRowsInternal<int>(
                info.Get<CardLifecycleCompanion>(PendingActionClcParameter).GetCardOrThrow(),
                i => info.Get<Func<IList<KrState>, IEnumerable<KrState>>>(PendingActionParameter)(i.Select(j => (KrState)j).ToList()).Select(j => (int)j),
                "KrPermissionStates",
                "StateID",
                r =>
                {
                    var currentState = r.Fields.Get<int>("StateID");

                    r.Fields["StateID"] = Int32Boxes.Box(currentState);
                    r.Fields["StateName"] = ((KrState)currentState).TryGetDefaultName() ?? "StateName";
                });

            return new ValueTask<ValidationResult>(ValidationResult.Empty);
        }
        
        private static ValueTask<ValidationResult> ModifyRolesInternal(
            PendingAction action,
            CancellationToken cancellationToken = default)
        {
            var info = action.Info;
            ModifyRowsInternal(
                info.Get<CardLifecycleCompanion>(PendingActionClcParameter).GetCardOrThrow(),
                info.Get<Func<IList<Guid>, IEnumerable<Guid>>>(PendingActionParameter),
                "KrPermissionRoles",
                "RoleID",
                r =>
                {
                    r.Fields["RoleName"] = "RoleName";
                    r.Fields["IsContext"] = action.Info.Get<object>(PendingActionIsContextParameter);
                });

            return new ValueTask<ValidationResult>(ValidationResult.Empty);
        }

        #endregion
    }
}