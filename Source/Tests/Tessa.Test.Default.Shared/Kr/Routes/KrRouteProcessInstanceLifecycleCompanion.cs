﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Cards.Workflow;
using Tessa.Files;
using Tessa.Platform;
using Tessa.Platform.Validation;

namespace Tessa.Test.Default.Shared.Kr.Routes
{
    /// <summary>
    /// Предоставляет методы для управления процессом маршрута документа запущенного в карточке которой управляет этот объект.
    /// </summary>
    public sealed class KrRouteProcessInstanceLifecycleCompanion :
        IPendingActionsProvider<KrRouteProcessInstanceLifecycleCompanion>,
        ICardLifecycleCompanion<KrRouteProcessInstanceLifecycleCompanion>
    {
        #region Fields

        private readonly CardLifecycleCompanion cardLifecycle;

        #endregion

        #region Constructors

        /// <summary>
        /// Иницициализирует новый экземпляр класса <see cref="KrRouteProcessInstanceLifecycleCompanion"/>.
        /// </summary>
        /// <param name="cardLifecycle">Объект, содержащий карточку в которой запущен экземпляр процесса.</param>
        /// <param name="processInstanceID">Идентификатор экземпляра процесса или значение <see langword="null"/>, если выполняется взаимодействие не по идентификатору экземпляра процесса.</param>
        public KrRouteProcessInstanceLifecycleCompanion(
            CardLifecycleCompanion cardLifecycle,
            Guid? processInstanceID)
        {
            Check.ArgumentNotNull(cardLifecycle, nameof(cardLifecycle));

            this.cardLifecycle = cardLifecycle;
            this.ProcessInstanceID = processInstanceID;
        }

        #endregion

        #region Properties

        /// <inheritdoc/>
        public Guid CardID => this.cardLifecycle.CardID;

        /// <inheritdoc/>
        public Guid CardTypeID => this.cardLifecycle.CardTypeID;

        /// <inheritdoc/>
        public string CardTypeName => this.cardLifecycle.CardTypeName;

        /// <inheritdoc/>
        public Card Card => this.cardLifecycle.Card;

        /// <inheritdoc/>
        public ICardLifecycleCompanionData LastData => this.cardLifecycle.LastData;

        /// <summary>
        /// Возвращает идентификатор экземпляра процесса или значение <see langword="null"/>, если выполняется взаимодействие не по идентификатору экземпляра процесса, например, отправка глобального сигнала.
        /// </summary>
        public Guid? ProcessInstanceID { get; private set; }

        #endregion

        #region Public methods

        /// <summary>
        /// Отправляет заданный сигнал.
        /// </summary>
        /// <param name="type">Тип сигнала.</param>
        /// <param name="name">Имя (алиас) сигнала.</param>
        /// <param name="info">Дополнительная информация передаваемая в процесс.</param>
        /// <returns>Объект <see cref="KrRouteProcessInstanceLifecycleCompanion"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Если <see cref="ProcessInstanceID"/> задано, то сигнал будет отправлен данному экземпляру процесса.<para/>
        /// Выполняет сохранение карточки с последующей загрузкой.
        /// </remarks>
        public KrRouteProcessInstanceLifecycleCompanion SendSignal(
            string type,
            string name,
            Dictionary<string, object> info = default)
        {
            this.Save();

            var pendingAction = this.GetLastPendingAction();
            pendingAction.AddPreparationAction(
                new PendingAction(
                    nameof(KrRouteProcessInstanceLifecycleCompanion) + "." + nameof(SendSignal),
                    (action, ct) =>
                    {
                        this.GetCardOrThrow()
                            .GetWorkflowQueue()
                            .AddSignal(type, name: name, parameters: info, processID: this.ProcessInstanceID);
                        return new ValueTask<ValidationResult>(ValidationResult.Empty);
                    }));

            return this;
        }

        #endregion

        #region ICardLifecycleCompanion Members

        /// <inheritdoc/>
        public ValueTask<ICardFileContainer> GetCardFileContainerAsync(
            IFileRequest request = default,
            IList<IFileTag> additionalTags = default,
            CancellationToken cancellationToken = default)
        {
            return this.cardLifecycle.GetCardFileContainerAsync(
                request: request,
                additionalTags: additionalTags,
                cancellationToken: cancellationToken);
        }

        /// <inheritdoc/>
        public Card GetCardOrThrow()
        {
            return this.cardLifecycle.GetCardOrThrow();
        }

        #endregion

        #region ICardLifecycleCompanion<T> Members

        /// <inheritdoc/>
        public KrRouteProcessInstanceLifecycleCompanion Create()
        {
            _ = this.cardLifecycle.Create();
            return this;
        }

        /// <inheritdoc/>
        public KrRouteProcessInstanceLifecycleCompanion Delete()
        {
            _ = this.cardLifecycle.Delete();
            return this;
        }

        /// <inheritdoc/>
        public KrRouteProcessInstanceLifecycleCompanion Load()
        {
            _ = this.cardLifecycle.Load();
            return this;
        }

        /// <inheritdoc/>
        public KrRouteProcessInstanceLifecycleCompanion Save()
        {
            _ = this.cardLifecycle.Save();
            return this;
        }

        /// <inheritdoc/>
        public KrRouteProcessInstanceLifecycleCompanion WithInfo(Dictionary<string, object> info)
        {
            _ = this.cardLifecycle.WithInfo(info);
            return this;
        }

        /// <inheritdoc/>
        public KrRouteProcessInstanceLifecycleCompanion WithInfoPair(string key, object val)
        {
            _ = this.cardLifecycle.WithInfoPair(key, val);
            return this;
        }

        /// <inheritdoc/>
        public KrRouteProcessInstanceLifecycleCompanion CreateOrLoadSingleton()
        {
            _ = this.cardLifecycle.CreateOrLoadSingleton();
            return this;
        }

        #endregion

        #region IExecutePendingActions Members

        /// <inheritdoc/>
        public async ValueTask<KrRouteProcessInstanceLifecycleCompanion> GoAsync(
            Action<ValidationResult> validationFunc = default,
            CancellationToken cancellationToken = default)
        {
            await this.cardLifecycle.GoAsync(validationFunc: validationFunc, cancellationToken: cancellationToken);
            return this;
        }

        #endregion

        #region ISealable Members

        /// <inheritdoc/>
        public bool IsSealed => this.cardLifecycle.IsSealed;

        /// <inheritdoc/>
        public void Seal()
        {
            this.cardLifecycle.Seal();
        }

        #endregion

        #region IProvidePendingActions Members

        /// <inheritdoc/>
        public bool HasPendingActions => this.cardLifecycle.HasPendingActions;

        /// <inheritdoc/>
        public void AddPendingAction(PendingAction pendingAction)
        {
            this.cardLifecycle.AddPendingAction(pendingAction);
        }

        /// <inheritdoc/>
        public PendingAction GetLastPendingAction()
        {
            return this.cardLifecycle.GetLastPendingAction();
        }

        #endregion

        #region Internal methods

        /// <summary>
        /// Задаёт идентификатор экземпляра процесса.
        /// </summary>
        /// <param name="processInstanceID">Идентификатор экземпляра процесса.</param>
        internal void SetProcessInstanceID(
            Guid? processInstanceID)
        {
            this.ProcessInstanceID = processInstanceID;
        }

        #endregion
    }
}
