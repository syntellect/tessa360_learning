﻿using System;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform;
using Tessa.Test.Default.Shared.Cards;

namespace Tessa.Test.Default.Shared.Kr.Routes
{
    /// <summary>
    /// Предоставляет методы для управления испонителями в этапе подсистемы маршрутов.
    /// </summary>
    public sealed class PerformerBuilder :
        ConfiguratorScopeManager<RouteBuilder>
    {
        #region Fields

        private readonly CardRow stageRow;

        private readonly CardSection performersSection;

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр <see cref="PerformerBuilder"/>.
        /// </summary>
        /// <param name="stageBuidler">Объект, выполняющий создание и настройку этапа для которого требуется выполнить конфигурирование исполнителей.</param>
        /// <param name="stageRow">Строка, содержащая информацию о конфигурируемом этапе.</param>
        /// <param name="performersSection">Секция содержащая информацию об исполнителях.</param>
        public PerformerBuilder(
            RouteBuilder stageBuidler,
            CardRow stageRow,
            CardSection performersSection)
            : base(stageBuidler)
        {
            Check.ArgumentNotNull(stageBuidler, nameof(stageBuidler));
            Check.ArgumentNotNull(stageRow, nameof(stageRow));
            Check.ArgumentNotNull(performersSection, nameof(performersSection));

            this.stageRow = stageRow;
            this.performersSection = performersSection;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Добавляет указанную роль в качестве исполнителя.
        /// </summary>
        /// <param name="roleID">Идентификатор роли.</param>
        /// <param name="roleName">Имя роли.</param>
        /// <returns>Объект <see cref="PerformerBuilder"/> для создания цепочки.</returns>
        public PerformerBuilder SetSinglePerformer(Guid roleID, string roleName)
        {
            this.stageRow[KrConstants.KrSinglePerformerVirtual.PerformerID] = roleID;
            this.stageRow[KrConstants.KrSinglePerformerVirtual.PerformerName] = roleName;
            return this;
        }

        /// <summary>
        /// Добавляет указанную роль в качестве исполнителя.
        /// </summary>
        /// <param name="roleID">Идентификатор роли.</param>
        /// <param name="roleName">Имя роли.</param>
        /// <param name="order">Порядковый номер.</param>
        /// <returns>Объект <see cref="PerformerBuilder"/> для создания цепочки.</returns>
        public PerformerBuilder AddPerformer(Guid roleID, string roleName, int order = int.MaxValue)
        {
            var rows = this.performersSection.Rows;
            var newRow = rows.Add();
            newRow.RowID = Guid.NewGuid();
            newRow.State = CardRowState.Inserted;
            newRow.Fields[KrConstants.KrPerformersVirtual.PerformerID] = roleID;
            newRow.Fields[KrConstants.KrPerformersVirtual.PerformerName] = roleName;
            newRow.Fields[KrConstants.StageRowID] = this.stageRow.RowID;
            newRow.Fields[KrConstants.Order] = Int32Boxes.Box(order);

            TestCardHelper.RepairCardRowOrders(this.performersSection.Rows);
            return this;
        }

        /// <summary>
        /// Удаляет всех исполнителей отвечающих указаному предикату.
        /// </summary>
        /// <param name="predicate">Предикат в соответствии с которым выполняется удаление исполнителей. Параметры: строка, содержащая информацию об исполнителе.</param>
        /// <returns>Объект <see cref="PerformerBuilder"/> для создания цепочки.</returns>
        public PerformerBuilder RemovePerformers(Func<CardRow, bool> predicate)
        {
            // Значение predicate будет проверено в RemoveRows.

            TestCardHelper.RemoveRows(this.performersSection.Rows, predicate);
            TestCardHelper.RepairCardRowOrders(this.performersSection.Rows);
            return this;
        }

        #endregion
    }
}