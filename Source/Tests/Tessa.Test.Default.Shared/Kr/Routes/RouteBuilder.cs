﻿using System;
using System.Linq;
using Tessa.Cards;
using Tessa.Extensions.Default.Shared.Workflow.KrProcess;
using Tessa.Platform;
using Tessa.Platform.Storage;
using Tessa.Test.Default.Shared;
using Tessa.Test.Default.Shared.Cards;

namespace Tessa.Test.Default.Shared.Kr.Routes
{
    /// <summary>
    /// Предоставляет методы для создания и настройки этапов подсистемы маршрутов.
    /// </summary>
    public sealed class RouteBuilder
    {
        #region Fields

        private readonly Card card;

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр объекта <see cref="RouteBuilder"/>.
        /// </summary>
        /// <param name="clc">Объект, управляющий жизненным циклом карточки в которой необходимо выполнить создание маршрута.</param>
        public RouteBuilder(
            ICardLifecycleCompanion clc)
        {
            Check.ArgumentNotNull(clc, nameof(clc));

            this.card = clc.GetCardOrThrow();
        }

        /// <summary>
        /// Инициализирует новый экземпляр объекта <see cref="RouteBuilder"/>.
        /// </summary>
        /// <param name="card">Карточка в которой необходимо выполнить создание маршрута.</param>
        public RouteBuilder(
            Card card)
        {
            Check.ArgumentNotNull(card, nameof(card));

            this.card = card;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Добавляет новый этап.
        /// </summary>
        /// <param name="name">Название этапа.</param>
        /// <param name="descriptor">Дескриптор типа этапа.</param>
        /// <param name="group">Дескриптор описывающий группу этапов. Должен быть задан, если карточка относится к типу содержащему выполняющийся маршрут.</param>
        /// <param name="order">Порядковый номер этапа. Используется, если карточка относится к типу содержащему шаблоны этапов.</param>
        /// <param name="modifyAction">Предикат выполняющий изменение строки содержащей информацию о новом этапе.</param>
        /// <exception cref="InvalidOperationException">Stage with name "<paramref name="name"/>" already exists.</exception>
        /// <returns>Объект <see cref="RouteBuilder"/> для создания цепочки.</returns>
        public RouteBuilder AddStage(
            string name,
            StageTypeDescriptor descriptor,
            KrStageGroupDescriptor group = default,
            int order = int.MaxValue,
            Action<CardRow, Card> modifyAction = default)
        {
            Check.ArgumentNotNull(name, nameof(name));
            Check.ArgumentNotNull(descriptor, nameof(descriptor));

            var rows = this.card.Sections[KrConstants.KrStages.Virtual].Rows;
            int effectiveOrder;
            if (KrProcessSharedHelper.DesignTimeCard(this.card.TypeID))
            {
                effectiveOrder = order != int.MaxValue
                    ? order
                    : rows.Count;
            }
            else
            {
                Check.ArgumentNotNull(group, nameof(group));

                effectiveOrder = KrProcessSharedHelper.ComputeStageOrder(
                    group.ID,
                    group.Order,
                    rows);
            }

            if (rows.Any(p => StageRowPredicate(p, name)))
            {
                throw new ArgumentException($"Stage with name \"{name}\" already exists.", nameof(name));
            }

            var row = rows.Add();
            row.RowID = Guid.NewGuid();
            row.State = CardRowState.Inserted;
            row.Fields[KrConstants.Name] = name;

            var state = KrStageState.Inactive;
            row.Fields[KrConstants.KrStages.StageStateID] = Int32Boxes.Box(state.ID);
            row.Fields[KrConstants.KrStages.StageStateName] = state.TryGetDefaultName();

            row.Fields[KrConstants.KrStages.StageTypeID] = descriptor.ID;
            row.Fields[KrConstants.KrStages.StageTypeCaption] = descriptor.Caption;

            if (group is not null)
            {
                SetStageGroupInternal(row, group);
            }

            row.Fields[KrConstants.Order] = Int32Boxes.Box(effectiveOrder);

            IncrementBottomStagesOrder(row, rows);
            modifyAction?.Invoke(row, this.card);

            return this;
        }

        /// <summary>
        /// Возвращает дескриптор группы этапов сформированный по данным хранящимся в строке этапа.
        /// </summary>
        /// <param name="row">Строка содержащая информацию по этапу.</param>
        /// <returns>Дескриптор группы этапов.</returns>
        public static KrStageGroupDescriptor GetStageGroup(
            CardRow row)
        {
            Check.ArgumentNotNull(row, nameof(row));

            var fields = row.Fields;
            var stageGroupID = fields.Get<Guid>(KrConstants.StageGroupID);
            var stageGroupName = fields.Get<string>(KrConstants.StageGroupName);
            var stageGroupOrder = fields.Get<int>(KrConstants.StageGroupOrder);

            return new KrStageGroupDescriptor(stageGroupID, stageGroupName, stageGroupOrder);
        }

        /// <summary>
        /// Задаёт информацию по группе этапов в строку содержащую информацию по этапу.
        /// </summary>
        /// <param name="row">Строка содержащая информацию по этапу.</param>
        /// <param name="groupDescriptor">Дескриптор группы этапов.</param>
        public static void SetStageGroup(
            CardRow row,
            KrStageGroupDescriptor groupDescriptor)
        {
            Check.ArgumentNotNull(row, nameof(row));
            Check.ArgumentNotNull(groupDescriptor, nameof(groupDescriptor));

            SetStageGroupInternal(row, groupDescriptor);
        }

        /// <summary>
        /// Изменяет значение поля в строке, содержащей информацию о этапе имеющем указанное название.
        /// </summary>
        /// <param name="name">Название этапа, в строке которого требуется изменить значение поля.</param>
        /// <param name="field">Название поля.</param>
        /// <param name="value">Значение.</param>
        /// <returns>Объект <see cref="RouteBuilder"/> для создания цепочки.</returns>
        public RouteBuilder ModifyStageField(
            string name,
            string field,
            object value)
        {
            this.ModifyStage(name, r => r.Fields[field] = value);
            return this;
        }

        /// <summary>
        /// Применяет указанное действие к строке, содержащей информацию о этапе имеющем указанное название.
        /// </summary>
        /// <param name="name">Название этапа.</param>
        /// <param name="modifyAction">Действие выполняющее модификацию строки, содержащей информацию об этапе.</param>
        /// <returns>Объект <see cref="RouteBuilder"/> для создания цепочки.</returns>
        public RouteBuilder ModifyStage(
            string name,
            Action<CardRow> modifyAction)
        {
            this.ModifyStages(r => StageRowPredicate(r, name), modifyAction);
            return this;
        }

        /// <summary>
        /// Применяет указанное действие к строке, содержащей информацию о этапе, удовлетворяющей заданному условию.
        /// </summary>
        /// <param name="filterPredicate">Поисковое условие.</param>
        /// <param name="modifyAction">Действие выполняющее модификацию строки, содержащей информацию об этапе.</param>
        /// <returns>Объект <see cref="RouteBuilder"/> для создания цепочки.</returns>
        public RouteBuilder ModifyStages(
            Func<CardRow, bool> filterPredicate,
            Action<CardRow> modifyAction)
        {
            Check.ArgumentNotNull(filterPredicate, nameof(filterPredicate));
            Check.ArgumentNotNull(modifyAction, nameof(modifyAction));

            foreach (var row in this.card.Sections[KrConstants.KrStages.Virtual].Rows)
            {
                if (filterPredicate(row))
                {
                    modifyAction(row);
                }
            }
            return this;
        }

        /// <summary>
        /// Для последнего этапа в списке этапов создаёт билдер позволяющий модифицировать его исполнителей.
        /// </summary>
        /// <returns>Объект <see cref="PerformerBuilder"/> для создания цепочки.</returns>
        /// <exception cref="InvalidOperationException">Секция, содержащая этапы маршрута, не содержит ни одной строки этапа.</exception>
        public PerformerBuilder ModifyPerformers() =>
            new PerformerBuilder(
                this,
                this.card.GetStagesSection(true).Rows.LastOrDefault() ?? throw new InvalidOperationException("The section containing the route stages does not contain any rows."),
                this.card.Sections[KrConstants.KrPerformersVirtual.Synthetic]);

        /// <summary>
        /// Для этапа с указынным именем создаёт билдер позволяющий модифицировать его исполнителей.
        /// </summary>
        /// <param name="stageName">Название этапа.</param>
        /// <returns>Объект <see cref="PerformerBuilder"/> для создания цепочки.</returns>
        public PerformerBuilder ModifyPerformers(string stageName) =>
            this.ModifyPerformers(p => StageRowPredicate(p, stageName));

        /// <summary>
        /// Для первого этапа, удовлетворяющего указанному предикату, создаёт билдер позволяющий модифицировать его исполнителей.
        /// </summary>
        /// <param name="predicate">Предикат определяющий строку этапа для которого требуется создать <see cref="PerformerBuilder"/>.</param>
        /// <returns>Объект <see cref="PerformerBuilder"/> для создания цепочки.</returns>
        public PerformerBuilder ModifyPerformers(Func<CardRow, bool> predicate) =>
            new PerformerBuilder(
                this,
                this.card.GetStagesSection(true).Rows.First(predicate),
                this.card.Sections[KrConstants.KrPerformersVirtual.Synthetic]);

        /// <summary>
        /// Удаляет этап удовлетворяющий указанному условию.
        /// </summary>
        /// <param name="filterPredicate">Условие в соответствии с котором выполняется удаление этапов.</param>
        /// <returns>Объект <see cref="RouteBuilder"/> для создания цепочки.</returns>
        public RouteBuilder RemoveStages(
            Func<CardRow, bool> filterPredicate)
        {
            // Проверка значения filterPredicate будет выполнена в RemoveRows.

            var rows = this.card.Sections[KrConstants.KrStages.Virtual].Rows;
            TestCardHelper.RemoveRows(rows, filterPredicate);
            TestCardHelper.RepairCardRowOrders(rows);
            return this;
        }

        #endregion

        #region Private methods

        private static void IncrementBottomStagesOrder(
            CardRow row,
            ListStorage<CardRow> rows)
        {
            var order = row.Fields.Get<int>(KrConstants.Order);

            foreach (var r in rows)
            {
                if (!ReferenceEquals(row, r)
                    && order <= r.Get<int>(KrConstants.Order))
                {
                    r.Fields[KrConstants.Order] = Int32Boxes.Box(r.Fields.Get<int>(KrConstants.Order) + 1);
                }
            }
        }

        private static bool StageRowPredicate(CardRow stageRow, string stageName) =>
            string.Equals(stageRow.Get<string>(KrConstants.Name), stageName, StringComparison.Ordinal);

        private static void SetStageGroupInternal(
            CardRow row,
            KrStageGroupDescriptor groupDescriptor)
        {
            row.Fields[KrConstants.StageGroupID] = groupDescriptor.ID;
            row.Fields[KrConstants.StageGroupName] = groupDescriptor.Name;
            row.Fields[KrConstants.StageGroupOrder] = Int32Boxes.Box(groupDescriptor.Order);
        }

        #endregion
    }
}