﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Platform;
using Tessa.Platform.Runtime;
using Tessa.Platform.Validation;

namespace Tessa.Test.Default.Shared.Kr
{
    /// <summary>
    /// Предоставляет методы выполняющие настройку параметров сервера.
    /// </summary>
    public class ServerConfigurator :
        CardLifecycleCompanion<ServerConfigurator>,
        IConfiguratorScopeManager<TestConfigurationBuilder>
    {
        #region Fields

        private readonly ICardFileSourceSettings cardFileSourceSettings;
        private readonly ConfiguratorScopeManager<TestConfigurationBuilder> configuratorScopeManager;

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ServerConfigurator"/>.
        /// </summary>
        /// <param name="deps">Зависимости используемые при взаимодействии с карточками.</param>
        /// <param name="cardFileSourceSettings">Потокобезопасный кэш настроек по всем местоположениям файлов. Если значение не задано, то выполняется инициализация значениями по умолчанию.</param>
        public ServerConfigurator(
            ICardLifecycleCompanionDependencies deps,
            ICardFileSourceSettings cardFileSourceSettings)
            : this(deps, cardFileSourceSettings, default)
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ServerConfigurator"/>.
        /// </summary>
        /// <param name="deps">Зависимости используемые при взаимодействии с карточками.</param>
        /// <param name="cardFileSourceSettings">Потокобезопасный кэш настроек по всем местоположениям файлов. Если значение не задано, то выполняется инициализация значениями по умолчанию.</param>
        /// <param name="scope">Конфигуратор верхнего уровня. Может быть не задан.</param>
        public ServerConfigurator(
            ICardLifecycleCompanionDependencies deps,
            ICardFileSourceSettings cardFileSourceSettings,
            TestConfigurationBuilder scope)
            : base(CardHelper.ServerInstanceTypeID, CardHelper.ServerInstanceTypeName, deps)
        {
            this.cardFileSourceSettings = cardFileSourceSettings;
            this.configuratorScopeManager = new ConfiguratorScopeManager<TestConfigurationBuilder>(scope);
        }

        #endregion

        #region Base Overrides

        /// <summary>
        /// Создаёт карточку типа <see cref="ICardLifecycleCompanion.CardTypeID"/>.
        /// </summary>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Можно указать дополнительную информацию, которая будет передана в запросе на создание карточки.<para/>
        /// После создания выполняется инициализация карточки значениями по умолчанию.
        /// </remarks>
        public override ServerConfigurator Create() =>
            base.Create().ApplyAction(this.InitializeServerInstanceAsync);

        /// <summary>
        /// Создаёт новую или загружает существующую карточку синглтон.
        /// </summary>
        /// <returns>Объект <typeparamref name="T"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.<para/>
        /// Если карточка была создана, то выполняется её инициализация значениями по умолчанию.
        /// </remarks>
        public override ServerConfigurator CreateOrLoadSingleton() =>
            base.CreateOrLoadSingleton().ApplyAction(this.InitializeServerInstanceAsync);

        #endregion

        #region IConfiguratorScopeManager<T> Members

        /// <inheritdoc/>
        public TestConfigurationBuilder Complete() =>
            this.configuratorScopeManager.Complete();

        #endregion

        #region Public Methods

        /// <summary>
        /// Изменяет путь к файловому хранилищу на файловое хранилище используемое для тестов. Настройка выполняется только для хранилищ у которых не стоит флаг "База данных".
        /// </summary>
        /// <param name="sourceID">Идентификатор настраиваемого файлового хранилища.</param>
        /// <param name="getPathFunc">Метод, возвращающий путь к файловому хранилищу. Параметр: строка, содержащая информацию о файловом хранилище. Если задано значение по умолчанию для типа, то используется значение, возвращаемое методом <see cref="TestHelper.GetFileStoragePath(bool)"/>.</param>
        /// <returns>Объект <see cref="ServerConfigurator"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public ServerConfigurator ChangeFileSourcePathWithTestSource(
            int sourceID,
            Func<CardRow, string> getPathFunc = default) =>
            this.ChangeFileSourcePathWithTestSource(i => i.Get<int>("SourceID") == sourceID, getPathFunc);

        /// <summary>
        /// Изменяет путь к файловому хранилищу на файловое хранилище используемое для тестов. Настройка выполняется только для хранилищ у которых не стоит флаг "База данных".
        /// </summary>
        /// <param name="predicate">Условие отбора настраиваемых файловых хранилищ.</param>
        /// <param name="getPathFunc">Метод, возвращающий путь к файловому хранилищу. Параметр: строка, содержащая информацию о файловом хранилище. Если задано значение по умолчанию для типа, то используется значение, возвращаемое методом <see cref="TestHelper.GetFileStoragePath(bool)"/>.</param>
        /// <returns>Объект <see cref="ServerConfigurator"/> для создания цепочки.</returns>
        /// <remarks>
        /// Этот метод реализуется с помощью отложенного выполнения. Для выполнения запрошенного действия необходимо вызвать метод <see cref="IPendingActionsExecutor{T}.GoAsync(Action{ValidationResult}, CancellationToken)"/>.
        /// </remarks>
        public ServerConfigurator ChangeFileSourcePathWithTestSource(
            Func<CardRow, bool> predicate,
            Func<CardRow, string> getPathFunc = default)
        {
            Check.ArgumentNotNull(predicate, nameof(predicate));

            this.ApplyAction((clc, action) =>
            {
                var serverInstance = clc.Card;
                if (serverInstance.StoreMode == CardStoreMode.Update)
                {
                    var fileSourceRows = serverInstance.Sections["FileSourcesVirtual"].Rows
                    .Where(i => !i.Get<bool>("IsDatabase"))
                    .Where(predicate);

                    getPathFunc ??= static _ => TestHelper.GetFileStoragePath();

                    foreach (var fileSourceRow in fileSourceRows)
                    {
                        fileSourceRow.Fields["Path"] = getPathFunc(fileSourceRow);
                    }
                }
            });

            return this;
        }

        #endregion

        #region Private methods

        private async ValueTask<ValidationResult> InitializeServerInstanceAsync(
            ServerConfigurator clc,
            PendingAction _,
            CancellationToken cancellationToken = default)
        {
            var card = clc.GetCardOrThrow();

            if (card.StoreMode == CardStoreMode.Update)
            {
                return ValidationResult.Empty;
            }

            var serverInstancesFields = card.Sections["ServerInstances"].Fields;
            serverInstancesFields["Name"] = RuntimeHelper.DefaultInstanceName;

            var newSourceRow = clc.LastData.NewResponse.SectionRows["FileSourcesVirtual"];
            var fileSourcesVirtualRows = card.Sections["FileSourcesVirtual"].Rows;
            ICardFileSource defaultCardFileSource;
            int defaultFileSourceID;

            if (this.cardFileSourceSettings is null
                || (defaultCardFileSource = await this.cardFileSourceSettings.GetDefaultAsync(cancellationToken)) is null)
            {
                defaultFileSourceID = CardFileSourceType.FileSystem.ID;

                var source = fileSourcesVirtualRows.Add();
                source.Set(newSourceRow);

                FillCardFileSourceRow(
                    row: source,
                    type: CardFileSourceType.FileSystem,
                    name: nameof(CardFileSourceType.FileSystem),
                    isDefault: true,
                    path: TestHelper.GetFileStoragePath(),
                    isDatabase: false,
                    useSimpleNamingScheme: TestSettings.UseSimpleNamingScheme,
                    size: 0,
                    maxSize: 0,
                    fileExtensions: default);

                source = fileSourcesVirtualRows.Add();
                source.Set(newSourceRow);

                FillCardFileSourceRow(
                    row: source,
                    type: CardFileSourceType.Database,
                    name: nameof(CardFileSourceType.Database),
                    isDefault: false,
                    path: CardFileSourceType.DefaultDatabasePath,
                    isDatabase: true,
                    useSimpleNamingScheme: false,
                    size: 0,
                    maxSize: 0,
                    fileExtensions: default);
            }
            else
            {
                var cardFileSources = await this.cardFileSourceSettings.GetAllAsync(cancellationToken);
                var defaultCardFileSourceTypeID = defaultCardFileSource.Type.ID;
                defaultFileSourceID = defaultCardFileSourceTypeID;

                foreach (var cardFileSource in cardFileSources)
                {
                    var row = fileSourcesVirtualRows.Add();
                    row.Set(newSourceRow);

                    FillCardFileSourceRow(
                        row: row,
                        type: cardFileSource.Type,
                        name: cardFileSource.Name,
                        isDefault: defaultCardFileSourceTypeID == cardFileSource.Type.ID,
                        path: cardFileSource.Path,
                        isDatabase: cardFileSource.IsDatabase,
                        useSimpleNamingScheme: cardFileSource.UseSimpleNamingScheme,
                        size: cardFileSource.Size,
                        maxSize: cardFileSource.MaxSize,
                        fileExtensions: default);
                }
            }

            serverInstancesFields["DefaultFileSourceID"] = Int32Boxes.Box(defaultFileSourceID);

            return ValidationResult.Empty;
        }

        private static void FillCardFileSourceRow(
            CardRow row,
            CardFileSourceType type,
            string name,
            bool isDefault,
            string path,
            bool isDatabase,
            bool useSimpleNamingScheme,
            int size,
            int maxSize,
            string fileExtensions)
        {
            row.RowID = Guid.NewGuid();
            var fields = row.Fields;
            fields["IsDatabase"] = BooleanBoxes.Box(isDatabase);
            fields["IsDefault"] = BooleanBoxes.Box(isDefault);
            fields["MaxSize"] = Int32Boxes.Box(maxSize);
            fields["Name"] = name;
            fields["Path"] = path;
            fields["Size"] = Int32Boxes.Box(size);
            fields["SourceID"] = Int32Boxes.Box(type.ID);
            fields["SourceIDText"] = default;
            fields["UseSimpleNamingScheme"] = BooleanBoxes.Box(useSimpleNamingScheme);
            fields["FileExtensions"] = fileExtensions;
            row.State = CardRowState.Inserted;
        }

        #endregion
    }
}