﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Cards;
using Tessa.Platform;
using Tessa.Platform.Validation;

namespace Tessa.Test.Default.Shared.Kr
{
    /// <summary>
    /// Предоставляет методы для планирования и удаления карточек после выполнения тестов.
    /// </summary>
    public class TestCardManager
    {
        #region Nested types

        private readonly struct PendingAction
        {
            #region Fields

            private readonly Func<ICardLifecycleCompanion, CancellationToken, ValueTask> successActionAsync;

            #endregion

            #region Properties

            public ICardLifecycleCompanion Clc { get; }

            #endregion

            #region Constructors

            public PendingAction(
                ICardLifecycleCompanion clc,
                Func<ICardLifecycleCompanion, CancellationToken, ValueTask> successActionAsync)
            {
                this.Clc = clc;
                this.successActionAsync = successActionAsync;
            }

            #endregion

            #region Public methods

            public ValueTask SuccessActionAsync(
                CancellationToken cancellationToken = default)
            {
                return this.successActionAsync?.Invoke(this.Clc, cancellationToken) ?? new ValueTask();
            }

            #endregion
        }

        #endregion

        #region Fields

        private readonly List<PendingAction> pendingActions = new List<PendingAction>();

        private readonly ICardLifecycleCompanionDependencies deps;

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр объекта <see cref="TestCardManager"/>.
        /// </summary>
        /// <param name="deps">Зависимости используемые объектами управляющими жизненным циклом карточек.</param>
        public TestCardManager(
            ICardLifecycleCompanionDependencies deps)
        {
            Check.ArgumentNotNull(deps, nameof(deps));

            this.deps = deps;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Асинхронно удаляет карточки, добавленные в список на удаление.
        /// </summary>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Асинхронная задача.</returns>
        public async Task AfterTestAsync(
            CancellationToken cancellationToken = default)
        {
            if (!this.pendingActions.Any())
            {
                return;
            }

            var validationResults = new ValidationResultBuilder();

            foreach (var card in this.pendingActions)
            {
                var results = new ValidationResult[1];

                // Создаём новый объект управления карточкой, т.к. исходный может иметь недопустимое состояние.
                await new CardLifecycleCompanion(
                        card.Clc.CardID,
                        card.Clc.CardTypeID,
                        card.Clc.CardTypeName,
                        this.deps)
                    .Delete()
                    .GoAsync(
                        (validationResult) => results[0] = validationResult,
                        cancellationToken: cancellationToken);

                var validationResult = results[0];

                if (validationResult.Items.Any(i => i.Type == ValidationResultType.Error
                    && i.Key.ID != CardValidationKeys.InstanceNotFound.ID))
                {
                    validationResults.Add(validationResult);
                }
                else
                {
                    await card.SuccessActionAsync(cancellationToken);
                }
            }

            ValidationAssert.IsSuccessful(validationResults);

            this.pendingActions.Clear();
        }

        /// <summary>
        /// Добавляет указанный объект, управляющий жизненным циклом карточки, в список на удаление после завершения теста.
        /// </summary>
        /// <param name="companion">Объект, управляющий жизненным циклом карточки которая должена быть удалена после завершения теста.</param>
        /// <param name="successActionAsync">Действие выполняемое при успешном удалении карточки.</param>
        /// <seealso cref="AfterTestAsync"/>
        public void DeleteCardAfterTest<T>(
            T companion,
            Func<ICardLifecycleCompanion, CancellationToken, ValueTask> successActionAsync = default)
            where T : ICardLifecycleCompanion<T>
        {
            Check.ArgumentNotNull(companion, nameof(companion));

            this.pendingActions.Add(
                new PendingAction(
                    companion,
                    successActionAsync));
        }

        #endregion
    }
}