﻿using System.Reflection;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Предоставляет информацию о встроенных ресурсах.
    /// </summary>
    /// <remarks>Класс-наследник, экземпляр к которого будет использоваться в тестах, должен распологать в сборке содержащей необходимые встроенные ресурсы.</remarks>
    public abstract class ResourceAssemblyManager
    {
        #region Fields

        private Assembly resourceAssembly;

        #endregion

        #region Properties

        /// <summary>
        /// Возвращает сборку содержащую встроенные ресурсы.
        /// </summary>
        protected Assembly ResourceAssembly => this.resourceAssembly ??= this.GetType().Assembly;

        #endregion
    }
}
