﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tessa.Platform;
using Tessa.Roles;

namespace Tessa.Test.Default.Shared.Roles
{
    /// <summary>
    /// Предоставляет статические вспомогательные методы для работы с ролями в тестах.
    /// </summary>
    public static class TestRoleHelper
    {
        #region Public Methods

        /// <summary>
        /// Добавляет пользователя со случайным идентификатором и именем.
        /// </summary>
        /// <param name="roleRepository">Репозиторий для управления ролевой моделью.</param>
        /// <param name="modifyAction">Функция используемая для изменения создаваемого пользователя.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Созданный пользователь.</returns>
        public static Task<PersonalRole> CreateUserAsync(
            IRoleRepository roleRepository,
            Action<PersonalRole> modifyAction = null,
            CancellationToken cancellationToken = default)
        {
            var roleID = Guid.NewGuid();
            var roleName = "User__" + TestHelper.GetPseudoRandomNumber().ToString();

            return CreateUserAsync(roleRepository, roleID, roleName, modifyAction, cancellationToken);
        }

        /// <summary>
        /// Добавляет в базу сотрудника с указанным идентификатором и именем.
        /// </summary>
        /// <param name="roleRepository">Репозиторий для управления ролевой моделью.</param>
        /// <param name="roleID">Идентификатор пользователя.</param>
        /// <param name="roleName">Имя пользователя.</param>
        /// <param name="modifyAction">Функция используемая для изменения создаваемого пользователя.</param>
        /// <param name="cancellationToken">Объект, посредством которого можно отменить асинхронную задачу.</param>
        /// <returns>Созданный пользователь.</returns>
        public static async Task<PersonalRole> CreateUserAsync(
            IRoleRepository roleRepository,
            Guid roleID,
            string roleName,
            Action<PersonalRole> modifyAction = null,
            CancellationToken cancellationToken = default)
        {
            Check.ArgumentNotNull(roleRepository, nameof(roleRepository));

            PersonalRole modifiedBy = await roleRepository.GetPersonalRoleAsync(TestHelper.AdminUserID, cancellationToken);

            PersonalRole role = new PersonalRole
            {
                ID = roleID,
                Name = roleName,
                Modified = DateTime.UtcNow,
                ModifiedBy = modifiedBy,
                Phone = "123",
                FullName = roleName
            };

            modifyAction?.Invoke(role);

            role.Users = new List<RoleUserRecord>
            {
                new RoleUserRecord
                {
                    RowID = Guid.NewGuid(),
                    ID = role.ID,
                    IsDeputy = false,
                    User = role,
                    UserID = role.ID,
                    Role = role,
                    RoleType = RoleType.Personal
                }
            };
            role.UpdateFromAssociations();

            await roleRepository.InsertAsync(role, cancellationToken);
            return role;
        }

        #endregion
    }
}
