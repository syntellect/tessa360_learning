﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using Tessa.Extensions.PostgreSql.Server;
using Tessa.Platform;
using Tessa.Platform.Data;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Устанавливает свойства на объекте <see cref="IDbScopeContainer"/>, являющемся TestFixture,
    /// с помощью фабрики указанного типа <see cref="IDbFactory"/>.
    /// </summary>
    /// <remarks>
    /// <para>Атрибут должен быть применён только на класс с тестом при условии,
    /// что соответствующий TestFixture (класс) реализует интерфейсы: <see cref="IDbScopeContainer"/> и <see cref="ITestActions"/>.
    /// В противном случае перед выполнением теста будет сгенерировано исключение <see cref="InvalidOperationException"/>.</para>
    /// <para>По умолчанию строка подключения к базе данных находится в конфигурационном файле по имени,
    /// указанном в константе <see cref="TestHelper.DefaultConfigurationString"/>.</para>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class SetupDbScopeAttribute :
        TestActionAttribute,
        IApplyToTest
    {
        #region Constructors

        /// <summary>
        /// Создаёт экземпляр атрибута с указанием типа фабрики, используемой для заполнения
        /// свойств объекта <see cref="IDbScopeContainer"/>.
        /// </summary>
        /// <param name="dbFactoryType">
        /// Тип фабрики, реализующей интерфейс <see cref="IDbFactory"/>.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="dbFactoryType"/> равен <c>null</c>.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// <paramref name="dbFactoryType"/> является типом, не реализующем интерфейс
        /// <see cref="IDbFactory"/>.
        /// </exception>
        public SetupDbScopeAttribute(Type dbFactoryType) => this.DbFactoryType = dbFactoryType;

        /// <summary>
        /// Создаёт экземпляр атрибута с использованием фабрики <see cref="DefaultDbFactory"/>
        /// для заполнения свойств объекта <see cref="IDbScopeContainer"/>.
        /// </summary>
        public SetupDbScopeAttribute() => this.dbFactoryType = typeof(DefaultDbFactory);

        #endregion

        #region DbFactoryDecorator Private Class

        private sealed class DbFactoryDecorator :
            IDbFactory
        {
            #region Constructors

            public DbFactoryDecorator(
                IDbFactory factory,
                string databaseName)
            {
                this.factory = factory;
                this.databaseName = databaseName;
            }

            #endregion

            #region Fields

            private readonly string databaseName;

            private readonly IDbFactory factory;

            #endregion

            #region IDbManagerFactory Members

            public DbManager Create()
            {
                DbManager db = null;
                try
                {
                    db = this.factory.Create();
                    if (db.DataConnection.Connection.Database != this.databaseName)
                    {
                        db.DataConnection.Connection.ChangeDatabase(this.databaseName);
                    }
                }
                catch (Exception)
                {
                    if (db is not null)
                    {
                        db.DisposeAsync().GetAwaiter().GetResult();
                    }

                    throw;
                }

                return db;
            }

            #endregion
        }

        #endregion

        #region Fields

        private IDbFactory dbManagerFactory;

        private static readonly SynchronizedOneTimeRegistrator postgreSqlMapperRegistrator =
            new SynchronizedOneTimeRegistrator(PostgreSqlMapper.Register);

        #endregion

        #region Properties

        private Type dbFactoryType;

        /// <summary>
        /// Тип фабрики для создания объектов <see cref="DbManager"/>.
        /// </summary>
        /// <exception cref="ArgumentNullException">Задаваемое значение равно <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException">
        /// Задаваемое значение является типом, не реализующим интерфейс <see cref="IDbFactory"/>.
        /// </exception>
        public Type DbFactoryType
        {
            get => this.dbFactoryType;
            set
            {
                if (this.dbFactoryType != value)
                {
                    TestHelper.CheckTypeOf<IDbFactory>(value, nameof(DbFactoryType));
                    this.dbFactoryType = value;
                }
            }
        }

        /// <summary>
        /// Имя строки подключения из конфигурационного файла,
        /// используемой для создания объектов <see cref="DbManager"/>.
        /// Значение по умолчанию: <see cref="TestHelper.DefaultConfigurationString"/>.
        /// </summary>
        public string ConfigurationString { get; set; } = TestHelper.DefaultConfigurationString;

        /// <summary>
        /// Тип СУБД, к которой происходит подключение.
        /// </summary>
        public Dbms Dbms { get; set; } = TestHelper.DefaultDbms;

        #endregion

        #region Protected Declarations

        /// <summary>
        /// Экземпляр фабрики, используемой для заполнения свойств объекта <see cref="IDbScopeContainer"/>.
        /// </summary>
        /// <remarks>
        /// Значение свойства может быть получено через отложенную инициализацию.
        /// </remarks>
        protected IDbFactory DbManagerFactory => TestHelper.InitValue(
            this.dbFactoryType,
            ref this.dbManagerFactory);

        #endregion

        #region Properties

        /// <summary>
        /// Признак того, что объект <see cref="DbScope"/> создаётся таким образом,
        /// что вызовы <see cref="DbScope.CreateNew"/> не создают новых соединений с базой данных и продолжают работать в пределах Scope.
        /// По умолчанию значение <see langword="false"/>.
        /// </summary>
        public bool SingleConnectionDbScope { get; set; } = false;

        #endregion

        #region Base Overrides

        /// <summary>
        /// Создаёт экземпляр класса с параметрами по умолчанию.
        /// </summary>
        /// <remarks>Атрибут всегда применяется на TestFixture.</remarks>
        public override ActionTargets Targets => ActionTargets.Suite;

        /// <summary>
        /// Метод, выполняемый перед запуском теста.
        /// </summary>
        /// <param name="test">Информация о тесте.</param>
        public override void BeforeTest(ITest test)
        {
            base.BeforeTest(test);

            var testActions = test.Get<ITestActions>();
            testActions.BeforeInitializeActions.Add(new TestAction(this, BeforeInitializeAsync));
            testActions.AfterOneTimeTearDownActions.Add(new TestAction(this, AfterOneTimeTearDownActionAsync));
        }

        #endregion

        #region Decorate Protected Static Method

        /// <summary>
        /// Создаёт экземпляр фабрики, декорирующей объекты, которые создаются заданной фабрикой <paramref name="factory"/>,
        /// таким образом, чтобы изменять их базу данных сразу после создания.
        /// </summary>
        /// <param name="factory">Декорируемая фабрика объектов.</param>
        /// <param name="databaseName">Имя базы данных.</param>
        /// <returns>Фабрика объектов, являющаяся декоратором для заданной фабрики.</returns>
        public static IDbFactory Decorate(IDbFactory factory, string databaseName) =>
            factory != null && factory.GetType() != typeof(DbFactoryDecorator)
                ? new DbFactoryDecorator(factory, databaseName)
                : factory;

        #endregion

        #region IApplyToTest Members

        /// <inheritdoc/>
        public void ApplyToTest(NUnit.Framework.Internal.Test test)
        {
            TestHelper.SetTestCategory(test.Properties, this.Dbms);
        }

        #endregion

        #region Private Methods

        private static ValueTask BeforeInitializeAsync(object sender)
        {
            var instance = (SetupDbScopeAttribute) sender;
            var test = TestExecutionContext.CurrentContext.CurrentTest;

            if (instance.Dbms == Dbms.PostgreSql)
            {
                postgreSqlMapperRegistrator.Register();
            }

            var container = test.Get<IDbScopeContainer>();
            if (container.DbFactory is null || container.DbScope is null)
            {
                var factory = instance.DbManagerFactory;
                if (factory is IConfigurationStringContainer factoryContainer)
                {
                    factoryContainer.ConfigurationString = instance.ConfigurationString;
                }

                container.DbFactory = factory;
                container.DbScope = instance.SingleConnectionDbScope
                    ? new SingleConnectionDbScope(factory.Create)
                    : new DbScope(factory.Create);
            }

            return new ValueTask();
        }

        private static ValueTask AfterOneTimeTearDownActionAsync(object sender)
        {
            var test = TestExecutionContext.CurrentContext.CurrentTest;
            var container = test.Get<IDbScopeContainer>();

            container.DbFactory = null;
            container.DbScope = null;

            return new ValueTask();
        }

        #endregion
    }
}