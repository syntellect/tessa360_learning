﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using Tessa.Platform;
using Tessa.Platform.Data;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// <para>Создаёт временную базу данных, выполняет на ней заданный скрипт и устанавливает
    /// свойства на объекте <see cref="IDbScopeContainer"/>, являющемся TestFixture,
    /// с помощью фабрики указанного типа <see cref="IDbFactory"/> таким образом,
    /// чтобы объект <see cref="DbManager"/> ссылался на временную базу данных.</para>
    /// <para>Затем выполняются все тесты в TestFixture, которые могут считывать и изменять
    /// временную базу данных. После завершения тестов временная база данных удаляется.</para>
    /// </summary>
    /// <remarks>
    /// <para>Атрибут должен быть применён только на класс с тестом при условии,
    /// что соответствующий TestFixture (класс) реализует интерфейсы: <see cref="IDbScopeContainer"/> и <see cref="ITestActions"/>.
    /// В противном случае перед выполнением теста будет сгенерировано исключение <see cref="InvalidOperationException"/>.</para>
    /// <para>Фабрика, доступная через свойство <see cref="IDbScopeContainer.DbFactory"/> на объекте
    /// <see cref="IDbScopeContainer"/>, создаёт объекты <see cref="DbManager"/>, указывающие
    /// на временную базу данных.</para>
    /// <para>По умолчанию строка подключения к временной базе данных находится в конфигурационном файле по имени,
    /// указанном в константе <see cref="TestHelper.TempConfigurationStringMs"/>.</para>
    /// </remarks>
    public class SetupTempDbAttribute :
        SetupDbScopeAttribute
    {
        #region Constructors

        /// <summary>
        /// Создаёт экземпляр атрибута с указанием имени скрипта, выполняемого на временной базе данных.
        /// </summary>
        /// <param name="testScriptFileNames">
        /// Имена SQL-скриптов, добавленных в ресурсы сборки (Build Action = Embedded Resource) и выполняемых в заданном порядке.
        /// Скрипты должны помещаться в папке <c>Sql</c> проекта содержащего класс к которому применён данный атрибут.
        /// Пример: <c>"Default.sql"</c>, <c>"SubFolder/Cards.sql"</c>.
        /// </param>
        public SetupTempDbAttribute(params string[] testScriptFileNames)
        {
            Check.ArgumentNotNull(testScriptFileNames, nameof(testScriptFileNames));

            foreach (string fileName in testScriptFileNames)
            {
                if (string.IsNullOrWhiteSpace(fileName))
                {
                    throw new ArgumentException("Invalid SQL script file name.", nameof(testScriptFileNames));
                }
            }

            this.testScriptFileNames = testScriptFileNames;
            this.ConfigurationString = TestHelper.TempConfigurationStringMs;
        }

        /// <summary>
        /// Создаёт экземпляр атрибута с указанием имени скрипта, выполняемого на временной базе данных.
        /// </summary>
        /// <param name="dbms">Тип СУБД.</param>
        /// <param name="configurationString">Имя используемой строки поддключения из конфигурационного файла.</param>
        /// <param name="testScriptFileNames">
        /// Имена SQL-скриптов, добавленных в ресурсы сборки (Build Action = Embedded Resource) и выполняемых в заданном порядке.
        /// Скрипты должны помещаться в папке <c>Sql</c> проекта содержащего класс к которому применён данный атрибут.
        /// Пример: <c>"Default.sql"</c>, <c>"SubFolder/Cards.sql"</c>.
        /// </param>
        public SetupTempDbAttribute(
            Dbms dbms,
            string configurationString,
            params string[] testScriptFileNames)
            : this(testScriptFileNames)
        {
            this.Dbms = dbms;
            this.ConfigurationString = configurationString;
        }

        #endregion

        #region Fields

        private readonly string[] testScriptFileNames;

        private ConfigurationConnection connection;

        #endregion

        #region Properties

        /// <summary>
        /// Признак того, что временную базу данных следует удалить после завершения всех тестов.
        /// По умолчанию равно <c>true</c>.
        /// </summary>
        public bool RemoveDatabase { get; set; } = true;

        /// <summary>
        /// Признак того, что имя базы данных надо рандомизировать в зависимости от имени класса тестов.
        /// Это позволяет параллельно выполнять тесты на разных базах данных.
        /// </summary>
        public bool RandomizeDbName { get; set; } = true;

        #endregion

        #region Base Overrides

        /// <summary>
        /// Создаёт экземпляр класса с параметрами по умолчанию.
        /// </summary>
        /// <remarks>Атрибут всегда применяется на TestFixture.</remarks>
        public override ActionTargets Targets => ActionTargets.Suite;

        /// <summary>
        /// Метод, выполняемый перед запуском теста.
        /// </summary>
        /// <param name="test">Информация о тесте.</param>
        public override void BeforeTest(ITest test)
        {
            base.BeforeTest(test);

            var testActions = test.Get<ITestActions>();
            testActions.BeforeInitializeActions.Add(new TestAction(this, BeforeInitializeAsync));
            testActions.AfterOneTimeTearDownActions.Add(new TestAction(this, AfterOneTimeTearDownActionAsync));
        }

        #endregion

        #region Private Methods

        private static async ValueTask BeforeInitializeAsync(object sender)
        {
            var instance = (SetupTempDbAttribute) sender;
            var test = TestExecutionContext.CurrentContext.CurrentTest;

            // если при загрузке файла произойдёт ошибка (неверное имя файла), то соединение с базой открыто не будет
            var sqlTextScripts = TestHelper.GetSqlTextScripts(
                test.Fixture.GetType().Assembly,
                instance.testScriptFileNames);

            // здесь уже есть активное соединение с базой
            var container = test.Get<IDbScopeContainer>();

            instance.connection = ConfigurationManager.Connections[instance.ConfigurationString];

            if (instance.RandomizeDbName)
            {
                string hash = test.TypeInfo.FullName.GetConstantHashCode().ToString("X4");

                instance.connection = TestHelper.RewriteConnection(
                    instance.connection,
                    (databaseName, _) => databaseName + "_" + hash,
                    out _);
            }

            await TestHelper.CreateDatabaseAsync(instance.connection);

            var factory = instance.DbManagerFactory;
            if (factory is IConfigurationStringContainer factoryContainer)
            {
                factoryContainer.ConnectionString = instance.connection.ConnectionString;
            }

            container.DbFactory = factory;
            container.DbScope = instance.SingleConnectionDbScope
                ? new SingleConnectionDbScope(factory.Create)
                : new DbScope(factory.Create);

            await TestHelper.ExecuteSqlScriptsAsync(container.DbScope, sqlTextScripts);
        }

        private static async ValueTask AfterOneTimeTearDownActionAsync(object sender)
        {
            var instance = (SetupTempDbAttribute) sender;
            var test = TestExecutionContext.CurrentContext.CurrentTest;

            if (instance.RemoveDatabase)
            {
                var container = test.Get<IDbScopeContainer>();

                await using (container.DbScope.Create())
                {
                    await TestHelper.DropDatabaseAsync(instance.connection);
                }
            }
        }

        #endregion
    }
}