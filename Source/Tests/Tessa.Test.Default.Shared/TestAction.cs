﻿using System;
using System.Threading.Tasks;
using Tessa.Platform;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Представляет действие.
    /// </summary>
    public sealed class TestAction :
        ITestAction
    {
        #region Fields

        private readonly Func<object, ValueTask> action;

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр структуры <see cref="TestAction"/>.
        /// </summary>
        /// <param name="sender">Источник действия.</param>
        /// <param name="action">Действие. При выполнении вместо первого параметра будет подставлен объект заданный в <paramref name="sender"/>.</param>
        public TestAction(
            object sender,
            Func<object, ValueTask> action)
        {
            Check.ArgumentNotNull(sender, nameof(sender));
            Check.ArgumentNotNull(action, nameof(action));

            this.Sender = sender;
            this.action = action;
        }

        #endregion

        #region ITestAction Members

        /// <inheritdoc/>
        public object Sender { get; }

        /// <inheritdoc/>
        public TestActionOptions Options { get; set; } = TestActionOptions.Default;

        /// <inheritdoc/>
        public TestActionState State { get; set; } = TestActionState.NotExecuted;

        /// <inheritdoc/>
        public ValueTask ExecuteAsync() =>
            this.action(this.Sender);

        #endregion
    }
}
