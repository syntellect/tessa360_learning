﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LinqToDB.DataProvider;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;
using Tessa.Cards;
using Tessa.Cards.Caching;
using Tessa.Localization;
using Tessa.Platform;
using Tessa.Platform.Data;
using Tessa.Platform.Runtime;
using Tessa.Roles;
using Tessa.Test.Default.Shared.Kr;
using Unity;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Абстрактный базовый класс для тестов.
    /// </summary>
    public abstract class TestBase :
        ResourceAssemblyManager,
        IUnityContainerHolder,
        IDbScopeContainer,
        ITestActions
    {
        #region Nested Types

        private sealed class TestDbFactory :
            IDbFactory
        {
            #region Fields

            private readonly IDataProvider dataProvider;
            private readonly string connectionString;

            #endregion

            #region Constructors

            public TestDbFactory(
                IDataProvider dataProvider,
                string connectionString)
            {
                this.dataProvider = dataProvider;
                this.connectionString = connectionString;
            }

            #endregion

            #region IDbFactory Members

            /// <inheritdoc/>
            public DbManager Create() =>
                new DbManager(this.dataProvider, this.connectionString);

            #endregion
        }

        #endregion

        #region Fields

        private readonly AsyncLock asyncLock;

        #endregion

        #region Properties

        /// <summary>
        /// Возвращает текущую сессию или значение по умолчанию для типа, если объект не зарегистрирован в <see cref="UnityContainer"/>.
        /// </summary>
        protected ISession Session { get; private set; }

        /// <summary>
        /// Возвращает репозиторий для управления карточками или значение по умолчанию для типа, если объект не зарегистрирован в <see cref="UnityContainer"/>.
        /// </summary>
        protected ICardRepository CardRepository { get; private set; }

        /// <summary>
        /// Возвращает репозиторий для управления карточками с конфигурацией по умолчанию (<see cref="CardRepositoryNames.Default"/>) или значение по умолчанию для типа, если объект не зарегистрирован в <see cref="UnityContainer"/>.
        /// </summary>
        protected ICardRepository DefaultCardRepository { get; private set; }

        /// <summary>
        /// Возвращает объект, управляющий операциями с карточками или значение по умолчанию для типа, если объект не зарегистрирован в <see cref="UnityContainer"/>.
        /// </summary>
        protected ICardManager CardManager { get; private set; }

        /// <summary>
        /// Возвращает метаинформацию, необходимую для использования типов карточек совместно с пакетом карточек или значение по умолчанию для типа, если объект не зарегистрирован в <see cref="UnityContainer"/>.
        /// </summary>
        protected ICardMetadata CardMetadata { get; private set; }

        /// <summary>
        /// Возвращает кэш карточек или значение по умолчанию для типа, если объект не зарегистрирован в <see cref="UnityContainer"/>.
        /// </summary>
        protected ICardCache CardCache { get; private set; }

        /// <summary>
        /// Возвращает зависимости используемые объектами управляющими жизненным циклом карточек или значение по умолчанию для типа, если объект не зарегистрирован в <see cref="UnityContainer"/>.
        /// </summary>
        protected ICardLifecycleCompanionDependencies CardLifecycleDependencies => this.UnityContainer.TryResolve<ICardLifecycleCompanionDependencies>();

        /// <summary>
        /// Возвращает объект управляющий удалением карточек после завершения каждого теста или значение по умолчанию для типа, если объект <see cref="ICardLifecycleCompanionDependencies"/> не зарегистрирован в <see cref="UnityContainer"/>.
        /// </summary>
        protected TestCardManager TestCardManager { get; private set; }

        /// <summary>
        /// Возвращает объект управляющий удалением карточек после завершения всех тестов, включая дочерние или значение по умолчанию для типа, если объект <see cref="ICardLifecycleCompanionDependencies"/> не зарегистрирован в <see cref="UnityContainer"/>.
        /// </summary>
        protected TestCardManager TestCardManagerOnce { get; private set; }

        /// <summary>
        /// Возвращает значение, показывающее, что инициализация зависимостей была выполнена.
        /// </summary>
        protected bool IsInitialized { get; private set; }

        /// <summary>
        /// Возвращает конфигуратор тестовой базы данных или значение по умолчанию для типа, если объект не зарегистрирован в <see cref="UnityContainer"/>.
        /// </summary>
        /// <remarks>
        /// Все запланированные действия будут выполнены автоматически после завершения выполнения метода. Безопасно вызывать исполнение запланированных действий явным образом.
        /// </remarks>
        protected TestConfigurationBuilder TestConfigurationBuilder { get; private set; }

        #region IDbScopeContainer Members

        /// <inheritdoc/>
        public IDbFactory DbFactory { get; set; }

        /// <inheritdoc/>
        public IDbScope DbScope { get; set; }

        #endregion

        #region IUnityContainerHolder

        /// <inheritdoc/>
        public virtual IUnityContainer UnityContainer { get; private set; }

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="TestBase"/>.
        /// </summary>
        protected TestBase()
        {
            this.asyncLock = new AsyncLock();
            this.BeforeInitializeActions = new List<ITestAction>();
            this.AfterInitializeActions = new List<ITestAction>();
            this.BeforeSetUpActions = new List<ITestAction>();
            this.AfterSetUpActions = new List<ITestAction>();
            this.BeforeOneTimeTearDownActions = new List<ITestAction>();
            this.AfterOneTimeTearDownActions = new List<ITestAction>();
            this.BeforeTearDownActions = new List<ITestAction>();
            this.AfterTearDownActions = new List<ITestAction>();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Выполняет действия перед выполнением тестов. Выполняет инициализацию зависимостей строго один раз для всех тестов.
        /// </summary>
        /// <returns>Асинхронная задача.</returns>
        [SetUp]
        public Task SetUpAsync()
        {
            LocalizationManager.SetEnglishLocalization();

            return this.IsInitialized
                ? this.SetUpSequenceAsync(false)
                : this.InitializeSequenceAsync();
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Выполняет инициализацию тестов. Данный метод выполняется один раз для всех тестов.
        /// </summary>
        /// <returns>Асинхронная задача.</returns>
        /// <remarks>
        /// В реализации по умолчанию в <see cref="TestBase"/> не выполняет никаких действий.<para/>
        /// Для настройки тестовой базы данных используйте объект предоставляемый свойством <see cref="TestConfigurationBuilder"/>. Все запланированные действия в <see cref="TestConfigurationBuilder"/> будут автоматически выполнены после завершения этого метода. Безопасно вызывать исполнение запланированных действий явным образом.<para/>
        /// Если в <see cref="UnityContainer"/> зарегистрирован объект типа <see cref="IDbScope"/>, то данный метод выполняется в контексте области видимости соединения с базой данных.
        /// </remarks>
        protected virtual Task InitializeCoreAsync() => Task.CompletedTask;

        /// <summary>
        /// Выполняется для каждого теста.
        /// </summary>
        /// <returns>Асинхронная задача.</returns>
        /// <remarks>
        /// В реализации по умолчанию в <see cref="TestBase"/> не выполняет никаких действий.
        /// </remarks>
        protected virtual Task SetUpCoreAsync() => Task.CompletedTask;

        /// <summary>
        /// Выполняет действия при инициализации каждого теста, только если инициализацию зависимостей не требовалось выполнять.
        /// </summary>
        /// <returns>Асинхронная задача.</returns>
        /// <remarks>В реализации по умолчанию в <see cref="TestBase"/> не выполняет никаких действий.</remarks>
        protected virtual Task NeedInitializeCoreAsync() => Task.CompletedTask;

        /// <summary>
        /// Выполняет действия при завершении каждого теста. Метод гарантированно будет вызван, даже если возникнет исключение.
        /// </summary>
        /// <returns>Асинхронная задача.</returns>
        [TearDown]
        public async Task TearDownAsync()
        {
            const FailureSite site = FailureSite.TearDown;
            static void SafeRecordAction(Exception e) => TestHelper.SetAssertionResult(site, e);

            await ExecuteAllActionsAsync(this.BeforeTearDownActions, SafeRecordAction);

            if (this.DbScope is not null)
            {
                await SafeExecuteAsync(this.RemoveCardAfterTestAsync, SafeRecordAction);
            }

            await SafeExecuteAsync(this.TearDownCoreAsync, SafeRecordAction);

            await ExecuteAllActionsReverseAsync(this.AfterTearDownActions, SafeRecordAction);
        }

        /// <summary>
        /// Выполняет действия при завершении каждого теста.
        /// </summary>
        /// <returns>Асинхронная задача.</returns>
        /// <remarks>В реализации по умолчанию в <see cref="TestBase"/> не выполняет никаких действий.</remarks>
        protected virtual Task TearDownCoreAsync() => Task.CompletedTask;

        /// <summary>
        /// Выполняет действия один раз после выполнения всех дочерних тестов. Метод гарантированно будет вызван, даже если возникнет исключение.
        /// </summary>
        /// <returns>Асинхронная задача.</returns>
        [OneTimeTearDown]
        public async Task OneTimeTearDownAsync()
        {
            Action<Exception> safeRecordAction = TestExecutionContext.CurrentContext.CurrentResult.RecordTearDownException;

            await ExecuteAllActionsAsync(this.BeforeOneTimeTearDownActions, safeRecordAction);

            if (this.DbScope is not null)
            {
                await SafeExecuteAsync(this.RemoveCardOnceAfterTestAsync, safeRecordAction);
            }

            await SafeExecuteAsync(this.OneTimeTearDownCoreAsync, safeRecordAction);

            await ExecuteAllActionsReverseAsync(this.AfterOneTimeTearDownActions, safeRecordAction);

            this.asyncLock.Dispose();
        }

        /// <summary>
        /// Выполняет действия один раз после выполнения всех дочерних тестов.
        /// </summary>
        /// <returns>Асинхронная задача.</returns>
        /// <remarks>В реализации по умолчанию в <see cref="TestBase"/> не выполняет никаких действий.</remarks>
        protected virtual Task OneTimeTearDownCoreAsync() => Task.CompletedTask;

        /// <summary>
        /// Удаляет карточки запланированные к удалению после завершения каждого теста.
        /// </summary>
        /// <returns>Асинхронная задача.</returns>
        protected virtual async Task RemoveCardAfterTestAsync()
        {
            if (this.TestCardManager is not null)
            {
                await this.TestCardManager.AfterTestAsync();
            }
        }

        /// <summary>
        /// Удаляет карточки запланированные к удалению после завершения всех дочерних тестов.
        /// </summary>
        /// <returns>Асинхронная задача.</returns>
        protected virtual async Task RemoveCardOnceAfterTestAsync()
        {
            if (this.TestCardManagerOnce is not null)
            {
                await this.TestCardManagerOnce.AfterTestAsync();
            }
        }

        /// <summary>
        /// Возвращает репозиторий для управления ролевой моделью.
        /// </summary>
        protected virtual IRoleRepository RoleRepository => this.UnityContainer?.TryResolve<IRoleRepository>();

        /// <summary>
        /// Создаёт и инициализирует Unity контейнер.
        /// </summary>
        /// <returns>Unity контейнер.</returns>
        protected virtual ValueTask<IUnityContainer> CreateContainerAsync() =>
            // есть много серверных тестов, которые не используют контейнер Unity, но выполняют инициализацию в InitializeSequenceAsync
            new(new UnityContainer());

        #endregion

        #region ITestActions Members

        /// <inheritdoc/>
        public IList<ITestAction> BeforeInitializeActions { get; }

        /// <inheritdoc/>
        public IList<ITestAction> AfterInitializeActions { get; }

        /// <inheritdoc/>
        public IList<ITestAction> BeforeSetUpActions { get; }

        /// <inheritdoc/>
        public IList<ITestAction> AfterSetUpActions { get; }

        /// <inheritdoc/>
        public IList<ITestAction> BeforeOneTimeTearDownActions { get; }

        /// <inheritdoc/>
        public IList<ITestAction> AfterOneTimeTearDownActions { get; }

        /// <inheritdoc/>
        public IList<ITestAction> BeforeTearDownActions { get; }

        /// <inheritdoc/>
        public IList<ITestAction> AfterTearDownActions { get; }

        #endregion

        #region Private Methods

        private async Task InitializeSequenceAsync()
        {
            using (await this.asyncLock.EnterAsync())
            {
                if (this.IsInitialized)
                {
                    await this.SetUpSequenceAsync(false);
                    return;
                }

                try
                {
                    await TestHelper.InitializeTestPlatformAsync();

                    await ExecuteAllActionsAsync(this.BeforeInitializeActions);

                    await TestHelper.InitializeDefaultLocalizationAsync(this.ResourceAssembly);

                    this.UnityContainer = await this.CreateContainerAsync();

                    // есть много серверных тестов, которые не используют контейнер Unity, но выполняют инициализацию
                    this.Session = this.UnityContainer.TryResolve<ISession>();

                    if (this.Session is not null)
                    {
                        this.CardRepository = this.UnityContainer.TryResolve<ICardRepository>();
                        this.DefaultCardRepository = this.UnityContainer.TryResolve<ICardRepository>(CardRepositoryNames.Default);
                        this.CardManager = this.UnityContainer.TryResolve<ICardManager>();
                        this.CardMetadata = this.UnityContainer.TryResolve<ICardMetadata>();
                        this.CardCache = this.UnityContainer.TryResolve<ICardCache>();

                        var deps = this.CardLifecycleDependencies;
                        if (deps is not null)
                        {
                            this.TestCardManager = new TestCardManager(deps);
                            this.TestCardManagerOnce = new TestCardManager(deps);

                            if (this.CardRepository is not null)
                            {
                                this.TestConfigurationBuilder = this.UnityContainer.TryResolve<TestConfigurationBuilder>();
                            }
                        }
                    }

                    var dbScope = this.UnityContainer.TryResolve<IDbScope>();

                    if (dbScope is null)
                    {
                        await this.InitializeCoreAsync();
                    }
                    else
                    {
                        if (this.DbScope is null)
                        {
                            this.DbScope = dbScope;
                        }

                        await using (dbScope.Create())
                        {
                            if (this.DbFactory is null)
                            {
                                var dataConnection = this.DbScope.Db.DataConnection;
                                this.DbFactory = new TestDbFactory(
                                    dataConnection.DataProvider,
                                    dataConnection.ConnectionString);
                            }

                            await this.InitializeCoreAsync();
                        }
                    }

                    if (this.TestConfigurationBuilder is not null)
                    {
                        await this.TestConfigurationBuilder.GoAsync();
                    }

                    await ExecuteAllActionsReverseAsync(this.AfterInitializeActions);
                    this.IsInitialized = true;
                }
                catch
                {
                    // Задание статуса выполнения для остановки выполнения параллельно запущенных тестов из этого же TestFixture.
                    TestExecutionContext.CurrentContext.ExecutionStatus = TestExecutionStatus.StopRequested;

                    // Повторное создание оригинального исключения для остановки выполнения текущего теста.
                    throw;
                }
            }

            await this.SetUpSequenceAsync(true);
        }

        private async Task SetUpSequenceAsync(bool initialization)
        {
            await ExecuteAllActionsAsync(this.BeforeSetUpActions);

            await this.SetUpCoreAsync();

            await ExecuteAllActionsReverseAsync(this.AfterSetUpActions);
            if (!initialization)
            {
                await this.NeedInitializeCoreAsync();
            }
        }

        private static async ValueTask ExecuteAllActionsAsync(
            IList<ITestAction> testActions,
            Action<Exception> safeRecordAction = default)
        {
            for (var i = 0; i < testActions.Count; i++)
            {
                var testAction = testActions[i];
                await ExecuteActionAsync(testAction, safeRecordAction);
            }
        }

        private static async ValueTask ExecuteAllActionsReverseAsync(
            IList<ITestAction> testActions,
            Action<Exception> safeRecordAction = default)
        {
            for (var i = testActions.Count - 1; i >= 0; i--)
            {
                var testAction = testActions[i];
                await ExecuteActionAsync(testAction, safeRecordAction);
            }
        }

        private static async ValueTask ExecuteActionAsync(
            ITestAction testAction,
            Action<Exception> safeRecordAction = default)
        {
            if (testAction.State == TestActionState.Completed
                && testAction.Options.Has(TestActionOptions.RunOnce))
            {
                return;
            }

            testAction.State = TestActionState.InProgress;

            await SafeExecuteAsync(testAction.ExecuteAsync, safeRecordAction);

            testAction.State = TestActionState.Completed;
        }

        private static async ValueTask SafeExecuteAsync(
            Func<ValueTask> funcAsync,
            Action<Exception> safeRecordAction = default)
        {
            try
            {
                await funcAsync();
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception e) when (safeRecordAction is not null)
            {
                safeRecordAction(e);
            }
        }

        private static async Task SafeExecuteAsync(
            Func<Task> funcAsync,
            Action<Exception> safeRecordAction = default)
        {
            try
            {
                await funcAsync();
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception e) when (safeRecordAction is not null)
            {
                safeRecordAction(e);
            }
        }

        #endregion
    }
}