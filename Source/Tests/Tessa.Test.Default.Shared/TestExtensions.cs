﻿using System;
using NUnit.Framework.Interfaces;
using Tessa.Platform;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Методы-расширения для реализации тестов.
    /// </summary>
    public static class TestExtensions
    {
        #region ITest Extensions

        /// <summary>
        /// Возвращает экземпляр класса TestFixture, приведённый к интерфейсу <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T">Тип интерфейса, к которому должен быть приведён экземпляр класса TestFixture.</typeparam>
        /// <param name="test">Информация о тесте.</param>
        /// <returns>Экземпляр класса TestFixture, приведённый к интерфейсу <typeparamref name="T"/>.</returns>
        /// <exception cref="ArgumentNullException">Параметр <paramref name="test"/> равен <c>null</c>.</exception>
        /// <exception cref="InvalidOperationException">
        /// Экземпляр класса TestFixture, полученный из <paramref name="test"/>,
        /// не реализует интерфейс <typeparamref name="T"/>.
        /// </exception>
        public static T Get<T>(this ITest test)
            where T : class
        {
            Check.ArgumentNotNull(test, nameof(test));

            if (test.Fixture is null)
            {
                throw new ArgumentException("Test fixture is null.", nameof(test));
            }

            if (!(test.Fixture is T container))
            {
                throw new InvalidOperationException($"Fixture ({test.Fixture.GetType().FullName}) should implement {typeof(T).FullName}.");
            }

            return container;
        }

        #endregion
    }
}
