﻿using Tessa.Platform;
using Tessa.Platform.Storage;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Настройки для тестов.
    /// </summary>
    public static class TestSettings
    {
        #region Properties

        /// <summary>
        /// Путь к файловому хранилищу.
        /// </summary>
        /// <remarks>Рекомендуется использовать рандомизированное значение, предоставляемое <see cref="TestHelper.GetFileStoragePath"/> для предотвращения конфликтов при работе с файлами при параллельном выполнении тестов.</remarks>
        public static string FileStoragePath => ConfigurationManager.Settings.TryGet<string>("FileStoragePath");

        /// <summary>
        /// Использовать режим обратной совместимости при именовании файлов.
        /// </summary>
        /// <remarks>
        /// Всегда используем этот режим, чтобы при удалении карточек или файлов
        /// очищались бы соответствующие папки.
        /// </remarks>
        public static bool UseSimpleNamingScheme => ConfigurationManager.Settings.TryGet<bool>("FileUseSimpleNamingScheme");

        #endregion
    }
}
