﻿using Unity;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Реализация <see cref="IUnityContainerHolder"/> по умолчанию.
    /// </summary>
    public abstract class UnityContainerHolder :
        IUnityContainerHolder
    {
        #region IUnityContainerHolder Members

        private IUnityContainer unityContainer;

        /// <summary>
        /// Контейнер <c>Unity</c>, содержащий основные зависимости теста.
        /// </summary>
        public IUnityContainer UnityContainer => this.unityContainer ??= new UnityContainer();

        #endregion
    }
}
