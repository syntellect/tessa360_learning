﻿using System;
using Tessa.Platform;
using Tessa.Platform.Validation;

namespace Tessa.Test.Default.Shared
{
    /// <summary>
    /// Объект выполняющий валидацию результатов валидации.
    /// </summary>
    public sealed class ValidationResultItemValidator
    {
        #region Constants

        private const int DefaultExpectedCount = 1;

        #endregion

        #region Fields

        private readonly Func<IValidationResultItem, bool> validationFunc;

        #endregion

        #region Properties

        /// <summary>
        /// Возвращает имя объекта выполняющего валидацию.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Возвращает ожидаемое число срабатываний объекта валидации.
        /// </summary>
        public int ExpectedCount { get; }

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ValidationResultItemValidator"/>.
        /// </summary>
        /// <param name="validationFunc">Метод выполняющий валидацию.</param>
        /// <param name="expectedCount">Ожидаемое число срабатываний объекта валидации.</param>
        /// <param name="name">Имя объекта выполняющего валидацию.</param>
        /// <exception cref="ArgumentOutOfRangeException">Expected number of operations with validation object is negative.</exception>
        public ValidationResultItemValidator(
            Func<IValidationResultItem, bool> validationFunc,
            int expectedCount = DefaultExpectedCount,
            string name = default)
        {
            Check.ArgumentNotNull(validationFunc, nameof(validationFunc));

            if (expectedCount < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(expectedCount), expectedCount, "Expected number of operations with validation object is negative.");
            }

            this.validationFunc = validationFunc;
            this.ExpectedCount = expectedCount;
            this.Name = name;
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ValidationResultItemValidator"/>.
        /// </summary>
        /// <param name="item">Сообщение о валидации на равенство которому выполняется проверка.</param>
        /// <param name="expectedCount">Ожидаемое число срабатываний объекта валидации.</param>
        /// <param name="name">Имя объекта выполняющего валидацию.</param>
        /// <exception cref="ArgumentOutOfRangeException">Ожидаемое число срабатываний объекта валидации меньше нуля.</exception>
        public ValidationResultItemValidator(
            IValidationResultItem item,
            int expectedCount = DefaultExpectedCount,
            string name = default)
            : this((itemActual) => itemActual != null && itemActual.Equals(item),
                  expectedCount,
                  name)
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ValidationResultItemValidator"/>.
        /// </summary>
        /// <param name="type">Тип сообщения о валидации которому должно соответствовать проверяемое сообщение.</param>
        /// <param name="key">Ключ сообщения о результате валидации который должно иметь проверяемое сообщение.</param>
        /// <param name="expectedCount">Ожидаемое число срабатываний объекта валидации.</param>
        /// <param name="name">Имя объекта выполняющего валидацию.</param>
        /// <exception cref="ArgumentOutOfRangeException">Ожидаемое число срабатываний объекта валидации меньше нуля.</exception>
        /// <remarks>
        /// Не рекомендуется использовать данный конструктор при задании значения <see cref="ValidationKey.Unknown"/> параметру <paramref name="key"/> из-за невозможности гарантирования правильности выполнения проверки.<para/>
        /// Данный конструктор имеет смысл использовать при задании значения параметра <paramref name="key"/> равным <see cref="ValidationKey.Unknown"/> только при проверке на отсутствие сообщений валидации с таким ключём. Для этого необходимо задать параметр <paramref name="expectedCount"/> равным 0.</remarks>
        public ValidationResultItemValidator(
            ValidationResultType type,
            ValidationKey key,
            int expectedCount = DefaultExpectedCount,
            string name = default)
            : this((itemActual) => itemActual != null && itemActual.Type == type && itemActual.Key == key,
                  expectedCount,
                  name)
        {
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Проверяет указанное сообщение валидации.
        /// </summary>
        /// <param name="validationResult">Проверяемое сообщение.</param>
        /// <returns>Значение <see langword="true"/>, если валидация пройдена успешно, иначе - <see langword="false"/>.</returns>
        public bool Validate(
            IValidationResultItem validationResult)
        {
            return this.validationFunc(validationResult);
        }

        #endregion
    }
}
