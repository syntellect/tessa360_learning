﻿using Tessa.Platform.Scopes;

namespace Tessa.Test.Default.Shared.Web
{
    /// <summary>
    /// Определяет контекст в котором доступен <see cref="IWebApplicationFactory"/>.
    /// </summary>
    public sealed class WebApplicationFactoryScope
    {
        #region Static

        /// <summary>
        /// Создаёт область видимости для значения в текущем потоке.
        /// </summary>
        /// <param name="webApplicationFactory">Объект, который должен быть доступен в создаваемой области видимости.</param>
        /// <returns>
        /// Созданная область видимости.
        /// </returns>
        public static IInheritableScopeInstance<IWebApplicationFactory> Create(IWebApplicationFactory webApplicationFactory) =>
            InheritableRetainingScope<IWebApplicationFactory>.Create(() => webApplicationFactory);

        /// <summary>
        /// Текущий контекст <see cref="IWebApplicationFactory"/>.
        /// </summary>
        public static IWebApplicationFactory Current => InheritableRetainingScope<IWebApplicationFactory>.Value;

        /// <summary>
        /// Признак того, что текущий код выполняется внутри операции с контекстом <see cref="IWebApplicationFactory"/>,
        /// а свойство <see cref="Current"/> ссылается на действительный контекст.
        /// </summary>
        /// <remarks>
        /// Если текущее свойство возвращает <c>false</c>, то свойство <see cref="Current"/>
        /// возвращает ссылку на пустой контекст.
        /// </remarks>
        public static bool HasCurrent => Current is not null;

        #endregion
    }
}
