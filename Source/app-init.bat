@echo off
rem Use OEM-866 (Cyrillic) encoding

setlocal EnableExtensions
setlocal EnableDelayedExpansion

echo Searching for absent app-local-*.json files

for %%a in (
    "app-local-client.json",
    "app-local-server.json"
) do (
    if not exist "%%a" (
        echo    ^> Creating "%%~a" from "%%~a.sample"
        echo F|xcopy "%%~a.sample" "%%~a" /Q /R /Y>nul
    )
)

echo;
pause

endlocal
