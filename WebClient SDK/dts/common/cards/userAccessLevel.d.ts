export declare enum UserAccessLevel {
    /**
     * Обычный пользователь.
     */
    Regular = 0,
    /**
     * Администратор системы, которому доступны расширенные привилегии.
     */
    Administrator = 1
}
