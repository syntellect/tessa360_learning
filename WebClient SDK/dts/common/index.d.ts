import * as cards from './cards';
export { cards };
import * as utility from './utility';
export { utility };
export * from './previewConsts';
export interface IDictionary<T> {
    [key: string]: T;
}
