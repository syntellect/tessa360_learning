export declare const supportedPdfExtensions: () => any;
export declare const supportedImageExtensions: string[];
export declare const supportedHtmlExtensions: string[];
export declare const supportedTxtExtensions: string[];
