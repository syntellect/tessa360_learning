/**
 * Описывает расположение элемента
 */
export declare enum ElementSide {
    /**
     * Слева
     * @type {Number}
     */
    ELEMENT_LEFT_SIDE_MENU = 1,
    /**
     * Справа
     * @type {Number}
     */
    ELEMENT_RIGHT_SIDE_MENU = 2
}
