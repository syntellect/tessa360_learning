import React from 'react';
import { IFileVersion } from 'tessa/files';
import { ICustomEditorViewModel } from 'tessa/ui/customFileEditor/customEditorViewModel';
import { CustomPreviewViewModelFactory } from 'tessa/ui/customFileEditor/customPreviewViewModelFactory';
export interface CustomPreviewerProps {
    previewFileVersion: IFileVersion | null;
    previewViewModelFactory: CustomPreviewViewModelFactory;
}
export interface CustomPreviewerState {
    viewModel: ICustomEditorViewModel | null;
    mainErrorMsg: string | null;
    additionalErrorMsg: string | null;
}
export declare class CustomPreviewer extends React.PureComponent<CustomPreviewerProps, CustomPreviewerState> {
    constructor(props: CustomPreviewerProps);
    componentDidMount(): Promise<void>;
    componentDidUpdate(prevProps: CustomPreviewerProps): Promise<void>;
    private tryCreateViewModel;
    private handleError;
    render(): JSX.Element | null;
}
