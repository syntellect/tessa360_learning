import * as React from 'react';
declare class NotFound extends React.Component {
    render(): JSX.Element;
}
export default NotFound;
