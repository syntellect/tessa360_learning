import { TileExtension, ITileGlobalExtensionContext } from 'tessa/ui/tiles';
/**
 * Пример тайла, который создает задачу в карточке.
 */
export declare class CustomBPTileExtension extends TileExtension {
    initializingGlobal(context: ITileGlobalExtensionContext): void;
    private static startCustomBP;
    private static enableIfCard;
}
