import { ApplicationExtension, IApplicationExtensionMetadataContext } from 'tessa';
import { TileExtension, ITileGlobalExtensionContext } from 'tessa/ui/tiles';
export declare class AdditionalMetaInitializationExtension extends ApplicationExtension {
    afterMetadataReceived(context: IApplicationExtensionMetadataContext): void;
}
export declare class AdditionalMetaTileExtension extends TileExtension {
    initializingGlobal(_context: ITileGlobalExtensionContext): void;
}
