import { TabPanelUIExtension, ITabPanelUIExtensionContext } from 'tessa/ui';
export declare class CustomTabPanelButtonUIExtension extends TabPanelUIExtension {
    initialize(context: ITabPanelUIExtensionContext): void;
}
