import { ILoginExtensionContext, LoginExtension } from 'tessa/ui/login';
export declare class ExampleLoginExtension extends LoginExtension {
    initializing(context: ILoginExtensionContext): void;
    initialized(context: ILoginExtensionContext): void;
}
