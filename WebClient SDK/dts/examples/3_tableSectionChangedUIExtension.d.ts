import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * Отслеживать добавление\удаление строк в коллекционной секции
 *
 * Когда изменяется секция "Получатели", то в поле "Тема" добавляется строка.
 */
export declare class TableSectionChangedUIExtension extends CardUIExtension {
    private _listener;
    initialized(context: ICardUIExtensionContext): void;
    finalized(): void;
}
