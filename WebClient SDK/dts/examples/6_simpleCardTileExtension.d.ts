import { TileExtension, ITileGlobalExtensionContext } from 'tessa/ui/tiles';
/**
 * Добавлять\cкрывать\показывать тайл в левой панели для карточек в зависимости от:
 * - типа карточки
 * - данных карточки
 *
 * Добавляем на левую панель тайл, который по нажатию открывает модальное окно.
 * Тайл виден только в карточке с типом WebTestType и темой карточки равной "Карточка с тайлом"
 */
export declare class SimpleCardTileExtension extends TileExtension {
    initializingGlobal(context: ITileGlobalExtensionContext): void;
    private static showMessageBoxCommand;
    private static enableIfCardWithSubject;
}
