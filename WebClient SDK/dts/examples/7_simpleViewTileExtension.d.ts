import { TileExtension, ITileGlobalExtensionContext } from 'tessa/ui/tiles';
/**
 * Добавлять\cкрывать\показывать тайл в левой панели для представления в зависимости от:
 * - идентификатора узла рабочего места
 * - алиаса представления
 * - данных выделенной строки или строк
 */
export declare class SimpleViewTileExtension extends TileExtension {
    private static myDocumentsAlias;
    initializingGlobal(context: ITileGlobalExtensionContext): void;
    private static showViewData;
    private static enableIfMyDocumentsViewAndHasSelectedRow;
}
