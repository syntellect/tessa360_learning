import { TileExtension, ITileGlobalExtensionContext } from 'tessa/ui/tiles';
/**
 * Добавить глобальную группу\тайл в левую панель. При нажатии:
 * - формируем кастомный реквест на сервер, выполняем его, читаем что-то из reponse, который пришел
 * с сервера (из info, например) и выводим сообщение с этим прочитанным результатом.
 * - формируем реквест на вьюху, добавляем туда для примера какой-то поисковый параметр и результат показываем в сообщении.
 */
export declare class RequestTileExtension extends TileExtension {
    initializingGlobal(context: ITileGlobalExtensionContext): void;
    private static cardRequestCommand;
    private static viewRequestCommand;
}
