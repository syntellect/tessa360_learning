import React from 'react';
export declare type ExamplePreviewerComponentProps = {
    text?: string;
    isLoading?: boolean;
};
export declare class ExamplePreviewerComponent extends React.PureComponent<ExamplePreviewerComponentProps> {
    render(): JSX.Element;
}
