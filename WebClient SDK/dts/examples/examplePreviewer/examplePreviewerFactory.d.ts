import { CustomPreviewViewModelFactory, ICustomEditorViewModel } from 'tessa/ui/customFileEditor';
import { IFileVersion } from 'tessa/files';
export declare class ExamplePreviewerFactory implements CustomPreviewViewModelFactory {
    canUsePreview(version: IFileVersion): boolean;
    createViewModel(placeholder: string, version: IFileVersion): Promise<ICustomEditorViewModel>;
}
