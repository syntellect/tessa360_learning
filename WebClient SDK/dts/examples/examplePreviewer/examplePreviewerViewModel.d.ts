import { ICustomEditorViewModel } from 'tessa/ui/customFileEditor';
import { IFileVersion } from 'tessa/files';
export declare class ExamplePreviewerViewModel implements ICustomEditorViewModel {
    readonly placeholder: string;
    readonly version: IFileVersion;
    private _container;
    constructor(placeholder: string, version: IFileVersion);
    load(): Promise<void>;
    destroy(): Promise<void>;
    private rerenderPreviewComponent;
}
