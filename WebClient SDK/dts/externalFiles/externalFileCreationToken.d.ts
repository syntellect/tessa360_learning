import { FileCreationToken, IFile } from 'tessa/files';
export declare class ExternalFileCreationToken extends FileCreationToken {
    description: string;
    set(file: IFile): void;
}
