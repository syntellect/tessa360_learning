import { WorkplaceViewComponentExtension } from 'tessa/ui/views/extensions';
import { IWorkplaceViewComponent } from 'tessa/ui/views';
export declare class OpenForumContextMenuViewExtension extends WorkplaceViewComponentExtension {
    constructor();
    private permissionProvider;
    getExtensionName(): string;
    initialize(model: IWorkplaceViewComponent): void;
    initialized(_model: IWorkplaceViewComponent): void;
    private getParticipantsMenuAction;
    private changeParticipantsCommand;
    private removeParticipantsCommand;
    private removeParticipants;
    private processingRemoveRole;
    private processingChangeRole;
    private hasModeratorPermissions;
    private hasSuperModeratorPermissions;
    private getCardId;
    private tryGetRows;
    private tryGetSelectedItemsFromViewContext;
    private getContextParameterValue;
}
