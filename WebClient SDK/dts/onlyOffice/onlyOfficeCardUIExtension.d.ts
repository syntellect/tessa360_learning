import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * Представляет собой расширение, которое предупреждает пользователя об открытых файлах на редактирование при операциях с карточкой.
 */
export declare class OnlyOfficeCardUIExtension extends CardUIExtension {
    reopening(context: ICardUIExtensionContext): void;
    finalizing(context: ICardUIExtensionContext): Promise<void>;
    private static checkOpenFiles;
    shouldExecute(_context: ICardUIExtensionContext): boolean;
}
