import { ICustomEditorViewModel, CustomPreviewViewModelFactory } from 'tessa/ui/customFileEditor';
import { IFileVersion } from 'tessa/files';
import { OnlyOfficeApi } from './onlyOfficeApi';
/**
 * Представляет собой фабрику, необходимую для создания моделей редактора OnlyOffice, используемых в предпросмотре.
 */
export declare class OnlyOfficePreviewFactory implements CustomPreviewViewModelFactory {
    private readonly _api;
    constructor(api: OnlyOfficeApi);
    canUsePreview(version: IFileVersion): boolean;
    createViewModel(placeholder: string, version: IFileVersion): Promise<ICustomEditorViewModel>;
}
