export declare const CACHE_NAME = "tessa-web-cache";
export declare function isLoginRequest(request: Request): boolean;
export declare function isMetaRequest(request: Request): boolean;
export declare function isApiRequest(request: Request): boolean;
export declare function isWallpaperRequest(request: Request): boolean;
export declare function isThemeImages(request: Request): boolean;
export declare function isLocosRequest(request: Request): boolean;
export declare function getLocoFromRequest(request: Request): string | null;
export declare enum SWMessageType {
    InitSW = 1,
    AnotherTabFound = 2,
    UrlFromAnotherTab = 3,
    DontLoadApp = 4,
    LoadApp = 5,
    AnotherTabNotFound = 6
}
