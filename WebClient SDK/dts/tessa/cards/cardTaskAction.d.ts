export declare enum CardTaskAction {
    None = 0,
    Progress = 1,
    Reinstate = 2,
    Complete = 3,
    Postpone = 4
}
