import { CardButtonType } from './cardButtonType';
import { IStorage, StorageObject, IStorageValueFactory } from 'tessa/platform/storage';
export declare class CardTaskDialogButtonInfo extends StorageObject {
    constructor(storage: IStorage);
    static readonly nameKey: string;
    static readonly cardButtonTypeKey: string;
    static readonly captionKey: string;
    static readonly iconKey: string;
    static readonly cancelKey: string;
    static readonly orderKey: string;
    static readonly completeDialogKey: string;
    get name(): string;
    set name(value: string);
    get cardButtonType(): CardButtonType;
    set cardButtonType(value: CardButtonType);
    get caption(): string;
    set caption(value: string);
    get icon(): string;
    set icon(value: string);
    get cancel(): boolean;
    set cancel(value: boolean);
    get order(): number;
    set order(value: number);
    get completeDialog(): boolean;
    set completeDialog(value: boolean);
}
export declare class CardTaskDialogButtonInfoFactory implements IStorageValueFactory<CardTaskDialogButtonInfo> {
    getValue(storage: IStorage): CardTaskDialogButtonInfo;
    getValueAndStorage(): {
        value: CardTaskDialogButtonInfo;
        storage: IStorage;
    };
}
