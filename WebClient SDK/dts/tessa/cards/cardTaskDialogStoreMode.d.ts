export declare enum CardTaskDialogStoreMode {
    Info = 0,
    Settings = 1,
    Card = 2
}
