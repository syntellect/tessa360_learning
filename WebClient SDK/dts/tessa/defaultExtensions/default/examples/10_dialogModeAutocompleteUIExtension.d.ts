import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * Показываем выбранный автокомплит всегда в диалоговом окне.
 */
export declare class DialogModeAutocompleteUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
}
