import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * Показываем кастомное диалоговое окно
 */
export declare class ShowCustomDialogUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
    private static showCustomDialog;
}
