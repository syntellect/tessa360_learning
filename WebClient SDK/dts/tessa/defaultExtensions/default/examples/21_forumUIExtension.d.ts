import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
export declare class ForumUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
    private tryGetForumControl;
}
