import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
/**
 * Скрывать\показывать вкладку карточки в зависимости:
 * - от наличия какого-то признака в info, пришедшем с сервера
 * - от значения какого-то справочника, загруженного в init-стриме
 * - от данных карточки
 */
export declare class HideFormUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
}
