import { ApplicationExtension, IApplicationExtensionContext } from 'tessa';
export declare class ExamplePreviewerInitialization extends ApplicationExtension {
    initialize(_context: IApplicationExtensionContext): void;
}
