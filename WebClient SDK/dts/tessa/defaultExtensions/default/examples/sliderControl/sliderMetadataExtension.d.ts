import { CardMetadataExtension, ICardMetadataExtensionContext } from 'tessa/cards/extensions';
export declare class SliderMetadataExtension extends CardMetadataExtension {
    initializing(context: ICardMetadataExtensionContext): void;
}
