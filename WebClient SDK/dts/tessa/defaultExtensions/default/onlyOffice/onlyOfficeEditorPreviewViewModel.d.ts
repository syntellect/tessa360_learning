import { ICustomEditorViewModel } from 'tessa/ui/customFileEditor';
import { IFileVersion } from 'tessa/files';
import { OnlyOfficeApi } from './onlyOfficeApi';
/**
 * Представляет собой модель редактора, используемого для предпросмотра файлов.
 */
export declare class OnlyOfficeEditorPreviewViewModel implements ICustomEditorViewModel {
    readonly placeholder: string;
    readonly version: IFileVersion;
    private readonly _api;
    private _closeEditorCallback;
    private _editor;
    constructor(api: OnlyOfficeApi, placeholder: string, version: IFileVersion);
    load: () => Promise<void>;
    destroy: () => Promise<void>;
    private openEditorFunc;
}
