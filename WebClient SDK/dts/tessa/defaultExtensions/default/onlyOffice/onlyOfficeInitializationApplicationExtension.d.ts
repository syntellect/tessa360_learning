import { ApplicationExtension, IApplicationExtensionMetadataContext } from 'tessa';
/**
 * Представляет собой расширение, которое инициализирует работу с OnlyOffice.
 */
export declare class OnlyOfficeInitializationApplicationExtension extends ApplicationExtension {
    afterMetadataReceived(context: IApplicationExtensionMetadataContext): void;
    /**
     * Получает необходимые данные из карточки настроек OnlyOffice или выбрасывает ошибку в случае неудачи.
     */
    private static getSettingsOrThrow;
}
