import { StandardViewControlContentItemsFactory, ViewControlInitializationContext, BaseViewControlItem } from 'tessa/ui/cards/controls';
export declare class FilesViewControlContentItemsFactory extends StandardViewControlContentItemsFactory {
    createTableContent(context: ViewControlInitializationContext): BaseViewControlItem | null;
}
