import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor } from 'tessa/workflow/krProcess';
export declare class ProcessManagementUIHandler extends KrStageTypeUIHandler {
    private _stageControl;
    private _groupControl;
    private _signalControl;
    private _modeId;
    descriptors(): StageTypeHandlerDescriptor[];
    initialize(context: IKrStageTypeUIHandlerContext): void;
    finalize(context: IKrStageTypeUIHandlerContext): void;
    private modeChanged;
    private updateVisibility;
}
