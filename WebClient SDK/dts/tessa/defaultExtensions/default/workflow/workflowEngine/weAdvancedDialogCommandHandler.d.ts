import { AdvancedDialogCommandHandler } from '../krProcess/commandInterpreter/advancedDialogCommandHandler';
import { CardTaskCompletionOptionSettings, CardTaskDialogActionResult } from 'tessa/cards';
import { IClientCommandHandlerContext } from 'tessa/workflow/krProcess';
import { ICardEditorModel } from 'tessa/ui/cards';
export declare class WeAdvancedDialogCommandHandler extends AdvancedDialogCommandHandler {
    protected prepareDialogCommand(context: IClientCommandHandlerContext): CardTaskCompletionOptionSettings | null;
    protected completeDialogCoreAsync(actionResult: CardTaskDialogActionResult, context: IClientCommandHandlerContext, parentCardEditor: ICardEditorModel | null): Promise<boolean>;
    private getProcessRequest;
}
