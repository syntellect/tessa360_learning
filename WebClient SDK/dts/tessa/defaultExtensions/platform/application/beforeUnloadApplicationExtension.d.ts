import { ApplicationExtension } from 'tessa';
export declare class BeforeUnloadApplicationExtension extends ApplicationExtension {
    initialize(): void;
}
