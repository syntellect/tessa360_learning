import { CardGetExtension, ICardGetExtensionContext } from 'tessa/cards/extensions';
/**
 * Открытие созданной из шаблона карточки по загруженной в специальном режиме карточке с шаблоном.
 */
export declare class TemplateRecordGetExtension extends CardGetExtension {
    private _contextKey;
    shouldExecute(context: ICardGetExtensionContext): boolean;
    beforeRequest(context: ICardGetExtensionContext): void;
    afterRequest(context: ICardGetExtensionContext): Promise<void>;
}
