import { CardNewExtension, ICardNewExtensionContext } from 'tessa/cards/extensions';
export declare class TemplateRecordNewExtension extends CardNewExtension {
    shouldExecute(context: ICardNewExtensionContext): boolean;
    afterRequest(context: ICardNewExtensionContext): Promise<void>;
}
