import { TabPanelUIExtension, ITabPanelUIExtensionContext } from 'tessa/ui';
export declare class AddWorkplaceTabPanelButtonExtension extends TabPanelUIExtension {
    initialize(context: ITabPanelUIExtensionContext): void;
}
