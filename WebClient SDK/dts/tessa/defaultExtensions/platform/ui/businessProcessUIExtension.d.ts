import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
export declare class BusinessProcessUIExtension extends CardUIExtension {
    initialized(context: ICardUIExtensionContext): void;
    private static initGrid;
    private static initRow;
    private static invalidateRow;
    private static onTableRowOpening;
    private static compileAllButtons;
    private static onCompileButtonPressed;
    private static compile;
}
