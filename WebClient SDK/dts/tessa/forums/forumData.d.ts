export interface ForumData {
    ReadTopicIDList: Map<string, {
        TopicID: string;
        UserID: string;
        PreLastVisitedAt: string;
        LastReadMessageTime: string;
    }>;
    TopicTypes: Map<string, {
        [key: string]: string;
    }>;
}
