export declare const ShortMessagesСount = 3;
export declare const MessagesInTopicСount = 3;
export declare const TopicsCount = 3;
export declare const AllTopicsCount = 10;
export declare const FromDate: () => string;
export declare const MinDateTime: () => string;
export declare const MessagesCountInTopicControl = 30;
export declare const TopicTabTypeID = "aed972eb-883d-4b02-a75b-c96d4a5aef4c";
export declare const AddParticipantsTabName = "AddParticipantsTabName";
export declare const AddRolesTabName = "AddRoleParticipantsTabName";
export declare const TopicParticipantsVirtual = "FmTopicParticipantsVirtual";
export declare const TopicParticipantsInfoVirtual = "FmTopicParticipantsInfoVirtual";
export declare const TopicRoleParticipantsInfoVirtual = "FmTopicRoleParticipantsInfoVirtual";
export declare const QuoteKey = "quote";
export declare const MessageBodyPrefix = "<div class=\"forum-div\">";
export declare const MessageBodySuffix = "</div>";
export declare const ListItemsTextPrefix = "<div class=\"forum-div-service\"><p><span>";
export declare const ListItemsTextSuffix = "</span></p></div>";
export declare const OpenTopicLinkAction = "OpenTopic";
export declare const CardIDLinkParameter = "CID";
export declare const TopicIDLinkParameter = "TID";
export declare const TopicTypeIDLinkParameter = "TTID";
export declare const MessageIDLinkParameter = "MID";
export declare const ExportCardTempateKey = ".forumExportCardTempate";
export declare const TicksSinceYearZero = 621355968000000000;
export declare const TicksPerMillisecond = 10000;
export declare const DefaultContentWidthRatio = 0.65;
export declare const MinContentWidthRatio = 0.3;
export declare const ForumDataKey = ".forumData";
export declare const TopicIDKey = "topicId";
export declare const TopicTypeIDKey = "topicTypeId";
export declare const EditorMinOffsetTop = 100;
export declare const EditorDefaultHeight = 190;
export declare const EditorMinHeight = 70;
export declare const UseForumField = "UseForum";
export declare const UseForumSuffix = "_UseForum";
export declare const UseDefaultDiscussionTabField = "UseDefaultDiscussionTab";
export declare type TopicLinkAttributes = WebLinkAttributes | DesktopLinkAttributes;
export interface QuoteLinkAttributes {
    topicId: string;
    topicTypeId: string;
    messageId: string;
    messageCreated: string;
}
export interface WebLinkAttributes {
    type: 'web';
    path: string;
    cardId: string;
    topicId: string;
    topicTypeId: string;
    messageId: string;
}
export interface DesktopLinkAttributes {
    type: 'desktop';
    instanceName: string;
    cardId: string;
    topicId: string;
    topicTypeId: string;
    messageId: string;
}
export declare function concatUsersName(users: string[], separator: string): string;
export declare function sanitizeMessage(html: string): string;
export declare function fixImagesAfterSanitize(html: string): string;
export declare function getQuoteAttributesFromUri(uri: string, prefix: string): QuoteLinkAttributes | null;
export declare function getTopicLinkAttributesFromUri(href: string): TopicLinkAttributes | null;
export declare function fixFontSizes(html: string): string;
