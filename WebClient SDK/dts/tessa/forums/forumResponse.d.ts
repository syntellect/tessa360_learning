import { TopicModel } from './topicModel';
import { MessageModel } from './messageModel';
import { CardResponseBase } from 'tessa/cards/service';
import { ICloneable } from 'tessa/platform';
import { IStorage, ArrayStorage, StorageObject } from 'tessa/platform/storage';
export declare class ForumResponse extends CardResponseBase implements ICloneable<ForumResponse> {
    constructor(storage?: IStorage);
    static readonly forumSettingsKey: string;
    static readonly satelliteIdKey: string;
    static readonly topicsKey: string;
    static readonly messagesKey: string;
    get forumSettings(): IStorage;
    set forumSettings(value: IStorage);
    get satelliteId(): guid | null;
    set satelliteId(value: guid | null);
    get topics(): ArrayStorage<TopicModel>;
    set topics(value: ArrayStorage<TopicModel>);
    get messages(): ArrayStorage<MessageModel>;
    set messages(value: ArrayStorage<MessageModel>);
    tryGetTopics(): ArrayStorage<TopicModel> | null | undefined;
    tryGetMessages(): ArrayStorage<MessageModel> | null | undefined;
    private static readonly _topicsFactory;
    private static readonly _messageFactory;
    clone(): ForumResponse;
}
export declare class ForumPermissionsResponse extends StorageObject {
    constructor(storage?: IStorage);
    static readonly isExistRequiredPermissionKey: string;
    get isExistRequiredPermission(): boolean;
    set isExistRequiredPermission(value: boolean);
}
