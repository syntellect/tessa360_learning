export interface ICloneable<T> {
    clone(): T;
}
export declare function isICloneable(object: any): boolean;
