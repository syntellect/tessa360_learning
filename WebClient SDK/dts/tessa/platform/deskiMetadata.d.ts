export declare class DeskiMetadata {
    private _enabled?;
    private _port?;
    private _info?;
    constructor(storage: {
        enabled: boolean;
        port?: number;
        info?: DeskiInfo;
    });
    get enabled(): boolean;
    get port(): number | undefined;
    get info(): DeskiInfo | undefined;
    get version(): string | null;
}
export interface DeskiInfo {
    FullName: string;
    ShortName: string;
    VerMajor: number;
    VerMinor: number;
    OS: 'aix' | 'android' | 'darwin' | 'dragonfly' | 'freebsd' | 'illumos' | 'js' | 'linux' | 'netbsd' | 'openbsd' | 'plan9' | 'solaris' | 'windows';
}
