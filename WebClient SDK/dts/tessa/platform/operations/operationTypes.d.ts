/**
 * Стандартные типы операций.
 */
export declare class OperationTypes {
    /**
     * Потоковое сохранение карточки.
     */
    static CardStore: string;
    /**
     * Потоковый импорт карточки.
     */
    static CardImport: string;
    /**
     * Расчёт календаря.
     */
    static CalendarRebuild: string;
    /**
     * Конвертация файла из одного формата в другой.
     */
    static FileConvert: string;
    /**
     * Синхронизация данных с Active Directory
     */
    static AdSync: string;
    /**
     * Безымянная операция.
     */
    static Other: string;
}
