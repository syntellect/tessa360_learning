import { IUIContextActionOverridings } from '../uiContext';
import { OpenCardArg, ShowCardArg, CreateCardArg } from '../uiHost/common';
export declare const CardDefaultDialog = "DefaultDialog";
export declare class AdvancedCardDialogManager {
    private constructor();
    private static _instance;
    static get instance(): AdvancedCardDialogManager;
    openCard(args: OpenCardArg & {
        dialogName?: string;
    }): Promise<void>;
    private openCardOverride;
    showCard(args: ShowCardArg): Promise<void>;
    private showCardOverride;
    createCard(args: CreateCardArg & {
        dialogName?: string;
    }): Promise<void>;
    private createCardOverride;
    private showCardCore;
    createUIContextActionOverridings(): IUIContextActionOverridings;
    private onDialogClosing;
    private invokeDialogClosingAction;
    private invokeDialogClosingBeforeSavingAction;
}
