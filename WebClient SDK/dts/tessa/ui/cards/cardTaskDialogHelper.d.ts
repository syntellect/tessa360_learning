import { IStorage } from 'tessa/platform/storage';
import { CardTaskCompletionOptionSettings } from 'tessa/cards';
export declare function getCompletionOptionSettings(settings: IStorage | null, completionOptionId: guid): CardTaskCompletionOptionSettings | null;
