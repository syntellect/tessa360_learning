import { ICardModel } from './interfaces';
import { IStorage, MapStorage } from 'tessa/platform/storage';
import { ICardEditorModel } from 'tessa/ui/cards';
import { Card, CardRow } from 'tessa/cards';
import { CardNewResponse } from 'tessa/cards/service';
export declare function tryCreateFromTemplate(templateCard: Card, templateInfo?: IStorage | null, modifyCardAction?: ((card: Card) => void) | null, modifyEditorAction?: ((editor: ICardEditorModel) => void) | null): Promise<ICardEditorModel | null>;
export declare function tryCreateFromTemplateResponse(newResponse: CardNewResponse, modifyCardAction?: ((card: Card) => void) | null, modifyEditorAction?: ((editor: ICardEditorModel) => void) | null): Promise<ICardEditorModel | null>;
export declare function tryEditCardInTemplate(templateCard: Card, templateSectionRows: MapStorage<CardRow>): Promise<ICardEditorModel | null>;
export declare function tryOpenTemplateFromCard(cardInTemplateModel: ICardModel): Promise<ICardEditorModel | null>;
