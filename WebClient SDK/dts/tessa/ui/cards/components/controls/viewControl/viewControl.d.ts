import React from 'react';
import { ControlProps } from './../controlProps';
import { ViewControlViewModel } from 'tessa/ui/cards/controls';
export declare class ViewControl extends React.Component<ControlProps<ViewControlViewModel>> {
    componentDidMount(): void;
    render(): JSX.Element | null;
    private renderTopItems;
    private renderBottomItems;
}
