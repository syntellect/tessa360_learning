import * as React from 'react';
import { IFormViewModel } from 'tessa/ui/cards/interfaces';
export interface CardFormProps {
    viewModel: IFormViewModel;
}
export interface CardFormState {
    prevViewModel: IFormViewModel | null;
    formComponent: Function | null;
}
export declare class CardForm extends React.Component<CardFormProps, CardFormState> {
    constructor(props: CardFormProps);
    static getDerivedStateFromProps(props: CardFormProps, state: CardFormState): CardFormState | null;
    render(): JSX.Element | null;
}
