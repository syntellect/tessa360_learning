import * as React from 'react';
import { IFormViewModel } from 'tessa/ui/cards/interfaces';
export interface DefaultFormSimpleProps {
    viewModel: IFormViewModel;
}
export declare class DefaultFormSimple extends React.Component<DefaultFormSimpleProps> {
    render(): JSX.Element;
}
