import Moment from 'moment';
import { ControlViewModelBase } from './controlViewModelBase';
import { ICardModel, ControlKeyDownEventArgs } from '../interfaces';
import { CardTypeEntryControl } from 'tessa/cards/types';
import { EventHandler } from 'tessa/platform';
/**
 * Элемент управления "Дата и время" в карточке.
 */
export declare class DateTimeViewModel extends ControlViewModelBase {
    constructor(control: CardTypeEntryControl, model: ICardModel);
    private _fields;
    private _fieldName;
    /**
     * Признак того, что элемент управления будет отображать или редактировать дату.
     */
    readonly dateEnabled: boolean;
    /**
     * Признак того, что элемент управления будет отображать или редактировать время.
     */
    readonly timeEnabled: boolean;
    /**
     * Признак того, что часовой пояс сотрудника игнорируется и дата всегда указывается как Utc.
     * Если контрол редактирует только дату, то часовой пояс всегда игнорируется,
     * независимо от этой настройки.
     */
    readonly ignoreTimezone: boolean;
    /**
     * Текущие дата/время, отображаемые элементом управления,
     * или null, если дата/время не выбраны.
     */
    get selectedDate(): Moment.Moment | null;
    set selectedDate(value: Moment.Moment | null);
    get error(): string | null;
    get hasEmptyValue(): boolean;
    readonly keyDown: EventHandler<(args: ControlKeyDownEventArgs) => void>;
}
