import { ForumItemViewModel } from './forumItemViewModel';
import { MenuAction } from 'tessa/ui/menuAction';
import { ParticipantTypes } from 'tessa/forums';
export interface IAttachmentMenuContext {
    item: ForumItemViewModel;
    menuActions: MenuAction[];
}
export declare type ForumActionParameters = {
    topicID?: string;
    cardTypeID?: string;
    participantsTypeID?: ParticipantTypes;
    participantsTabName?: string;
    rolesTabName?: string;
    isSuperModerator?: boolean;
    onlyMyDepartment?: boolean;
    [key: string]: any;
};
export declare function convertFromCustomStyle(customStyle: string, elem: Element): void;
