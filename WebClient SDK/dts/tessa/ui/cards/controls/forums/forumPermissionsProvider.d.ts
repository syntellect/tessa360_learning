import { Result } from 'tessa/platform';
import { Card } from 'tessa/cards';
export interface IForumPermissionsProvider {
    checkHasPermissionAddTopic(cardId: guid): Promise<Result<boolean>>;
    checkHasPermissionIsSuperModerator(cardId: guid): Promise<Result<boolean>>;
    isEnableAddTopic(card: Card): boolean;
    isEnableSuperModeratorMode(card: Card): boolean;
}
export declare class ForumPermissionsProvider implements IForumPermissionsProvider {
    checkHasPermissionAddTopic(cardId: guid): Promise<Result<boolean>>;
    checkHasPermissionIsSuperModerator(cardId: guid): Promise<Result<boolean>>;
    isEnableAddTopic(card: Card): boolean;
    isEnableSuperModeratorMode(card: Card): boolean;
    private tryGetKrToken;
}
