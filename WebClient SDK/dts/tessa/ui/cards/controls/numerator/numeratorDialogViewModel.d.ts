import { FieldMapStorage } from 'tessa/cards/fieldMapStorage';
export declare class NumeratorDialogViewModel {
    constructor(fields: FieldMapStorage, fullNumberFieldName: string, numberFieldName: string, sequenceFieldName: string | null, isReadOnly: boolean, sequenceIsVisible: boolean);
    private readonly _fields;
    private readonly _fullNumberFieldName;
    private readonly _numberFieldName;
    private readonly _sequenceFieldName;
    readonly isReadOnly: boolean;
    readonly sequenceIsVisible: boolean;
    readonly sequenceWarningIsVisible: boolean;
    readonly serialNumberWarningIsVisible: boolean;
    get fullNumber(): string | null;
    set fullNumber(value: string | null);
    get number(): string | null;
    set number(value: string | null);
    get sequence(): string | null;
    set sequence(value: string | null);
}
