import type { IViewControlInitializationStrategy } from './viewControlInitializationStrategy';
import { IViewControlDataProvider, ViewControlDataProviderRequest } from './viewControlDataProvider';
import type { BaseViewControlItem } from './contents';
import { ViewControlTableGridViewModel } from './contents';
import { ViewControlViewComponent } from './viewControlViewComponent';
import { ControlViewModelBase } from '../controlViewModelBase';
import type { ICardModel } from '../../interfaces';
import { IParametersMappingContext } from '../viewMappingHelper';
import type { CardTypeControl } from 'tessa/cards/types';
import type { IStorage } from 'tessa/platform/storage';
import type { ViewMetadataSealed, RequestParameter } from 'tessa/views/metadata';
import { ViewSelectionMode } from 'tessa/views/metadata';
import type { IViewSorting, IViewComponentBase, ISelectionState, DoubleClickAction } from 'tessa/ui/views';
import type { IViewParameters } from 'tessa/ui/views/parameters';
import { SortingColumn } from 'tessa/views';
import { Paging } from 'tessa/views';
import type { SchemeDbType, EventHandler } from 'tessa/platform';
import { ValidationResultBuilder } from 'tessa/platform/validation';
export declare class ViewControlViewModel extends ControlViewModelBase implements IViewComponentBase {
    constructor(control: CardTypeControl, model: ICardModel, viewComponentFactory?: () => ViewControlViewComponent);
    protected _initializedStrategy: boolean;
    protected _initialRefreshed: boolean;
    protected _refreshTimer: number | undefined;
    get initialized(): boolean;
    get initializedStrategy(): boolean;
    readonly settings: IStorage;
    readonly viewComponent: ViewControlViewComponent;
    readonly cardModel: ICardModel;
    dataProvider: IViewControlDataProvider | null;
    get viewMetadata(): ViewMetadataSealed | null;
    set viewMetadata(value: ViewMetadataSealed | null);
    get masterView(): ViewControlViewComponent | null;
    set masterView(value: ViewControlViewComponent | null);
    masterControl: ViewControlViewModel | null;
    get columns(): ReadonlyMap<string, SchemeDbType>;
    get data(): ReadonlyArray<ReadonlyMap<string, any>> | null;
    get isDataLoading(): boolean;
    set isDataLoading(value: boolean);
    get parameters(): IViewParameters;
    set parameters(value: IViewParameters);
    get parametersSetName(): string;
    get sorting(): IViewSorting;
    set sorting(value: IViewSorting);
    get sortingColumns(): ReadonlyArray<SortingColumn>;
    get selectionState(): ISelectionState;
    get currentPage(): number;
    set currentPage(value: number);
    get optionalPagingStatus(): boolean;
    set optionalPagingStatus(value: boolean);
    get pageCount(): number;
    set pageCount(value: number);
    get pageCountStatus(): boolean;
    set pageCountStatus(value: boolean);
    get calculatedRowCount(): number;
    set calculatedRowCount(value: number);
    get actualRowCount(): number;
    get hasNextPage(): boolean;
    get hasPreviousPage(): boolean;
    get pageLimit(): number;
    set pageLimit(value: number);
    get pagingMode(): Paging;
    set pagingMode(value: Paging);
    get selectedCellValue(): any | null;
    get selectedColumn(): string | null;
    get selectedRow(): ReadonlyMap<string, any> | null;
    get selectedRows(): ReadonlyArray<ReadonlyMap<string, any>> | null;
    get refSection(): string;
    set refSection(value: string);
    get selectionMode(): ViewSelectionMode;
    set selectionMode(value: ViewSelectionMode);
    get multiSelect(): boolean;
    set multiSelect(value: boolean);
    get multiSelectEnabled(): boolean;
    set multiSelectEnabled(value: boolean);
    get quickSearchEnabled(): boolean;
    set quickSearchEnabled(value: boolean);
    get firstRowSelection(): boolean;
    set firstRowSelection(value: boolean);
    get doubleClickAction(): DoubleClickAction;
    set doubleClickAction(value: DoubleClickAction);
    get selectRowOnContextMenu(): boolean;
    set selectRowOnContextMenu(value: boolean);
    get onRefreshing(): EventHandler<() => void>;
    get onRefreshed(): EventHandler<() => void>;
    readonly topItems: BaseViewControlItem[];
    readonly bottomItems: BaseViewControlItem[];
    readonly rightItems: BaseViewControlItem[];
    readonly leftItems: BaseViewControlItem[];
    content: BaseViewControlItem | null;
    topContent: BaseViewControlItem | null;
    bottomContent: BaseViewControlItem | null;
    readonly parametersActions: Array<(parameters: RequestParameter[]) => void>;
    get table(): ViewControlTableGridViewModel | null;
    initializeStrategy(initializationStrategy: IViewControlInitializationStrategy, codeGenerated?: boolean): void;
    initialize(): void;
    dispose(): void;
    protected initializeGrouping(): void;
    refresh(): Promise<void>;
    refreshWithDelay(delay: number): void;
    canRefresh(): boolean;
    filter(): Promise<void>;
    canFilter(): boolean;
    sortColumn(column: string, addOrInverse: boolean, descendingByDefault: boolean): void;
    getViewData(): Promise<{
        columns: ReadonlyMap<string, SchemeDbType>;
        rows: ReadonlyArray<ReadonlyMap<string, any>>;
        rowCount: number;
    } | null>;
    protected createDataRequest(validationResult: ValidationResultBuilder, selectedMasterRowData: ReadonlyMap<string, any> | null, selectedMasterColumnName: string | null): ViewControlDataProviderRequest;
    protected setupPagingParameters: (parameters: RequestParameter[]) => void;
    protected setupViewParameters: (parameters: RequestParameter[]) => void;
    protected convertRows(columns: ReadonlyMap<string, SchemeDbType>, rows: ReadonlyArray<IStorage>): ReadonlyArray<ReadonlyMap<string, any>>;
    getParametersMappingContext(): IParametersMappingContext;
    initialRefresh(): Promise<void>;
}
