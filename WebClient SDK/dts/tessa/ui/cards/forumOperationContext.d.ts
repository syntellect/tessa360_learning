import { ParticipantTypes } from 'tessa/forums';
import { ForumPermissionsProvider } from './controls';
export interface IForumOperationContext {
    participantType: ParticipantTypes;
    isSuperModerator: boolean;
    cardID: string;
    cardTypeID: string;
    participantsTabName: string;
    rolesTabName: string;
    permissionProvider: ForumPermissionsProvider;
}
