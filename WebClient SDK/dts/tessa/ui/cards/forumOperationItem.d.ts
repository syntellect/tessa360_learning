import { ParticipantTypes } from 'tessa/forums';
import { CardOperationItem } from './cardOperationItem';
export declare class ForumOperationItem extends CardOperationItem {
    constructor(cardId: string, displayValue: string);
    topicId: string;
    roleType: number;
    roleId: string;
    roleName: string;
    readOnly: boolean;
    participantType: ParticipantTypes;
    cardTypeId: string;
    participantsTabName: string;
    rolesTabName: string;
    subscribed: boolean;
}
