import { ICardEditorModel } from './interfaces';
import { Card } from 'tessa/cards';
export declare class RemovableInfo {
    constructor(modifyCardAction?: (card: Card) => void, modifyEditorAction?: (editor: ICardEditorModel) => void);
    readonly modifyCardAction: ((card: Card) => void) | null;
    readonly modifyEditorAction: ((editor: ICardEditorModel) => void) | null;
    editor: ICardEditorModel | null;
}
