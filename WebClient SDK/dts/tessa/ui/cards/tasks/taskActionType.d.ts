export declare enum TaskActionType {
    Complete = 0,
    NavigateToForm = 1,
    Progress = 2,
    Reinstate = 3,
    Postpone = 4,
    UnlockForAuthor = 5,
    LockForAuthor = 6,
    NavigateToPostpone = 7,
    NavigateBack = 8,
    ReturnFromPostponed = 9,
    AdditionalActions = 10,
    Separator = 11,
    Custom = 12
}
