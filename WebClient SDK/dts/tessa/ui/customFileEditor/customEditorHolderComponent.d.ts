import React from 'react';
import { ICustomEditorViewModel } from './customEditorViewModel';
interface CustomEditorHolderComponentProps {
    viewModel: ICustomEditorViewModel;
    onError: (e: Error) => void;
}
export declare class CustomEditorHolderComponent extends React.PureComponent<CustomEditorHolderComponentProps> {
    private readonly instanceRef;
    private checkExistsIntervalId;
    constructor(props: CustomEditorHolderComponentProps);
    private loadEditor;
    private destroyEditor;
    componentDidMount(): Promise<void>;
    componentDidUpdate(prevProps: Readonly<CustomEditorHolderComponentProps>): Promise<void>;
    componentWillUnmount(): Promise<void>;
    render(): JSX.Element;
}
export {};
