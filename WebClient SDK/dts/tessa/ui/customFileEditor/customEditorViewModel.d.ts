/**
 * Представляет собой модель редактора.
 */
export interface ICustomEditorViewModel {
    /**
     * Наименование идентификатора элемента DOM, вместо которого должен быть загружен редактор.
     */
    readonly placeholder: string;
    /**
     * Выполняет загрузку редактора.
     */
    load(): Promise<void>;
    /**
     * Выполяет немедленное уничтожение редактора.
     */
    destroy(): Promise<void>;
}
