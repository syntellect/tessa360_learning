import { IFileVersion } from 'tessa/files';
import { ICustomEditorViewModel } from './customEditorViewModel';
/**
 * Предоставляет возможность создания моделей, используемых для предпросмотра.
 */
export interface CustomPreviewViewModelFactory {
    /**
     * Получает признак того, что предпросмотр может быть использован для указанного файла.
     */
    canUsePreview(version: IFileVersion): boolean;
    /**
     * Создаёт модель для предпросмотра, не выполняя загрузку.
     * @param placeholder Идентификатор элемента DOM, вместо которого должен быть встроен элемент, предоставляющий предпросмотр.
     * @param version Версия файла, которую нужно отобразить в предпросмотре.
     */
    createViewModel(placeholder: string, version: IFileVersion): Promise<ICustomEditorViewModel>;
}
