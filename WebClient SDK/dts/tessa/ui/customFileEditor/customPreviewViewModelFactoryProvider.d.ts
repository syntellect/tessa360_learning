import { CustomPreviewViewModelFactory } from './customPreviewViewModelFactory';
import { IFileVersion } from 'tessa/files';
/**
 * Предоставляет фабрики, созданные модели которых могут быть использованы в качестве предпросмотра.
 */
export declare class CustomPreviewViewModelFactoryProvider {
    private static _instance;
    /**
     * Список доступных фабрик.
     * Для предпросмотра выбирается первая подходящая из списка, вернувшая canUsePreview === true.
     * В случае, если ни одна фабрика не является подходящей, используются стандартные средства предпросмотра.
     */
    readonly factories: Array<CustomPreviewViewModelFactory>;
    private constructor();
    static get instance(): CustomPreviewViewModelFactoryProvider;
    getFirstAvailableFactory(version: IFileVersion): CustomPreviewViewModelFactory | null;
}
