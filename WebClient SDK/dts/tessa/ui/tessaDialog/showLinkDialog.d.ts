declare type LinkInfo = {
    caption: string;
    link: string;
};
export declare function showLinkDialog(info?: LinkInfo): Promise<LinkInfo | null>;
export {};
