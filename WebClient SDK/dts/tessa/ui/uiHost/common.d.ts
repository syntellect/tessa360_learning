import { ICardEditorModel, ICardModel, CardEditorCreationContext, CardEditorOpeningContext } from 'tessa/ui/cards';
import { IUIContextActionOverridings, IUIContext } from '../uiContext';
import { IStorage } from 'tessa/platform/storage';
export interface CreateCardArg {
    cardId?: guid;
    cardTypeId?: guid;
    cardTypeName?: string;
    context?: IUIContext;
    displayValue?: string;
    info?: IStorage;
    cardModifierAction?: (context: CardEditorCreationContext) => void;
    cardModelModifierAction?: (context: CardEditorCreationContext) => void;
    cardEditorModifierAction?: (context: CardEditorCreationContext) => void;
    alwaysNewTab?: boolean;
    openToTheRightOfSelectedTab?: boolean;
    withUIExtensions?: boolean;
    saveCreationRequest?: boolean;
    creationModeDisplayText?: string;
    splashResolve?: () => void;
}
export interface ShowCardArg {
    editor: ICardEditorModel;
    displayValue?: string;
    alwaysNewTab?: boolean;
    openToTheRightOfSelectedTab?: boolean;
    needDispatch?: boolean;
    prepareEditorAction?: (editor: ICardEditorModel) => void;
    beforeShowAction?: () => Promise<void>;
}
export interface OpenCardArg {
    cardId?: guid;
    cardTypeId?: guid;
    cardTypeName?: string;
    context?: IUIContext;
    displayValue?: string;
    info?: IStorage;
    cardModifierAction?: (context: CardEditorOpeningContext) => void;
    cardModelModifierAction?: (context: CardEditorOpeningContext) => void;
    cardEditorModifierAction?: (context: CardEditorOpeningContext) => void;
    alwaysNewTab?: boolean;
    openToTheRightOfSelectedTab?: boolean;
    withUIExtensions?: boolean;
    needDispatch?: boolean;
    splashResolve?: () => void;
}
export declare function getWorkspaceName(model: ICardModel, displayValue?: string): Promise<string | null>;
export declare function subscribeEditorToCardModelInitialized(editor: ICardEditorModel, displayValue?: string): void;
export declare function getAncestorOverridings(context: IUIContext | null | undefined, predicate: (ao: IUIContextActionOverridings) => boolean): IUIContextActionOverridings | null;
