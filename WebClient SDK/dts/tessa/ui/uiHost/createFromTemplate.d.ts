import { IUIContext } from 'tessa/ui';
import { IStorage } from 'tessa/platform/storage';
import { ICardEditorModel } from 'tessa/ui/cards';
import { Card } from 'tessa/cards';
export declare function createFromTemplate(args: {
    templateId: guid;
    context?: IUIContext;
    templateInfo?: IStorage;
    modifyCardAction?: (card: Card) => void;
    modifyEditorAction?: (editor: ICardEditorModel) => void;
}): Promise<ICardEditorModel | null>;
