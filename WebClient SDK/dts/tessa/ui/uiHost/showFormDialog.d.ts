import * as React from 'react';
import { CardTypeFormSealed } from 'tessa/cards/types';
import { ICardModel, IFormViewModel } from 'tessa/ui/cards';
import { UIButton } from 'tessa/ui/uiButton';
import { DialogProps } from 'ui/dialog/dialog';
export interface ShowFormDialogOptions {
    backgroundHolder?: boolean;
    hideTopCloseIcon?: boolean;
}
export interface CustomFormDialogProps {
    form: IFormViewModel;
    buttons: UIButton[];
    onClose: (result: any) => void;
    dialogOptions?: ShowFormDialogOptions | null;
    dialogComponentProps?: DialogProps | null;
}
export declare class CustomFormDialog extends React.Component<CustomFormDialogProps> {
    render(): JSX.Element;
    private renderForm;
    private renderButtons;
    private handleClose;
}
export declare function showFormDialog(form: CardTypeFormSealed, model: ICardModel, initializeAction?: ((form: IFormViewModel, closeFunc: (result?: any) => void) => void) | null, buttons?: UIButton[], dialogOptions?: ShowFormDialogOptions | null, onDialogClosed?: (() => void) | null, dialogComponentProps?: DialogProps | null): Promise<any>;
export declare function showDialog(formModel: IFormViewModel, initializeAction?: ((form: IFormViewModel, closeFunc: (result?: any) => void) => void) | null, buttons?: UIButton[], dialogOptions?: ShowFormDialogOptions | null, onDialogClosed?: (() => void) | null, dialogComponentProps?: DialogProps | null): Promise<any>;
