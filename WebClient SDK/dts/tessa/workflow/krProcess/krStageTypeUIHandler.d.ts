import { IKrStageTypeUIHandlerContext } from './krStageTypeUIHandlerContext';
import { StageTypeHandlerDescriptor } from './stageTypeHandlerDescriptor';
import { IExtension } from 'tessa/extensions';
export interface IKrStageTypeUIHandler extends IExtension {
    initialize(context: IKrStageTypeUIHandlerContext): any;
    validate(context: IKrStageTypeUIHandlerContext): any;
    finalize(context: IKrStageTypeUIHandlerContext): any;
}
export declare class KrStageTypeUIHandler implements IKrStageTypeUIHandler {
    static readonly type = "KrStageTypeUIHandler";
    shouldExecute(context: IKrStageTypeUIHandlerContext): boolean;
    descriptors(): StageTypeHandlerDescriptor[];
    initialize(_context: IKrStageTypeUIHandlerContext): void;
    validate(_context: IKrStageTypeUIHandlerContext): void;
    finalize(_context: IKrStageTypeUIHandlerContext): void;
}
