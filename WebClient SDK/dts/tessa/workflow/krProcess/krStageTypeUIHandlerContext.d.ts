import { CardRow } from 'tessa/cards';
import { GridRowAction, GridViewModel } from 'tessa/ui/cards/controls';
import { ICardModel } from 'tessa/ui/cards';
import { IValidationResultBuilder } from 'tessa/platform/validation';
export interface IKrStageTypeUIHandlerContext {
    readonly stageTypeId: guid;
    readonly action: GridRowAction | null;
    readonly control: GridViewModel | null;
    readonly row: CardRow;
    readonly rowModel: ICardModel | null;
    readonly cardModel: ICardModel;
    readonly validationResult: IValidationResultBuilder;
}
export declare class KrStageTypeUIHandlerContext implements IKrStageTypeUIHandlerContext {
    constructor(stageTypeId: guid, action: GridRowAction | null, control: GridViewModel | null, row: CardRow, rowModel: ICardModel | null, cardModel: ICardModel, validationResult?: IValidationResultBuilder | null);
    readonly stageTypeId: guid;
    readonly action: GridRowAction | null;
    readonly control: GridViewModel | null;
    readonly row: CardRow;
    readonly rowModel: ICardModel | null;
    readonly cardModel: ICardModel;
    readonly validationResult: IValidationResultBuilder;
}
