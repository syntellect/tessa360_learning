import * as React from 'react';
import { AutocompleteDropDownItemsCollection } from './autocomplete';
declare class AutocompleteDropdown extends React.Component<IAutocompleteDropdownProps, {}> {
    static defaultProps: {
        focusedItemIndex: number;
    };
    render(): JSX.Element;
    renderList(): JSX.Element[];
}
export interface IAutocompleteDropdownProps {
    dropdownItems: AutocompleteDropDownItemsCollection;
    focusedItemIndex?: number;
    onItemSelect: any;
}
export default AutocompleteDropdown;
