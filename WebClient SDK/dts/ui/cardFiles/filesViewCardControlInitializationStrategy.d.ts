import { ViewControlInitializationStrategy, ViewControlInitializationContext } from 'tessa/ui/cards/controls';
export declare class FilesViewCardControlInitializationStrategy extends ViewControlInitializationStrategy {
    initializeMetadata(context: ViewControlInitializationContext): void;
    initializeDataProvider(context: ViewControlInitializationContext): void;
}
