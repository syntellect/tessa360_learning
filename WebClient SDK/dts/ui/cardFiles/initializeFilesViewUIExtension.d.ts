import { FilesViewGeneratorBaseUIExtension } from './filesViewGeneratorBaseUIExtension';
import { ICardUIExtensionContext } from 'tessa/ui/cards';
import { TypeExtensionContext } from 'tessa/cards';
export declare class InitializeFilesViewUIExtension extends FilesViewGeneratorBaseUIExtension {
    initializing(context: ICardUIExtensionContext): Promise<void>;
    initialized(context: ICardUIExtensionContext): Promise<void>;
    executeInitializingAction: (context: TypeExtensionContext) => Promise<void>;
    executeInitializedAction: (context: TypeExtensionContext) => Promise<void>;
}
