import * as React from 'react';
declare class DatePickerDay extends React.Component<IDatePickerDayProps, {}> {
    isSameDay(other: any): any;
    isOutsideMonth(): boolean;
    render(): JSX.Element;
}
export interface IDatePickerDayProps {
    date: any;
    month: number;
    selectedDate?: object;
    onDaySelect: any;
}
export default DatePickerDay;
