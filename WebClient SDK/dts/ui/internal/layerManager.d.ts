declare class DialogOverlayHelper {
    currentZIndex: number;
    setZIndex(index: number): void;
}
export declare class LayerManager {
    zIndex: number;
    dialogs: {
        order: number;
        dialog: any;
    }[];
    helper: DialogOverlayHelper;
    constructor();
    private calculateZIndex;
    getIndex(): number;
    getNextIndex(): number;
    pushDialog(dialog: any): void;
    removeDialog(dialog: any): void;
    isTopDialog(dialog: any): boolean;
    focusCurrentDialog(): void;
    isAnyDialogsExists(): boolean;
}
declare const LayerManagerRef: LayerManager;
export default LayerManagerRef;
