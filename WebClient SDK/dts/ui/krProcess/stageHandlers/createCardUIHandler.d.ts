import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor } from 'tessa/workflow/krProcess';
export declare class CreateCardUIHandler extends KrStageTypeUIHandler {
    private _settings;
    private _templateId;
    private _templateCaption;
    private _typeId;
    private _typeCaption;
    private _modeId;
    descriptors(): StageTypeHandlerDescriptor[];
    validate(context: IKrStageTypeUIHandlerContext): void;
    initialize(context: IKrStageTypeUIHandlerContext): void;
    finalize(_context: IKrStageTypeUIHandlerContext): void;
    private onSettingsFieldChanged;
}
