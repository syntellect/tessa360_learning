import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor } from 'tessa/workflow/krProcess';
export declare class SigningUIHandler extends KrStageTypeUIHandler {
    private settings;
    private returnIfNotSignedFlagControl?;
    private returnAfterSigningFlagControl?;
    private NotReturnEdit;
    descriptors(): StageTypeHandlerDescriptor[];
    initialize(context: IKrStageTypeUIHandlerContext): void;
    finalize(_context: IKrStageTypeUIHandlerContext): void;
    private onSettingsFieldChanged;
    private notReturnEditConfigureFields;
}
