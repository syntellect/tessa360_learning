import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext } from 'tessa/workflow/krProcess';
export declare class TabCaptionUIHandler extends KrStageTypeUIHandler {
    private _indicator;
    private _dispose;
    initialize(context: IKrStageTypeUIHandlerContext): void;
    finalize(): void;
}
