import { Editor, Element, Text } from 'slate';
import { BlockEditor } from './withBlocks';
import { ImageEditor } from './withImages';
import { WordlikeListEditor as ListEditor } from './withWordlikeLists';
import { LinkEditor } from './withLinks';
import { StyleEditor } from './withStyles';
import { NormalizingEditor } from './withImportNormalization';
import { HistoryEditor } from 'slate-history';
import { ReactEditor, RenderElementProps, RenderLeafProps } from 'slate-react';
import { AttachmentEditor } from './withAttachments';
import { ClipboardEditor } from './withClipboard';
import { SmartSerializationEditor } from './withSmartSerialization';
export declare class Config {
    static readonly maxNestedBlocks = 5;
    static readonly maxNestedLists = 1;
    static readonly maxImageWidth = 130;
    static readonly maxImageHeight = 130;
    static readonly baseFontSize = 14;
    static readonly defaultLinkColor = "rgba(0, 102, 204, 1)";
}
export interface RangeStyle {
    isBold: boolean;
    isItalic: boolean;
    isUnderline: boolean;
    isStrikethrough: boolean;
    fontSize: string;
    listType?: List;
}
export interface ImageData {
    file: File;
    type: string;
    path: string;
    previewUrl: string;
    width: number;
    height: number;
    id: string;
}
export declare type ImageResizeHandler = (data: string, width: number, height: number) => void;
export declare type ImageDataHandler = (image: ImageData) => void;
export declare type LinkInfo = {
    caption: string;
    link: string;
};
export declare enum RichTextBoxAttachmentType {
    File = 0,
    Link = 1,
    InnerItem = 2
}
export declare type RichTextBoxAttachmentFile = {
    id: string;
    type: RichTextBoxAttachmentType.File;
    caption: string;
    data?: File;
    fileLink?: string;
};
export declare type RichTextBoxAttachmentLink = {
    id: string;
    type: RichTextBoxAttachmentType.Link;
} & LinkInfo;
export declare type RichTextBoxAttachmentInnerItem = {
    id: string;
    type: RichTextBoxAttachmentType.InnerItem;
    caption: string;
    data?: File;
};
export declare type RichTextBoxAttachmentInnerItemDuplicate = {
    id: string;
    originalCaption: string;
    caption: string;
    data?: File;
};
export declare type RichTextBoxAttachment = RichTextBoxAttachmentFile | RichTextBoxAttachmentLink | RichTextBoxAttachmentInnerItem;
export declare enum HTMLElementType {
    Element = 1,
    Text = 3
}
export interface TessaElementAttributes {
    class?: ForumClasses;
    width?: string;
    height?: string;
    href?: string;
    src?: string;
    name?: string;
    id?: string;
    quoteId?: string;
    caption?: string;
    backgroundColor?: string;
    color?: string;
}
export interface TessaTextAttributes {
    bold?: boolean;
    italic?: boolean;
    underline?: boolean;
    strikethrough?: boolean;
    color?: string;
    backgroundColor?: string;
    fontSize?: string;
}
export interface TessaElement extends TessaElementAttributes, Element {
    type: NodeType;
    children: TessaNode[];
}
export interface TessaText extends TessaTextAttributes, Text {
    text: string;
}
export declare function isTessaElement(arg: TessaNode): arg is TessaElement;
export declare function isTessaText(arg: TessaNode): arg is TessaText;
export declare type TessaNode = TessaEditor | TessaElement | TessaText | Element | Text;
export declare type TessaNodeAttributes = TessaElementAttributes & TessaTextAttributes;
export declare type TessaEditor = Editor & BlockEditor & ImageEditor & ClipboardEditor & ListEditor & LinkEditor & SmartSerializationEditor & StyleEditor & NormalizingEditor & AttachmentEditor & HistoryEditor & ReactEditor;
export interface RenderTessaElementProps extends RenderElementProps {
    element: TessaElement;
}
export interface RenderTessaLeafProps extends RenderLeafProps {
    leaf: TessaText;
    children: TessaNode[];
}
export declare enum NodeType {
    Div = "div",
    Span = "span",
    Paragraph = "paragraph",
    UList = "unorderedList",
    OList = "orderedList",
    ListItem = "listItem",
    ForumBlock = "forumBlock",
    MonospaceBlock = "monospaceBlock",
    QuoteBlock = "quoteBlock",
    Link = "link",
    Image = "image",
    Delete = "delete"
}
export declare type Block = NodeType.ForumBlock | NodeType.MonospaceBlock | NodeType.QuoteBlock;
export declare type List = NodeType.UList | NodeType.OList;
export declare const nonBlockableKeys: ReadonlyArray<string>;
export declare enum MarkType {
    Bold = "bold",
    Italic = "italic",
    Underline = "underline",
    Strikethrough = "strikethrough",
    Color = "color",
    BackgroundColor = "backgroundColor",
    BlockBackgroundColor = "BlockBackgroundColor",
    FontSize = "fontSize",
    Width = "width",
    Height = "height"
}
export declare type BlockToggleHandler = (type: NodeType, value?: string | number | boolean) => void;
export declare type MarkToggleHandler = (type: MarkType, value?: string | number | boolean) => void;
export declare enum ForumClasses {
    Block = "forum-block",
    MonospaceBlock = "forum-block-monospace",
    Quote = "forum-quote",
    QuoteBody = "forum-quote-body",
    Ul = "forum-ul",
    Ol = "forum-ol",
    Url = "forum-url"
}
export declare enum SelectionType {
    Collapsed = 0,
    Inline = 1,
    Multiline = 2,
    ListMultiline = 3,
    CrossBlock = 4,
    NoSelection = 5
}
export declare enum InlineSelectionType {
    Partial = 0,
    WholeLine = 1,
    TouchStart = 2,
    TouchEnd = 3
}
export declare enum CursorPositionType {
    TextStart = 0,
    TextEnd = 1,
    Inline = 2,
    AtEmptyLine = 3,
    NoSelection = 4
}
export interface SelectionInfo {
    selectionType: SelectionType;
    inlineSelectionType?: InlineSelectionType;
}
export declare const fontSizes: number[];
