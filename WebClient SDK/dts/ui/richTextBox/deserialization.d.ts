import { TessaNode, TessaNodeAttributes } from './common';
export declare function fromHTML(html: string): TessaNode[];
export declare function normalizeTessaHtml(html: string): string;
export declare function fromHTMLElement(element: HTMLElement): TessaNode[];
export declare function deserialize(element: HTMLElement): TessaNode | null;
export declare function elementToAttributes(element: HTMLElement): TessaNodeAttributes;
