import { KeyboardEvent } from 'react';
import { TessaEditor } from './common';
export declare function keyHandler(editor: TessaEditor, event: KeyboardEvent): void;
export declare function handleScroll(marker: HTMLSpanElement, wrapper: HTMLDivElement): void;
