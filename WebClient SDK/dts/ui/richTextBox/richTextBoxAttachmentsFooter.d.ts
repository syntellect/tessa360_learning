import { FC } from 'react';
import { RichTextBoxAttachment } from './common';
export interface RichTextBoxAttachmentsFooterProps {
    attachments: ReadonlyArray<RichTextBoxAttachment>;
    onRemoveAttachments?(attachment: RichTextBoxAttachment): void;
    onChangeAttachment?(attachment: RichTextBoxAttachment): void;
    onInsertLink?(attachment: RichTextBoxAttachment): void;
    onInsertFile?(attachment: RichTextBoxAttachment): void;
    onOpenFileAttachment?(id: string): void;
    onOpenLinkAttachment?(id: string): void;
    readOnlyMode: boolean;
}
export declare const RichTextBoxAttachmentsFooter: FC<RichTextBoxAttachmentsFooterProps>;
