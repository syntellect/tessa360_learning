import { FC } from 'react';
import { LinkInfo, RichTextBoxAttachment, BlockToggleHandler, MarkToggleHandler, RangeStyle } from './common';
import { IColorPalette } from 'ui/colorPicker/';
import { UIButton } from 'tessa/ui';
export interface RichTextBoxToolbarProps {
    readOnlyMode?: boolean;
    hasReadOnlyMode?: boolean;
    onChangeReadOnlyMode: (value: boolean) => void;
    onBlockToggle: BlockToggleHandler;
    onStyleToggle: MarkToggleHandler;
    foregroundPalette: IColorPalette;
    backgroundPalette: IColorPalette;
    blockPalette: IColorPalette;
    dropDownDirectionUp?: boolean;
    hasReadonlyMode?: boolean;
    showFileDialog?(): Promise<readonly File[] | File | null>;
    showLinkDialog?(): Promise<LinkInfo | null>;
    onAddFiles(attachments: RichTextBoxAttachment[]): void;
    onAddLink(attachments: RichTextBoxAttachment[]): void;
    onAddImages(attachments: RichTextBoxAttachment[]): void;
    showAttachmentsMenu: boolean;
    selectionStyle: RangeStyle;
    hoverButtons?: readonly UIButton[];
}
export declare const RichTextBoxToolbar: FC<RichTextBoxToolbarProps>;
