import { TessaNode } from './common';
export declare type TagGenerator = (node: TessaNode, attributes: string, content: string) => string;
export declare type AttributeGenerator = (node: TessaNode) => string;
export declare function adaptRawHtmlToTessa(html: string): string;
export declare function toHTML(nodes: TessaNode[]): string;
export declare function serialize(node: TessaNode, nodeToTag: TagGenerator, nodeToAttributes: AttributeGenerator): string;
