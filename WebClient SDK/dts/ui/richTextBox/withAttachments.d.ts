import { Editor, Range } from 'slate';
import { RichTextBoxAttachment, RichTextBoxAttachmentInnerItem } from './common';
import { ImageEditor } from './withImages';
import { LinkEditor } from './withLinks';
import { BlockEditor } from './withBlocks';
export interface AttachmentEditor extends Editor {
    insertImageAttachment: (editor: ImageEditor, attachment: RichTextBoxAttachmentInnerItem, range: Range) => void;
    insertImageAttachments: (editor: AttachmentEditor & ImageEditor, attachments: RichTextBoxAttachment[], range: Range) => void;
    removeAttachment: (editor: BlockEditor & LinkEditor, attachment: RichTextBoxAttachment) => void;
}
export declare function withAttachments<T extends Editor>(e: T): T & AttachmentEditor;
