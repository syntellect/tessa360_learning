import { Editor, Point } from 'slate';
import { ReactEditor } from 'slate-react';
import { NodeType, TessaNode } from './common';
export interface BlockEditor extends Editor {
    isBlockActive: (editor: BlockEditor, type: NodeType) => boolean;
    getBlockNesting: (editor: BlockEditor, point: Point, type: NodeType) => number;
    clearContent: (editor: BlockEditor) => void;
    removeById: (editor: BlockEditor, id: string) => void;
    unwrapParentBlock: (editor: BlockEditor, point: Point) => void;
    removeBlockWithContent: (editor: BlockEditor, point: Point, type: NodeType) => void;
    moveFocusToStart: (editor: BlockEditor) => void;
    moveFocusToEnd: (editor: BlockEditor) => void;
    moveCursorForwardInline: (editor: BlockEditor, distance: number) => void;
    insertParagraphAtDocumentStart: (editor: BlockEditor) => void;
    insertParagraphAtDocumentEnd: (editor: BlockEditor) => void;
    wrapBlock: (editor: BlockEditor, type: NodeType, className?: string) => void;
    wrapInTessaForumBlock: (editor: BlockEditor) => void;
    wrapInTessaForumMonospaceBlock: (editor: BlockEditor) => void;
    toggleBlock: (editor: BlockEditor, type: NodeType) => void;
    pasteFragment: (editor: BlockEditor, fragment?: TessaNode[]) => void;
    forceBrowserToReverseSelection: boolean;
    fixBrowserSelection: (editor: BlockEditor & ReactEditor) => void;
}
export declare function withBlocks<T extends Editor>(e: T): T & BlockEditor;
