import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
import { BlockEditor, ImageDataHandler } from '.';
import { NormalizingEditor } from './withImportNormalization';
export interface ClipboardEditor extends Editor, ReactEditor, BlockEditor, NormalizingEditor {
    onImageReady: ImageDataHandler;
    insertHtml: (editor: NormalizingEditor, html: string) => void;
}
export declare function withClipboard<T extends Editor>(e: T): T & ClipboardEditor;
