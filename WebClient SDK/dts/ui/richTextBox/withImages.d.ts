import { Editor, Range } from 'slate';
import { ImageData, RichTextBoxAttachmentInnerItemDuplicate, RichTextBoxAttachment } from './common';
interface DuplicateEntry {
    id: string;
    entries: number;
}
export interface ImageEditor extends Editor {
    clearImages: (editor: ImageEditor) => void;
    insertImage: (editor: ImageEditor, image: ImageData, range: Range) => void;
    deleteImage: (editor: ImageEditor, id: string) => void;
    getDuplicateImageEntries(editor: ImageEditor): DuplicateEntry[];
    resolveDuplicateImageEntries(editor: ImageEditor, duplicates: DuplicateEntry[]): RichTextBoxAttachmentInnerItemDuplicate[];
    allowInnerAttachmentDuplicates?: boolean;
    onAddInnerItemDuplicate?: (attachment: RichTextBoxAttachmentInnerItemDuplicate[]) => void;
    onGetAttahcments?: () => readonly RichTextBoxAttachment[];
}
export declare function withImages<T extends Editor>(e: T): T & ImageEditor;
export {};
