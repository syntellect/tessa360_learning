import { Editor } from 'slate';
import { ReactEditor } from 'slate-react';
import { BlockEditor } from '.';
import { TessaNode } from './common';
export interface NormalizingEditor extends Editor, ReactEditor, BlockEditor {
    normalizeFragment: (editor: NormalizingEditor, fragment: TessaNode[]) => void;
}
export declare function withImportNormalization<T extends Editor>(e: T): T & NormalizingEditor & ReactEditor;
