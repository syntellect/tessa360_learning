import { Editor, Range } from 'slate';
export interface LinkEditor extends Editor {
    insertLink: (editor: LinkEditor, range: Range, id: string, caption: string, href: string) => void;
    deleteLinksById: (editor: LinkEditor, id: string) => void;
    unwrapLinksById: (editor: LinkEditor, id: string) => void;
    unwrapLinksByHref: (editor: LinkEditor, href: string) => void;
}
export declare function withLinks<T extends Editor>(e: T): T & LinkEditor;
