import { Editor } from 'slate';
import { TessaNode } from './common';
export interface SmartSerializationEditor extends Editor {
    serializationCache: Map<TessaNode, string>;
    serializationDirtyNodes: Map<TessaNode, TessaNode>;
    toHtml: () => string;
}
export declare function withSmartSerialization<T extends Editor>(e: T): T & SmartSerializationEditor;
