import { Editor } from 'slate';
import { MarkType } from './common';
export declare type StyleValue = string | number | boolean | undefined;
export interface StyleEditor extends Editor {
    isStyleActive: (editor: StyleEditor, type: MarkType) => boolean;
    canApplyStyle: (Editor: StyleEditor) => boolean;
    toggleStyle: (editor: StyleEditor, type: MarkType, onValue: StyleValue) => void;
    setStyle: (editor: StyleEditor, type: MarkType, value: StyleValue) => void;
    toggleBold: (editor: StyleEditor) => void;
    toggleItalic: (editor: StyleEditor) => void;
    toggleUnderline: (editor: StyleEditor) => void;
    toggleStrikethrough: (editor: StyleEditor) => void;
    setColor: (editor: StyleEditor, color?: string) => void;
    setBackgroundColor: (editor: StyleEditor, color?: string) => void;
    setBlockBackgroundColor: (editor: StyleEditor, color?: string) => void;
    setFontSize: (editor: StyleEditor, size?: string) => void;
    toggleMark: (editor: StyleEditor, type: MarkType, value?: string) => void;
}
export declare function withStyles<T extends Editor>(e: T): T & StyleEditor;
