import { Editor, Path } from 'slate';
import { NodeType, List } from './common';
import { StyleEditor } from './withStyles';
export interface WordlikeListEditor extends Editor, StyleEditor {
    splitListItem: (editor: WordlikeListEditor) => void;
    isInList: (editor: WordlikeListEditor) => List | undefined;
    getListNesting: (editor: WordlikeListEditor, at: Path) => number;
    toggleListBlock: (editor: WordlikeListEditor, type: NodeType) => void;
    increaseIndent: (editor: WordlikeListEditor, listType: List) => void;
    toggleBlock: (editor: Editor, type: NodeType) => void;
}
export declare function withWordlikeLists<T extends Editor>(e: T): T & WordlikeListEditor;
