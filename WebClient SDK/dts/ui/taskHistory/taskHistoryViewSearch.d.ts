import React from 'react';
import { TaskHistoryViewSearchViewModel } from './taskHistoryViewSearchViewModel';
export interface TaskHistoryViewSearchProps {
    viewModel: TaskHistoryViewSearchViewModel;
}
export declare class TaskHistoryViewSearch extends React.Component<TaskHistoryViewSearchProps> {
    render(): JSX.Element;
}
