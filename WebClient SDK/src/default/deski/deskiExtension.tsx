import { DeskiManager, CipherInfo, DeskiSuccessResponse, FileProcessingInfo } from './deskiManager';
import { APP_URL } from './deskiCommon';
import {
  ApplicationExtension,
  IApplicationExtensionMetadataContext,
  WorkspaceStorage,
  MetadataStorage
} from 'tessa';
import { MenuAction, showConfirm, showDownloadDeskiDialog, showNotEmpty } from 'tessa/ui';
import {
  FileExtension,
  FileExtensionContext,
  FileControlExtension,
  IFileControlExtensionContext,
  FileVersionExtension,
  FileVersionExtensionContext
} from 'tessa/ui/files';
import { FileContainer, IFile, FileType, IFileVersion } from 'tessa/files';

import {
  ValidationResultBuilder,
  ValidationResultType,
  ValidationResult
} from 'tessa/platform/validation';
import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { LocalizationManager } from 'tessa/localization';
import { FileListViewModel, FileViewModel } from 'tessa/ui/cards/controls';
import Platform from 'common/platform';
import { getNameAndExtForFile } from 'common/utility';
import { CardFileType } from 'tessa/cards';

const MASTER_KEYS_UPDATE_INTERVAL = 1000 * 60 * 60 * 4;
let ExtensionWasInitialized = false;

export class DeskiExtension extends ApplicationExtension {
  private _lastMasterKeysUpdate: number;

  public afterMetadataReceived(_context: IApplicationExtensionMetadataContext) {
    if (!DeskiManager.instance.deskiEnabled || Platform.isMobile()) {
      return;
    }

    ExtensionWasInitialized = true;

    (async () => {
      await DeskiManager.instance.checkDeski(true);
      const cipher: CipherInfo = MetadataStorage.instance.info.get('DeskiCipher');
      const result = await DeskiManager.instance.updateMasterKeys(cipher);
      MetadataStorage.instance.info.delete('DeskiCipher');
      if (!result.isSuccessful) {
        console.error(result.format());
        await showNotEmpty(result);
        return;
      }
      this._lastMasterKeysUpdate = Date.now();

      // каждые 5 минут проверяем время, которое прошло с последнего обновления мастер ключа
      // если прошло больше константы (4 часа), то обновляемся
      setInterval(async () => {
        const now = Date.now();
        if (now - this._lastMasterKeysUpdate >= MASTER_KEYS_UPDATE_INTERVAL) {
          const result = await DeskiManager.instance.updateMasterKeys();
          if (!result.isSuccessful) {
            console.error(result.format());
            return;
          }
          this._lastMasterKeysUpdate = now;
        }
      }, 300000);
    })();
  }
}

export class DeskiFileExtension extends FileExtension {
  public openingMenu(context: FileExtensionContext) {
    if (!DeskiManager.instance.deskiAvailable || Platform.isMobile()) {
      return;
    }

    const control = context.control;
    const singleMode = context.files.length === 1;
    const editCollapsed = !context.file.model.permissions.canEdit;
    const previewIndex = context.actions.findIndex(x => x.name === 'Preview');

    const deskiInfo = MetadataStorage.instance.commonMetadata.deskiInfo;
    const is21OrHigherVersion = parseFloat(deskiInfo.version || '0') >= 2.1;
    const isWordSupported = deskiInfo.info?.OS
      ? deskiInfo.info?.OS === 'windows'
      : window.navigator?.platform?.toLocaleLowerCase().includes('win');
    const isSomeNotDocFile = (files: readonly FileViewModel[]) =>
      files.some(
        x => getNameAndExtForFile(x.model.lastVersion.name).ext?.toLocaleLowerCase() !== 'docx'
      );

    const canEdit = context.file.model.permissions.canEdit;
    // скрываем "Открыть для чтения", если это новый файл или файл с заменённой версией, который доступен для редактирования;
    // т.е. показываем, если либо недоступен для редактирования, либо несколько файлов, либо есть добавленная версия
    const canRead = !canEdit || context.files.length > 1 || !context.file.model.versionAdded;

    context.actions.splice(
      previewIndex + 1,
      0,
      new MenuAction(
        'OpenForEdit',
        '$UI_Controls_FilesControl_OpenForEdit',
        'ta icon-thin-002',
        async () => {
          for (let fileVM of context.files) {
            const file = fileVM.model;
            await openFile(file, file.lastVersion, 'file', true);
          }
          control.multiSelectionMode = false;
        },
        null,
        !canEdit
      ),
      new MenuAction(
        'OpenInFolderForEdit',
        '$UI_Controls_FilesControl_OpenInFolderForEdit',
        'ta icon-thin-101',
        async () => {
          const file = context.file.model;
          await openFile(file, file.lastVersion, 'folder', true);
        },
        null,
        !singleMode || editCollapsed
      ),
      new MenuAction(
        'OpenForRead',
        '$UI_Controls_FilesControl_OpenForRead',
        'ta icon-thin-008',
        async () => {
          for (let fileVM of context.files) {
            const file = fileVM.model;
            await openFile(file, file.lastVersion, 'file', false);
          }
          control.multiSelectionMode = false;
        },
        null,
        !canRead
      ),
      new MenuAction(
        'OpenInFolderForRead',
        '$UI_Controls_FilesControl_OpenInFolderForRead',
        'ta icon-thin-101',
        async () => {
          const file = context.file.model;
          await openFile(file, file.lastVersion, 'folder', false);
        },
        null,
        !singleMode || !editCollapsed
      ),

      new MenuAction(
        'MergeWithFollowingInWord',
        '$UI_Controls_FilesControl_MergeWithFollowingInWord',
        'ta icon-thin-359',
        async () => {
          if (!(await checkDeskiVersion(is21OrHigherVersion))) {
            return;
          }
          const file = context.file.model;
          await processMassFiles(
            file,
            file.lastVersion,
            context.files.map(x => x.model.lastVersion),
            DeskiManager.instance.mergeFiles.bind(DeskiManager.instance),
            true
          );
        },
        null,
        singleMode || editCollapsed || !isWordSupported || isSomeNotDocFile(context.files)
      ),
      new MenuAction(
        'CompareInWord',
        '$UI_Controls_FilesControl_CompareInWord',
        'ta icon-thin-420',
        async () => {
          if (!(await checkDeskiVersion(is21OrHigherVersion))) {
            return;
          }
          const file = context.file.model;
          await processMassFiles(
            file,
            file.lastVersion,
            context.files.map(x => x.model.lastVersion),
            DeskiManager.instance.compareFiles.bind(DeskiManager.instance),
            false
          );
        },
        null,
        singleMode ||
          !isWordSupported ||
          context.files.length > 2 ||
          isSomeNotDocFile(context.files)
      ),
      new MenuAction(
        'CopyToClipboard',
        '$UI_Controls_FilesControl_CopyToClipboard',
        'ta icon-thin-063',
        async () => {
          if (!(await checkDeskiVersion(is21OrHigherVersion))) {
            return;
          }
          const file = context.file.model;
          await processMassFiles(
            file,
            file.lastVersion,
            context.files.map(x => x.model.lastVersion),
            (appUrl, source, otherFiles) =>
              DeskiManager.instance.copyToClipboard(appUrl, [source, ...otherFiles]),
            false
          );
        },
        null
      )
    );
  }
}

export class DeskiFileControlExtension extends FileControlExtension {
  //#region FileControlExtension

  public openingMenu(context: IFileControlExtensionContext) {
    if (!DeskiManager.instance.deskiAvailable || Platform.isMobile()) {
      return;
    }

    const control = context.control;

    const deskiInfo = MetadataStorage.instance.commonMetadata.deskiInfo;
    const is21OrHigherVersion = parseFloat(deskiInfo.version || '0') >= 2.1;

    let index = 0;
    const uploadIndex = context.actions.findIndex(x => x.name === 'Upload');
    if (uploadIndex > -1) {
      index = uploadIndex + 1;
    }

    context.actions.splice(
      index,
      0,
      new MenuAction(
        'PasteFromClipboard',
        '$UI_Controls_FilesControl_PasteFromClipboard',
        'ta icon-thin-050',
        async () => {
          if (!(await checkDeskiVersion(is21OrHigherVersion))) {
            return;
          }
          const contents = await pasteFromClipboard();
          if (contents.length === 0) {
            return;
          }

          await control.handleDropFiles(contents);
        },
        null,
        !context.control.fileContainer.permissions.canAdd
      )
    );
  }
}

export class DeskiFileVersionExtension extends FileVersionExtension {
  public openingMenu(context: FileVersionExtensionContext) {
    if (!DeskiManager.instance.deskiAvailable || Platform.isMobile()) {
      return;
    }

    const control = context.control;
    const singleMode = context.versions.length === 1;
    const editCollapsed = !context.file.model.permissions.canEdit;
    const downloadIndex = context.actions.findIndex(x => x.name === 'VersionDownload');

    const deskiInfo = MetadataStorage.instance.commonMetadata.deskiInfo;
    const is21OrHigherVersion = parseFloat(deskiInfo.version || '0') >= 2.1;
    const isWordSupported = deskiInfo.info?.OS
      ? deskiInfo.info?.OS === 'windows'
      : window.navigator?.platform?.toLocaleLowerCase().includes('win');
    const IsSomeNotDocVersion = (versions: readonly IFileVersion[]) =>
      versions.some(x => getNameAndExtForFile(x.name).ext?.toLocaleLowerCase() !== 'docx');

    context.actions.splice(
      downloadIndex + 1,
      0,
      new MenuAction(
        'OpenForRead',
        '$UI_Controls_FilesControl_OpenForRead',
        'ta icon-thin-008',
        async () => {
          for (let version of context.versions) {
            await openFile(context.file.model, version, 'file', false);
          }
          control.multiSelectionMode = false;
        }
      ),
      new MenuAction(
        'OpenInFolderForRead',
        '$UI_Controls_FilesControl_OpenInFolderForRead',
        'ta icon-thin-101',
        async () => {
          const file = context.file.model;
          await openFile(file, file.lastVersion, 'folder', false);
        },
        null,
        !singleMode || !editCollapsed
      ),

      new MenuAction(
        'MergeNewVersionWithFollowingInWord',
        '$UI_Controls_FilesControl_MergeNewVersionWithFollowingInWord',
        'ta icon-thin-359',
        async () => {
          if (
            context.versions.length === 1 &&
            context.version.id === context.file.model.lastVersion.id
          ) {
            return;
          }

          if (!(await checkDeskiVersion(is21OrHigherVersion))) {
            return;
          }
          const file = context.file.model;
          const lastVersion = file.lastVersion;
          const versions = [...context.versions];
          if (!versions.some(x => x.id === lastVersion.id)) {
            versions.push(lastVersion);
          }

          await processMassFiles(
            file,
            lastVersion,
            versions,
            DeskiManager.instance.mergeFiles.bind(DeskiManager.instance),
            true
          );
        },
        null,
        editCollapsed ||
          !isWordSupported ||
          IsSomeNotDocVersion([context.file?.model.lastVersion, ...context.versions])
      ),
      new MenuAction(
        'CompareInWord',
        '$UI_Controls_FilesControl_CompareInWord',
        'ta icon-thin-420',
        async () => {
          if (!(await checkDeskiVersion(is21OrHigherVersion))) {
            return;
          }
          const file = context.file.model;
          await processMassFiles(
            file,
            context.version,
            context.versions,
            DeskiManager.instance.compareFiles.bind(DeskiManager.instance),
            false
          );
        },
        null,
        singleMode ||
          !isWordSupported ||
          context.versions.length > 2 ||
          IsSomeNotDocVersion(context.versions)
      ),
      new MenuAction(
        'CopyToClipboard',
        '$UI_Controls_FilesControl_CopyToClipboard',
        'ta icon-thin-063',
        async () => {
          if (!(await checkDeskiVersion(is21OrHigherVersion))) {
            return;
          }
          const file = context.file.model;
          await processMassFiles(
            file,
            context.version,
            context.versions,
            (appUrl, source, otherFiles) =>
              DeskiManager.instance.copyToClipboard(appUrl, [source, ...otherFiles]),
            false
          );
        },
        null
      )
    );
  }
}

export class DeskiUIExtension extends CardUIExtension {
  shouldExecute() {
    if (!DeskiManager.instance.deskiAvailable) {
      return false;
    }
    return true;
  }
  public initialized(context: ICardUIExtensionContext) {
    const fileControls = context.model.controlsBag.filter(
      x => x instanceof FileListViewModel
    ) as FileListViewModel[];
    for (let control of fileControls) {
      control.fileDoubleClickAction = async fileVM => {
        const file = fileVM.model;
        const canEdit = file.permissions.canEdit;
        await openFile(file, file.lastVersion, 'file', canEdit);
      };
    }
  }
  public async saving(context: ICardUIExtensionContext) {
    let files = context.fileContainer.files;
    if (!files || files.length === 0) {
      return;
    }

    const editedFiles = files.filter(x => x.isDirty);
    if (editedFiles.length === 0) {
      return;
    }

    const savingResult = new ValidationResultBuilder();
    for (let file of editedFiles) {
      try {
        const { info: fileInfo, result: fileInfoResult } = await DeskiManager.instance.getFileInfo(
          APP_URL,
          file.lastVersion.id,
          true
        );
        if (fileInfoResult) {
          savingResult.add(fileInfoResult);
          continue;
        }

        if (!fileInfo) {
          savingResult.add(
            ValidationResultType.Error,
            `Can not find deski's FileInfo. File version: ${file.lastVersion.id}`
          );
          continue;
        }

        if (fileInfo!.IsLocked) {
          savingResult.add(
            ValidationResultType.Error,
            LocalizationManager.instance.format('$UI_Cards_FileSaving_Locked', fileInfo.Name)
          );
          continue;
        }

        if (!fileInfo.IsModified) {
          await removeFile(file);
          continue;
        }

        const cardFile = context.card.files.find(x => x.rowId === file.id);
        if (!cardFile) {
          savingResult.add(
            ValidationResultType.Error,
            `Can not find file in card storage. File: ${file.id}`
          );
          continue;
        }

        const cacheResult = await DeskiManager.instance.cacheModFileWithNewId(
          APP_URL,
          fileInfo.ID,
          cardFile.versionRowId
        );
        if (!cacheResult.success) {
          savingResult.add(cacheResult.result);
        }
        await removeFile(file);
      } catch (error) {
        savingResult.add(ValidationResult.fromError(error));
      }
    }

    const result = savingResult.build();
    if (result.items.length > 0) {
      console.error(result.format());
    }
  }
  public async finalized(context: ICardUIExtensionContext) {
    await clearEditedFiles(context.fileContainer);
  }
}

async function openFile(
  file: IFile,
  version: IFileVersion,
  mode: 'file' | 'folder',
  editable: boolean
): Promise<void> {
  if (!(await DeskiManager.instance.checkDeski())) {
    return;
  }
  let result = await DeskiManager.instance.setAppInfo(APP_URL, DeskiManager.instance.masterKeys);
  if (!result.success) {
    await showNotEmpty(result.result);
    return;
  }

  const contentInfo = await DeskiManager.instance.getContentInfo(APP_URL, version.id);
  if (contentInfo.result) {
    await showNotEmpty(contentInfo.result);
    return;
  }
  if (!contentInfo.isCached || isVirtualFile(file.type)) {
    if (isVirtualFile(file.type)) {
      await DeskiManager.instance.removeFile(APP_URL, version.id, editable);
    }

    await version.ensureContentDownloaded();
    const content = version.content;

    if (!content) {
      await showNotEmpty(
        ValidationResult.fromText('$Deski_Cant_Load_Content', ValidationResultType.Error)
      );
      return;
    }
    result = await DeskiManager.instance.cacheContent(APP_URL, version.id, version.name, content);
    if (!result.success) {
      await showNotEmpty(result.result);
      return;
    }
  }

  result = await DeskiManager.instance.openFile(APP_URL, version.id, mode, editable);
  if (!result.success) {
    await showNotEmpty(result.result);
    return;
  }

  if (editable) {
    file.isDirty = true;
  }
}

async function processMassFiles(
  sourceFile: IFile,
  sourceVersion: IFileVersion,
  fileVersions: readonly IFileVersion[],
  func: (
    appUrl: string,
    source: FileProcessingInfo,
    otherFiles: FileProcessingInfo[]
  ) => DeskiSuccessResponse,
  editable: boolean = false
): Promise<void> {
  if (!(await DeskiManager.instance.checkDeski())) {
    return;
  }

  const forProcessingFiles: FileProcessingInfo[] = [];
  for (let version of fileVersions) {
    let result = await DeskiManager.instance.setAppInfo(APP_URL, DeskiManager.instance.masterKeys);
    if (!result.success) {
      await showNotEmpty(result.result);
      return;
    }
    const contentInfo = await DeskiManager.instance.getContentInfo(APP_URL, version.id);
    if (contentInfo.result) {
      await showNotEmpty(contentInfo.result);
      return;
    }
    if (!contentInfo.isCached) {
      await version.ensureContentDownloaded();
      const content = version.content;

      if (!content) {
        await showNotEmpty(
          ValidationResult.fromText('$Deski_Cant_Load_Content', ValidationResultType.Error)
        );
        return;
      }
      result = await DeskiManager.instance.cacheContent(APP_URL, version.id, version.name, content);
      if (!result.success) {
        await showNotEmpty(result.result);
        return;
      }
    }
    if (version.id !== sourceVersion.id) {
      forProcessingFiles.push({ id: version.id, author: version.createdByName });
    }
  }

  let result = await func(
    APP_URL,
    { id: sourceVersion.id, author: sourceVersion.createdByName },
    forProcessingFiles
  );
  if (!result.success) {
    await showNotEmpty(result.result);
    return;
  }

  if (editable) {
    sourceFile.isDirty = true;
  }
}

async function pasteFromClipboard(): Promise<File[]> {
  if (!(await DeskiManager.instance.checkDeski())) {
    return [];
  }

  let result = await DeskiManager.instance.pasteFromClipboard(APP_URL);
  if (result.result) {
    await showNotEmpty(result.result);
    return [];
  }
  return result.files;
}

// при закрытии вкладки или обновлении страницы вручную надо удалить окрытые файлы
window.addEventListener('beforeunload', function () {
  if (!ExtensionWasInitialized || !DeskiManager.instance.deskiAvailable) {
    return;
  }

  const cards = WorkspaceStorage.instance.cards;
  if (cards.size === 0) {
    return;
  }

  (async () => {
    for (let card of cards.values()) {
      if (card.editor.cardModel) {
        await clearEditedFiles(card.editor.cardModel.fileContainer);
      }
    }
  })();
});

const clearEditedFiles = async (fileContainer: FileContainer) => {
  const files = fileContainer.files;
  if (!files) {
    return;
  }

  const editedFiles = files.filter(x => x.isDirty);
  if (editedFiles.length === 0) {
    return;
  }

  for (let file of editedFiles) {
    try {
      await removeFile(file);
    } catch (error) {
      console.error(error);
    }
  }
};

const removeFile = async (file: IFile) => {
  await DeskiManager.instance.removeFile(APP_URL, file.lastVersion.id, true);
};

const isVirtualFile = (fileType: FileType | null): boolean => {
  if (fileType && fileType instanceof CardFileType) {
    return fileType.isVirtual;
  }

  return false;
};

const checkDeskiVersion = async (is21OrHigherVersion: boolean) => {
  if (!is21OrHigherVersion) {
    if (await showConfirm('$Deski_Unlock_Feature')) {
      await showDownloadDeskiDialog();
    }
    return false;
  }
  return true;
};
