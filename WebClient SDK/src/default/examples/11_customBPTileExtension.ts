import { TileExtension, ITileGlobalExtensionContext, TileEvaluationEventArgs,
  TileGroups, Tile } from 'tessa/ui/tiles';
import { UIContext } from 'tessa/ui';
import { Guid, createTypedField, DotNetType } from 'tessa/platform';
import { CardStoreMode } from 'tessa/cards';

/**
 * Пример тайла, который создает задачу в карточке.
 */
export class CustomBPTileExtension extends TileExtension {

  public initializingGlobal(context: ITileGlobalExtensionContext) {
    const panel = context.workspace.leftPanel;
    const tile = new Tile({
      name: 'StartCustomBPTileExtension',
      caption: 'Запустить кастомный БП',
      icon: 'ta icon-thin-002',
      contextSource: panel.contextSource,
      group: TileGroups.Cards,
      order: 100,
      command: CustomBPTileExtension.startCustomBP,
      evaluating: CustomBPTileExtension.enableIfCard
    });
    panel.tiles.push(tile);
  }

  private static async startCustomBP() {
    const context = UIContext.current;
    const editor = context.cardEditor;

    if (!editor || !editor.cardModel) {
      return;
    }

    // если карточка новая, то сохраняем её
    const cardIsNew = editor.cardModel.card.storeMode === CardStoreMode.Insert;
    if (cardIsNew) {
      const saved = await editor.saveCard(context);
      if (!saved) {
        return;
      }
    }

    // сохраняем карточку и записываем флажок в info
    // флажок нуобходимо добавлять через createTypedField, чтобы на сервере правильно отрезолвить тип
    editor.saveCard(context, {
      '.startProcess': createTypedField('WfResolution', DotNetType.String)
    });
  }

  private static enableIfCard(e: TileEvaluationEventArgs) {
    const editor = UIContext.current.cardEditor;
    e.setIsEnabledWithCollapsing(e.currentTile,
      !!editor
      && !!editor.cardModel
      && Guid.equals(editor.cardModel.cardType.id, '4bbd2f5e-6c65-41bd-a159-1a373355a26c')
    );
  }

}