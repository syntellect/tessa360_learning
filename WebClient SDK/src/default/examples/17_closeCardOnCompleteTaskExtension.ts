import { CardStoreExtension, ICardStoreExtensionContext } from 'tessa/cards/extensions';
import { Guid, createTypedField, DotNetType } from 'tessa/platform';
import { CardTaskAction } from 'tessa/cards';
import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { tryGetFromInfo } from 'tessa/ui';

export class CloseCardOnCompleteTaskStoreExtension extends CardStoreExtension {

  private _taskComplete: boolean = false;

  public beforeRequest(context: ICardStoreExtensionContext) {
    const card = context.request.card;
    // если карточка не для тестов, то ничего не делаем
    if (!Guid.equals(card.typeId, '4bbd2f5e-6c65-41bd-a159-1a373355a26c')) {
      return;
    }

    const tasks = card.tryGetTasks();
    if (!tasks) {
      return;
    }

    this._taskComplete = tasks.some(
      x => Guid.equals(x.typeId, '928132fe-202d-4f9f-8ec5-5093ea2122d1') // WfResolution
      && x.action === CardTaskAction.Complete
    );
  }

  public afterRequest(context: ICardStoreExtensionContext) {
    if (!context.requestIsSuccessful
      || !context.response
      || !this._taskComplete
    ) {
      return;
    }

    context.response.info['.closeCardAfterStore'] = createTypedField(true, DotNetType.Boolean);
  }

}

export class CloseCardOnCompleteTaskUIExtension extends CardUIExtension {

  public reopening(context: ICardUIExtensionContext) {
    // если карточка не для тестов, то ничего не делаем
    if (!Guid.equals(context.card.typeId, '4bbd2f5e-6c65-41bd-a159-1a373355a26c')) {
      return;
    }

    const response = context.getResponse;
    if (!response) {
      return;
    }

    const close = tryGetFromInfo(response.info, '.closeCardAfterStore', false);
    if (close) {
      const editor = context.uiContext.cardEditor;
      if (editor) {
        editor.closePending = true;
      }
    }
  }

}