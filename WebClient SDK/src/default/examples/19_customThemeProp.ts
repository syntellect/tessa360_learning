import { ApplicationExtension, IApplicationExtensionMetadataContext } from 'tessa';
import { ThemeManager, ThemeSettings } from 'tessa/ui/themes';
import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { Guid } from 'tessa/platform';
import { TextBoxViewModel } from 'tessa/ui/cards/controls';
import { addToMediaStyle } from 'ui';

export class CustomThemePropApplicationExtension extends ApplicationExtension {

  public afterMetadataReceived(_context: IApplicationExtensionMetadataContext) {
    if (!ThemeManager.instance.isInitialized) {
      return;
    }

    for (let pair of ThemeManager.instance.themes) {
      const name = pair[0];
      const theme = pair[1];
      const backgroundColor = name === 'Cold'
        ? 'rgba(150, 150, 150, 0.5)'
        : 'rgba(66, 88, 111, 0.75)';
      theme.settings.common['MyCustomBackgroundProp'] = backgroundColor;
    }
  }

}

export class CustomThemePropUIExtension extends CardUIExtension {

  public initialized(context: ICardUIExtensionContext) {
    // если не карточка "автомобиль", то ничего не делаем
    if (!Guid.equals(context.card.typeId, 'd0006e40-a342-4797-8d77-6501c4b7c4ac')) {
      return;
    }

    const carNameControl = context.model.controls.get('CarName') as TextBoxViewModel;
    if (!carNameControl) {
      return;
    }

    this.changeCaptionStyle(carNameControl, ThemeManager.instance.currentTheme.settings);
    carNameControl.onThemeChanged.add(e => this.changeCaptionStyle(carNameControl, e.theme.settings));
  }

  private changeCaptionStyle = (control: TextBoxViewModel, theme: ThemeSettings) => {
    control.captionStyle = addToMediaStyle(
      control.captionStyle, 'default', {background: theme.common['MyCustomBackgroundProp']});
  }

}