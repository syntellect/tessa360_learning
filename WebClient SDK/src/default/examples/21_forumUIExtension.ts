import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { ForumViewModel } from 'tessa/ui/cards/controls';
import { UIButton, showMessage, MenuAction } from 'tessa/ui';

export class ForumUIExtension extends CardUIExtension {

  public initialized(context: ICardUIExtensionContext) {
    const forumControl = this.tryGetForumControl(context);
    if (!forumControl) {
      return;
    }

    forumControl.addOnContentChangedAndInvoke(() => {
      const editor = forumControl.tryGetTopicEditor();
      if (!editor) {
        return;
      }

      editor.rightButtons.push(
        UIButton.create({
          name: 'TestButton',
          icon: 'icon-thin-100',
          buttonAction: async () => {
            await showMessage('Hello from test button!');
          }
        })
      );

      editor.attachmentContextMenuGenerators.push(ctx => {
        ctx.menuActions.push(
          MenuAction.create({
            name: 'TestMenu',
            caption: 'TestMenu',
            icon: 'icon-thin-099',
            action: async () => {
              await showMessage('Hello from test menu!');
            }
          })
        );
      });
    });
  }

  private tryGetForumControl(context: ICardUIExtensionContext): ForumViewModel | null {
    const forumTab = context.model.forms.find(x => x.name === 'Forum');
    if (!forumTab) {
      return null;
    }

    const topicsBlock = forumTab.blocks.find(x => x.name === 'Topics');
    if (!topicsBlock) {
      return null;
    }

    const forumControl = topicsBlock.controls[0] as ForumViewModel;
    if (!forumControl
      || !(forumControl instanceof ForumViewModel)
    ) {
      return null;
    }

    return forumControl;
  }

}