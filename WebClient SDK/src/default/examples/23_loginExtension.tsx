import React from 'react';
import { Observer } from 'mobx-react';
import { observable, runInAction } from 'mobx';
import type { Location } from 'history';
import { showMessage, UIButton } from 'tessa/ui';
import {
  ILoginExtensionContext,
  LoginExtension,
  LoginFormViewModel,
  LoginForm,
  LoginLogo,
  LoginFields,
  LoginMessage,
  LoginButtons
} from 'tessa/ui/login';
import { TextField } from 'ui';
import { ValidationResult, ValidationResultType } from 'tessa/platform/validation';
import { captchaExampleImg, captchaExampleValue } from './captchaExample';

export class ExampleLoginExtension extends LoginExtension {
  initializing(context: ILoginExtensionContext) {
    context.loginFormViewModelFactory = () => new ExampleLoginFormViewModel();
    context.loginFormFactory = viewModel => (
      <ExampleLoginForm viewModel={viewModel as ExampleLoginFormViewModel} />
    );
  }

  initialized(context: ILoginExtensionContext) {
    const { viewModel } = context;
    if (!viewModel) {
      return;
    }

    viewModel.buttons.push(
      UIButton.create({
        name: 'TestButton',
        caption: 'TestButton',
        icon: 'icon-thin-025',
        buttonAction: () => {
          showMessage('TestButton click!', 'Test', { OKButtonText: 'OK!' });
        }
      })
    );
  }
}

class ExampleLoginFormViewModel extends LoginFormViewModel {
  //#region props

  @observable.ref
  captchaValue: string = '';

  @observable.ref
  captchaImg: string = '';

  captchaValidator: (value: string) => boolean;

  onCaptchaFieldChanged = (_e: React.SyntheticEvent<HTMLInputElement>, validatedText: string) => {
    runInAction(() => (this.captchaValue = validatedText));
  };

  //#endregion

  //#region methods

  private initializeCaptcha() {
    runInAction(() => {
      this.captchaImg = captchaExampleImg;
    });
    this.captchaValidator = (value: string) =>
      !!value && value.toLowerCase() === captchaExampleValue;
  }

  private captchaLoginCheck(): boolean {
    return runInAction(() => {
      this.message = null;
      if (!this.captchaValidator(this.captchaValue)) {
        this.message = ValidationResult.fromText(
          'Captcha value is wrong.',
          ValidationResultType.Error
        );
        this.captchaValue = '';
        return false;
      }

      return true;
    });
  }

  //#endregion

  //#region overrides

  async initialize(location: Location) {
    await super.initialize(location);
    this.initializeCaptcha();
  }

  protected onLogin() {
    if (this.captchaLoginCheck()) {
      super.onLogin();
    }
  }

  protected onWinLogin() {
    if (this.captchaLoginCheck()) {
      super.onWinLogin();
    }
  }

  protected onSAMLLogin() {
    if (this.captchaLoginCheck()) {
      super.onSAMLLogin();
    }
  }

  //#endregion
}

interface ExampleLoginFormProps {
  viewModel: ExampleLoginFormViewModel;
}

const ExampleLoginForm = (props: ExampleLoginFormProps) => {
  const { viewModel } = props;
  return (
    <LoginForm viewModel={viewModel}>
      <LoginLogo viewModel={viewModel} />
      <LoginFields viewModel={viewModel} />
      <Observer>
        {() => {
          return (
            <>
              <div style={{ marginBottom: '10px' }}>
                <img src={viewModel.captchaImg} />
              </div>
              <TextField
                type={'text'}
                placeholder={'captcha'}
                value={viewModel.captchaValue}
                onChange={viewModel.onCaptchaFieldChanged}
              />
            </>
          );
        }}
      </Observer>
      <LoginMessage viewModel={viewModel} />
      <LoginButtons viewModel={viewModel} />
    </LoginForm>
  );
};
