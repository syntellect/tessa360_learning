import { TileExtension, ITileGlobalExtensionContext, Tile, TileGroups, TileEvaluationEventArgs } from 'tessa/ui/tiles';
import { UIContext, showMessage, MessageBoxResult } from 'tessa/ui';
import { ICardModel } from 'tessa/ui/cards';
import { Guid } from 'tessa/platform';
import { CardSection } from 'tessa/cards';

/**
 * Добавлять\cкрывать\показывать тайл в левой панели для карточек в зависимости от:
 * - типа карточки
 * - данных карточки
 *
 * Добавляем на левую панель тайл, который по нажатию открывает модальное окно.
 * Тайл виден только в карточке с типом WebTestType и темой карточки равной "Карточка с тайлом"
 */
export class SimpleCardTileExtension extends TileExtension {

  public initializingGlobal(context: ITileGlobalExtensionContext) {
    const panel = context.workspace.leftPanel;
    const tile = new Tile({
      name: 'SimpleCardTile',
      caption: 'SimpleCardTile',
      icon: 'ta icon-thin-002',
      contextSource: panel.contextSource,
      group: TileGroups.Cards,
      order: 100,
      command: SimpleCardTileExtension.showMessageBoxCommand,
      evaluating: SimpleCardTileExtension.enableIfCardWithSubject
    });
    panel.tiles.push(tile);
  }

  private static async showMessageBoxCommand() {
    // пытаемся получить текущий СardEditor
    const editor = UIContext.current.cardEditor;
    let cardModel: ICardModel;
    if (!editor
      || !(cardModel = editor.cardModel!)
    ) {
      return;
    }

    const card = cardModel.card;
    const section = card.sections.tryGet('DocumentCommonInfo');
    if (!section) {
      return;
    }

    const joke =
      '<b>Анекдот:</b> The past, the present, and the future walked into a bar. It was tense.';
    const subject = section.fields.tryGet('Subject') || '';

    const result = await showMessage(`${joke}<br/><b>Тема карточки:</b> ${subject}`);
    if (result === MessageBoxResult.OK) {
      console.log('Окно закрыто по кнопке ОК. Шутка понравилась.');
    }
  }

  private static enableIfCardWithSubject(e: TileEvaluationEventArgs) {
    // пытаемся получить текущий СardEditor
    const editor = UIContext.current.cardEditor;
    let section: CardSection;
    e.setIsEnabledWithCollapsing(e.currentTile,
      !!editor
      && !!editor.cardModel
      && Guid.equals(editor.cardModel.cardType.id, '4bbd2f5e-6c65-41bd-a159-1a373355a26c')
      && !!(section = editor.cardModel.card.sections.tryGet('DocumentCommonInfo')!)
      && section.fields.tryGet('Subject') === 'Карточка с тайлом'
    );
  }

}