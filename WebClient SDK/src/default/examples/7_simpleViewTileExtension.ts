import { TileExtension, ITileGlobalExtensionContext, Tile, TileGroups, TileEvaluationEventArgs } from 'tessa/ui/tiles';
import { UIContext, showMessage } from 'tessa/ui';
import { ITessaView } from 'tessa/views';

/**
 * Добавлять\cкрывать\показывать тайл в левой панели для представления в зависимости от:
 * - идентификатора узла рабочего места
 * - алиаса представления
 * - данных выделенной строки или строк
 */
export class SimpleViewTileExtension extends TileExtension {

  private static myDocumentsAlias = 'MyDocuments';

  public initializingGlobal(context: ITileGlobalExtensionContext) {
    const panel = context.workspace.leftPanel;
    const tile = new Tile({
      name: 'SimpleViewTile',
      caption: 'SimpleViewTile',
      icon: 'ta icon-thin-002',
      contextSource: panel.contextSource,
      group: TileGroups.Views,
      order: 100,
      command: SimpleViewTileExtension.showViewData,
      evaluating: SimpleViewTileExtension.enableIfMyDocumentsViewAndHasSelectedRow
    });
    panel.tiles.push(tile);
  }

  private static async showViewData() {
    // пытаемся получить текущий ViewContext
    const viewContext = UIContext.current.viewContext;
    // tslint:disable-next-line:no-any
    let currentRow: ReadonlyMap<string, any>;
    if (!viewContext
      || !(currentRow = viewContext.selectedRow!)
    ) {
      return;
    }

    let text: string[] = [];
    currentRow.forEach((v, k) => {
      text.push(`${k}: ${v}`);
    });

    await showMessage(text.join('\n'));
  }

  private static enableIfMyDocumentsViewAndHasSelectedRow(e: TileEvaluationEventArgs) {
    // пытаемся получить текущий ViewContext
    const viewContext = UIContext.current.viewContext;
    let view: ITessaView;
    e.setIsEnabledWithCollapsing(e.currentTile,
      !!viewContext
      && !!(view = viewContext.view!) // получаем модель текущего представления
      && view.metadata.alias === SimpleViewTileExtension.myDocumentsAlias
      && !!viewContext.selectedRow
    );
  }

}