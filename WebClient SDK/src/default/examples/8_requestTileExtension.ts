import { TileExtension, ITileGlobalExtensionContext, Tile, TileGroups } from 'tessa/ui/tiles';
import { CardGetRequest, CardService } from 'tessa/cards/service';
import { showNotEmpty, showMessage } from 'tessa/ui';
import { RequestParameterBuilder, ViewService,
  TessaViewRequest, ITessaViewResult, convertRowsToMap } from 'tessa/views';
import { isNotNullCriteriaOperator } from 'tessa/views/metadata';
import { ValidationResult } from 'tessa/platform/validation';

/**
 * Добавить глобальную группу\тайл в левую панель. При нажатии:
 * - формируем кастомный реквест на сервер, выполняем его, читаем что-то из reponse, который пришел
 * с сервера (из info, например) и выводим сообщение с этим прочитанным результатом.
 * - формируем реквест на вьюху, добавляем туда для примера какой-то поисковый параметр и результат показываем в сообщении.
 */
export class RequestTileExtension extends TileExtension {

  public initializingGlobal(context: ITileGlobalExtensionContext) {
    const panel = context.workspace.leftPanel;
    const groupTile = new Tile({
      name: 'TestGroupRequestTileExtension',
      caption: 'Запросы',
      icon: 'ta icon-thin-100',
      contextSource: panel.contextSource,
      group: TileGroups.Cards,
      order: 100
    });

    const cardRequestTile = new Tile({
      name: 'TestCardRequestTileExtension',
      caption: 'Запрос карточки',
      icon: 'ta icon-thin-100',
      contextSource: panel.contextSource,
      group: TileGroups.Cards,
      order: 1,
      command: RequestTileExtension.cardRequestCommand
    });
    groupTile.tiles.push(cardRequestTile);

    const viewRequestTile = new Tile({
      name: 'TestViewRequestTileExtension',
      caption: 'Запрос представления',
      icon: 'ta icon-thin-100',
      contextSource: panel.contextSource,
      group: TileGroups.Cards,
      order: 2,
      command: RequestTileExtension.viewRequestCommand
    });
    groupTile.tiles.push(viewRequestTile);

    panel.tiles.push(groupTile);
  }

  private static async cardRequestCommand() {
    const cardGetRequest = new CardGetRequest();
    cardGetRequest.cardId = '9fac55ba-afab-4a40-8789-79c97d96ace2';
    cardGetRequest.cardTypeId = 'fa9dbdac-8708-41df-bd72-900f69655dfa';
    cardGetRequest.cardTypeName = 'KrPermissions';

    // при запросе все расширения CardGetExtension будут вызываны
    const cardGetResponse = await CardService.instance.get(cardGetRequest);
    if (!cardGetResponse.validationResult.isSuccessful) {
      // если в validationResult есть ошибки, то показываем их
      await showNotEmpty(cardGetResponse.validationResult.build());
      return;
    }

    // берем storage карточки как строку
    const cardStr = JSON.stringify(cardGetResponse.card.getStorage());
    await showMessage(cardStr.substr(0, 100));
  }

  private static async viewRequestCommand() {
    // пытаемся найти представление "Контрагенты"
    const partnersView = ViewService.instance.getByName('Partners');
    if (!partnersView) {
      return;
    }

    const request = new TessaViewRequest(partnersView.metadata);

    // добавляем параметр фильтрации по имени контрагента (для примера, что имя не равно null)
    const nameParam = new RequestParameterBuilder()
      .withMetadata(partnersView.metadata.parameters.get('Name')!)
      .addCriteria(isNotNullCriteriaOperator())
      .asRequestParameter();
    request.values.push(nameParam);

    let result: ITessaViewResult;
    try {
      // в getData будут добавлены параметры currentUserId и locale
      result = await partnersView.getData(request);
    } catch (err) {
      await showNotEmpty(ValidationResult.fromError(err));
      return;
    }

    // конвертируем строки в Map<string, any>[] для удобства
    const rows = convertRowsToMap(result.columns, result.rows);

    let text: string[] = [];
    rows.forEach(row => {
      const rowText: string[] = [];
      row.forEach((v, k) => {
        rowText.push(`${k}: ${v}`);
      });
      text.push(rowText.join(';'));
    });

    await showMessage(text.join('\n'));
  }

}