import React from 'react';

export type ExamplePreviewerComponentProps = {
  text?: string;
  isLoading?: boolean;
};

export class ExamplePreviewerComponent extends React.PureComponent<ExamplePreviewerComponentProps> {
  public render() {
    const { text, isLoading } = this.props;

    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <h3>Example Previewer</h3>
        {isLoading && <h4>Text is loading...</h4>}
        <span>{text}</span>
      </div>
    );
  }
}
