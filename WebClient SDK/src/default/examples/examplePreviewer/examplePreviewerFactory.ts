import { CustomPreviewViewModelFactory, ICustomEditorViewModel } from 'tessa/ui/customFileEditor';
import { IFileVersion } from 'tessa/files';
import { ExamplePreviewerViewModel } from './examplePreviewerViewModel';

export class ExamplePreviewerFactory implements CustomPreviewViewModelFactory {
  public canUsePreview(version: IFileVersion): boolean {
    return version.getExtension() === 'exampleTxt';
  }

  public async createViewModel(
    placeholder: string,
    version: IFileVersion
  ): Promise<ICustomEditorViewModel> {
    return new ExamplePreviewerViewModel(placeholder, version);
  }
}
