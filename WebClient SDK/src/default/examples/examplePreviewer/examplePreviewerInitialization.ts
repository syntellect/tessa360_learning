import { ApplicationExtension, IApplicationExtensionContext } from 'tessa';
import { ExamplePreviewerFactory } from './examplePreviewerFactory';
import { CustomPreviewViewModelFactoryProvider } from 'tessa/ui/customFileEditor';

export class ExamplePreviewerInitialization extends ApplicationExtension {
  public initialize(_context: IApplicationExtensionContext) {
    CustomPreviewViewModelFactoryProvider.instance.factories.push(new ExamplePreviewerFactory());
  }
}
