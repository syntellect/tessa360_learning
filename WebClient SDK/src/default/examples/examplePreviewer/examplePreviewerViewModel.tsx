import { ICustomEditorViewModel } from 'tessa/ui/customFileEditor';
import { IFileVersion } from 'tessa/files';
import React from 'react';
import ReactDOM from 'react-dom';
import {
  ExamplePreviewerComponent,
  ExamplePreviewerComponentProps
} from './examplePreviewerComponent';

export class ExamplePreviewerViewModel implements ICustomEditorViewModel {
  public readonly placeholder: string;
  public readonly version: IFileVersion;

  private _container: Element | null = null;

  constructor(placeholder: string, version: IFileVersion) {
    this.placeholder = placeholder;
    this.version = version;
  }

  public async load(): Promise<void> {
    const placeholderElement = document.querySelector(this.placeholder)!;
    this._container = placeholderElement.parentElement!;

    this.rerenderPreviewComponent({ isLoading: true });

    await this.version.ensureContentDownloaded();
    const text = await this.version.content!.text();

    this.rerenderPreviewComponent({ isLoading: false, text: text });
  }

  public async destroy(): Promise<void> {
    if (this._container) {
      // выполнять размонтирование компонента в данном примере не обязательно, но желательно.
      ReactDOM.unmountComponentAtNode(this._container);
      this._container = null;
    }
  }

  private rerenderPreviewComponent(props: ExamplePreviewerComponentProps) {
    ReactDOM.render(<ExamplePreviewerComponent {...props} />, this._container);
  }
}
