import { SliderControlType } from './sliderControlType';
import { CardMetadataExtension, ICardMetadataExtensionContext } from 'tessa/cards/extensions';
import { CardTypeEntryControl } from 'tessa/cards/types';

export class SliderMetadataExtension extends CardMetadataExtension {

  public initializing(context: ICardMetadataExtensionContext) {
    const cardCarType = context.cardMetadata
      .getCardTypeById('d0006e40-a342-4797-8d77-6501c4b7c4ac');
    if (!cardCarType) {
      return;
    }

    const firstBlock = cardCarType.blocks[0];
    const maxOrder = Math.max(...firstBlock.controls.map(x => x.order), 0);

    const sliderType = new CardTypeEntryControl();
    sliderType.type = SliderControlType;
    sliderType.order = maxOrder + 1;
    sliderType.name = 'OurSuperMegaCoolSlider';
    sliderType.caption = 'Slider';
    sliderType.controlSettings = {
      'MinValeue': 20,
      'MaxValue': 150,
      'Step': 1
    };
    sliderType.blockSettings = {
      'StartAtNewLine': true
    };
    sliderType.sectionId = '509d961f-00cf-4403-a78f-6736841de448';
    sliderType.physicalColumnIdList = ['ef4db447-b0b5-4474-a6b7-6c5c75465355'];

    firstBlock.controls.push(sliderType);
  }

}