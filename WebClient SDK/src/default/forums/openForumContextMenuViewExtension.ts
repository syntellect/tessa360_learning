import { WorkplaceViewComponentExtension } from 'tessa/ui/views/extensions';
import { IWorkplaceViewComponent, IViewContextMenuContext, IViewContext } from 'tessa/ui/views';
import { ParticipantTypes, ForumHelper, ForumProvider, ForumResponse } from 'tessa/forums';
import { MenuAction, showError, showNotEmpty, showConfirm } from 'tessa/ui';
import { LocalizationManager } from 'tessa/localization';
import { userSession } from 'common/utility';
import { CriteriaOperatorConst } from 'tessa/views/metadata';
import {
  ForumDialogManager,
  ForumPermissionsProvider,
  IForumPermissionsProvider
} from 'tessa/ui/cards/controls';
import { ValidationResultType, ValidationResult } from 'tessa/platform/validation';
import { IStorage } from 'tessa/platform/storage';
import { ForumOperationItem } from 'tessa/ui/cards';

export class OpenForumContextMenuViewExtension extends WorkplaceViewComponentExtension {
  //#region ctor

  constructor() {
    super();
    this.permissionProvider = new ForumPermissionsProvider();
  }

  ////#endregion

  //#region fields

  private permissionProvider: IForumPermissionsProvider;

  //#endregion

  //#region CardUIExtension

  public getExtensionName(): string {
    return 'Tessa.Extensions.Default.Client.Forum.OpenForumContextMenuViewExtension';
  }

  public initialize(model: IWorkplaceViewComponent) {
    model.contextMenuGenerators.push(this.getParticipantsMenuAction());
  }

  public initialized(_model: IWorkplaceViewComponent) {}

  //#endregion

  //#region private methods

  private getParticipantsMenuAction(): (ctx: IViewContextMenuContext) => void {
    return c => {
      if (c.viewContext.refSection == null) {
        return;
      }

      const { viewContext } = c;
      const { selectedRows } = viewContext;

      if (!selectedRows || selectedRows.length === 0) {
        return;
      }

      const items = this.tryGetRows(selectedRows);

      if (items && items.length !== 0) {
        c.menuActions.push(
          new MenuAction('ChangeParticipant', '$Forum_MenuAction_ChangeParticipants', null, _e => {
            c.uiContextExecutor(_ctx => {
              this.changeParticipantsCommand(viewContext, items);
            });
          }),
          new MenuAction('ChangeParticipant', '$Forum_MenuAction_RemoveParticipants', null, _e => {
            c.uiContextExecutor(_ctx => {
              this.removeParticipantsCommand(viewContext, items);
            });
          })
        );
      }
    };
  }

  private async changeParticipantsCommand(context: IViewContext, items: ForumOperationItem[]) {
    if (items.length === 0) {
      return;
    }

    if (!this.hasModeratorPermissions(context)) {
      await showError(
        LocalizationManager.instance.format(
          '$Forum_UI_Cards_RemoveParticipants_NoRequiredPermissions',
          ForumHelper.concatUsersName(
            items.map(i => i.roleName),
            ','
          )
        )
      );

      return;
    }

    const currentUserId = userSession.UserID;
    if (items.find(i => i.cardId === currentUserId)) {
      await showError(
        LocalizationManager.instance.localize(
          '$Forum_UI_Cards_ChangeParticipants_СannotEditYourself'
        )
      );
      return;
    }

    if (items.length > 1) {
      await showError(
        LocalizationManager.instance.localize(
          '$Forum_UI_Cards_ChangeParticipants_YouCanEditOnlyOneEntry'
        )
      );
      return;
    }

    const item = items[0];

    item.cardTypeId = this.getContextParameterValue(context, 'CardTypeID');
    item.participantsTabName = this.getContextParameterValue(context, 'ParticipantsTabName');
    item.rolesTabName = this.getContextParameterValue(context, 'RolesTabName');

    let newItem: ForumOperationItem | null;
    if (item.participantType === ParticipantTypes.ParticipantFromRole) {
      newItem = await this.processingChangeRole(
        item,
        this.getCardId(context),
        this.hasSuperModeratorPermissions(context)
      );
    } else {
      newItem = await ForumDialogManager.instance.changeParticipantsShowDialog(item);
    }

    if (newItem === null) {
      return;
    }

    const participants = [newItem.roleId];

    const forumResponse =
      newItem.participantType === ParticipantTypes.ParticipantFromRole
        ? await ForumProvider.instance.updateRoles(
            newItem.topicId,
            participants,
            newItem.readOnly,
            undefined,
            newItem.subscribed
          )
        : await ForumProvider.instance.updateParticipants(
            newItem.topicId,
            participants,
            newItem.readOnly,
            newItem.participantType,
            undefined,
            newItem.subscribed
          );

    if (forumResponse.validationResult.isSuccessful) {
      if (context.canRefreshView()) {
        await context.refreshView();
      }
    } else {
      await showNotEmpty(forumResponse.validationResult.build());
    }
  }

  private async removeParticipantsCommand(context: IViewContext, items: ForumOperationItem[]) {
    if (items.length === 0) {
      return;
    }

    if (!this.hasModeratorPermissions(context)) {
      await showError(
        LocalizationManager.instance.format(
          '$Forum_UI_Cards_RemoveParticipants_NoRequiredPermissions',
          ForumHelper.concatUsersName(
            items.map(i => i.roleName),
            ','
          )
        )
      );

      return;
    }

    const currentUserId = userSession.UserID;
    if (!items.find(i => i.cardId === currentUserId)) {
      try {
        const participants = items.map(i => i.roleId);

        const forumResponse = await this.removeParticipants(
          items,
          currentUserId,
          participants,
          this.getCardId(context),
          this.hasSuperModeratorPermissions(context)
        );

        if (forumResponse.validationResult.isSuccessful) {
          if (context.canRefreshView()) {
            await context.refreshView();
          }
        } else {
          await showNotEmpty(forumResponse.validationResult.build());
        }
      } catch (error) {
        await showError(error);
      }
    } else {
      await showError('$Forum_UI_Cards_RemoveParticipants_CantRemoveCurrentParticipant');
    }
  }

  private async removeParticipants(
    items: ForumOperationItem[],
    currentUserId: string,
    participants: string[],
    cardId: string,
    isSuperModerator: boolean
  ) {
    let response = new ForumResponse();

    const groupedItems = items.filter((item, i, self) => {
      self.indexOf(item) === i;
    });

    if (groupedItems.length > 1) {
      response.validationResult.add(
        ValidationResult.fromText(
          LocalizationManager.instance.localize(
            '$Forum_UI_Cards_RemoveParticipants_CannotDeleteParticipantsAndRoles'
          ),
          ValidationResultType.Error
        )
      );

      return response;
    }

    const item = items[0];

    if (item.participantType === ParticipantTypes.ParticipantFromRole) {
      response = await this.processingRemoveRole(
        items,
        currentUserId,
        participants,
        cardId,
        isSuperModerator
      );
    } else {
      const result = await showConfirm(
        LocalizationManager.instance.format(
          '$Forum_UI_Cards_RemoveParticipant_ConfirmSingle',
          ForumHelper.concatUsersName(
            items.map(i => i.roleName),
            ','
          )
        )
      );
      if (result) {
        response = await ForumProvider.instance.removeParticipants(
          item.topicId,
          currentUserId,
          participants
        );
      }
    }

    return response;
  }

  private async processingRemoveRole(
    items: ForumOperationItem[],
    currentUserId: string,
    participants: string[],
    cardId: string,
    isSuperModerator: boolean
  ): Promise<ForumResponse> {
    if (isSuperModerator) {
      const confirmed = await showConfirm(
        LocalizationManager.instance.format(
          '$Forum_UI_Cards_RemoveParticipant_ConfirmSingle',
          ForumHelper.concatUsersName(
            items.map(i => i.roleName),
            ','
          )
        )
      );

      if (confirmed) {
        return await ForumProvider.instance.removeRoles(
          items[0].topicId,
          currentUserId,
          participants
        );
      }

      return new ForumResponse();
    }

    let response = new ForumResponse();

    const result = await showConfirm(
      '$Forum_UI_Cards_ChangeParticipants_TryGetSupermoderatorPermission'
    );

    if (result) {
      const {
        data: isEnableSuperModeratorMode,
        validationResult
      } = await this.permissionProvider.checkHasPermissionIsSuperModerator(cardId);

      if (!validationResult.isSuccessful) {
        response.validationResult.add(validationResult);
        return response;
      }

      if (isEnableSuperModeratorMode) {
        const result = await showConfirm(
          LocalizationManager.instance.format(
            '$Forum_UI_Cards_RemoveParticipant_ConfirmSingle',
            ForumHelper.concatUsersName(
              items.map(i => i.roleName),
              ','
            )
          )
        );

        if (result) {
          response = await ForumProvider.instance.removeRoles(
            items[0].topicId,
            currentUserId,
            participants
          );
        }
      }
    }

    return response;
  }

  private async processingChangeRole(
    item: ForumOperationItem,
    cardId: string,
    isSuperModerator: boolean = false
  ): Promise<ForumOperationItem | null> {
    if (isSuperModerator) {
      return await ForumDialogManager.instance.changeRoleParticipantsShowDialog(item);
    }

    const result = await showConfirm(
      '$Forum_UI_Cards_ChangeParticipants_TryGetSupermoderatorPermission'
    );

    if (result) {
      const {
        data: isEnableSuperModeratorMode,
        validationResult
      } = await this.permissionProvider.checkHasPermissionIsSuperModerator(cardId);

      if (!validationResult.isSuccessful) {
        await showNotEmpty(validationResult);
        return null;
      }

      if (isEnableSuperModeratorMode) {
        return await ForumDialogManager.instance.changeRoleParticipantsShowDialog(item);
      }
    }

    return null;
  }

  private hasModeratorPermissions(context: IViewContext): boolean {
    const participantType = this.getContextParameterValue(
      context,
      'ParticipantTypeID'
    ) as ParticipantTypes;

    return (
      participantType !== ParticipantTypes.Participant &&
      participantType !== ParticipantTypes.ParticipantFromRole
    );
  }

  private hasSuperModeratorPermissions(context: IViewContext): boolean {
    return (
      context.parameters.parameters.find(p => p.name === 'IsSuperModeratorMode')?.criteriaValues[0]
        .criteriaName === CriteriaOperatorConst.IsTrue
    );
  }

  private getCardId(context: IViewContext): string {
    return this.getContextParameterValue(context, 'CardID');
  }

  private tryGetRows(rows: readonly IStorage[]): ForumOperationItem[] {
    const result: ForumOperationItem[] = [];
    for (const row of rows) {
      result.push(this.tryGetSelectedItemsFromViewContext(row));
    }

    return result;
  }

  private tryGetSelectedItemsFromViewContext(selectedRows: IStorage): ForumOperationItem {
    const forumItem = new ForumOperationItem(
      selectedRows.get('RoleID') as string,
      selectedRows.get('RoleName') as string
    );

    forumItem.roleType = parseInt(selectedRows.get('TypeID') as string);
    forumItem.topicId = selectedRows.get('TopicID') as string;
    forumItem.readOnly = selectedRows.get('ReadOnly') as boolean;
    forumItem.participantType = selectedRows.get('TypeParticipant') as ParticipantTypes;
    forumItem.subscribed = selectedRows.get('Subscribed');

    return forumItem;
  }

  // tslint:disable-next-line: no-any
  private getContextParameterValue(context: IViewContext, name: string): any {
    return context.parameters.parameters.find(p => p.name === name)?.criteriaValues[0].values[0]
      .value;
  }

  //#endregion
}
