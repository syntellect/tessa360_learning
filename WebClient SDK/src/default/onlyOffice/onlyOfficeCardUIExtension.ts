import { CardUIExtension, ICardUIExtensionContext } from 'tessa/ui/cards';
import { OnlyOfficeApiSingleton } from './onlyOfficeApiSingleton';
import { OnlyOfficeOpenFileInfo } from './onlyOfficeOpenFileInfo';
import { LocalizationManager } from 'tessa/localization';
import { ValidationResult, ValidationResultType } from 'tessa/platform/validation';
import { IFile } from 'tessa/files';
import { showNotEmpty } from 'tessa/ui';

/**
 * Представляет собой расширение, которое предупреждает пользователя об открытых файлах на редактирование при операциях с карточкой.
 */
export class OnlyOfficeCardUIExtension extends CardUIExtension {
  public reopening(context: ICardUIExtensionContext) {
    const res = OnlyOfficeCardUIExtension.checkOpenFiles(
      context.fileContainer.files,
      context.card.id
    );

    if (res.hasErrors) {
      context.validationResult.add(res);
    }
  }

  public async finalizing(context: ICardUIExtensionContext) {
    const res = OnlyOfficeCardUIExtension.checkOpenFiles(
      context.fileContainer.files,
      context.card.id
    );

    if (res.hasErrors) {
      context.cancel = true;
      await showNotEmpty(res);
    }
  }

  private static checkOpenFiles(files: ReadonlyArray<IFile>, cardId: guid): ValidationResult {
    const openFilesForEdit: OnlyOfficeOpenFileInfo[] = [];

    for (let file of files) {
      for (let version of file.versions) {
        const openedFileInfo = OnlyOfficeApiSingleton.instance.openFiles.find(
          of => of.forEdit && of.version.id === version.id && of.cardId === cardId
        );

        if (openedFileInfo) {
          openFilesForEdit.push(openedFileInfo);
        }
      }
    }

    if (openFilesForEdit.length === 0) return ValidationResult.empty;

    const openFileNames = openFilesForEdit.map(of => of.version.file.name).join('\n');
    return ValidationResult.fromText(
      LocalizationManager.instance.format(
        '$OnlyOffice_Error_CloseEditableFilesBeforeContinue',
        openFileNames
      ),
      ValidationResultType.Error
    );
  }

  public shouldExecute(_context: ICardUIExtensionContext): boolean {
    return OnlyOfficeApiSingleton.isAvailable;
  }
}
