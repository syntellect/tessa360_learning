import { ICustomEditorViewModel } from 'tessa/ui/customFileEditor';
import { IFileVersion } from 'tessa/files';
import { OnlyOfficeApi } from './onlyOfficeApi';

/**
 * Представляет собой модель редактора, используемого для предпросмотра файлов.
 */
export class OnlyOfficeEditorPreviewViewModel implements ICustomEditorViewModel {
  public readonly placeholder: string;
  public readonly version: IFileVersion;

  private readonly _api: OnlyOfficeApi;
  private _closeEditorCallback: (() => void) | null = null;
  private _editor: object | null = null;

  public constructor(api: OnlyOfficeApi, placeholder: string, version: IFileVersion) {
    this._api = api;
    this.placeholder = placeholder;
    this.version = version;
  }

  public load = async (): Promise<void> => {
    OnlyOfficeApi.throwIfApiScriptIsNotLoaded(window);
    await this._api.openFile(this.version, this.openEditorFunc, this.destroy, false, null, false);
  };

  public destroy = async (): Promise<void> => {
    if (this._editor) {
      if ('destroyEditor' in this._editor) this._editor['destroyEditor']();
      this._editor = null;
    }

    if (this._closeEditorCallback) {
      this._closeEditorCallback();
      this._closeEditorCallback = null;
    }
  };

  private openEditorFunc = async (id: guid): Promise<void> => {
    const config = this._api.createDefaultDocEditorConfig(id, this.version, 'preview');
    this._editor = this._api.createDocEditorFrame(this.placeholder, config);

    const closePromise = new Promise(resolve => {
      this._closeEditorCallback = resolve;
    });
    await closePromise;
  };
}
