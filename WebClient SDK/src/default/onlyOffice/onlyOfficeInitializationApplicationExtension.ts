import { ApplicationExtension, IApplicationExtensionMetadataContext } from 'tessa';
import { OnlyOfficeSettings } from './onlyOfficeSettings';
import { CardSingletonCache } from 'tessa/cards';
import { OnlyOfficeApi } from './onlyOfficeApi';
import { OnlyOfficeApiSingleton } from './onlyOfficeApiSingleton';
import { OnlyOfficePreviewFactory } from './onlyOfficePreviewFactory';
import { CustomPreviewViewModelFactoryProvider } from 'tessa/ui/customFileEditor';

/**
 * Представляет собой расширение, которое инициализирует работу с OnlyOffice.
 */
export class OnlyOfficeInitializationApplicationExtension extends ApplicationExtension {
  public afterMetadataReceived(context: IApplicationExtensionMetadataContext) {
    if (!context.mainPartResponse) {
      return;
    }

    // инициализировать нужно именно в afterMetadataReceived, так как тут подгрузится карточка настроек
    try {
      const settings = OnlyOfficeInitializationApplicationExtension.getSettingsOrThrow();
      const api = new OnlyOfficeApi(settings);

      if (settings.previewEnabled) {
        // загружаем скрипт редактора в текущий документ для работы предпросмотра.
        api.ensureApiScriptAdded(document);
        // устанавливаем средство предпросмотра
        CustomPreviewViewModelFactoryProvider.instance.factories.push(
          new OnlyOfficePreviewFactory(api)
        );
      }

      OnlyOfficeApiSingleton.init(api);

      window.addEventListener('beforeunload', () => {
        api.openFiles.forEach(f => f.forceCloseCallback());
      });
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Получает необходимые данные из карточки настроек OnlyOffice или выбрасывает ошибку в случае неудачи.
   */
  private static getSettingsOrThrow(): OnlyOfficeSettings {
    const settingsCard = CardSingletonCache.instance.cards.get('OnlyOfficeSettings');
    if (!settingsCard) throw new Error('OnlyOfficeSettings card not found.');

    const section = settingsCard.sections.get('OnlyOfficeSettings')!;
    const apiScriptUrl = section.fields.get('ApiScriptUrl');
    if (!apiScriptUrl) throw new Error('ApiScriptUrl in OnlyOfficeSettings card not specified.');

    const previewEnabled = section.fields.get('PreviewEnabled');

    const excludedPreviewFormatsJoined = section.fields.get('ExcludedPreviewFormats') as
      | string
      | null;
    const excludedPreviewFormats = excludedPreviewFormatsJoined
      ? excludedPreviewFormatsJoined.split(' ')
      : [];

    return {
      apiScriptUrl,
      previewEnabled,
      excludedPreviewFormats: excludedPreviewFormats
    };
  }
}
