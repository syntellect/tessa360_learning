import { ICustomEditorViewModel, CustomPreviewViewModelFactory } from 'tessa/ui/customFileEditor';
import { IFileVersion } from 'tessa/files';
import { OnlyOfficeApi } from './onlyOfficeApi';
import { OnlyOfficeEditorPreviewViewModel } from './onlyOfficeEditorPreviewViewModel';

/**
 * Представляет собой фабрику, необходимую для создания моделей редактора OnlyOffice, используемых в предпросмотре.
 */
export class OnlyOfficePreviewFactory implements CustomPreviewViewModelFactory {
  private readonly _api: OnlyOfficeApi;

  public constructor(api: OnlyOfficeApi) {
    this._api = api;
  }

  public canUsePreview(version: IFileVersion): boolean {
    const fileExt = version.getExtension();

    return (
      OnlyOfficeApi.isSupportedFormat(fileExt) &&
      !this._api.settings.excludedPreviewFormats.includes(fileExt)
    );
  }

  public async createViewModel(
    placeholder: string,
    version: IFileVersion
  ): Promise<ICustomEditorViewModel> {
    return new OnlyOfficeEditorPreviewViewModel(this._api, placeholder, version);
  }
}
