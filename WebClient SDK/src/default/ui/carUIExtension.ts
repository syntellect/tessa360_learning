import {
  AdvancedCardDialogManager,
  CardToolbarAction,
  CardUIExtension,
  ICardUIExtensionContext,
  VirtualSchemePresenter
} from 'tessa/ui/cards';
import {
  AutoCompleteEntryViewModel,
  AutoCompleteTableViewModel,
  ButtonViewModel,
  FileListViewModel,
  FilePreviewViewModel,
  GridRowAction,
  GridViewModel,
  RowAutoCompleteItem,
  TextBoxViewModel
} from 'tessa/ui/cards/controls';
import {
  LoadingOverlay,
  MenuAction,
  showError,
  showLoadingOverlay,
  showMessage,
  showNotEmpty,
  tryGetFromInfo,
  UIButton,
  UIContext
} from 'tessa/ui';
import { createTypedField, deepClone, DotNetType, Guid, unseal } from 'tessa/platform';
import { showDialog, showFormDialog } from 'tessa/ui/uiHost';
import { MetadataStorage } from 'tessa';
import { CardTypeSection } from 'tessa/cards';
import { SchemeTableContentType } from 'tessa/scheme';
import { getTessaIcon } from 'common/utility';
import { CardRequest, CardResponse, CardService } from 'tessa/cards/service';
import { ValidationResultType } from 'tessa/platform/validation';
import { CardTableViewControlViewModel } from './tableViewExtension/cardTableViewControlViewModel';
import { CardTableViewRowData } from './tableViewExtension/cardTableViewRowData';

export class CarUIExtension extends CardUIExtension {
  private _disposes: Array<Function | null> = [];

  public initialized(context: ICardUIExtensionContext) {
    if (context.card.typeId !== 'd0006e40-a342-4797-8d77-6501c4b7c4ac') {
      // CardTypeID
      return;
    }

    const cardModel = context.model;
    const imageFileControl = tryGetFromInfo<FileListViewModel | null>(
      cardModel.info,
      'ImageFilesControl',
      null
    );

    // показываем файлы только с категорией Image
    if (imageFileControl) {
      for (let i = imageFileControl.files.length - 1; i > -1; i--) {
        const file = imageFileControl.files[i];
        if (!(file.model.category && file.model.category.caption === 'Image')) {
          imageFileControl.removeFile(file);
        }
      }

      this._disposes.push(
        imageFileControl.containerFileAdding.addWithDispose(e => {
          if (!(e.file.category && e.file.category.caption === 'Image')) {
            e.cancel = true;
          }
        })
      );
    }

    // показываем файлы без категории Image
    const allFilesControl = tryGetFromInfo<FileListViewModel | null>(
      cardModel.info,
      'AllFilesControl',
      null
    );
    if (allFilesControl) {
      for (let i = allFilesControl.files.length - 1; i > -1; i--) {
        const file = allFilesControl.files[i];
        if (file.model.category && file.model.category.caption === 'Image') {
          allFilesControl.removeFile(file);
        }
      }

      this._disposes.push(
        allFilesControl.containerFileAdding.addWithDispose(e => {
          if (e.file.category && e.file.category.caption === 'Image') {
            e.cancel = true;
          }
        })
      );
    }

    // запрещаем добавлять файлы с расширением .exe
    this._disposes.push(
      context.fileContainer.containerFileChanging.addWithDispose(async e => {
        const file = e.added;
        if (file && file.getExtension() === '.exe') {
          await showMessage("Can't add file: " + file.name);
          e.cancel = true;
        }
      })
    );

    const driverControl = context.model.controls.get('DriverName2') as AutoCompleteEntryViewModel;
    if (driverControl) {
      driverControl.openCardCommand.func = async () => {
        if (
          !driverControl.item ||
          // tslint:disable-next-line:triple-equals
          driverControl.item.reference == undefined
        ) {
          return;
        }

        const info = {};
        info['.autocomplete'] = driverControl.getRefInfo();

        await cardModel.executeInContext(async context => {
          await showLoadingOverlay(async splashResolve => {
            await AdvancedCardDialogManager.instance.openCard({
              cardId: driverControl.item!.reference,
              context,
              info,
              splashResolve
            });
          });
        });
      };
    }

    const ownersControl = context.model.controls.get('Owners') as AutoCompleteTableViewModel;
    if (ownersControl) {
      this._disposes.push(
        ownersControl.valueSet.addWithDispose(e => {
          const items = e.autocomplete.items as RowAutoCompleteItem[];
          const minOrder = Math.min(...items.map(x => x.order));
          const firstItem = items.find(x => x.order === minOrder);
          if (!firstItem) {
            return;
          }
          const displayText: string = firstItem.row.get('UserName');
          if (!displayText.endsWith(' (*)')) {
            firstItem.row.rawSet(
              'UserName',
              createTypedField(`${displayText} (*)`, DotNetType.String)
            );
          }
        })
      );
    }

    const carNameControl = context.model.controls.get('CarName');
    if (carNameControl) {
      carNameControl.hasActiveValidation = true;
      carNameControl.validationFunc = c => {
        if ((c as TextBoxViewModel).text === '42') {
          return "Can't enter magic number here";
        }

        return null;
      };
    }

    const preview1 = context.model.controls.get('Preview1') as FilePreviewViewModel;
    const preview2 = context.model.controls.get('Preview2') as FilePreviewViewModel;
    if (preview1 && preview2) {
      this._disposes.push(
        preview1.fileControlManager.pageChanged.addWithDispose((args: { page: number }) => {
          preview2.fileControlManager.setPageNumber(args.page);
        })!
      );
    }

    // Добавляем тестовую кнопку в тулбар.
    if (context.toolbar && context.toolbar.items.length === 0) {
      context.toolbar.addItem(
        new CardToolbarAction({
          name: 'TestButton',
          caption: 'Test Button',
          icon: getTessaIcon('Thin285'),
          command: () => {
            showMessage('Test button was pressed.');
          }
        })
      );
    }

    const get1CButton = context.model.controls.get('Get1CButton') as ButtonViewModel;
    if (get1CButton) {
      get1CButton.onClick = this.get1CfileAsync;
      get1CButton.isReadOnly = !context.model.fileContainer.permissions.canAdd;
    }

    // Использование апи выставления поворотов
    // this._disposes.push(
    //   preview1.fileControlManager.rotateChanged.addWithDispose(
    //     (args: { fileVersionId: string; page: number; angle: number }) => {
    //       const version = preview2.fileControlManager.fileVersion;
    //       if (!version) {
    //         return;
    //       }
    //       preview2.fileControlManager.setRotateAngle(
    //         version.id,
    //         preview2.fileControlManager.pageNumber,
    //         args.angle
    //       );
    //     }
    //   )!
    // );

    // Использование апи масштабирования
    // this._disposes.push(
    //   preview1.fileControlManager.scaleChanged.addWithDispose(
    //     (args: { fileVersionId: string; scale: ScaleOption; customScaleValue: number }) => {
    //       preview2.fileControlManager.setScale(args.scale, args.customScaleValue);
    //     }
    //   )!
    // );

    const showDialogTypeFormButton = cardModel.controls.get(
      'ShowDialogTypeForm'
    ) as ButtonViewModel;
    if (showDialogTypeFormButton) {
      showDialogTypeFormButton.onClick = async () => {
        const virtualSchemeDialog = MetadataStorage.instance.cardMetadata.getCardTypeById(
          'b2b4d2c2-8f92-4262-9951-fe1a64bf9b30'
        ); // VirtualSchemeCardTypeID
        const virtualSchemeDialogClone = deepClone(virtualSchemeDialog)!;

        const generalSection = new CardTypeSection();
        generalSection.id = Guid.newGuid();
        generalSection.name = 'Instances';
        generalSection.description = 'Fake Instances';
        generalSection.tableType = SchemeTableContentType.Entries;
        const presenter = new VirtualSchemePresenter(
          unseal<CardTypeSection[]>(virtualSchemeDialogClone.cardTypeSections),
          [generalSection]
        );
        const form = await presenter.createForm();
        if (!form) {
          showError('Dialog type "VirtualScheme" not found.');
          return;
        }

        await showDialog(
          form,
          null,
          [
            UIButton.create({
              caption: '$UI_Common_Close',
              buttonAction: btn => btn.close()
            })
          ],
          {
            hideTopCloseIcon: true
          }
        );
      };
    }

    // в контроле-таблице "Список акций"
    const grid = context.model.controls.get('ShareList') as GridViewModel;
    if (grid) {
      // добавляем валидацию при сохранении редактируемой строки
      this._disposes.push(
        grid.rowValidating.addWithDispose(e => {
          if (!e.row.get('Name') || e.row.get('Name') === '') {
            e.validationResult.add(
              ValidationResultType.Error,
              "Share's name is empty (from RowValidating)."
            );
          }
        }),
        // нажатие Ctrl+Enter показывает окно для выбранной строки в фокусе
        grid.keyDown.addWithDispose(e => {
          const code = e.event.keyCode || e.event.charCode;
          if (code === 13 && e.event.ctrlKey) {
            e.event.stopPropagation();
            e.event.preventDefault();

            const gridViewModel = e.control as GridViewModel;

            if (gridViewModel.selectedRow) {
              showMessage(`Share name is ${gridViewModel.selectedRow.row.get('Name')}`);
            }
          }
        }),
        // при открытии окна добавления/редактирования строки добавляем горячую клавишу
        grid.keyDown.addWithDispose(e => {
          const code = e.event.keyCode || e.event.charCode;
          if (code === 116) {
            // f5
            e.event.stopPropagation();
            e.event.preventDefault();
            showMessage('F5 key is pressed');
          }
        }),
        // при клике по ячейке из первой колонки "Акция" в открытом окне будет поставлен фокус
        // на первый контрол типа "Строка", в котором также будет выделен весь текст
        grid.rowInitialized.addWithDispose(e => {
          if (e.action === GridRowAction.Opening && e.columnIndex === 0) {
            const textBox = e.rowModel?.controlsBag.find(
              x => x instanceof TextBoxViewModel
            ) as TextBoxViewModel;
            if (textBox) {
              textBox.focus();
              textBox.selectAll();
            }
          }
        })
      );

      // контекстное меню таблицы, зависит от кликнутой ячейки
      grid.contextMenuGenerators.push(ctx => {
        const text = `Name=${ctx.row.row.get('Name')}, Count=${
          ctx.control.selectedRows.length
        }, Cell="${ctx.cell?.value}"`;

        ctx.menuActions.push(
          MenuAction.create({
            name: 'Name',
            caption: text,
            action: () => {
              showMessage(`Share name is ${ctx.row.row.get('Name')}`);
            }
          }),
          MenuAction.create({
            name: 'EditRow',
            caption: 'Edit row',
            icon: getTessaIcon('Thin2'),
            action: async () => {
              await grid.editRow(ctx.row, ctx.columnIndex);
            }
          })
        );
      });
    }

    const shareListView = context.model.controls.get(
      'ShareListView'
    ) as CardTableViewControlViewModel;
    if (shareListView) {
      // добавляем валидацию при сохранении редактируемой строки
      this._disposes.push(
        shareListView.rowValidating.addWithDispose(e => {
          if (!e.row.get('Name')) {
            e.validationResult.add(
              ValidationResultType.Error,
              "Share's name is empty (from RowValidating)."
            );
          }
        }),
        // при клике по ячейке из первой колонки "Акция" в открытом окне будет поставлен фокус
        // на первый контрол типа "Строка", в котором также будет выделен весь текст
        shareListView.rowInitialized.addWithDispose(e => {
          // e.Cell всегда null, если ViewSelectionMode.Row
          if (e.action === GridRowAction.Opening && e.cell?.column.columnName === '0') {
            const textBox = e.rowModel?.controlsBag.find(
              x => x instanceof TextBoxViewModel
            ) as TextBoxViewModel;
            if (textBox) {
              textBox.focus();
              textBox.selectAll();
            }
          }
        })
      );

      // контекстное меню таблицы, зависит от кликнутой ячейки
      if (shareListView.table) {
        this._disposes.push(
          // при открытии окна добавления/редактирования строки добавляем горячую клавишу
          shareListView.table.keyDown.addWithDispose(e => {
            const code = e.event.keyCode || e.event.charCode;
            if (code === 116) {
              // f5
              e.event.stopPropagation();
              e.event.preventDefault();
              showMessage('F5 key is pressed');
            }
          }),
          // нажатие Ctrl+Enter показывает окно для выбранной строки в фокусе
          shareListView.table.keyDown.addWithDispose(e => {
            const code = e.event.keyCode || e.event.charCode;
            if (code === 13 && e.event.ctrlKey) {
              const selectedRow = e.control.viewComponent.selectedRow;

              e.event.stopPropagation();
              e.event.preventDefault();
              showMessage(`Share name is ${selectedRow?.get('Name')}`);
            }
          })
        );

        shareListView.table?.rowContextMenuGenerators?.push(ctx => {
          const data = ctx.row.data as CardTableViewRowData;
          // e.Cell всегда null, если ViewSelectionMode.Row
          const text = `Name=${data.get('0')}, Count=${
            ctx.tableGrid.viewComponent.selectedRows?.length
          }, Cell="${ctx.cell?.value ?? ctx.cell?.convertedValue}"`;

          ctx.menuActions.push(
            MenuAction.create({
              name: 'Name',
              caption: text,
              action: () => {
                showMessage(`Share name is ${data.get('0')}`);
              }
            }),
            MenuAction.create({
              name: 'EditRow',
              caption: 'Edit row',
              icon: getTessaIcon('Thin2'),
              action: async () => {
                await shareListView.editRow(ctx.row);
              }
            })
          );
        });
      }
    }
  }

  public finalized() {
    for (let func of this._disposes) {
      if (func) {
        func();
      }
    }
    this._disposes.length = 0;
  }

  public saving(context: ICardUIExtensionContext) {
    // Добавим ошибку валидации, если найден файл с определённым именем.
    if (context.fileContainer.files.find(x => x.name === 'show error.txt')) {
      context.validationResult.add(
        ValidationResultType.Error,
        'File "show error.txt" was added, can\'t save the card.'
      );

      return;
    }

    // Удаляем файл с определённым именем.
    const fileToRemove = context.fileContainer.files.find(x => x.name === 'remove me.txt');
    if (fileToRemove) {
      fileToRemove.changeName('file was removed.txt');
    }
  }

  private async get1CfileAsync() {
    const cardEditor = UIContext.current?.cardEditor;
    const cardModel = cardEditor?.cardModel;
    if (!cardEditor || !cardModel) {
      return;
    }

    const dialogForm = cardModel.cardType.forms.find(x => x.name === 'Dialog_1C');
    if (!dialogForm) {
      return;
    }

    let ok = false;
    await showFormDialog(dialogForm, cardModel, null, [
      new UIButton('$UI_Common_OK', async btn => {
        ok = true;
        btn.close();
      }),
      new UIButton('$UI_Common_Cancel', btn => {
        btn.close();
      })
    ]);

    if (!ok) {
      return;
    }

    const request = new CardRequest();
    const requestInfo = request.info;
    const testCardSection = cardModel.card.sections.get('TEST_CarMainInfo');

    request.requestType = '86333B21-A1C5-4698-B023-B427C8BCCF94';
    requestInfo['Name'] = createTypedField(testCardSection?.fields.get('Name'), DotNetType.String);
    requestInfo['Driver'] = createTypedField(
      testCardSection?.fields.get('DriverName'),
      DotNetType.String
    );

    // показываем сплэш и задаём блокирующую операцию для карточки:
    // пользователь не сможет закрыть вкладку или отрефрешить карточку, пока она не закончится
    let response: CardResponse = new CardResponse();
    await LoadingOverlay.instance.show(async () => {
      await cardEditor.setOperationInProgress(async () => {
        setTimeout(() => '42', 2000);
        response = await CardService.instance.request(request);
      });
    });

    const result = response.validationResult.build();

    await showNotEmpty(result);

    if (!result.isSuccessful) {
      return;
    }

    const content = tryGetFromInfo<string>(response.info, 'Xml');
    const fileName = '1C.xml';
    const newFile = new File([content], fileName);

    const file = cardModel.fileContainer.files.find(f => f.name === fileName);
    if (file) {
      await cardModel.previewManager.resetIfInPreview(file);
      file.replace(new File([content], fileName), true);
    } else {
      await cardModel.fileContainer.addFile(
        cardModel.fileContainer.createFile(newFile),
        false,
        true
      );
    }
  }
}
