export class FileControlCreationParams {
  categoriesViewAlias: string = 'FileCategoriesFiltered';

  previewControlName: string = '';

  isCategoriesEnabled: boolean = false;

  isManualCategoriesCreationDisabled: boolean = false;

  isNullCategoryCreationDisabled: boolean = false;

  isIgnoreExistingCategories: boolean = false;
}
