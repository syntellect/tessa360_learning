import { FilesViewGeneratorBaseUIExtension } from './filesViewGeneratorBaseUIExtension';
import { FileControlCreationParams } from './fileControlCreationParams';
import { FilesViewCardControlInitializationStrategy } from './filesViewCardControlInitializationStrategy';
import { FilesViewControlContentItemsFactory } from './filesViewControlContentItemsFactory';
import { ICardUIExtensionContext } from 'tessa/ui/cards';
import { executeExtensions, CardTypeExtensionTypes, TypeExtensionContext } from 'tessa/cards';
import { tryGetFromSettings } from 'tessa/ui';
import { DefaultFormTabWithTasksViewModel } from 'tessa/ui/cards/forms';

export class InitializeFilesViewUIExtension extends FilesViewGeneratorBaseUIExtension {
  public async initializing(context: ICardUIExtensionContext) {
    const result = await executeExtensions(
      CardTypeExtensionTypes.initializeFilesView,
      context.card,
      context.model.generalMetadata,
      this.executeInitializingAction,
      context
    );

    context.validationResult.add(result);
  }

  public async initialized(context: ICardUIExtensionContext) {
    const result = await executeExtensions(
      CardTypeExtensionTypes.initializeFilesView,
      context.card,
      context.model.generalMetadata,
      this.executeInitializedAction,
      context
    );

    context.validationResult.add(result);
  }

  executeInitializingAction = async (context: TypeExtensionContext) => {
    const settings = context.settings;
    const filesViewAlias = tryGetFromSettings<string>(settings, 'FilesViewAlias', '');

    const options = new FileControlCreationParams();
    options.categoriesViewAlias = tryGetFromSettings<string>(settings, 'CategoriesViewAlias', '');
    options.previewControlName = tryGetFromSettings<string>(settings, 'PreviewControlName', '');
    options.isCategoriesEnabled = tryGetFromSettings<boolean>(
      settings,
      'IsCategoriesEnabled',
      false
    );
    options.isIgnoreExistingCategories = tryGetFromSettings<boolean>(
      settings,
      'IsIgnoreExistingCategories',
      false
    );
    options.isManualCategoriesCreationDisabled = tryGetFromSettings<boolean>(
      settings,
      'IsManualCategoriesCreationDisabled',
      false
    );
    options.isNullCategoryCreationDisabled = tryGetFromSettings<boolean>(
      settings,
      'IsNullCategoryCreationDisabled',
      false
    );

    const uiContext = context.externalContext as ICardUIExtensionContext;
    const cardTask = context.cardTask;
    if (!cardTask) {
      await this.initializeFileControl(uiContext.model, filesViewAlias, options);
    } else {
      uiContext.model.taskInitializers.push(async taskCardModel => {
        if (taskCardModel.cardTask === cardTask) {
          await this.initializeFileControl(taskCardModel, filesViewAlias, options);
        }
      });
    }
  };

  executeInitializedAction = async (context: TypeExtensionContext) => {
    const uiContext = context.externalContext as ICardUIExtensionContext;
    const settings = context.settings;
    const filesViewAlias = tryGetFromSettings<string>(settings, 'FilesViewAlias', '');
    if (!context.cardTask) {
      const fileControl = await this.attachViewToFileControl(
        uiContext.model,
        filesViewAlias,
        new FilesViewCardControlInitializationStrategy(new FilesViewControlContentItemsFactory())
      );
      if (!fileControl) {
        return;
      }
      const defaultGroup = tryGetFromSettings<string>(settings, 'DefaultGroup', '');
      if (defaultGroup) {
        fileControl.selectedGrouping =
          fileControl.groupings.find(x => x.name === defaultGroup) ?? null;
      }
    } else {
      const model = uiContext.model;
      const tasks = (model.mainForm as DefaultFormTabWithTasksViewModel).tasks;
      if (!tasks) {
        return;
      }
      const task = tasks.find(x => x.taskModel.cardTask === context.cardTask);
      if (task) {
        task.modifyWorkspace(async () => {
          const fileControl = await this.attachViewToFileControl(
            task.taskModel,
            filesViewAlias,
            new FilesViewCardControlInitializationStrategy(
              new FilesViewControlContentItemsFactory()
            )
          );
          if (!fileControl) {
            return;
          }
          const defaultGroup = tryGetFromSettings<string>(settings, 'DefaultGroup', '');
          if (defaultGroup) {
            fileControl.selectedGrouping =
              fileControl.groupings.find(x => x.name === defaultGroup) ?? null;
          }
        });
      }
    }
  };
}
