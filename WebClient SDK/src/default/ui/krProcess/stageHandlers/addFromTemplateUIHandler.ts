import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor,
  addFromTemplateDescriptor } from 'tessa/workflow/krProcess';
import { plainColumnName } from 'tessa/workflow';
import { ValidationResult, ValidationResultType } from 'tessa/platform/validation';

export class AddFromTemplateUIHandler extends KrStageTypeUIHandler {

  private _templateId: string = plainColumnName('KrAddFromTemplateSettingsVirtual', 'FileTemplateID');

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [addFromTemplateDescriptor];
  }

  public validate(context: IKrStageTypeUIHandlerContext) {
    const template = context.row.tryGet(this._templateId);
    // tslint:disable-next-line:triple-equals
    if (template == undefined) {
      context.validationResult.add(ValidationResult.fromText(
        '$KrStages_AddFromTemplate_TemplateIsRequiredWarning', ValidationResultType.Warning));
    }
  }

}