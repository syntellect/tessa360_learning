import {
  KrStageTypeUIHandler,
  IKrStageTypeUIHandlerContext,
  approvalDescriptor,
  StageTypeHandlerDescriptor
} from 'tessa/workflow/krProcess';
import { IFormViewModel, IBlockViewModel, IControlViewModel } from 'tessa/ui/cards';
import { TabControlViewModel } from 'tessa/ui/cards/controls';
import { CardRow, CardFieldChangedEventArgs } from 'tessa/cards';
import { plainColumnName } from 'tessa/workflow';
import { DotNetType, Visibility } from 'tessa/platform';
import { ViewService, TessaViewRequest, RequestParameterBuilder } from 'tessa/views';
import { equalsCriteriaOperator } from 'tessa/views/metadata';

export class ApprovalUIHandler extends KrStageTypeUIHandler {
  private _settings: CardRow;
  private _returnIfNotApprovedFlagControl: IControlViewModel;
  private _returnAfterApprovalFlagControl: IControlViewModel;

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [approvalDescriptor];
  }

  public initialize(context: IKrStageTypeUIHandlerContext) {
    let flagsBlock: IBlockViewModel;
    let flagsTabs: IControlViewModel;
    let tabViewModel: IFormViewModel;
    let innerFlagsBlock: IBlockViewModel;
    if (
      (flagsBlock = context.rowModel!.blocks.get('ApprovalStageFlags')!) &&
      (flagsTabs = flagsBlock.controls.find(x => x.name === 'FlagsTabs')!) &&
      flagsTabs instanceof TabControlViewModel
    ) {
      if (
        (tabViewModel = flagsTabs.tabs.find(x => x.name === 'CommonSettings')!) &&
        (innerFlagsBlock = tabViewModel.blocks.find(x => x.name === 'StageFlags')!)
      ) {
        this._returnIfNotApprovedFlagControl = innerFlagsBlock.controls.find(
          x => x.name === 'ReturnIfNotApproved'
        )!;
      }

      if (
        (tabViewModel = flagsTabs.tabs.find(x => x.name === 'AdditionalSettings')!) &&
        (innerFlagsBlock = tabViewModel.blocks.find(x => x.name === 'StageFlags')!)
      ) {
        this._returnAfterApprovalFlagControl = innerFlagsBlock.controls.find(
          x => x.name === 'ReturnAfterApproval'
        )!;
      }
    }

    this._settings = context.row;
    this._settings.fieldChanged.add(this.onSettingsFieldChanged);

    this.advisoryConfigureFields(
      this._settings.tryGet(plainColumnName('KrApprovalSettingsVirtual', 'Advisory'))
    );
    this.notReturnEditConfigureFields(
      this._settings.tryGet(plainColumnName('KrApprovalSettingsVirtual', 'NotReturnEdit'))
    );
  }

  public finalize() {
    this._settings.fieldChanged.remove(this.onSettingsFieldChanged);
    this._settings = null!;
  }

  private onSettingsFieldChanged = async (e: CardFieldChangedEventArgs) => {
    if (e.fieldName === plainColumnName('KrApprovalSettingsVirtual', 'Advisory')) {
      const advisory = e.fieldValue;
      this.advisoryConfigureFields(advisory);

      if (advisory) {
        if (this._settings.tryGet(plainColumnName('KrTaskKindSettingsVirtual', 'KindID')) == null) {
          const { kindId, kindCaption } = await this.getKind(
            '2e6c5d3e-d408-4f98-8a55-e9d1316bf2cc'
          ); // AdvisoryTaskKindID
          if (kindId) {
            this._settings.set(
              plainColumnName('KrTaskKindSettingsVirtual', 'KindID'),
              kindId,
              DotNetType.Guid
            );
            this._settings.set(
              plainColumnName('KrTaskKindSettingsVirtual', 'KindCaption'),
              kindCaption,
              DotNetType.String
            );
          }
        }
      } else {
        if (
          this._settings.tryGet(plainColumnName('KrTaskKindSettingsVirtual', 'KindID')) ===
          '2e6c5d3e-d408-4f98-8a55-e9d1316bf2cc'
        ) {
          // AdvisoryTaskKindID
          this._settings.set(plainColumnName('KrTaskKindSettingsVirtual', 'KindID'), null);
          this._settings.set(plainColumnName('KrTaskKindSettingsVirtual', 'KindCaption'), null);
        }

        if (this._returnIfNotApprovedFlagControl) {
          this._returnIfNotApprovedFlagControl.isReadOnly = false;
        }
      }

      return;
    }

    if (e.fieldName === plainColumnName('KrApprovalSettingsVirtual', 'NotReturnEdit')) {
      this.notReturnEditConfigureFields(e.fieldValue);
    }
  };

  private async getKind(id: guid): Promise<{ kindId: guid | null; kindCaption: string }> {
    const taskKindsView = ViewService.instance.getByName('TaskKinds')!;

    const request = new TessaViewRequest(taskKindsView.metadata);

    const idParam = new RequestParameterBuilder()
      .withMetadata(taskKindsView.metadata.parameters.get('ID')!)
      .addCriteria(equalsCriteriaOperator(), '', id)
      .asRequestParameter();

    request.values.push(idParam);

    const result = await taskKindsView.getData(request);

    if (!result.rows || result.rows.length === 0) {
      return { kindId: null, kindCaption: '' };
    }

    const row = result.rows[0];
    return { kindId: row[0], kindCaption: row[1] };
  }

  private advisoryConfigureFields(isAdvisory: boolean) {
    if (isAdvisory) {
      if (this._returnIfNotApprovedFlagControl) {
        this._returnIfNotApprovedFlagControl.isReadOnly = true;
        this._settings.set(
          plainColumnName('KrApprovalSettingsVirtual', 'ReturnWhenDisapproved'),
          false,
          DotNetType.Boolean
        );
      }
    }
  }

  private notReturnEditConfigureFields(isNotReturnEdit: boolean) {
    if (isNotReturnEdit) {
      if (this._returnIfNotApprovedFlagControl) {
        this._returnIfNotApprovedFlagControl.controlVisibility = Visibility.Collapsed;
      }
      if (this._returnAfterApprovalFlagControl) {
        this._returnAfterApprovalFlagControl.controlVisibility = Visibility.Collapsed;
      }
    } else {
      if (this._returnIfNotApprovedFlagControl) {
        this._returnIfNotApprovedFlagControl.controlVisibility = Visibility.Visible;
      }
      if (this._returnAfterApprovalFlagControl) {
        this._returnAfterApprovalFlagControl.controlVisibility = Visibility.Visible;
      }
    }
  }
}
