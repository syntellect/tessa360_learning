import { plainColumnName } from 'tessa/workflow';
import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor,
  createCardDescriptor } from 'tessa/workflow/krProcess';
import { ValidationResult, ValidationResultType } from 'tessa/platform/validation';
import { CardRow, CardFieldChangedEventArgs } from 'tessa/cards';

// tslint:disable:triple-equals

export class CreateCardUIHandler extends KrStageTypeUIHandler {

  private _settings: CardRow | null = null;
  private _templateId: string = plainColumnName('KrCreateCardStageSettingsVirtual', 'TemplateID');
  private _templateCaption: string = plainColumnName('KrCreateCardStageSettingsVirtual', 'TemplateCaption');
  private _typeId: string = plainColumnName('KrCreateCardStageSettingsVirtual', 'TypeID');
  private _typeCaption: string = plainColumnName('KrCreateCardStageSettingsVirtual', 'TypeCaption');
  private _modeId: string = plainColumnName('KrCreateCardStageSettingsVirtual', 'ModeID');

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [createCardDescriptor];
  }

  public validate(context: IKrStageTypeUIHandlerContext) {
    const template = context.row.tryGet(this._templateId);
    const type = context.row.tryGet(this._typeId);
    const mode = context.row.tryGet(this._modeId);
    if (template == undefined && type == undefined) {
      context.validationResult!.add(ValidationResult.fromText(
        '$KrStages_CreateCard_TemplateAndModeRequired', ValidationResultType.Warning));
    } else if (template != undefined && type != undefined) {
      context.validationResult!.add(ValidationResult.fromText(
        '$KrStages_CreateCard_TemplateAndTypeSelected', ValidationResultType.Warning));
    }
    if (mode == undefined) {
      context.validationResult!.add(ValidationResult.fromText(
        '$KrStages_CreateCard_ModeRequired', ValidationResultType.Error));
    }
  }

  public initialize(context: IKrStageTypeUIHandlerContext) {
    this._settings = context.row;
    this._settings.fieldChanged.add(this.onSettingsFieldChanged);
  }

  public finalize(_context: IKrStageTypeUIHandlerContext) {
    this._settings!.fieldChanged.remove(this.onSettingsFieldChanged);
    this._settings = null;
  }

  private onSettingsFieldChanged = (e: CardFieldChangedEventArgs) => {
    if (e.fieldName === this._typeId) {
      if (e.fieldValue != undefined) {
        this._settings!.set(this._templateId, null);
        this._settings!.set(this._templateCaption, null);
      }
    } else if (e.fieldName === this._templateId) {
      if (e.fieldValue != undefined) {
        this._settings!.set(this._typeId, null);
        this._settings!.set(this._typeCaption, null);
      }
    }
  }

}