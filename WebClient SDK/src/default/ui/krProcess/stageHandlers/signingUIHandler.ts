import {
  KrStageTypeUIHandler,
  IKrStageTypeUIHandlerContext,
  StageTypeHandlerDescriptor,
  signingDescriptor
} from 'tessa/workflow/krProcess';
import { plainColumnName } from 'tessa/workflow';
import { IControlViewModel, IBlockViewModel, IFormViewModel } from 'tessa/ui/cards';
import { TabControlViewModel } from 'tessa/ui/cards/controls';
import { CardRow, CardFieldChangedEventArgs } from 'tessa/cards';
import { Visibility } from 'tessa/platform';

export class SigningUIHandler extends KrStageTypeUIHandler {
  private settings: CardRow | null = null;
  private returnIfNotSignedFlagControl?: IControlViewModel;
  private returnAfterSigningFlagControl?: IControlViewModel;
  private NotReturnEdit = plainColumnName('KrSigningStageSettingsVirtual', 'NotReturnEdit');

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [signingDescriptor];
  }

  public initialize(context: IKrStageTypeUIHandlerContext) {
    if (!context.rowModel) {
      return;
    }

    let innerFlagsBlock: IBlockViewModel;
    let tabViewModel: IFormViewModel;
    let flagsTabs: IControlViewModel;
    let flagsBlock: IBlockViewModel;

    if (
      (flagsBlock = context.rowModel.blocks.get('SigningStageFlags')!) &&
      (flagsTabs = flagsBlock.controls.find(x => x.name === 'FlagsTabs')!) &&
      flagsTabs instanceof TabControlViewModel
    ) {
      if (
        (tabViewModel = flagsTabs.tabs.find(x => x.name === 'CommonSettings')!) &&
        (innerFlagsBlock = tabViewModel.blocks.find(x => x.name === 'StageFlags')!)
      ) {
        this.returnIfNotSignedFlagControl = innerFlagsBlock.controls.find(
          x => x.name === 'ReturnIfNotSigned'
        );
      }

      if (
        (tabViewModel = flagsTabs.tabs.find(x => x.name === 'AdditionalSettings')!) &&
        (innerFlagsBlock = tabViewModel.blocks.find(x => x.name === 'StageFlags')!)
      ) {
        this.returnAfterSigningFlagControl = innerFlagsBlock.controls.find(
          x => x.name === 'ReturnIfNotSigned'
        );
      }
    }

    this.settings = context.row;
    this.settings.fieldChanged.add(this.onSettingsFieldChanged);

    this.notReturnEditConfigureFields(this.settings.tryGet(this.NotReturnEdit, false));
  }

  public finalize(_context: IKrStageTypeUIHandlerContext) {
    if (this.settings) {
      this.settings.fieldChanged.remove(this.onSettingsFieldChanged);
      this.settings = null;
    }
  }

  private onSettingsFieldChanged = (e: CardFieldChangedEventArgs) => {
    if (e.fieldName === this.NotReturnEdit) {
      this.notReturnEditConfigureFields(e.fieldValue);
    }
  };

  private notReturnEditConfigureFields(isNotReturnEdit: boolean) {
    if (!this.returnIfNotSignedFlagControl || !this.returnAfterSigningFlagControl) {
      return;
    }

    if (isNotReturnEdit) {
      this.returnIfNotSignedFlagControl.controlVisibility = Visibility.Collapsed;
      this.returnAfterSigningFlagControl.controlVisibility = Visibility.Collapsed;
    } else {
      this.returnIfNotSignedFlagControl.controlVisibility = Visibility.Visible;
      this.returnAfterSigningFlagControl.controlVisibility = Visibility.Visible;
    }
  }
}
