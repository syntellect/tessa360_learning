import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext } from 'tessa/workflow/krProcess';

export class TestStageTypeUIHandler extends KrStageTypeUIHandler {

  public initialize(context: IKrStageTypeUIHandlerContext) {
    console.log(context);
  }

}