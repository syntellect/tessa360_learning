import { KrStageTypeUIHandler, IKrStageTypeUIHandlerContext, StageTypeHandlerDescriptor, typedTaskDescriptor } from 'tessa/workflow/krProcess';
import { plainColumnName } from 'tessa/workflow';
import { ValidationResult, ValidationResultType } from 'tessa/platform/validation';

export class TypedTaskUIHandler extends KrStageTypeUIHandler {

  public descriptors(): StageTypeHandlerDescriptor[] {
    return [typedTaskDescriptor];
  }

  public validate(context: IKrStageTypeUIHandlerContext) {
    if (context.row.tryGet(plainColumnName('KrTypedTaskSettingsVirtual', 'TaskTypeID')) == null) {
      context.validationResult.add(ValidationResult.fromText('$KrStages_TypedTask_TaskType', ValidationResultType.Error));
    }
  }

}