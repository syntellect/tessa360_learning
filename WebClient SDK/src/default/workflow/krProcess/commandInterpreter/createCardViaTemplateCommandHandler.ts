import { ClientCommandHandlerBase } from 'tessa/workflow/krProcess/clientCommandInterpreter';
import { IClientCommandHandlerContext } from 'tessa/workflow/krProcess';
import { getTypedFieldValue } from 'tessa/platform';
import { createFromTemplate } from 'tessa/ui/uiHost';

export class CreateCardViaTemplateCommandHandler extends ClientCommandHandlerBase {
  public async handle(context: IClientCommandHandlerContext) {
    const command = context.command;
    const templateId = getTypedFieldValue(command.parameters['TemplateID']);

    if (templateId) {
      await createFromTemplate({
        templateId,
        templateInfo: {
          ['.NewBilletCard']: command.parameters['NewCard'],
          ['.NewBilletCardSignature']: command.parameters['NewCardSignature']
        }
      });
    }
  }
}
